import React from 'react';
import Layout from '@icedesign/layout';
import styles from './index.module.scss';


export default function WebShell({ children }) {


  return (
    <Layout style={{height:'calc(100% - 0px)',width:'100%'}}>
        {children}
    </Layout>
  );
}

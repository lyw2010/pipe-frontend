import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import {Balloon, Icon, Message, Nav} from '@alifd/next';
import server from '@/server'
import IceImg from '@icedesign/img';
import {headerMenuConfig} from '@/menuConfig';
import styles from './index.module.scss';
import responsive from '@/request'

const {Item} = Nav;


function Header(props) {
    const {location = {}, history} = props;
    const {pathname} = location;
    const {match} = props;
    const {url} = match;

    /**
     * 默认重定向到主页
     */
    const handleClick = (selectedKeys) => {
        if (selectedKeys.key !== '/') {
            Message.success('可以使用 Iceworks 按需添加页面');
            history.push('/');
        }
    };

    const exitClick = () => {
        this.props.history.push('/user/login');
    }

    const user = JSON.parse(sessionStorage.getItem("userInfoVO"));
    // const logo = user.logo;
    const fileUrl = responsive.responsive.url + '/v1/file/download/' + user.logo;
    return (
        <div className={styles.headerContainer}>
            <div className={styles.iceLogo}>
                <img src={require('./images/logo.png')} alt=""/>
            </div>
            <div className={styles.header}>
                {/* 导航条 */}
                <Nav
                    className={styles.headerNavbarMenu}
                    selectedKeys={[pathname]}
                    defaultSelectedKeys={[pathname]}
                    direction="hoz"
                    type="secondary"
                    onClick={handleClick}
                >
                    {headerMenuConfig
                    && headerMenuConfig.length > 0
                    && headerMenuConfig.map((nav, index) => {
                        if (nav.children && nav.children.length > 0) {

                            return (
                                <Item
                                    triggerType="click"
                                    key={index}
                                    title={(
                                        <span>
                                {nav.icon ? (
                                    <Icon size="small" type={nav.icon}/>
                                ) : null}
                                            <span>{nav.name}</span>
                            </span>
                                    )}
                                >
                                    {nav.children.map((item) => {
                                        const linkProps = {};
                                        if (item.external) {
                                            if (item.newWindow) {
                                                linkProps.target = '_blank';
                                            }

                                            linkProps.href = item.path;
                                            return (
                                                <Item key={item.path}>
                                                    <a {...linkProps}>
                                                        <span>{item.name}</span>
                                                    </a>
                                                </Item>
                                            );
                                        }
                                        linkProps.to = item.path;
                                        const has = server.hasAuth(nav);
                                        return has ? (
                                            <Item key={item.path}>
                                                <Link {...linkProps}>
                                                    <span>{item.name}</span>
                                                </Link>
                                            </Item>
                                        ): null;
                                    })}
                                </Item>
                            );
                        }
                        const linkProps = {};
                        if (nav.external) {
                            if (nav.newWindow) {
                                linkProps.target = '_blank';
                            }
                            linkProps.href = nav.path;
                            const has = server.hasAuth(nav);
                            return has ? (
                                <Item key={nav.path}>
                                    <a {...linkProps}>
                            <span>
                                {nav.icon ? (
                                    <Icon size="small" type={nav.icon}/>
                                ) : null}
                                {nav.name}
                            </span>
                                    </a>
                                </Item>
                            ):null;
                        }
                        linkProps.to = nav.path;
                        const has = server.hasAuth(nav);
                        return has? (
                            <Item key={nav.path}
                                style={{background: nav.path.indexOf(url) > -1 ? 'url("'+ require('./images/triangle.png')+'") no-repeat center bottom' : ''}}>
                                <Link {...linkProps}>
                            <span style={{fontSize: nav.path.indexOf(url) > -1 ? '16px' : '12px'}} >
                            {nav.icon ? (
                                <Icon size="small" type={nav.icon}/>
                            ) : null}
                                {nav.name}
                            </span>
                                </Link>
                            </Item>
                        ):null;
                    })}
                </Nav>
                {/* 用户信息 */}
                <div className={styles.userInfo}>
                    {/* <div className={styles.warning}>
                        <img src={require('./images/warning.png')} alt=""/>
                    </div> */}
                    <div className={styles.userMessage}>
                        {
                            user.logo? <img src={fileUrl} alt=""/>:<img src={require('./images/portrait.png')} alt=""/>
                        }
                        <div>
                            <p className={styles.userNameEn}> {user.name} </p>
                            <p className={styles.userNameCn}> {user.rolePermissionVO.roleName} </p>
                        </div>
                    </div>
                    <div className={styles.userExit}>
                        {/* <img src={require('./images/exit.png')} alt=""/>
                        <p> 退出 </p> */}
                        <Link to="/user/login">
                            <img src={require('./images/exit.png')} alt=""/>
                            <p> 退出 </p>
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default withRouter(Header);

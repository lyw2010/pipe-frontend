import React from 'react';
import Layout from '@icedesign/layout';
import cx from 'classnames';
import Logo from '../Logo';

import styles from './index.module.scss';

export default function Footer(props) {
  const { className, style } = props;
  return (
    <Layout.Footer
        className={cx(styles.iceDesignLayoutFooter, className)}
        style={{
            ...style,
            width: '100%',
            height: '40px',
            lineHeight: '40px',
            padding: '0 20px',
            textAlign: 'center',
            fontSize: '12px',
            background: '#f2f2f2',
            position: 'absolute',
            bottom: 0
        }}
    >
        poem © 2020 版权所有
    </Layout.Footer>
  );
}

/**
 * @description: 管理页面入口
 * @author: hj
 * @update: hj(2020-01-14)
 */
import React from 'react';
import {Icon, Nav, Shell, Message} from '@alifd/next';
import {Link} from 'react-router-dom';
import styles from './index.module.scss';
import {headerMenuConfig} from '@/menuConfig';

import {manageMenuConfig} from '@/menuConfig';
import event from '@/request'
import $http from '@/service/Services';
import server from '@/server'

const {SubNav, Item} = Nav;


/**
 * 获取主题列表
 */
const getSubjectList = () => {
    let url = event.event.url + '/v1/subjectConfig/getCurrentMetadataDetail';
    return $http.get(url)
        .then(function (response) {
            const {data} = response;
            localStorage.setItem("subject-status", "false");
            return data;
        })
        .catch(function (error) {
            Message.error(error);
            return [];
        })
};
/**
 * 获取参数据中心center的菜单配置
 */
const getCenterMenu = () => {
    const promise = getSubjectList();
    return promise.then((result) => {
        const centerMenus = [];
        const datas = result.data;
        if (datas) {
            for (let i = 0; i < datas.length; i++) {
                const obj = datas[i];
                centerMenus.push(
                    {
                        name: obj.name,
                        auth: "DATA_CENTER$_SUBJECT",
                        data: [
                            {
                                label: '元数据',
                                icon: 'subPage',
                                auth: "DATA_CENTER$_METADATA",
                                subAsideData: [
                                    {
                                        name: '元数据管理',
                                        path: '/dataCenter/metadata-list?id=' + obj.id,
                                        icon: 'home',
                                        auth: "DATA_CENTER$_METADATA$_METADATA_LIST"
                                    }
                                ]
                            }, {
                                label: '数据源',
                                icon: 'subPage',
                                auth: "DATA_CENTER$_DATASOURCE",
                                subAsideData: [
                                    {
                                        name: '数据源管理',
                                        path: '/dataCenter/dataSource-list?id=' + obj.id,
                                        icon: 'home',
                                        auth: "DATA_CENTER$_DATASOURCE$_LIST"
                                    }
                                ]
                            }, {
                                label: '数据集',
                                icon: 'subPage',
                                subAsideData: [
                                    {
                                        name: '数据表管理',
                                        path: '/dataCenter/metadata-list?id=' + obj.id,
                                        icon: 'home',
                                    }
                                ]
                            }, {
                                label: '任务',
                                icon: 'subPage',
                                subAsideData: [
                                    {
                                        name: '任务管理',
                                        path: '/dataCenter/task-list?id=' + obj.id,
                                        icon: 'home',
                                    }, {
                                        name: '任务调度',
                                        path: '/dataCenter/metadata-list?id=' + obj.id,
                                        icon: 'home',
                                    }, {
                                        name: '调度监控',
                                        path: '/dataCenter/metadata-list?id=' + obj.id,
                                        icon: 'home',
                                    }
                                ]
                            }
                        ]
                    }
                )
            }
        }

        return centerMenus;
    });
};

class ManageLayout extends React.Component {
    constructor(props) {
        super(props);
        // 获取当前导航子菜单
        this.props = props;
        this.location = props.location;
        this.pathname = this.location.pathname;
        this.interval = null;
        this.loading = false;
        this.state = {
            asideData: [],
            localData: [],
            localName: '首页',
            localIndex: -1,
        };
        this.localNavChange = this.localNavChange.bind(this);
    }

    /**
     * 获取菜单
     */
    getMenusList = () => {
        manageMenuConfig.map((item, index) => {
            if (this.pathname.indexOf(item.title) > -1) {
                this.setState(
                    {
                        asideData: item.asideData
                    }
                );
                if ("dataCenter" === item.title && !this.loading) {
                    const promise = getCenterMenu();
                    const _this = this;
                    this.loading = true;
                    promise.then((result) => {
                        const centerMenus = result;
                        const asideData = [];
                        for (let i = 0; i < item.asideData.length; i++) {
                            asideData.push(item.asideData[i]);
                        }
                        for (let i = 0; i < centerMenus.length; i++) {
                            asideData.push(centerMenus[i]);
                        }
                        _this.loading = false;
                        _this.setState(
                            {
                                asideData: asideData
                            }
                        );
                    })
                } else {
                    this.clearListenCenterMenus();
                }
            }
        });
    };

    /**
     * 启动监控
     */
    listenCenterMenus = () => {
        const that = this;
        this.interval = setInterval(
            () => {
                const change = localStorage.getItem("subject-status");
                if ("true" === change) {
                    that.getMenusList();
                }
            }
            , 200)
    };

    /**
     * 清除监听
     */
    clearListenCenterMenus = () => {
        // if (this.interval){
        //     clearInterval(this.interval)
        // }
    };

    /**
     * 路由监控
     */
    componentDidMount() {
        const that = this;
        that.listenCenterMenus();
        this.props.history.listen((listener) => {
            const pathname = listener.pathname;
            if (pathname.indexOf("/dataCenter") > -1) {
                const change = localStorage.getItem("subject-status");
                if ("true" === change) {
                    that.getMenusList();
                }
            } else {
                if (that.clearInterval && typeof that.clearInterval === 'function') {
                    that.clearInterval(this.interval)
                }
            }
        })
    }

    /**
     *
     */
    componentWillMount = () => {
        const auths = server.getUserAuth();
        this.setState({
            auths: auths
        }, () => {
            this.getMenusList();
        });

    };

    /**
     * 侧边栏菜单是否折叠
     */
    btnClick = () => {
        this.setState({
            navcollapse: !this.state.navcollapse
        });
    }
    /**
     * 侧边栏菜单---默认按钮触发的展开收起状态
     */
    onCollapseChange = (visible, e) => {
        this.setState({
            navcollapse: visible
        });
    }

    /**
     * 默认重定向到主页
     */
    handleClick = (selectedKeys) => {
        if (selectedKeys.key !== '/') {
            Message.success('可以使用 Iceworks 按需添加页面');
            history.push('/');
        }
    };

    /**
     * 侧边栏菜单点击切换子菜单
     */
    localNavChange = (item, index) => {
        this.setState({
            localData: []
        })

        setTimeout(() => {
            this.setState({
                localName: item.name,
                localData: item.data,
                localIndex: index
            })
        })
    }


    render() {
        // 获取导航数据
        const {location = {}, history} = this.props;
        const {pathname} = location;
        const {match} = this.props;
        const {url} = match;
        const user = JSON.parse(sessionStorage.getItem("userInfoVO"));

        // 获取当前导航子菜单
        let {asideData} = this.state;
        const iconClass = ['maidianguanli', 'mokuaiguanli', 'maidianshijian', 'banbenguanli', 'tongjiguanli', 'lishitongjiliebiao'];

        // 初始化多级菜单
        let localName = this.state.localName;
        let localData = this.state.localData;
        if (this.state.localIndex != -1) {
            let itemNavData = this.state.asideData[this.state.localIndex];
            localName = itemNavData.name;
            localData = itemNavData.data;
        }
        const fileUrl = event.event.url + '/v1/file/download/' + user.logo;


        return (<div style={{height: '100%'}}>
            <Shell className="pipe-shell" device='desktop' style={{width: '100%', height: '100%'}}>
                <Shell.Branding>
                    <div>
                        <img src={require('./images/logo.png')} alt=""
                             style={{display: 'inline-block', width: '100%', height: '100%'}}/>
                    </div>
                </Shell.Branding>
                <Shell.Navigation direction="hoz">
                    {/* 导航条 */}
                    <Nav
                        className={styles.headerNavbarMenu}
                        selectedKeys={[pathname]}
                        defaultSelectedKeys={[pathname]}
                        direction="hoz"
                        type="secondary"
                        onClick={this.handleClick}
                    >
                        {headerMenuConfig
                        && headerMenuConfig.length > 0
                        && headerMenuConfig.map((nav, index) => {
                            if (nav.children && nav.children.length > 0) {
                                return (
                                    <Item
                                        triggerType="click"
                                        key={index}
                                        title={(
                                            <span>
                                    {nav.icon ? (
                                        <Icon size="small" type={nav.icon}/>
                                    ) : null}
                                                <span>{nav.name}</span>
                                </span>
                                        )}
                                    >
                                        {nav.children.map((item) => {
                                            const linkProps = {};
                                            if (item.external) {
                                                if (item.newWindow) {
                                                    linkProps.target = '_blank';
                                                }

                                                linkProps.href = item.path;
                                                return (
                                                    <Item key={item.path}>
                                                        <a {...linkProps}>
                                                            <span>{item.name}</span>
                                                        </a>
                                                    </Item>
                                                );
                                            }
                                            linkProps.to = item.path;
                                            const has = server.hasAuth(nav);
                                            return has ? (
                                                <Item key={item.path}>
                                                    <Link {...linkProps}>
                                                        <span>{item.name}</span>
                                                    </Link>
                                                </Item>
                                            ) : null;
                                        })}
                                    </Item>
                                );
                            }
                            const linkProps = {};
                            if (nav.external) {
                                if (nav.newWindow) {
                                    linkProps.target = '_blank';
                                }
                                linkProps.href = nav.path;
                                const has = server.hasAuth(nav);
                                return has ? (
                                    <Item key={nav.path}>
                                        <a {...linkProps}>
                                <span>
                                    {nav.icon ? (
                                        <Icon size="small" type={nav.icon}/>
                                    ) : null}
                                    {nav.name}
                                </span>
                                        </a>
                                    </Item>
                                ) : null;
                            }
                            linkProps.to = nav.path;
                            const has = server.hasAuth(nav);
                            return has ? (
                                <Item key={nav.path}
                                      style={{background: nav.path.indexOf(url) > -1 ? 'url("' + require('./images/triangle.png') + '") no-repeat center bottom' : ''}}>
                                    <Link {...linkProps}>
                                <span style={{fontSize: nav.path.indexOf(url) > -1 ? '16px' : '12px'}}>
                                {nav.icon ? (
                                    <Icon size="small" type={nav.icon}/>
                                ) : null}
                                    {nav.name}
                                </span>
                                    </Link>
                                </Item>
                            ) : null;
                        })}
                    </Nav>
                </Shell.Navigation>
                <Shell.Action>
                    {/* 用户信息 */}
                    <div className={styles.userInfo}>
                        {/* <div className={styles.warning}>
                            <img src={require('./images/warning.png')} alt=""/>
                        </div> */}
                        <div className={styles.userMessage}>
                            {
                                user.logo ? <img src={fileUrl} alt=""/> :
                                    <img src={require('./images/portrait.png')} alt=""/>
                            }
                            {/*<img src={require('./images/portrait.png')} alt=""/>*/}
                            <div>
                                <p className={styles.userNameEn}> {user.name} </p>
                                <p className={styles.userNameCn}> {user.rolePermissionVO.roleName} </p>
                            </div>
                        </div>
                        <div className={styles.userExit}>
                            <Link to="/user/login">
                                <img src={require('./images/exit.png')} alt=""/>
                                <p> 退出 </p>
                            </Link>
                        </div>
                    </div>
                </Shell.Action>

                <Shell.Navigation collapse={this.state.navcollapse} onCollapseChange={this.onCollapseChange}>
                    <Nav className={styles.iceNav} selectedKeys={[this.pathname]} replace="true" embeddable="true"
                         mode="inline" openMode="single" triggerType="click">
                        {Array.isArray(asideData)
                        && asideData.length > 0
                        && asideData.map((nav, index) => {
                            if (nav.data && nav.data.length > 0) {
                                if (this.state.localIndex == -1) {
                                    this.setState({
                                        localIndex: index
                                    })
                                }
                                const has = server.hasAuth(nav);
                                return has ? (
                                    <Item key={nav.name} onClick={(e) => this.localNavChange(nav, index)}
                                          className={localName == nav.name ? 'activeNav' : ''}>
                                        <span
                                            className={'iconfont icon-' + (iconClass[index] ? iconClass[index] : iconClass[0])}
                                            style={{fontSize: '14px'}}></span>
                                        <span className={styles.iceMenuLinkText}>
                                            {nav.name}
                                        </span>
                                        <Icon type="arrow-right" size="xs"
                                              style={{float: 'right', width: '24px', height: '100%'}}/>
                                    </Item>
                                ) : null;
                            } else {
                                const has = server.hasAuth(nav);
                                let locationUrl = this.props.location;
                                let oldPathName = locationUrl.pathname;
                                let pathFlag = locationUrl.search.indexOf('?') > -1 ? oldPathName.indexOf(nav.path) > -1 : oldPathName == nav.path;
                                return has ? (
                                    [0].map(ooooo => {
                                        if (nav.children && nav.children.length > 0) {
                                            return (
                                                <Nav.SubNav label={nav.name}  icon={nav.icon}>
                                                    {
                                                        Array.isArray(nav.children)
                                                        && nav.children.length > 0
                                                        && nav.children.map(nn =>{
                                                            return (
                                                                <Item key={nn.path} className={pathFlag ? 'activeNav' : ''}>
                                                                    <span
                                                                        className={'iconfont icon-' + (iconClass[index] ? iconClass[index] : iconClass[0])}
                                                                        style={{fontSize: '14px'}}></span>
                                                                        {/* <img src={require('./images/navIcon.png')} alt=""/> */}
                                                                        <Link to={nn.path}>
                                                                            {nn.icon ? <Icon size="small" type={nn.icon}/> : null}
                                                                            <span className={styles.iceMenuLinkText}> {nn.name} </span>
                                                                        </Link>
                                                                </Item>
                                                            )
                                                        })
                                                    }

                                                </Nav.SubNav>
                                            )
                                        } else {
                                            return (
                                                <Item key={nav.path} className={pathFlag ? 'activeNav' : ''}>
                                                    <span
                                                        className={'iconfont icon-' + (iconClass[index] ? iconClass[index] : iconClass[0])}
                                                        style={{fontSize: '14px'}}></span>
                                                    {/* <img src={require('./images/navIcon.png')} alt=""/> */}
                                                    <Link to={nav.path}>
                                                        {nav.icon ? <Icon size="small" type={nav.icon}/> : null}
                                                        <span className={styles.iceMenuLinkText}>
                                                        {nav.name}
                                                        </span>
                                                    </Link>
                                                </Item>
                                            )
                                        }
                                    })

                                ) : null;
                            }
                        })}
                    </Nav>
                    {/* <div style={{textAlign: 'center', background: '#eee'}} onClick={this.btnClick}>
                        { this.state.navcollapse ? <Icon type="arrow-right" size="xs" /> : <Icon type="arrow-left" size="xs" /> }
                    </div> */}
                </Shell.Navigation>

                {localData.length > 0 &&
                <Shell.LocalNavigation>
                    <Nav embeddable aria-label="local navigation">
                        {localData.map((item) => {
                            const has = server.hasAuth(item);
                            if (has) {
                                return (
                                    <SubNav label={item.label} key={item.label} style={{fontWeight: 'blod'}}>
                                        {Array.isArray(item.subAsideData)
                                        && item.subAsideData.length > 0
                                        && item.subAsideData.map((v, i) => {
                                            return (
                                                <Item key={v.name}
                                                      className={this.props.location.pathname.indexOf(v.path) > -1 ? 'activeNav' : ''}>
                                                    <Link to={v.path}>
                                                        {v.icon ? <Icon size="small" type={v.icon}/> : null}
                                                        <span>
                                                        {v.name}
                                                    </span>
                                                    </Link>
                                                </Item>
                                            )
                                        })}
                                    </SubNav>
                                )
                            } else {
                                return null
                            }

                        })}
                    </Nav>
                </Shell.LocalNavigation>}

                <Shell.Content>
                    <div style={{height: '100%', background: '#fff'}}>{this.props.children}</div>
                </Shell.Content>

                <Shell.Footer style={{
                    width: localData.length > 0 ? 'calc(100% - 480px)' : 'calc(100% - 240px)',
                    height: '50px',
                    lineHeight: '50px',
                    padding: '0 20px',
                    textAlign: 'center',
                    fontSize: '12px',
                    color: '#333',
                    background: '#f2f2f2',
                    position: 'absolute',
                    bottom: 0
                }}>
                    poem © 2020 版权所有
                </Shell.Footer>

            </Shell>
        </div>);
    }
}

export default ManageLayout;


import React from 'react';
import Layout from '@icedesign/layout';
import Footer from './Footer';
import styles from './index.module.scss';



export default function UserLayout({ children }) {
    let activeFlag = false;
    setInterval(() => {
        if (activeFlag) {
            $('.'+styles.img).addClass(styles.imgActive);
        } else {
            $('.'+styles.img).removeClass(styles.imgActive);
        }
        activeFlag = !activeFlag;
    },3000);


  return (
    <Layout className={styles.userLayout}>
        {/*<img className={styles.img} src={require('./images/userBg2.png')} alt="" />*/}
        <div className={styles.layerMask} />
        <div className={styles.userContent}>
            {children}
        </div>

        <Footer />
    </Layout>
  );
}

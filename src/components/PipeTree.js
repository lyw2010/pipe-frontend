/**
  * @description: 权限列表处理
  * @author: 漆红
  */
import React, {Component} from 'react'
import {Layout, Tree} from 'antd';
import {recursionChildren} from "../common";

const { Content } = Layout;

class PipeTree extends Component {
    constructor() {
        super()
        this.state = {
            authTree: [],
            checkedKeys: [],
            current: '',
            authTreeChild: [],
            title: '',
            initFinsh:false
        }
    }

    spliceParentNode = (authTreeChildList, halfCheckedKeys) => {
        authTreeChildList.forEach(item => {
            if (halfCheckedKeys.includes(item.id)) {
                if (this.state.checkedKeys.indexOf(item.id) !== -1) {
                    this.state.checkedKeys.splice(this.state.checkedKeys.indexOf(item.id), 1)
                }
            }
            
            if (item.children && item.children.length > 0) {
                this.spliceParentNode(item.children, halfCheckedKeys)
            }
        })
    };

    /**
     * 检查
     * @param checkedKeys
     * @param info
     */
    onCheck = (checkedKeys, info) => {
        const { authTree } = this.state
        // const parentsKeys =this.setParentId(checkedKeys)
        // console.log("parentsKeys",parentsKeys)
        console.log("info--------",info)
        if (info.checked) {
            this.setState({
                checkedKeys: Array.from(new Set(checkedKeys.concat(this.state.checkedKeys)))
            }, () => {
                // recursionReset(authTree, this.state.checkedKeys)
                // console.log(this.state.checkedKeys)
                // const allkeys=[...this.state.checkedKeys,...parentsKeys]
                // this.props.callback(allkeys, authTree)
                this.props.callback(this.state.checkedKeys, authTree)
            })
        } else {
            const { authTreeChild } = this.state

            // if (this.state.checkedKeys.indexOf(info.node.id) !== -1 && info.halfCheckedKeys && this.state.checkedKeys.indexOf(info.halfCheckedKeys[0]) !== -1) {
            //     // 父元素全部选中得单个check
            //     this.state.checkedKeys.splice(this.state.checkedKeys.indexOf(info.halfCheckedKeys[0]), 1)
            // }

            if (this.state.checkedKeys.indexOf(info.node.id) !== -1) {
                // 点击单个选中得check 使其pop
                this.state.checkedKeys.splice(this.state.checkedKeys.indexOf(info.node.id), 1)

                if (info.halfCheckedKeys) {
                    this.spliceParentNode(authTreeChild, info.halfCheckedKeys)
                    // authTreeChild.forEach(item => {
                    //     if (info.halfCheckedKeys.includes(item.id)) {
                    //         this.state.checkedKeys.splice(this.state.checkedKeys.indexOf(item.id), 1)
                    //     }
                    // })
                }
            }

            const spliceArr = []
            if (authTreeChild.some(item => item.id === info.node.id)) {
                // 点击某个父元素check 

                authTreeChild.forEach(item => {
                    if (item.id === info.node.id) {
                        item.children.forEach(obj => {
                            spliceArr.push(obj.id)

                            if (obj.children && obj.children.length > 0) {
                                obj.children.forEach(ele => {
                                    spliceArr.push(ele.id)
                                })
                            }
                        })
                    }
                })
            }

            authTreeChild.forEach(item => {
                if (item.children.some(obj => obj.id === info.node.id)) {
                    item.children.forEach(ele => {
                        if (ele.id === info.node.id) {
                            if (ele.children && ele.children.length > 0) {
                                ele.children.forEach(element => {
                                    spliceArr.push(element.id)
                                })
                            }
                        }
                    })
                }
            })
            this.state.checkedKeys = this.state.checkedKeys.filter(ele => !spliceArr.includes(ele))

            this.setState({
                checkedKeys: Array.from(new Set(checkedKeys.concat(this.state.checkedKeys)))
            }, () => {
                console.log(this.state.checkedKeys)
                // recursionReset(authTree, this.state.checkedKeys)
                // const allkeys=[...this.state.checkedKeys,...parentsKeys]
                // this.props.callback(allkeys, authTree)
                this.props.callback(this.state.checkedKeys, authTree)
            })
        }
    }

    onCheckTree=(checkedKeys, info)=>{
        console.log("checkedKeys-----",checkedKeys)
        this.setState({
            checkedKeys:checkedKeys
        })
        this.props.callback(checkedKeys, this.state.authTree)

    }

    setParentId = (checkedKeys) => {
        const { authTree } = this.state
        let parentsKey = []
        authTree.map(parent => {
            console.log("parent", parent)
            const list = this.expandTreeKey(parent.children)
            console.log("parent list", list)
            let intersection = list.filter(v => checkedKeys.includes(v)) // [2]
            if (intersection.length) {
                parentsKey.push(parent.id)
            }

        })
        return parentsKey
    }


    //展开父节点中包含的字节的id
    expandTreeKey(data) {
        let keys = []
        data.map(item => {
            if (item.children) {
                const children = this.expandTreeKey(item.children)
                keys = [...keys, ...children]
            }
            keys.push(item.id)

        })
        return keys
    }



    serialization = list => {
        list = list.map(item => {
            item.title = item.name
            item.key = item.id
            if (item.children) {
                this.serialization(item.children)
            }
            return item
        })
        return list
    }

    /**
     * 初始化树
     * @param resp
     */
    initTree = resp => {
        console.log("init---",resp.selectedAuthList)
        const authTree = this.serialization(resp.allAuthList);
        recursionChildren(authTree, resp.selectedAuthList)
        console.log("recursionChildren",resp.selectedAuthList)
        // recursionChildren(authTree, resp.selectedAuthList)
        console.log(resp)
        this.setState({
            authTree,
            checkedKeys: resp.selectedAuthList,
            current: authTree[0].id,
            authTreeChild: authTree[0].children,
            initFinsh:true
        }, () => {
            // recursionReset(authTree, this.state.checkedKeys)
            this.props.callback(this.state.checkedKeys, authTree)
            console.log("initTree---------",this.state.checkedKeys, this.state.authTree)
            console.log(this.state.authTreeChild)
        });
    }

    componentDidMount() {
        console.log(this.props)
        const {
            id = '',
            functions,
            title
        } = this.props

        this.setState({
            title
        })

        var vo = {}
        title === 'roleAdd' ? vo = {
            id
        } : vo = {
            roleId: id
        };

        title ? functions(vo).then(resp => {
            this.initTree(resp.data.data)
        }) : functions(id).then(resp => {
            this.initTree(resp.data.data)
        })
    }


    render() {
        const { authTree, checkedKeys, current, authTreeChild, title, initFinsh } = this.state
        return (
            <Layout
                style={{
                    width: '71%',
                    marginLeft: '13%',
                    backgroundColor: '#fff',
                    marginBottom: 40
                }}
            >
                <Tree
                    showLine
                    multiple
                    checkable
                    checkStrictly={false}
                    onCheck={this.onCheckTree}
                    treeData={authTree}
                    // defaultCheckedKeys={checkedKeys}
                    checkedKeys={initFinsh?checkedKeys:[]}
                    disabled={title === 'roleLook' ? true : false}
                />
            </Layout>
        )
    }
}
export default PipeTree
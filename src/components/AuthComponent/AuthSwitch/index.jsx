/**
  * @description: switch（权限判断）组件
  * @author: hj
  * @update: hj(2020-01-14)
  */
 import React from 'react';
import styles from "../../../pages/CustomLayout/MyDashboardList/index.module.scss";
import {Button, Switch} from "@alifd/next";
import {Link} from "react-router-dom";
import server from '@/server'


class AuthSwitch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: this.props.checked
        }
    }

    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {

    };


    /**
     * 调用父级的权限操作
     */
    authOnChange = () => {
        if (this.props.onChange && typeof this.props.onChange == "function") {
            this.props.onChange();
        }
    };

    render() {
        const has = server.hasAuth({auth:this.state.auth});
        return has ? (
            <Switch checked={this.props.checked} size="small" className="table-btn-switch"
                    onChange={this.authOnChange}/>
        ) : null
    }
}

export default AuthSwitch;
/**
  * @description: 星空动画背景
  * @author: hj
  * @update: hj(2020-01-14)
  */

import React, {useState} from 'react';
import styles from './index.module.scss';
import { Collapse, Message } from '@alifd/next';




class StarHighlight extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            width: this.props.width ? this.props.width : 1920,
            height: this.props.height ? this.props.height : 1080,
        };
    }


    // 组件挂载之前
    componentWillMount = () => {
    }
    // 组件挂载完成
    componentDidMount = () => {
        console.log("this.props.width && this.props.height",this.props.width ,this.props.height)
        if (this.props.width && this.props.height) {
            this.starHighlight();
        }
    }
    // 接收父组件的数据改变
    componentWillReceiveProps = (nextProps) => {
        const _this = this;

        if (nextProps.width && nextProps.height) {
            _this.setState({
                width: nextProps.width,
                height: nextProps.height,
            }, function () {
                _this.starHighlight();
            })
        }
    }

    // 闪点星空
    starHighlight = () => {
        let canvas = document.getElementById('starCanvas'),
            ctx = canvas.getContext('2d'),
            w = canvas.width,
            h = canvas.height,
            hue = 217,
            stars = [],
            count = 0,
            maxStars = 1200;//星星数量
            let canvas2 = document.createElement('canvas'), ctx2 = canvas2.getContext('2d');
            canvas2.width = 100;
            canvas2.height = 100;
            let half = canvas2.width / 2, gradient2 = ctx2.createRadialGradient(half, half, 0, half, half, half);
            gradient2.addColorStop(0.025, '#808080');//#CCC
            // gradient2.addColorStop(0.025, 'hsla(' + hue + ', 1%, 1%, 2)');

            gradient2.addColorStop(0.1, 'hsl(' + hue + ', 61%, 33%)');
            gradient2.addColorStop(0.25, 'hsl(' + hue + ', 64%, 6%)');
            // gradient2.addColorStop(0.1, 'hsl(' + hue + ' , 1%, 1%)');
            // gradient2.addColorStop(0.25, 'hsl(' + hue + ', 1%, 1%)');
            gradient2.addColorStop(1, 'transparent');
            ctx2.fillStyle = gradient2;
            ctx2.beginPath();
            ctx2.arc(half, half, half, 0, Math.PI * 2);
            ctx2.fill();
        // End cache
        function random(min, max) {
            if (arguments.length < 2) {
                max = min;
                min = 0;
            }
            if (min > max) {
                let hold = max;
                max = min;
                min = hold;
            }
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
        function maxOrbit(x, y) {
            let max = Math.max(x, y),
                diameter = Math.round(Math.sqrt(max * max + max * max));
            return diameter / 2;
            //星星移动范围，值越大范围越小，
        }
        let Star = function() {
            this.orbitRadius = random(maxOrbit(w, h));
            this.radius = random(200, this.orbitRadius) / 20;//8
            //星星大小
            this.orbitX = w / 2;
            this.orbitY = h / 2;
            this.timePassed = random(0, maxStars);
            // this.speed = random(this.orbitRadius) / 50000;
            this.speed = random(this.orbitRadius) / 400000;
            //星星移动速度
            // this.alpha = random(2, 10) / 10;
            count++;
            stars[count] = this;
        }
        Star.prototype.draw = function() {
            let x = Math.sin(this.timePassed) * this.orbitRadius + this.orbitX,
                y = Math.cos(this.timePassed) * this.orbitRadius + this.orbitY,
                twinkle = random(10);
            if (twinkle === 1 && this.alpha > 0) {
                this.alpha -= 0.05;
            } else if (twinkle === 2 && this.alpha < 1) {
                this.alpha += 0.05;
            }
            ctx.globalAlpha = this.alpha;
            ctx.drawImage(canvas2, x - this.radius / 2, y - this.radius / 2, this.radius, this.radius);
            this.timePassed += this.speed;
        }
        for (let i = 0; i < maxStars; i++) {
            new Star();
        }
        let img = new Image();
        img.src = require("./images/bg.png");
        // img.src = '';
        function animation() {
            ctx.drawImage(img , 0 , 0 , w , h);
            ctx.globalCompositeOperation = 'source-over';
            ctx.globalAlpha = 0.5; //尾巴
            ctx.fillStyle = 'hsla(' + hue + ', 64%, 6%, 2)';
            ctx.fillRect(0, 0, w, h)
            ctx.globalCompositeOperation = 'lighter';
            for (let i = 1, l = stars.length; i < l; i++) {
                stars[i].draw();
            };
            window.requestAnimationFrame(animation);
        }
        animation();
    }




    render() {

        return (
            <div className={styles.dynamicBg + 'starCanvas'}
                style={{
                    width: this.state.width-4+'px',
                    height: this.state.height-4+'px',
                    position: 'absolute',left: 0,top: 0,zIndex: this.props.pageFlag == 'preview' ? 0 : -1}}>
                <canvas id="starCanvas" width={this.state.width} height={this.state.height} ></canvas>
            </div>
        )
    }

}

export default StarHighlight;

/**
  * @description: 5120静态背景
  * @author: hj
  * @update: hj(2020-01-14)
  */

import React, {useState} from 'react';
import styles from './index.module.scss';


class StaticBg5120 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            date: '',
            week: ''
        };
    }


    // 组件挂载之前
    componentWillMount = () => {
        const _this = this;
        this.getSystemTime();
    }
    // 组件挂载完成
    componentDidMount = () => {
        const _this = this;
    }
    // 接收父组件的数据改变
    componentWillReceiveProps = (nextProps) => {
        const _this = this;
    }

    // 获取当前系统时间
    getSystemTime = () => {
        let vWeek,vWeek_s,vDay;
            vWeek = ["星期日","星期一","星期二","星期三","星期四","星期五","星期六"];
        let date =  new Date(),
            year = date.getFullYear(),
            month = (date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1,
            day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate(),
            hours = date.getHours(),
            minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes(),
            seconds = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
            vWeek_s = date.getDay();
            
        let dateTime = year + "年" + month + "月" + day + "日" + "\xa0\xa0" + hours + ":" + minutes +":" + seconds;
        let weekTime = vWeek[vWeek_s];
        
        this.setState({
            date: dateTime,
            week: weekTime
        })
        
    };




    render() {
        
        // 实时显示系统时间
        setTimeout(() => {
            setInterval(this.getSystemTime(),1000)
        },1000);

        return (
            <div className={styles.staticBg} style={{width: this.props.width+'px',height: this.props.height+'px'}}>
                <div className={styles.staticTitle}>
                    <p style={{lineHeight: Math.floor(this.props.height * 0.11 * 0.68) + 'px'}}>{ this.props.title }</p>
                    <span className={styles.dateIcon}></span>
                    <span className={styles.date}>{ this.state.date }</span>
                    <span className={styles.week}>{ this.state.week }</span>
                </div>
            </div>
        )
    }

}

export default StaticBg5120;

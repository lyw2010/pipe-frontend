/**
  * @description: 空白组件
  * @author: hj
  * @update: hj(2020-01-14)
  */

import React, { Component } from 'react';
 

class NotFound extends Component {
    constructor(props) {
        super(props);
        this.state = {
            params: [],
            data: [],
            forceFit: true, //forceFit自动试用宽度，默认false  
            width: this.props.data && this.props.data.width ? this.props.data.width : '100%',   // 固定宽度    当自适应宽度开启，此功能失效
            height: this.props.data && this.props.data.height ? this.props.data.height : '100%',
            color: this.props.data && this.props.data.color ? this.props.data.color : 'transparent',
            title: '',
            chartId: this.props.data && this.props.data.chartId ? this.props.data.chartId : '',
            id: this.props.data && this.props.data.componentId ? this.props.data.componentId : '',
        };
    }

   

    componentWillMount() { //初始化
        
    }


    render() {
        const _this = this;
        

        return (
            
            <div style={{zIndex: 9999}} id={this.state.id+'_'+this.state.chartId} style={{width: this.state.width,height: this.state.height}}></div>
            
        )
    }


}
export default NotFound;


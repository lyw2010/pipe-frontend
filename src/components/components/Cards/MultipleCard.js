/**
  * @description: 多行多卡片
  * @author: hj
  * @update: hj(2020-04-22)
  */

import React, { Component } from 'react';
import styles from './index.module.scss';
import { getMediaById } from '@/service/Api/Chart';
import { observer,inject,Provider } from 'mobx-react';

 
@inject('chartStore') 
@observer


class MultipleCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            params: [],
            data: [],
            forceFit: true, //forceFit自动试用宽度，默认false
            width: this.props.data.width ? this.props.data.width : '100%',   // 固定宽度    当自适应宽度开启，此功能失效
            height: this.props.data.height ? this.props.data.height : '100%',
            condition: this.props.data.condition ? this.props.data.condition : '',
            pageFlag: this.props.data.pageFlag ? this.props.data.pageFlag : '',
            nofirst: this.props.data.nofirst ? this.props.data.nofirst : '',
            color: this.props.data.color,
            title: '',
            chartId: this.props.data.chartId,
            id: this.props.data.componentId,
            chartData: [],
            axisArr: {},
            haveData: false,
            colorColumns: [], // 特殊字段

            dataNull: false, // 暂无数据

            // 全局变量
            mainData: [], // 操作组件可操作数据
            mainName: '', // 操作组件可操作当前维度
            mainFlag: false, // 是否是操作组件

            pageInitFlag: true,
            affectName: '',
            requireCondition: '',
        };
    };


    componentWillMount() { //初始化
        const _this = this;
        
        // 预览界面
        if (_this.state.pageFlag == 'preview') {
            // 判断当前组件是否是可操作组件
            const { globalData } = _this.props.chartStore;
            if (globalData.slice().length > 0) {
                globalData.slice().map((item,index) => {
                    if (item.affect == _this.state.chartId) { // 可操作组件
                        let affectName = item.affectDimension;
                        _this.setState({
                            mainFlag: true,
                            mainName: affectName
                        })
                    }
                })
            }

            _this.interfaceRequest(_this.state.chartId,_this.state.condition);
        } else { // 配置面板
            if (_this.state.nofirst == 'nofirst') {
                _this.saveHandleData();
            } else {
                _this.interfaceRequest(_this.state.chartId,_this.state.condition);
            }
        }
    }

    // 钩子函数--组件挂载完成后
    componentDidMount() { //初始化
        this.setState({
            haveData: true
        })
    }
    // 接收父组件的数据改变
    componentWillReceiveProps = (nextProps) => {
        const _this = this;
        const { globalData, affectId } = _this.props.chartStore;

        // 预览界面---判断是否是变化组件
        if (nextProps.data.pageFlag == 'preview') {
            if (nextProps.data.condition != _this.state.requireCondition && globalData.slice().length > 0) {
                globalData.slice().map((item,index) => {
                    if (item.affect == affectId && item.require == nextProps.data.chartId) { // 联动组件
                        _this.setState({
                            pageInitFlag: false
                        })
                        let condition = {};
                        Object.keys(_this.state.requireCondition).forEach(function(keys){
                            condition[keys] = keys == item.requireDimension ? nextProps.data.condition[keys] : _this.state.requireCondition[keys];
                        })
                        _this.interfaceRequest(nextProps.data.chartId,condition);
                    }
                })
            }
        }
    }

    // 数据接口请求
    interfaceRequest = (chartId,condition) => {
        this.setState({
            requireCondition: condition,
        });

        let data = {
            charId: chartId,
            condition: condition
        };
        this.getMediaById(data);
    }

    // 数据存储刷新
    saveHandleData = () => {
        const { chartArrData } = this.props.chartStore;
        if (chartArrData.slice().length > 0) {
            chartArrData.slice().map((item) => {
                if (item.id == this.state.chartId) {
                    this.setState({
                        haveData: false
                    },function() {
                        // 数据处理
                        this.handleData(item.data);
                    })
                }
            })
        }
    }


    async getMediaById(data) {
        const res = await getMediaById(data);
        if (res.data && res.data.code === 0) {
            let str = res.data;
            if (str.data && str.data.data && str.data.data.length > 0) {
                // 存储图例原始数据
                const { saveChartData } = this.props.chartStore;
                let oldData = {
                    id: this.state.chartId,
                    data: str.data.data
                }
                saveChartData(oldData);

                // 数据处理
                this.handleData(str.data.data);
            } else {
                this.setState({
                    dataNull: true
                })
            }
        } else {
            this.setState({
                dataNull: true
            })
        }
    }
    // 数据处理
    handleData = (storeAllData) => {
        let storeData = storeAllData[0];
        this.setState({
            dataNull: true
        })

        // 非预览界面
        if (this.state.pageFlag != 'preview') {
            // 存储可操作组件数据
            if (storeData.affect && storeData.affect.length > 0) {
                let obj = {
                    id: this.state.chartId,
                    label: storeData.title,
                    dimensionData: []
                }
                const { saveAffectData } = this.props.chartStore;

                let dimensionData = [];
                storeData.affect.slice().map((item,index) => {
                    dimensionData.push(item);
                })
                obj.dimensionData = dimensionData;
                saveAffectData(obj);
            }
            // 存储联动组件数据
            if (storeData.requires && storeData.requires.length > 0) {
                let obj = {
                    id: this.state.chartId,
                    label: storeData.title,
                    dimensionData: []
                }
                const { saveRequireData } = this.props.chartStore;

                let dimensionData = [];
                storeData.requires.slice().map((item,index) => {
                    dimensionData.push(item);
                })
                obj.dimensionData = dimensionData;
                saveRequireData(obj);
            }
        }

        // 页面初始化
        if (this.state.pageInitFlag) {
            if (storeData.affect &&  storeData.affect.length > 0) {
                this.setState({
                    affectName: storeData.affect[0]
                });
            }
            // 获取选择项
            if (storeData.conditionValues) {
                let selectData = storeData.conditionValues[storeData.affect[0]] ? storeData.conditionValues[storeData.affect[0]] : [];
                this.setState({
                    mainData: selectData
                });
            }
        }


        // 组装当前图例的维度和度量
        let xyArr = [];
        let colorColumns = [];
        Object.keys(storeData.columnMaps).forEach(function (key) {
            if (storeData.colorColumns && storeData.colorColumns.length > 0 && storeData.colorColumns.indexOf(key) > -1) {
                colorColumns.push(storeData.columnMaps[key]);
            }
            xyArr.push(key);
        });

        let icons = storeData.icons;
        let {data} = storeData;
        for(let i =0 ;i < data.length; i++){
            //not exist , give default icon
            data[i]["icon"] = icons && icons.length > i && icons[i] ? icons[i] : "total" ;
        }
        this.setState({
            title: storeData.title,
            chartData: data,
            haveData: true,
            dataNull: data.length  > 0 ? false : true,
            colorColumns: colorColumns
        })
    }

    // 改变全局变量
    onGlobalVariable = (val) => {
        const { saveAffectId } = this.props.chartStore;
        saveAffectId(this.state.chartId);
        this.props.onGlobalVariable(val);
    }
    



    render() {
        const _this = this;
        let Layer = this.props.layer;
        let cardData = _this.state.chartData;
        const {colorColumns} = this.state;
        
        let heightNum = cardData.length > 9 ? '25%' : cardData.length > 6 ? '33.3%' : cardData.length > 3 ? '50%' : '100%';

        return (
            <div id={this.state.id+'_'+this.state.chartId} style={{zIndex: 99,width: this.state.width,height: this.state.height,padding: '40px 2% 10px 2%'}}>
                <Layer width={this.state.width} title={this.state.title} chartId={this.state.chartId} choiceData={this.state.mainData} onGlobalVariable={this.onGlobalVariable}></Layer>
                <div className={styles.cardBox}  id={'chart_'+this.state.id+'_'+this.state.chartId} style={{height: this.state.height - 50,display: 'flex',flexWrap: 'wrap'}}>
                    {cardData.length > 0 ? cardData.map((item, index) => {
                        let color = '#97FAFF';
                        if (colorColumns && colorColumns.length > 0 && colorColumns.indexOf(item.name) > -1) {
                            color = parseInt(item.tier) == 1 ? '#c00000' : parseInt(item.tier) == 2 ? '#ff0000' : parseInt(item.tier) == 3 ? '#ffa500' : parseInt(item.tier) == 4 ? '#ffff00' : parseInt(item.tier) == 5 ? '#52C72E' : parseInt(item.tier) == 6 ? '#0FA33D' : '#97FAFF';
                        }
                        return (
                            <div className={styles.card} key={index} style={{width: cardData.length < 3 && '50%',minWidth: '33.3%',maxWidth: '50%',height: heightNum,display: 'flex',alignItems: 'center'}}>
                                <img src={require('./images/'+item.icon+'.png')} className={styles.icon} alt="" style={{position: 'relative',width: '40px',height: '40px',top: 0}} />
                                <div className={styles.textBox} style={{position: 'relative',left: 0,marginLeft: '8px'}}>
                                    <p className={styles.number} title={item.value} style={{color: color,height: '28px',lineHeight: '28px'}}> {item.value} </p>
                                    <p className={styles.text} title={item.name}> {item.name} </p>
                                </div>
                            </div>
                        )
                    }) : ''}
                    { this.state.dataNull && <p style={{width: '100%', height: this.state.height - 50,lineHeight: this.state.height - 50 + 'px',color: '#fff',textAlign: 'center'}}> 暂无数据 </p>}
                </div>
            </div>

        )
    }


}
export default MultipleCard;


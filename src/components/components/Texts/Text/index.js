/**
  * @description: 纯文本组件
  * @author: hj
  * @update: hj(2020-01-14)
  */

import React, { Component } from 'react';
import styles from './index.module.scss';
import { getMediaById } from '@/service/Api/Chart';
import { observer,inject,Provider } from 'mobx-react';

 
@inject('chartStore') 
@observer
class ComponentTitle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            params: [],
            data: [],
            forceFit: true, //forceFit自动试用宽度，默认false
            width: this.props.data.width ? this.props.data.width : '100%',   // 固定宽度    当自适应宽度开启，此功能失效
            height: this.props.data.height ? this.props.data.height : '100%',
            condition: this.props.data.condition ? this.props.data.condition : '',
            pageFlag: this.props.data.pageFlag ? this.props.data.pageFlag : '',
            nofirst: this.props.data.nofirst ? this.props.data.nofirst : '',
            color: [],
            title: '',
            chartId: this.props.data.chartId,
            id: this.props.data.componentId,
            chartData: [],
            axisArr: {},
            haveData: false,
        };
    }


    componentWillMount() { //初始化
        let data = {
            charId: this.state.chartId,
            condition: this.state.condition
        };
        this.getMediaById(data);
    }


    // 数据接口请求
    async getMediaById(data) {
        const res = await getMediaById(data);
        if (res.data && res.data.code === 0) {
            let str = res.data;
            if (str.data && str.data.data && str.data.data.length > 0) {
                // 存储图例原始数据
                this.setState({
                    title: str.data.data[0].title
                })
            }
        }
    }


    render() {
        const _this = this;
        let Layer = this.props.layer;
        

        return (
            <div className={styles.textBox} style={{zIndex: 9999,width: this.state.width,height: this.state.height}}>
                <Layer width={this.state.width} title={this.state.title}></Layer> 
                <div className={styles.titleName} style={{height: this.state.height,lineHeight: this.state.height+'px'}}> {this.state.title} </div>
                {/* <span className={styles.titleIcon}>>>></span>
                <span className={styles.titleName} > {this.state.title} </span> */}
            </div>
        )
    }


}
export default ComponentTitle;


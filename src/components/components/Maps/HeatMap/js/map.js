let heatMap, PixelTimer, markerArr = [], iconObj, iconUrl = '', heatMapSecond, currentCenter = [104.072604,30.661966], markerPosition = [104.072604,30.661966];
// (function () {30.577127,104.071220
//     createHeatMap();
// })();
let mapId;
let PixelData = [
    {
        name: '王先生',
        info: '希望青羊区可以多增加一些室内游泳馆。',
        position: {
            P: 30.643513,
            Q: 104.05286,
            lat: 30.643513,
            lng: 104.05286,
        }
    }, {
        name: '李先生',
        info: '有没有喜欢打网球的可以约一波？',
        position: {
            P: 30.664335,
            Q: 104.062817,
            lat: 30.664335,
            lng: 104.062817,
        }
    }, {
        name: '张女士',
        info: '每时运动馆环境真的很不错，赞一个！',
        position: {
            P: 30.644104,
            Q: 104.083588,
            lat: 30.644104,
            lng: 104.083588,
        }
    }, {
        name: '黄女士',
        info: '锦江区羽毛球馆多开几个呗。',
        position: {
            P: 30.663597,
            Q: 104.076378,
            lat: 30.663597,
            lng: 104.076378,
        }
    }, {
        name: '何女士',
        info: '金牛区一品天下附近哪家游泳馆最好啊，求推荐。',
        position: {
            P: 30.659167,
            Q: 104.056637,
            lat: 30.659167,
            lng: 104.056637,
        }
    }, {
        name: '陆先生',
        info: '室外篮球馆夏天晚上能不能延长到十点关门呢？',
        position: {
            P: 30.628595,
            Q: 104.111397,
            lat: 30.628595,
            lng: 104.111397,
        }
    }, {
        name: '汪先生',
        info: '强烈建议增加室外足球场。',
        position: {
            P: 30.665811,
            Q: 104.025051,
            lat: 30.665811,
            lng: 104.025051,
        }
    }, {
        name: '赵女士',
        info: '推荐大家去春熙路职工之家，场地很赞哦。',
        position: {
            P: 30.661966,
            Q: 104.07260400000001,
            lat: 30.661966,
            lng: 104.072604,
        }
    }
];
let PixelIndex = 0;
window.createHeatMap = function(mapId) {
    console.log('mapId----',mapId);
    mapId = mapId;
    if (PixelTimer) window.clearInterval(PixelTimer);
    document.getElementById(mapId).innerHTML = '';
    createHeatMap(mapId);
} 

function createHeatMap(mapId) {
    // heatMap = new AMap.Map(mapId, {
    //     resizeEnable: true,
    //     zoomEnable: false,
    //     zoom: 15,
    //     center: currentCenter
    // });
    
    heatMap = new AMap.Map(mapId, {
        //features: ['bg', 'road'],
         //mapStyle: 'amap://styles/1de318cbb8d12c02303a22c550b9ccc9',
        //  center: [104.072604,30.661966],
         center: currentCenter,
         pitch: 0,
         zoom: 13,
         zoomEnable: false,
         viewMode: '2D'
     });

    // heatMap.setMapStyle("amap://styles/darkblue");
    // let marker = new AMap.Marker({
    //     position: markerPosition//位置
    // });
    // heatMap.add(marker);//添加到地图
    loadMapData();

    heatMap.on('zoomend', function () {
        // loadMapData();
    });
    // Scale
    heatMap.on('moveend', function () {
        currentCenter = [heatMap.getCenter().lng, heatMap.getCenter().lat];
        // loadMapData();
    });

    PixelTimer = window.setInterval(() => {
        // 添加标注冒泡
        addPixel(PixelData[PixelIndex]);
        if (PixelIndex == PixelData.length - 2) {
            PixelIndex = 0;
        } else {
            PixelIndex++;
        }
    }, 5000);
    
    
}

let layer;
function loadMapData() {
    let mapData = [
        {"coordinate":[104.010288,30.75997],"count":88},{"coordinate":[103.966,30.743447],"count":48},{"coordinate":[103.969433,30.71305],"count":4},
        {"coordinate":[103.96291,30.694452],"count":42},{"coordinate":[103.970806,30.658428],"count":52},{"coordinate":[104.015782,30.695928],"count":40},
        {"coordinate":[104.045994,30.745808],"count":90},{"coordinate":[104.099209,30.750824],"count":72},{"coordinate":[104.076893,30.712164],"count":36},
        {"coordinate":[104.040501,30.672603],"count":29},{"coordinate":[104.055264,30.623573],"count":35},{"coordinate":[104.05801,30.626822],"count":7},
        {"coordinate":[104.113629,30.629481],"count":21},{"coordinate":[104.115002,30.668174],"count":90},{"coordinate":[104.119122,30.687662],"count":77},
        {"coordinate":[104.094059,30.692386],"count":71},{"coordinate":[104.085133,30.689138],"count":21},{"coordinate":[104.084103,30.695338],"count":71},
        {"coordinate":[104.055607,30.692976],"count":17},{"coordinate":[104.06316,30.683528],"count":53},{"coordinate":[104.054577,30.672603],"count":83},
        {"coordinate":[104.052517,30.664335],"count":54},{"coordinate":[104.050114,30.654884],"count":2},{"coordinate":[104.065563,30.64691],"count":84},
        {"coordinate":[104.050457,30.641002],"count":87},{"coordinate":[104.06419,30.661972],"count":63},{"coordinate":[104.062817,30.666402],"count":21},
        {"coordinate":[104.089596,30.672308],"count":25},{"coordinate":[104.06831,30.644547],"count":31},{"coordinate":[104.062473,30.6602],"count":15},
        {"coordinate":[104.083759,30.660791],"count":75},{"coordinate":[104.086506,30.673194],"count":37},{"coordinate":[104.080326,30.68589],"count":58},
        {"coordinate":[104.105732,30.671127],"count":27},{"coordinate":[104.104015,30.654294],"count":14},{"coordinate":[104.101956,30.636571],"count":37},
        {"coordinate":[104.066593,30.636571],"count":53},{"coordinate":[104.201176,30.626527],"count":4},{"coordinate":[104.051487,30.65134],"count":78},
        {"coordinate":[104.067623,30.710688],"count":84},{"coordinate":[104.122212,30.692091],"count":50},{"coordinate":[104.106075,30.683233],"count":6},
        {"coordinate":[104.216969,30.681757],"count":17},{"coordinate":[104.181263,30.697405],"count":89},{"coordinate":[104.030544,30.656952],"count":15},
        {"coordinate":[104.030201,30.646024],"count":25},{"coordinate":[104.035008,30.634503],"count":3},{"coordinate":[104.05183,30.630663],"count":51},
        {"coordinate":[104.057324,30.642774],"count":84},{"coordinate":[104.044621,30.664335],"count":80},{"coordinate":[104.050457,30.672899],"count":25},
        {"coordinate":[104.052517,30.66463],"count":11},{"coordinate":[104.073803,30.667583],"count":66},{"coordinate":[104.061787,30.679099],"count":27},
        {"coordinate":[104.070027,30.679985],"count":81},{"coordinate":[104.07449,30.676737],"count":10},{"coordinate":[104.075863,30.665811],"count":30},
        {"coordinate":[104.079296,30.673194],"count":11},{"coordinate":[104.07346,30.675556],"count":57},{"coordinate":[104.079983,30.675851],"count":93},
        {"coordinate":[104.07964,30.66906],"count":26},{"coordinate":[104.087879,30.671422],"count":5},{"coordinate":[104.088909,30.666402],"count":32},
        {"coordinate":[104.088909,30.657247],"count":81},{"coordinate":[104.077236,30.649568],"count":94},{"coordinate":[104.057324,30.643956],"count":88},
        {"coordinate":[104.06007,30.638344],"count":25},{"coordinate":[104.071743,30.637457],"count":94},{"coordinate":[104.075176,30.649863],"count":41},
        {"coordinate":[104.081013,30.642774],"count":42},{"coordinate":[104.091313,30.650159],"count":37},{"coordinate":[104.096119,30.662268],"count":45},
        {"coordinate":[104.083073,30.670241],"count":24},{"coordinate":[104.067967,30.673489],"count":48},{"coordinate":[104.07346,30.678804],"count":72},
        {"coordinate":[104.05698,30.679395],"count":23},{"coordinate":[104.057324,30.656066],"count":81},{"coordinate":[104.066937,30.676147],"count":46},
        {"coordinate":[104.079296,30.679099],"count":58},{"coordinate":[104.087193,30.669946],"count":12},{"coordinate":[104.065563,30.648977],"count":68},
        {"coordinate":[104.059384,30.634208],"count":40},{"coordinate":[104.077923,30.634503],"count":92},{"coordinate":[104.086163,30.653998],"count":20},
        {"coordinate":[104.089253,30.664335],"count":71},{"coordinate":[104.092686,30.667288],"count":12},{"coordinate":[104.0611,30.673784],"count":54},
        {"coordinate":[104.081013,30.686481],"count":57},{"coordinate":[104.06213,30.641593],"count":81},{"coordinate":[104.076893,30.641593],"count":45},
        {"coordinate":[104.078953,30.646319],"count":62},{"coordinate":[104.053547,30.654884],"count":52},{"coordinate":[104.005825,30.671127],"count":83},
        {"coordinate":[104.068653,30.674966],"count":67},{"coordinate":[104.058697,30.663523],"count":53},{"coordinate":[104.05904,30.664261],"count":8},
        {"coordinate":[104.05904,30.664261],"count":10},{"coordinate":[104.05904,30.664852],"count":83},{"coordinate":[104.058525,30.664852],"count":47},
        {"coordinate":[104.056809,30.664999],"count":77},{"coordinate":[104.05698,30.662637],"count":93},{"coordinate":[104.062473,30.663375],"count":83},
        {"coordinate":[104.064705,30.659536],"count":16},{"coordinate":[104.064533,30.659536],"count":56},{"coordinate":[104.05904,30.658945],"count":29},
        {"coordinate":[104.058525,30.657173],"count":17},{"coordinate":[104.056465,30.659241],"count":48},{"coordinate":[104.054577,30.661308],"count":40},
        {"coordinate":[104.053719,30.659683],"count":49},{"coordinate":[104.056122,30.666181],"count":93},{"coordinate":[104.06728,30.665885],"count":53},
        {"coordinate":[104.072086,30.667362],"count":65},{"coordinate":[104.07758,30.6681],"count":85},{"coordinate":[104.067108,30.668395],"count":41},
        {"coordinate":[104.069168,30.671644],"count":35},{"coordinate":[104.0714,30.670758],"count":14},{"coordinate":[104.07037,30.672382],"count":7},
        {"coordinate":[104.073803,30.674154],"count":15},{"coordinate":[104.083588,30.680354],"count":85},{"coordinate":[104.078953,30.669872],"count":97},
        {"coordinate":[104.086678,30.669577],"count":36},{"coordinate":[104.085648,30.662637],"count":20},{"coordinate":[104.090454,30.6523],"count":10},
        {"coordinate":[104.086849,30.666181],"count":58},{"coordinate":[104.082901,30.666476],"count":57},{"coordinate":[104.079125,30.664556],"count":97},
        {"coordinate":[104.072086,30.658945],"count":44},{"coordinate":[104.07037,30.651119],"count":52},{"coordinate":[104.07037,30.651562],"count":88},
        {"coordinate":[104.06419,30.651709],"count":90},{"coordinate":[104.058354,30.653038],"count":16},{"coordinate":[104.052689,30.656139],"count":28},
        {"coordinate":[104.048054,30.656287],"count":25},{"coordinate":[104.046337,30.6523],"count":93},{"coordinate":[104.048054,30.647427],"count":47},
        {"coordinate":[104.055264,30.647279],"count":16},{"coordinate":[104.057667,30.649642],"count":95},{"coordinate":[104.059727,30.641224],"count":24},
        {"coordinate":[104.071228,30.647427],"count":53},{"coordinate":[104.072086,30.649642],"count":20},{"coordinate":[104.061787,30.655253],"count":39},
        {"coordinate":[104.061787,30.659683],"count":67},{"coordinate":[104.047196,30.667657],"count":11},{"coordinate":[104.05389,30.678435],"count":24},
        {"coordinate":[104.060928,30.679764],"count":41},{"coordinate":[104.065907,30.679469],"count":4},{"coordinate":[104.068825,30.680502],"count":93},
        {"coordinate":[104.07346,30.681535],"count":88},{"coordinate":[104.077408,30.679764],"count":7},{"coordinate":[104.092686,30.670019],"count":62},
        {"coordinate":[104.094402,30.665295],"count":37},{"coordinate":[104.089939,30.66308],"count":83},{"coordinate":[104.098179,30.664999],"count":39},
        {"coordinate":[104.089768,30.657468],"count":29},{"coordinate":[104.095261,30.653334],"count":15},{"coordinate":[104.089253,30.650085],"count":91},{"coordinate":[104.071743,30.639008],"count":24},{"coordinate":[104.064018,30.639304],"count":67},{"coordinate":[104.043934,30.626601],"count":55},{"coordinate":[104.045994,30.668838],"count":2},{"coordinate":[104.046509,30.673858],"count":25},{"coordinate":[104.046509,30.687293],"count":51},{"coordinate":[104.057495,30.689802],"count":11},{"coordinate":[104.05389,30.689212],"count":73},{"coordinate":[104.063847,30.690688],"count":84},{"coordinate":[104.069168,30.690393],"count":1},{"coordinate":[104.08067,30.686702],"count":71},{"coordinate":[104.087879,30.68685],"count":12},{"coordinate":[104.098351,30.679764],"count":61},{"coordinate":[104.033806,30.679026],"count":21},{"coordinate":[104.016812,30.680059],"count":15},{"coordinate":[104.0187,30.686998],"count":10},{"coordinate":[104.032948,30.69246],"count":61},{"coordinate":[104.009773,30.646393],"count":86},{"coordinate":[104.014408,30.648017],"count":14},{"coordinate":[104.0187,30.657025],"count":82},{"coordinate":[104.026253,30.650085],"count":84},{"coordinate":[104.030373,30.628226],"count":68},{"coordinate":[104.059899,30.62926],"count":88},{"coordinate":[104.050457,30.629703],"count":26},{"coordinate":[104.058697,30.624976],"count":91},{"coordinate":[104.094402,30.625567],"count":75},{"coordinate":[104.104874,30.639894],"count":96},{"coordinate":[104.104702,30.643291],"count":29},{"coordinate":[104.077408,30.63694],"count":70},{"coordinate":[104.067967,30.63502],"count":95},{"coordinate":[104.08067,30.649051],"count":36},{"coordinate":[104.089253,30.656582],"count":61},{"coordinate":[104.095776,30.664113],"count":23},{"coordinate":[104.10659,30.663966],"count":1},{"coordinate":[104.037411,30.667657],"count":95},{"coordinate":[104.054749,30.667952],"count":79},{"coordinate":[104.042904,30.673563],"count":77},{"coordinate":[104.061443,30.677697],"count":87},{"coordinate":[104.066078,30.663523],"count":97},{"coordinate":[104.056637,30.661898],"count":53},{"coordinate":[104.054749,30.650676],"count":98},{"coordinate":[104.068653,30.646097],"count":55},{"coordinate":[104.06831,30.648313],"count":95},{"coordinate":[104.082901,30.648608],"count":84},{"coordinate":[104.061958,30.632509],"count":68},{"coordinate":[104.066422,30.642996],"count":84},{"coordinate":[104.054234,30.657468],"count":98},{"coordinate":[104.054749,30.664704],"count":67},{"coordinate":[104.081013,30.669134],"count":96},{"coordinate":[104.091313,30.678288],"count":93},{"coordinate":[104.06316,30.687145],"count":88},{"coordinate":[104.060413,30.68995],"count":85},{"coordinate":[104.100239,30.682421],"count":79},{"coordinate":[104.133885,30.66116],"count":83},{"coordinate":[104.052002,30.648017],"count":63},{"coordinate":[104.203407,30.567053],"count":67},{"coordinate":[104.193451,30.570305],"count":90},{"coordinate":[104.183495,30.56927],"count":92},
        {"coordinate":[104.06213,30.661308],"count":88},{"coordinate":[104.061443,30.661898],"count":64},{"coordinate":[104.061443,30.66367],"count":69},{"coordinate":[104.063675,30.662784],"count":90},{"coordinate":[104.061787,30.660126],"count":65},{"coordinate":[104.059555,30.661603],"count":73},{"coordinate":[104.057152,30.664113],"count":89},{"coordinate":[104.058525,30.668986],"count":95},{"coordinate":[104.053719,30.669724],"count":84},{"coordinate":[104.054405,30.666328],"count":84},{"coordinate":[104.06007,30.669134],"count":77},{"coordinate":[104.06522,30.669281],"count":81},{"coordinate":[104.063847,30.673711],"count":81},{"coordinate":[104.056465,30.674006],"count":83},{"coordinate":[104.053204,30.676811],"count":53},{"coordinate":[104.059384,30.677402],"count":90},{"coordinate":[104.06007,30.679469],"count":63},{"coordinate":[104.068138,30.677402],"count":61},{"coordinate":[104.06419,30.673711],"count":97},{"coordinate":[104.06831,30.675039],"count":91},{"coordinate":[104.06934,30.670315],"count":92},{"coordinate":[104.068825,30.667509],"count":65},{"coordinate":[104.069683,30.671053],"count":93},{"coordinate":[104.074661,30.672677],"count":80},{"coordinate":[104.079125,30.672234],"count":96},{"coordinate":[104.0714,30.670019],"count":99},{"coordinate":[104.069168,30.666919],"count":50},{"coordinate":[104.066765,30.666181],"count":62},{"coordinate":[104.062645,30.665147],"count":53},{"coordinate":[104.058525,30.665885],"count":82},{"coordinate":[104.053547,30.667066],"count":84},{"coordinate":[104.054234,30.664704],"count":69},{"coordinate":[104.056294,30.662784],"count":78},{"coordinate":[104.061615,30.665442],"count":89},{"coordinate":[104.060413,30.660422],"count":70},{"coordinate":[104.064018,30.663375],"count":85},{"coordinate":[104.0611,30.660274],"count":77},{"coordinate":[104.05801,30.663375],"count":97},{"coordinate":[104.056809,30.660717],"count":97},{"coordinate":[104.066422,30.658059],"count":99},{"coordinate":[104.066422,30.658798],"count":99},{"coordinate":[104.061787,30.660126],"count":94},{"coordinate":[104.061787,30.656878],"count":88},{"coordinate":[104.055607,30.656287],"count":55},{"coordinate":[104.051659,30.656878],"count":92},{"coordinate":[104.061787,30.655844],"count":68},{"coordinate":[104.055607,30.651119],"count":52},{"coordinate":[104.056122,30.646836],"count":87},{"coordinate":[104.061272,30.651266],"count":85},{"coordinate":[104.066078,30.650085],"count":83},{"coordinate":[104.071057,30.651562],"count":51},{"coordinate":[104.068653,30.655696],"count":67},{"coordinate":[104.072258,30.660569],"count":93},{"coordinate":[104.067795,30.666624],"count":69},{"coordinate":[104.07037,30.669724],"count":84},{"coordinate":[104.07346,30.673858],"count":66},{"coordinate":[104.080841,30.671644],"count":78},{"coordinate":[104.075691,30.667214],"count":72},{"coordinate":[104.087364,30.670758],"count":75},{"coordinate":[104.085648,30.663523],"count":89},{"coordinate":[104.077065,30.662932],"count":85},{"coordinate":[104.082214,30.66308],"count":50},{"coordinate":[104.066422,30.662932],"count":73},{"coordinate":[104.058525,30.664999],"count":54},{"coordinate":[104.053204,30.669134],"count":57},{"coordinate":[104.050285,30.668986],"count":85},{"coordinate":[104.050629,30.673711],"count":66},{"coordinate":[104.06316,30.678583],"count":94},{"coordinate":[104.066937,30.692017],"count":79},{"coordinate":[104.052689,30.659831],"count":88},{"coordinate":[104.048226,30.657173],"count":65},{"coordinate":[104.053032,30.652743],"count":67},{"coordinate":[104.059384,30.64846],"count":78},{"coordinate":[104.058525,30.638565],"count":62},{"coordinate":[104.065907,30.646983],"count":55},{"coordinate":[104.083416,30.654515],"count":73},{"coordinate":[104.081528,30.649346],"count":95},{"coordinate":[104.076893,30.66559],"count":86},{"coordinate":[104.086849,30.670019],"count":65},{"coordinate":[104.097321,30.667952],"count":61},{"coordinate":[104.100067,30.682717],"count":64},{"coordinate":[104.095604,30.674892],"count":54},{"coordinate":[104.084274,30.68995],"count":73},{"coordinate":[104.082558,30.697478],"count":50},{"coordinate":[104.053719,30.689064],"count":72},{"coordinate":[104.044792,30.690098],"count":64},{"coordinate":[104.042217,30.686112],"count":85},{"coordinate":[104.037068,30.680059],"count":50},{"coordinate":[104.041016,30.679469],"count":81},{"coordinate":[104.032604,30.675335],"count":89},{"coordinate":[104.025223,30.655844],"count":60},{"coordinate":[104.036896,30.652005],"count":93},{"coordinate":[104.036038,30.650528],"count":65},{"coordinate":[104.094059,30.674301],"count":55},{"coordinate":[104.101612,30.674154],"count":59},{"coordinate":[104.102127,30.667214],"count":71},{"coordinate":[104.112942,30.657616],"count":92},{"coordinate":[104.092686,30.671644],"count":59},{"coordinate":[104.10659,30.687441],"count":76},{"coordinate":[104.078095,30.705892],"count":63},{"coordinate":[104.062817,30.705154],"count":51},{"coordinate":[104.051487,30.700873],"count":86},{"coordinate":[104.044792,30.694526],"count":61},{"coordinate":[104.065392,30.698807],"count":77},{"coordinate":[104.064018,30.70722],"count":78},{"coordinate":[104.036381,30.703382],"count":99},{"coordinate":[104.031403,30.695707],"count":77},{"coordinate":[104.030201,30.688917],"count":62},{"coordinate":[104.032433,30.688622],"count":83},{"coordinate":[104.019558,30.681093],"count":69},{"coordinate":[104.006683,30.681093],"count":85},{"coordinate":[104.012005,30.659683],"count":90},{"coordinate":[104.019558,30.675925],"count":74},{"coordinate":[103.999645,30.662932],"count":98},{"coordinate":[104.014752,30.657321],"count":65},{"coordinate":[104.069512,30.608726],"count":71},{"coordinate":[104.072086,30.603702],"count":68},{"coordinate":[104.084446,30.566166],"count":74},{"coordinate":[104.092514,30.56661],"count":60},{"coordinate":[104.090111,30.560697],"count":79},{"coordinate":[104.066937,30.558184],"count":83},{"coordinate":[104.056294,30.555819],"count":55},{"coordinate":[104.042904,30.565871],"count":87},{"coordinate":[104.052345,30.567792],"count":78},{"coordinate":[103.978016,30.578877],"count":52},{"coordinate":[103.988659,30.581389],"count":72},{"coordinate":[103.950207,30.574148],"count":66},{"coordinate":[103.948834,30.555376],"count":76},{"coordinate":[103.928578,30.529799],"count":76},{"coordinate":[103.887207,30.53039],"count":50},{"coordinate":[103.859226,30.53527],"count":61},{"coordinate":[103.823177,30.521222],"count":96},{"coordinate":[103.873303,30.521814],"count":63},{"coordinate":[103.901627,30.501997],"count":72},{"coordinate":[103.984196,30.486169],"count":65},{"coordinate":[103.990891,30.483211],"count":55},
        {"coordinate":[104.025566,30.652743],"count":58},{"coordinate":[104.027626,30.653186],"count":68},{"coordinate":[104.032948,30.653777],"count":59},{"coordinate":[104.037754,30.653777],"count":60},{"coordinate":[104.027455,30.652743],"count":82},{"coordinate":[104.038784,30.652152],"count":92},{"coordinate":[104.026425,30.652595],"count":80},{"coordinate":[104.029171,30.66559],"count":61},{"coordinate":[104.024365,30.66367],"count":96},{"coordinate":[104.038613,30.662194],"count":62},{"coordinate":[104.027969,30.664999],"count":56},{"coordinate":[104.027626,30.670019],"count":92},{"coordinate":[104.025223,30.673563],"count":77},{"coordinate":[104.029171,30.671053],"count":52},{"coordinate":[104.036381,30.671053],"count":72},{"coordinate":[104.035351,30.671496],"count":91},{"coordinate":[104.031746,30.674006],"count":95},{"coordinate":[104.033291,30.679469],"count":74},{"coordinate":[104.035866,30.675335],"count":68},{"coordinate":[104.036896,30.682569],"count":60},{"coordinate":[104.038441,30.685226],"count":86},{"coordinate":[104.042561,30.688917],"count":67},{"coordinate":[104.042217,30.685817],"count":65},{"coordinate":[104.045822,30.685521],"count":61},{"coordinate":[104.047367,30.688769],"count":92},{"coordinate":[104.053032,30.688769],"count":89},{"coordinate":[104.055092,30.68995],"count":79},{"coordinate":[104.055264,30.691574],"count":76},{"coordinate":[104.062645,30.691574],"count":51},{"coordinate":[104.060242,30.688917],"count":56},{"coordinate":[104.068482,30.690688],"count":53},{"coordinate":[104.069683,30.691722],"count":52},{"coordinate":[104.071228,30.688917],"count":70},{"coordinate":[104.078953,30.688769],"count":92},{"coordinate":[104.077236,30.687883],"count":60},{"coordinate":[104.081013,30.689212],"count":96},{"coordinate":[104.088909,30.701316],"count":92},{"coordinate":[104.092514,30.686555],"count":91},{"coordinate":[104.092343,30.680502],"count":53},{"coordinate":[104.085819,30.681093],"count":79},{"coordinate":[104.101097,30.677992],"count":54},{"coordinate":[104.099037,30.67563],"count":84},{"coordinate":[104.10556,30.673415],"count":65},{"coordinate":[104.103501,30.669872],"count":72},{"coordinate":[104.102127,30.666181],"count":52},{"coordinate":[104.115173,30.656287],"count":63},{"coordinate":[104.117405,30.662194],"count":68},{"coordinate":[104.124272,30.664704],"count":50},{"coordinate":[104.111397,30.657173],"count":76},{"coordinate":[104.125301,30.652448],"count":69},{"coordinate":[104.113972,30.656582],"count":60},{"coordinate":[104.118607,30.659831],"count":50},{"coordinate":[104.119637,30.659241],"count":58},{"coordinate":[104.106419,30.651709],"count":68},{"coordinate":[104.109165,30.656878],"count":55},{"coordinate":[104.033806,30.643734],"count":86},{"coordinate":[104.088738,30.706334],"count":92},{"coordinate":[104.090798,30.707515],"count":58},{"coordinate":[104.076206,30.70722],"count":51},{"coordinate":[104.077408,30.703235],"count":87},{"coordinate":[104.072773,30.700726],"count":73},{"coordinate":[104.085648,30.703825],"count":98},{"coordinate":[104.10865,30.716961],"count":72},{"coordinate":[104.117577,30.668986],"count":87},{"coordinate":[104.117577,30.664409],"count":98},{"coordinate":[104.122727,30.662489],"count":68},{"coordinate":[104.197228,30.669281],"count":97},{"coordinate":[104.204952,30.679173],"count":51},{"coordinate":[104.203407,30.672087],"count":62},{"coordinate":[104.198258,30.669724],"count":88},{"coordinate":[104.212162,30.685521],"count":67},{"coordinate":[104.208386,30.682274],"count":62},{"coordinate":[104.215424,30.688031],"count":67},{"coordinate":[104.218857,30.685521],"count":57},{"coordinate":[104.152252,30.669134],"count":66},{"coordinate":[104.16959,30.667805],"count":87},{"coordinate":[104.161865,30.658945],"count":96},{"coordinate":[104.195854,30.665885],"count":66},{"coordinate":[104.176972,30.658798],"count":63},{"coordinate":[104.167015,30.660274],"count":81},{"coordinate":[104.18092,30.658355],"count":50},{"coordinate":[104.164269,30.649051],"count":57},{"coordinate":[104.18092,30.634134],"count":83},{"coordinate":[104.184525,30.633543],"count":59},{"coordinate":[104.191563,30.629112],"count":52},{"coordinate":[104.167874,30.633839],"count":75},{"coordinate":[104.123928,30.578729],"count":83},{"coordinate":[104.123928,30.578729],"count":50},{"coordinate":[104.130108,30.57932],"count":75},{"coordinate":[104.130108,30.57932],"count":75},{"coordinate":[104.131825,30.579468],"count":86},{"coordinate":[104.1344,30.585527],"count":81},{"coordinate":[104.145214,30.578581],"count":95},{"coordinate":[104.118263,30.542958],"count":51},{"coordinate":[104.117062,30.543697],"count":62},{"coordinate":[104.093372,30.537487],"count":97},{"coordinate":[104.117405,30.534826],"count":71},{"coordinate":[104.136631,30.542219],"count":78},{"coordinate":[104.16856,30.536452],"count":83},{"coordinate":[104.168732,30.536896],"count":97},{"coordinate":[104.178517,30.544436],"count":85},{"coordinate":[104.166672,30.549758],"count":70},{"coordinate":[104.031746,30.651857],"count":96},{"coordinate":[104.025395,30.649199],"count":77},{"coordinate":[104.01355,30.647131],"count":68},{"coordinate":[104.00737,30.639599],"count":56},{"coordinate":[104.034664,30.637531],"count":90},{"coordinate":[104.012863,30.625567],"count":52},{"coordinate":[104.023678,30.624237],"count":55},{"coordinate":[104.029343,30.625271],"count":60},{"coordinate":[104.024365,30.637088],"count":85},{"coordinate":[104.032948,30.631032],"count":59},{"coordinate":[104.033119,30.628817],"count":64},{"coordinate":[104.01149,30.627487],"count":66},{"coordinate":[104.001877,30.618919],"count":94},{"coordinate":[104.01149,30.622908],"count":94},{"coordinate":[103.995354,30.613306],"count":88},{"coordinate":[104.028484,30.608726],"count":83},{"coordinate":[104.043247,30.614931],"count":75},{"coordinate":[104.057324,30.615965],"count":57},{"coordinate":[104.060242,30.616408],"count":59},{"coordinate":[104.083588,30.607691],"count":57},{"coordinate":[104.095089,30.617738],"count":72},{"coordinate":[104.093372,30.612567],"count":69},{"coordinate":[104.106934,30.620101],"count":83},{"coordinate":[104.111397,30.62084],"count":81},{"coordinate":[104.070885,30.630294],"count":71},{"coordinate":[104.089081,30.625567],"count":70},{"coordinate":[104.089596,30.625715],"count":88},{"coordinate":[104.11277,30.623499],"count":86},{"coordinate":[104.110882,30.630146],"count":50},{"coordinate":[104.048226,30.632066],"count":82},{"coordinate":[104.031918,30.620397],"count":70},{"coordinate":[104.06007,30.605623],"count":58},{"coordinate":[104.084103,30.612124],"count":55},{"coordinate":[104.079468,30.61301],"count":59},{"coordinate":[104.11174,30.528024],"count":76},

    ];
    layer = new Loca.HeatmapLayer({
        map: heatMap,
    });

    // console.log('data---',data);
    // let heatmapData = data.data ? data.data : [];
    // let list = [];
    // if (heatmapData.length > 0) {
    //     heatmapData.map((item,index) => {
    //         let count = Math.floor(Math.random()*(1 - 100) + 100);
    //         list.push({
    //             coordinate: [item.longitude, item.latitude],
    //             count: count
    //         })
    //     })
    // }
    // console.log('list---',list);

    layer.setData(mapData, {
        lnglat: 'coordinate',
        value: 'count'
    });

    layer.setOptions({
        style: {
            radius: 16,
            color: {
                0.5: '#2c7bb6',
                0.65: '#abd9e9',
                0.7: '#ffffbf',
                0.9: '#fde468',
                1.0: '#d7191c'
            }
        }
    });

    layer.render();
}

var marker;
// 添加标注冒泡
function addPixel(data) {
    AMapUI.loadUI(['overlay/SimpleInfoWindow'], function(SimpleInfoWindow) {
        marker = new AMap.Marker({
            map: heatMap,
            zIndex: 9999999,
            // height: 10,
            // fillColor: 'red', //填充色
            showPositionPoint: true,
            position: data.position
        });

        var infoWindow = new SimpleInfoWindow({

            infoTitle: '<strong>'+data.name+'</strong>',
            infoBody: '<p class="my-desc">'+data.info+'</p>',

            //基点指向marker的头部位置
            offset: new AMap.Pixel(0, -31)
        });

        function openInfoWin() {
            infoWindow.open(heatMap, marker.getPosition());
        }

        openInfoWin();

        setTimeout(() => {
            heatMap.remove(marker);
            heatMap.remove(infoWindow);
            // infoWindow = null;
        },2000)
    });
}

// define('Sprite', ['maptalks'], function (maptalks) {

    /**
     * 精灵类
     * @param {Object} opt 配置选项
     */
    function Sprite(opt) {
        var options = opt || {};
        /**
         * 当前半径
         */
        this.currentRadius = options.currentRadius || 0;
        /**
         * 最大半径
         */
        this.maxRadius = options.maxRadius || 20;
        /**
         * canvas上下文
         */
        this.context = options.context || undefined;
        /**
         * 开始时间，初始化之后一秒内的随机增量
         */
        this.start = +new Date() + Math.random() * 3000;
        /**
         * 动画持续时间
         */
        this.duration = options.duration || 3000;
        /**
         * 边线颜色(数组)
         */
        this.strokeStyle = options.strokeStyle || [0, 255, 44];
        /**
         * 圆环类型
         */
        this.type = options.type || 'circle';
        /**
         * 圆环数量
         */
        this.number = options.number || 3;
        /**
         * 边线透明度
         */
        this.opacity = options.opacity || 1;
        /**
         * 线宽
         */
        this.lineWidth = options.lineWidth || 2;
        /**
         * 坐标
         */
        this.coordinate = options.coordinate;

        this.map = options.map;

        this.init();
    }

    Sprite.prototype = {
        /**初始化 */
        init() {
            let self = this;
            /**
             * 间隔距离
             */
            this.interval = this.maxRadius / this.number;
        },
        setContext(context) {
            this.context = context;
        },
        getContext() {
            return this.context;
        },
        getStyle(color) {
            return 'rgba(' + this.strokeStyle.join(',') + ',' + this.opacity + ')';
        },
        draw() {
            if (!this.context) return;
            var context = this.context;
            let start = this.start, now = +new Date(), elapsed = now - start;
            if (elapsed > 0) {
                if (elapsed >= 0) {
                    var elapsedRatio = elapsed / this.duration;
                    var pixel = [0, 0];
                    if (this.coordinate && this.map) {
                        var point = this.map.coordinateToContainerPoint(this.coordinate);
                        pixel[0] = point.x;
                        pixel[1] = point.y;
                    }
                    if (elapsedRatio <= 1) {
                        this.currentRadius = this.maxRadius * elapsedRatio;
                        var radiusArr = [this.currentRadius];
                        var fontNumber = Math.floor(this.currentRadius / this.interval);

                        if (fontNumber > 0) {
                            for (var i = 0; i < fontNumber; i++) {
                                radiusArr.push(this.currentRadius - (i + 1) * this.interval);
                            }
                        }

                        var backNumber = Math.floor((this.maxRadius - this.currentRadius) / this.interval);
                        if (backNumber > 0) {
                            for (var i = 0; i < backNumber; i++) {
                                radiusArr.push(this.currentRadius + (i + 1) * this.interval);
                            }
                        }

                        radiusArr.forEach((item, index) => {
                            context.save();
                            context.beginPath();
                            context.lineWidth = this.lineWidth;
                            if (pixel) {
                                if (this.type == 'circle') {
                                    context.arc(pixel[0], pixel[1], item, 0, Math.PI * 2);
                                } else {
                                    context.ellipse(pixel[0], pixel[1], item, item * 3 / 4, 0, 0, 2 * Math.PI, false)
                                }
                            }
                            context.closePath();
                            this.opacity = Math.sin((item / this.maxRadius) * Math.PI);
                            context.strokeStyle = this.getStyle();
                            context.stroke();
                            context.restore();
                        })
                    } else {
                        this.start += this.duration;
                    }
                }
            }
        }
    }

//     return Sprite;
// })
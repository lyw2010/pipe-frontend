var SVG_Parser = function() { "use strict";
	
	this._baseObj;
	this._baseList;
	this._subRatio;
	this._svgArry = [];
	
	this.parser = function( obj, list, subRatio ){
		_SP._baseObj = $('<div></div>').append( obj ).find('svg').get(0);
		_SP._baseList = $.extend( true, {}, df_List, list );
		_SP._subRatio = isNaN(subRatio*1)? .6: Math.max( .5, Math.floor( subRatio*1 ) );
		
		convertPath();
		convertRect();
		convertCircle();
		convertEllipse();
		convertPolygon();
		
		return getSvgData();
	};
	
	var _SP = this;
	var df_List = { name: '', coord: '', color: '', height: 1, weight: 0 };
	
	function getSvgData() {
        console.log('getSvgData');
		var len = _SP._svgArry.length, 
			i=0, obj = {}, series = [];
		
		for ( ; i<len; i++ ) {
			var _sort = $.extend( true, {}, _SP._baseList );
			
			_sort['_id'] = '_path_'+i;
			_sort['path'] = _SP._svgArry[i];
			
			series.push( _sort );
		}
		
		obj.series = series;
		
		var _VBbaseVal = _SP._baseObj.viewBox.baseVal;
		obj._width = _VBbaseVal.width - _VBbaseVal.x;
		obj._height = _VBbaseVal.height - _VBbaseVal.y;
		
		return obj;
	}
	
	function convertPath() {
        console.log('convertPath');
		var paths = _SP._baseObj.getElementsByTagName( "path" ),
			len = paths.length, tran, node;
		if ( len < 1 ) { return; }
		
		for (var n = 0; n < len; n++) {
			node = paths.item(n);
			// tran = node.getAttribute("transform") || node.parentElement.getAttribute('transform') || '';
			// var transform = tran.match( /translate\s*\(\s*([^\)]*)\s*\)/g ) || [''];
			// transform = transform[0].match(/(-?\d+)(\.\d+)?/g) || [0, 0];
			
			_SP._svgArry.push( node.getAttribute("d") );
			// _SP._svgArry.push( tranPath( node, transform ) );
		}
	}
	
	function convertRect() {
        console.log('convertRect');
		var rects = _SP._baseObj.getElementsByTagName( "rect" ),
			len = rects.length, x, y, w, h, deg = 0, proxy = [],
			tran, tranParam = [], node;
		if ( len < 1 ) { return; }
		
		for ( var n = 0; n < len; n++ ) {
			node = rects.item(n);
			x = +node.getAttribute("x");
			y = +node.getAttribute("y");
			w = +node.getAttribute("width");
			h = +node.getAttribute("height");
			tran = node.getAttribute("transform");

			if (tran && tran.indexOf("matrix") !== -1) {
				tranParam = tran.replace(/^matrix\s*\(([\d.\s-]+)\)/g, "$1").split(/\s|,/);
			}

			if (tranParam.length > 0) {
				deg = Math.acos(tranParam[0]) * 180 / Math.PI;
				if (tranParam[tranParam.length - 1] > 0) {
					deg *= -1;
				}
			}
			proxy = getProxy(x, y, w, h, deg);
			var path = "M" + proxy[0].x.toFixed(3) + " " + proxy[0].y.toFixed(3)
									+ " L" + proxy[1].x.toFixed(3) + " " + proxy[1].y.toFixed(3)
									+ " L" + proxy[2].x.toFixed(3) + " " + proxy[2].y.toFixed(3)
									+ " L" + proxy[3].x.toFixed(3) + " " + proxy[3].y.toFixed(3)
									+ " Z";
		   _SP._svgArry.push( path );
		}
	}

	function convertCircle() {
        console.log('convertCircle');
		var circles = _SP._baseObj.getElementsByTagName( "circle" ),
			len = circles.length, cx, cy, r, node;
		if ( len < 1 ) { return; }
		
		for ( var n = 0; n < len; n++ ) {
			node = circles.item(n);
			cx = +node.getAttribute("cx");
			cy = +node.getAttribute("cy");
			r = +node.getAttribute("r");
			var path = "M" + (cx - r).toFixed(3) + " " + cy.toFixed(3) + " A" + r.toFixed(3) + " " + 
						r.toFixed(3) + " 0 1 0 " + (cx + r).toFixed(3) + " " + cy.toFixed(3) + " A" + 
						r.toFixed(3) + " " + r.toFixed(3) + " 0 1 0 " + (cx - r).toFixed(3) + " " + 
						cy.toFixed(3) + " Z";
			_SP._svgArry.push( path );
		}
	}

	function convertEllipse() {
        console.log('convertEllipse');
		var ellipses = _SP._baseObj.getElementsByTagName( "ellipse" ),
			len = ellipses.length, cx, cy, rx, ry, deg = 0, tran, tranParam = [], node;
		if ( len < 1 ) {  return; }
		
		for ( var n = 0; n < len; n++ ) {
			node = ellipses.item(n);
			cx = +node.getAttribute("cx");
			cy = +node.getAttribute("cy");
			rx = +node.getAttribute("rx");
			ry = +node.getAttribute("ry");
			tran = node.getAttribute("transform");
			if (tran && tran.indexOf("matrix") !== -1) {
				tranParam = tran.replace(/^matrix\s*\(([\d.\s-]+)\)/g, "$1").split(/\s|,/);
			}
			if (tranParam.length > 0) {
				deg = Math.acos(tranParam[0]) * 180 / Math.PI;
				if (tranParam[tranParam.length - 1] > 0) {
					deg *= -1;
				}
			}
			points = getProxyEllipse(cx, cy, rx, ry, deg);
			var path = "M" + points[0].x.toFixed(3) + " " + points[0].y.toFixed(3)
										+ " A" + rx.toFixed(3) + " " + ry.toFixed(3) + " " + deg.toFixed(3) + " 1 0 " + points[1].x.toFixed(3) + " " + points[1].y.toFixed(3)
										+ " A" + rx.toFixed(3) + " " + ry.toFixed(3) + " " + deg.toFixed(3) + " 1 0 " + points[0].x.toFixed(3) + " " + points[0].y.toFixed(3)
										+ " Z";
		   _SP._svgArry.push( path );
		}
	}

	function convertPolygon() {
        console.log('convertPolygon');
		var polygons = _SP._baseObj.getElementsByTagName( "polygon" ),
			len = polygons.length, points, data, node;
		if ( len < 1 ) { return; }
		
		for ( var n = 0; n < len; n++ ) {
			node = polygons.item( n );
			points = node.getAttribute("points").split(/\s|,/);
			data = "M" + points[0] + " " + points[1];
			points = points.slice(2);
			for (var i = 0, size = points.length - 2; i < size; i += 2) {
				data += " L" + points[i] + " " + points[i + 1];
			}
			var path = data + " Z";
			_SP._svgArry.push( path );
		}
	}
	
	function getProxy( x, y, w, h, deg ) {
        console.log('getProxy');
		var c = {x: x + w / 2, y: y + h / 2}, points = [], r;
		r = Math.sqrt(Math.pow(w, 2) + Math.pow(h, 2)) / 2;
		deg = deg * Math.PI / 180;
		var deg1 = (Math.PI - Math.acos((w / 2) / r)) - parseFloat(deg),
			deg2 = Math.acos((w / 2) / r) - parseFloat(deg),
			deg3 = - Math.acos((w / 2) / r) - parseFloat(deg),
			deg4 = Math.PI + Math.acos((w / 2) / r) - parseFloat(deg);
		points.push({
			x: c.x + r * Math.cos(deg1),
			y: c.y - r * Math.sin(deg1)
		});
		points.push({
			x: c.x + r * Math.cos(deg2),
			y: c.y - r * Math.sin(deg2)
		});
		points.push({
			x: c.x + r * Math.cos(deg3),
			y: c.y - r * Math.sin(deg3)
		});
		points.push({
			x: c.x + r * Math.cos(deg4),
			y: c.y - r * Math.sin(deg4)
		});
		return points;
	}

	function getProxyEllipse( cx, cy, rx, ry, deg ) {
        console.log('getProxyEllipse');
		var points = [];

		deg = deg * Math.PI / 180;
		points.push({
			x: cx - rx * Math.cos(deg),
			y: cy - rx * Math.sin(deg)
		});
		points.push({
			x: points[0].x + 2 * rx * Math.cos(deg),
			y: points[0].y + 2 * rx * Math.sin(deg)
		});
		return points;
	}
	
	function tranPath( path, transform ) {
		var tlen = path.getTotalLength();
		var len = Math.floor( tlen*_SP._subRatio ),
			l = tlen/len, str = '';
			
		for ( var i=0; i<len; i++ ) {
			var _p = path.getPointAtLength( l*i ),
				_px = ( _p.x + (transform[0]||0)*1 ).toFixed(4),
				_py = ( _p.y + (transform[1]||0)*1 ).toFixed(4);
			
			if ( i==0 ) str +='M'+_px+' '+_py;
			else str +=' L'+_px+' '+_py;
		}
		return str+'Z';
	}
	
};
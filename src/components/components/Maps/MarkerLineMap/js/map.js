var expendMap, markerArr = [], iconObj, iconUrl = '', expendMapSecond, currentCenter = [104.06617,30.658426], markerPosition = [104.008325, 30.669995];
// (function () {30.577127,104.071220
//     createMap();
// })();
var mapId;
window.createMap = function(mapId) {
    // console.log('mapId----',mapId);
    mapId = mapId;
    createMap(mapId);
} 

function createMap(mapId) {
    expendMap = new AMap.Map(mapId, {
        resizeEnable: true,
        zoomEnable: false,
        zoom: 15,
        center: currentCenter
    });
    // expendMap.setMapStyle("amap://styles/darkblue");
    var marker = new AMap.Marker({
        position: markerPosition//位置
    });
    expendMap.add(marker);//添加到地图

    console.log("marker add")
    loadMapData();

    expendMap.on('zoomend', function () {
        loadMapData();
    });
    // Scale
    expendMap.on('moveend', function () {
        currentCenter = [expendMap.getCenter().lng, expendMap.getCenter().lat];
        loadMapData();
    });
}

function loadMapData() {
    var params = {
        currentLatitude: '30.669995',
        currentLongitude: '104.008325',
        latitude: currentCenter[1],
        longitude: currentCenter[0]
    };
    $.ajax({
        url: 'https://preprod.cdzghome.com:8100/merchant/user/appQueryOfflineShopList',
        type: 'POST',
        data: JSON.stringify(params),
        dataType: "json",
        headers: {"Access-Control-Allow-Origin": "*", "Content-Type": "application/json;charset=UTF-8"},
        success: function (data) {
            if (data.code == 200) {
                for (var i = 0; i < markerArr.length; i++) {
                    expendMap.remove(markerArr[i]);
                }
                markerArr = [];

                for (var j = 0; j < data.data.length; j++) {
                    if (data.data[j].merchantLogo === 'https://cdzg-app20.oss-cn-beijing.aliyuncs.com/pro/merchant-http-api/5c6c04de8524e7000ce0c7ff/productImage/productImage_1550583006160.jpg') {
                        iconUrl = require('../images/hongqi.png');
                    } else if (data.data[j].merchantLogo === 'https://cdzg-app20.oss-cn-beijing.aliyuncs.com/pro/merchant-http-api/5c34052b81750c000c6d2b41/productImage/productImage_1546913067068.png') {
                        iconUrl = require('../images/wudongfeng.png');
                    } else if (data.data[j].merchantLogo === 'https://cdzg-app20.oss-cn-beijing.aliyuncs.com/pro/admin-http-api/83/customerHead/productImage/customerHead/productImage_1546928014782.png') {
                        iconUrl = require('../images/renrenle.png');
                    } else if (data.data[j].merchantLogo === 'https://cdzg-app20.oss-cn-beijing.aliyuncs.com/pro/admin-http-api/83/customerHead/productImage/customerHead/productImage_1547090627296.jpg') {
                        iconUrl = require('../images/laolingju.png');
                    } else if (data.data[j].merchantLogo === 'https://cdzg-app20.oss-cn-beijing.aliyuncs.com/pro/admin-http-api/83/customerHead/productImage/customerHead/productImage_1547106343558.jpg') {
                        iconUrl = require('../images/zhenzhengbingye.png');
                    } else if (data.data[j].merchantLogo === 'https://cdzg-app20.oss-cn-beijing.aliyuncs.com/pro/admin-http-api/83/customerHead/productImage/customerHead/productImage_1547110755138.jpg') {
                        iconUrl = require('../images/zhongshuge.png');
                    } else if (data.data[j].merchantLogo === 'https://cdzg-app20.oss-cn-beijing.aliyuncs.com/pro/admin-http-api/83/customerHead/productImage/customerHead/productImage_1547111373582.jpg') {
                        iconUrl = require('../images/haolilai.png');
                    } else if (data.data[j].merchantLogo === 'https://cdzg-app20.oss-cn-beijing.aliyuncs.com/pro/admin-http-api/83/customerHead/productImage/customerHead/productImage_1547185233588.jpg') {
                        iconUrl = require('../images/qinyuan.png');
                    } else if (data.data[j].merchantLogo === 'https://cdzg-app20.oss-cn-beijing.aliyuncs.com/pro/admin-http-api/83/customerHead/productImage/customerHead/productImage_1547455752543.jpg') {
                        iconUrl = require('../images/jingdian.png');
                    } else if (data.data[j].merchantLogo === 'https://cdzg-app20.oss-cn-beijing.aliyuncs.com/pro/merchant-http-api/5c4017efe39c07000c77e392/productImage/productImage_1547704303170.png') {
                        iconUrl = require('../images/meipuli.png');
                    } else if (data.data[j].merchantLogo === 'https://cdzg-app20.oss-cn-beijing.aliyuncs.com/pro/merchant-http-api/5c986e37002413000c7697ef/productImage/productImage_1553493559531.png') {
                        iconUrl = require('../images/darunfa.png');
                    } else if (data.data[j].merchantLogo === 'https://cdzg-app20.oss-cn-beijing.aliyuncs.com/pro/admin-http-api/83/customerHead/productImage/customerHead/productImage_1553498856691.jpg') {
                        iconUrl = require('../images/bencaotang.png');
                    }

                    iconObj = new AMap.Icon({
                        // 图标尺寸
                        size: new AMap.Size(40, 40),
                        // 图标的取图地址
                        image: iconUrl,
                        // 图标所用图片大小
                        imageSize: new AMap.Size(40, 40),
                        // 图标取图偏移量
                        // imageOffset: new AMap.Pixel(-60, -60)
                    });

                    markerArr[j] = new AMap.Marker({
                        position: [data.data[j].longitude, data.data[j].latitude],
                        offset: new AMap.Pixel(-20, -20),
                        icon: iconObj, // 添加 Icon 图标 URL
                        title: data.data[j].shopName,
                    });
                    expendMap.add(markerArr[j]);
                }

            } else {
            }
        }, error: function (err) {
            console.log("数据请求错误", err, new Date());
        }
    });
    // document.getElementsByClassName('amap-icon').style.borderRadius = '20px';
    // document.getElementsByClassName('amap-icon').style.backgroundColor = '#ADD8E6';
}

function show_map(obj, id, scale) {
    var center = expendMap.getCenter();
    expendMap && expendMap.destroy();
    createMap();
    model_show_normal(obj, id, scale);
    expendMapSecond = new AMap.Map('map-box-2', {
        resizeEnable: true,
        zoomEnable: false,
        zoom: 15,
        center: [center.lng, center.lat]
    });
    // expendMapSecond.setMapStyle("amap://styles/darkblue");
    var marker = new AMap.Marker({
        position: markerPosition//位置
    });
    expendMapSecond.add(marker);//添加到地图
    loadMapDataSecond();

    expendMapSecond.on('zoomend', function () {
        loadMapDataSecond();
    });
    expendMapSecond.on('moveend', function () {
        currentCenter = [expendMapSecond.getCenter().lng, expendMapSecond.getCenter().lat];
        loadMapDataSecond();
    });
}

function loadMapDataSecond() {
    var params = {
        currentLatitude: '30.669995',
        currentLongitude: '104.008325',
        latitude: currentCenter[1],
        longitude: currentCenter[0]
    };
    $.ajax({
        url: 'https://preprod.cdzghome.com:8100/merchant/user/appQueryOfflineShopList',
        type: 'POST',
        data: JSON.stringify(params),
        dataType: "json",
        headers: {"Access-Control-Allow-Origin": "*", "Content-Type": "application/json;charset=UTF-8"},
        success: function (data) {
            if (data.code == 200) {
                for (var i = 0; i < markerArr.length; i++) {
                    expendMapSecond.remove(markerArr[i]);
                }
                markerArr = [];

                for (var j = 0; j < data.data.length; j++) {
                    if (data.data[j].merchantLogo === 'https://cdzg-app20.oss-cn-beijing.aliyuncs.com/pro/merchant-http-api/5c6c04de8524e7000ce0c7ff/productImage/productImage_1550583006160.jpg') {
                        iconUrl = './static/image/merchant-logo/hongqi.png';
                    } else if (data.data[j].merchantLogo === 'https://cdzg-app20.oss-cn-beijing.aliyuncs.com/pro/merchant-http-api/5c34052b81750c000c6d2b41/productImage/productImage_1546913067068.png') {
                        iconUrl = './static/image/merchant-logo/wudongfeng.png';
                    } else if (data.data[j].merchantLogo === 'https://cdzg-app20.oss-cn-beijing.aliyuncs.com/pro/admin-http-api/83/customerHead/productImage/customerHead/productImage_1546928014782.png') {
                        iconUrl = './static/image/merchant-logo/renrenle.png';
                    } else if (data.data[j].merchantLogo === 'https://cdzg-app20.oss-cn-beijing.aliyuncs.com/pro/admin-http-api/83/customerHead/productImage/customerHead/productImage_1547090627296.jpg') {
                        iconUrl = './static/image/merchant-logo/laolingju.png';
                    } else if (data.data[j].merchantLogo === 'https://cdzg-app20.oss-cn-beijing.aliyuncs.com/pro/admin-http-api/83/customerHead/productImage/customerHead/productImage_1547106343558.jpg') {
                        iconUrl = './static/image/merchant-logo/zhenzhengbingye.png';
                    } else if (data.data[j].merchantLogo === 'https://cdzg-app20.oss-cn-beijing.aliyuncs.com/pro/admin-http-api/83/customerHead/productImage/customerHead/productImage_1547110755138.jpg') {
                        iconUrl = './static/image/merchant-logo/zhongshuge.png';
                    } else if (data.data[j].merchantLogo === 'https://cdzg-app20.oss-cn-beijing.aliyuncs.com/pro/admin-http-api/83/customerHead/productImage/customerHead/productImage_1547111373582.jpg') {
                        iconUrl = './static/image/merchant-logo/haolilai.png';
                    } else if (data.data[j].merchantLogo === 'https://cdzg-app20.oss-cn-beijing.aliyuncs.com/pro/admin-http-api/83/customerHead/productImage/customerHead/productImage_1547185233588.jpg') {
                        iconUrl = './static/image/merchant-logo/qinyuan.png';
                    } else if (data.data[j].merchantLogo === 'https://cdzg-app20.oss-cn-beijing.aliyuncs.com/pro/admin-http-api/83/customerHead/productImage/customerHead/productImage_1547455752543.jpg') {
                        iconUrl = './static/image/merchant-logo/jingdian.png';
                    } else if (data.data[j].merchantLogo === 'https://cdzg-app20.oss-cn-beijing.aliyuncs.com/pro/merchant-http-api/5c4017efe39c07000c77e392/productImage/productImage_1547704303170.png') {
                        iconUrl = './static/image/merchant-logo/meipuli.png';
                    } else if (data.data[j].merchantLogo === 'https://cdzg-app20.oss-cn-beijing.aliyuncs.com/pro/merchant-http-api/5c986e37002413000c7697ef/productImage/productImage_1553493559531.png') {
                        iconUrl = './static/image/merchant-logo/darunfa.png';
                    } else if (data.data[j].merchantLogo === 'https://cdzg-app20.oss-cn-beijing.aliyuncs.com/pro/admin-http-api/83/customerHead/productImage/customerHead/productImage_1553498856691.jpg') {
                        iconUrl = './static/image/merchant-logo/bencaotang.png';
                    }

                    iconObj = new AMap.Icon({
                        // 图标尺寸
                        size: new AMap.Size(40, 40),
                        // 图标的取图地址
                        image: iconUrl,
                        // 图标所用图片大小
                        imageSize: new AMap.Size(40, 40),
                        // 图标取图偏移量
                        // imageOffset: new AMap.Pixel(-60, -60)
                    });

                    markerArr[j] = new AMap.Marker({
                        position: [data.data[j].longitude, data.data[j].latitude],
                        offset: new AMap.Pixel(-20, -20),
                        icon: iconObj, // 添加 Icon 图标 URL
                        title: data.data[j].shopName
                    });
                    expendMapSecond.add(markerArr[j]);
                }

            } else {

            }
        }, error: function (err) {
            console.log("数据请求错误", err, new Date());
        }
    });
}

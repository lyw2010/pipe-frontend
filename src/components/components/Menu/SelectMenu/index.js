/**
  * @description: 下拉菜单
  * @author: hj
  * @update: hj(2020-01-14)
  */

import React, {Component} from 'react';
// import {Table} from '@alifd/next';
import {Link} from "react-router-dom";
import styles from './index.module.scss';
import {getMediaById} from '@/service/Api/Chart';
import { observer,inject,Provider } from 'mobx-react';



 
@inject('previewStore')
@observer
class SelectMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            params: [],
            data: [],
            forceFit: true, //forceFit自动试用宽度，默认false
            width: this.props.data.width ? this.props.data.width : '100%',   // 固定宽度    当自适应宽度开启，此功能失效
            height: this.props.data.height ? this.props.data.height: '100%',
            condition: this.props.data.condition ? this.props.data.condition : '',
            pageFlag: this.props.data.pageFlag ? this.props.data.pageFlag : '',
            nofirst: this.props.data.nofirst ? this.props.data.nofirst : '',
            color: this.props.data.color,
            title: '',
            chartId: this.props.data.chartId,
            id: this.props.data.componentId,

            // 菜单数据
            menuData: [], // 菜单列表
            pageId: '', // 当前页面id
            pageName: '', // 当前页面名称
            visibleFlag: false, // 组件显隐
            application: '', // 应用id
        };
        this.menuChange = this.menuChange.bind(this);
    };
    


    componentWillMount() { //初始化
        const _this = this;

        // 获取应用id
        let url = window.location.href;
        if (url.indexOf('appid') > -1) {
            _this.setState({
                visibleFlag: true
            })

            // 请求菜单列表
            const { getViewMenus } = _this.props.previewStore;

            // const userVo =  JSON.parse(sessionStorage.getItem("userInfoVO"));
            // let application = userVo.dashboardAppIds.length > 0 ? userVo.dashboardAppIds[0] : '';
            let application = url.split('appid=')[1];
            
            getViewMenus(application).then(() => {
                const { releaseData } = _this.props.previewStore;
                if (releaseData.slice().length > 0) {
                    // 获取面板id
                    let url = window.location.href;
                    // 判断页面是预览还是发布了的大屏
                    if (url.indexOf('did') > -1) { // 预览
                        releaseData.slice().map((page,index) => {
                            if (page.id == url.split('=')[1].split('&')[0]) {
                                _this.setState({
                                    pageId: page.id,
                                    pageName: page.name
                                })
                            }
                        })
                    }
                    
                    _this.setState({
                        menuData: releaseData.slice(),
                        application: application
                    })
                }
            });
        }

        
    }

    // 钩子函数--组件挂载完成后
    componentDidMount() { //初始化
        const _this = this;

        document.body.onclick = function(e) {
            if (e.target.className.indexOf('SelectMenu--select') == -1 && _this.state.visible) {
                _this.setState({
                    visible: false
                })
            }
        };
    }
    // 接收父组件的数据改变
    componentWillReceiveProps = (nextProps) => {
        const _this = this;
        
    }
    // 抽屉菜单点击
    menuChange = (item) => {
        let newVisible = !this.state.visible;
        this.setState({
            visible: newVisible
        })
    }
    
    



    render() {
        const _this = this;
        let Layer = this.props.layer;
        let { menuData } = this.state;
        let ulH = menuData && menuData.length * 40 + 20 + 'px';

        
        // 监听点击选择框外的其他地方
        document.body.onclick = function(e) {
            if (e.target.className.indexOf('SelectMenu--select') == -1 && _this.state.visible) {
                _this.setState({
                    visible: false
                })
            }
        };

        return (
            
            <div id={this.state.id+'_'+this.state.chartId} style={{zIndex: 9999,width: this.state.width,height: this.state.height,display: this.state.visibleFlag ? 'block' : 'none'}}>
                <Layer width={this.state.width}></Layer>
                <div className={styles.menu} id={'chart_'+this.state.id+'_'+this.state.chartId}>
                    <div className={styles.choice}>
                        <div className={styles.select} onClick={this.menuChange}> {this.state.pageName} </div>
                        <div className={styles.selectDailog} style={{display: this.state.visible ? 'block' : 'none'}}>
                            <div style={{height: ulH}}>
                                <img src={require('./images/selectDailog-top.png')} alt="" style={{top: 0,width: '100%',height: '30%'}} />
                                <img src={require('./images/selectDailog-center.png')} alt="" style={{top: '30%',width: '100%',height: '50%'}} />
                                <img src={require('./images/selectDailog-bottom.png')} alt="" style={{bottom: 0,width: '100%',height: '20%'}} />
                                <ul>
                                    {menuData && menuData.length > 0 && menuData.map((item,index) => {
                                        return (
                                            <li className={this.state.pageId == item.id ? styles.liActive : ''}>
                                                <Link href={'#/preview?did='+item.id} to={'/preview?did='+item.id+'&appid='+this.state.application} target="_top"> {item.name} </Link>
                                            </li>
                                        )
                                    })}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }


}

export default SelectMenu;


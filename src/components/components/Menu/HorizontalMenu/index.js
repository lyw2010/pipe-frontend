/**
  * @description: 横向菜单
  * @author: hj
  * @update: hj(2020-01-14)
  */

import React, {Component} from 'react';
// import {Table} from '@alifd/next';
import {Link} from "react-router-dom";
import styles from './index.module.scss';
import {getMediaById} from '@/service/Api/Chart';
import { observer,inject,Provider } from 'mobx-react';

let tableTimer;

 
@inject('previewStore')
@observer
class HorizontalMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            params: [],
            data: [],
            forceFit: true, //forceFit自动试用宽度，默认false
            width: this.props.data.width ? this.props.data.width : '100%',   // 固定宽度    当自适应宽度开启，此功能失效
            height: this.props.data.height ? this.props.data.height: '100%',
            condition: this.props.data.condition ? this.props.data.condition : '',
            pageFlag: this.props.data.pageFlag ? this.props.data.pageFlag : '',
            nofirst: this.props.data.nofirst ? this.props.data.nofirst : '',
            color: this.props.data.color,
            title: '',
            chartId: this.props.data.chartId,
            id: this.props.data.componentId,

            // 菜单数据
            menuData: [], // 菜单列表
            pageId: '', // 当前页面id
            visibleFlag: false, // 组件显隐
            application: '', // 应用id
        };
    };
    


    componentWillMount() { //初始化
        const _this = this;

        // 获取应用id
        let url = window.location.href;
        if (url.indexOf('appid') > -1) {
            _this.setState({
                visibleFlag: true
            })

            // 请求菜单列表
        
            const { getViewMenus } = _this.props.previewStore;

            // const userVo =  JSON.parse(sessionStorage.getItem("userInfoVO"));
            // let application = userVo.dashboardAppIds.length > 0 ? userVo.dashboardAppIds[0] : '';
            let application = url.split('appid=')[1];
            //
            getViewMenus(application).then(() => {
                const { releaseData } = _this.props.previewStore;
                if (releaseData.length > 0) {
                    // 获取面板id
                    let url = window.location.href;
                    // 判断页面是预览还是发布了的大屏
                    if (url.indexOf('did') > -1) { // 预览
                        _this.setState({
                            pageId: url.split('=')[1].split('&')[0]
                        })
                    }
                    
                    _this.setState({
                        menuData: releaseData.slice(),
                        application: application
                    })
                }
            });
        }
    }

    // 钩子函数--组件挂载完成后
    componentDidMount() { //初始化
        
    }
    // 接收父组件的数据改变
    componentWillReceiveProps = (nextProps) => {
        const _this = this;
        
    }
    
    



    render() {
        const _this = this;
        let Layer = this.props.layer;
        
        let w = this.state.menuData.length > 0 ? (100 / this.state.menuData.length).toFixed(2) : 100;

        return (
            
            <div id={this.state.id+'_'+this.state.chartId} style={{zIndex: 9999,width: this.state.width,height: this.state.height,display: this.state.visibleFlag ? 'block' : 'none'}}>
                <Layer width={this.state.width}></Layer>
                <div className={styles.menu} id={'chart_'+this.state.id+'_'+this.state.chartId}>
                    <div>
                        {this.state.menuData.length > 0 && this.state.menuData.map((item,index) => {
                            let left = w * index;
                            return (
                                <div style={{
                                            width: w + '%',
                                            left: index == 0 ? 0 : 'calc('+ left +'% - '+ 15 * index +'px)',
                                            background: this.state.pageId == item.id ? 'url('+require("./images/bg_active.png")+') no-repeat' : 'url('+require("./images/bg.png")+') no-repeat',
                                            backgroundSize: '100% 100%'
                                    }}
                                > 
                                    <Link href={'#/preview?did='+item.id} to={'/preview?did='+item.id+'&appid='+this.state.application} target="_top"> {item.name} </Link>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
        );
    }


}

export default HorizontalMenu;


/**
  * @description: g2--空心饼图
  * @author: hj
  * @update: hj(2020-01-14)
  */

import React, { Component } from 'react';
import G2 from '@antv/g2';
import DataSet from '@antv/data-set';
import { getMediaById } from '@/service/Api/Chart';
import { observer,inject,Provider } from 'mobx-react';

 
@inject('chartStore') 
@observer
class PieHollowChart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            params: [],
            data: [],
            forceFit: true, //forceFit自动试用宽度，默认false
            width: this.props.data.width ? this.props.data.width : '100%',   // 固定宽度    当自适应宽度开启，此功能失效
            height: this.props.data.height ? this.props.data.height - 10 : '100%',
            condition: this.props.data.condition ? this.props.data.condition : '',
            pageFlag: this.props.data.pageFlag ? this.props.data.pageFlag : '',
            nofirst: this.props.data.nofirst ? this.props.data.nofirst : '',
            color: [],
            title: '',
            chartId: this.props.data.chartId,
            id: this.props.data.componentId,
            chartData: [],
            axisArr: {},
            haveData: false,
            legend: 'top',
            padding: '',

            dataNull: false, // 暂无数据

            // 全局变量
            mainData: [], // 操作组件可操作数据
            mainName: '', // 操作组件可操作当前维度
            mainFlag: false, // 是否是操作组件

            pageInitFlag: true,
            affectName: '',
            requireCondition: '',
        };
    };


    componentWillMount() { //初始化
        const _this = this;
        
        // 预览界面
        if (_this.state.pageFlag == 'preview') {
            // 判断当前组件是否是可操作组件
            const { globalData } = _this.props.chartStore;
            if (globalData.slice().length > 0) {
                globalData.slice().map((item,index) => {
                    if (item.affect == _this.state.chartId) { // 可操作组件
                        let affectName = item.affectDimension;
                        _this.setState({
                            mainFlag: true,
                            mainName: affectName
                        })
                    }
                })
            }

            _this.interfaceRequest(_this.state.chartId,_this.state.condition);
        } else { // 配置面板
            if (_this.state.nofirst == 'nofirst') {
                _this.saveHandleData();
            } else {
                _this.interfaceRequest(_this.state.chartId,_this.state.condition);
            }
        }
    }

    // 钩子函数--组件挂载完成后
    componentDidMount() { //初始化
        this.setState({
            haveData: true
        })
    }
    // 接收父组件的数据改变
    componentWillReceiveProps = (nextProps) => {
        const _this = this;
        const { globalData, affectId } = _this.props.chartStore;

        // 预览界面---判断是否是变化组件
        if (nextProps.data.pageFlag == 'preview') {
            if (nextProps.data.condition != _this.state.requireCondition && globalData.slice().length > 0) {
                globalData.slice().map((item,index) => {
                    if (item.affect == affectId && item.require == nextProps.data.chartId) { // 联动组件
                        _this.setState({
                            pageInitFlag: false
                        })
                        let condition = {};
                        Object.keys(_this.state.requireCondition).forEach(function(keys){
                            condition[keys] = keys == item.requireDimension ? nextProps.data.condition[keys] : _this.state.requireCondition[keys];
                        })
                        _this.interfaceRequest(nextProps.data.chartId,condition);
                    }
                })
            }
        }
    }

    // 数据接口请求
    interfaceRequest = (chartId,condition) => {
        this.setState({
            requireCondition: condition,
        });

        let data = {
            charId: chartId,
            condition: condition
        };
        this.getMediaById(data);
    }

    // 数据存储刷新
    saveHandleData = () => {
        const { chartArrData } = this.props.chartStore;
        if (chartArrData.slice().length > 0) {
            chartArrData.slice().map((item) => {
                if (item.id == this.state.chartId) {
                    this.setState({
                        haveData: false
                    },function() {
                        // 数据处理
                        this.handleData(item.data);
                    })
                }
            })
        }
    }


    async getMediaById(data) {
        const res = await getMediaById(data);
        if (res.data && res.data.code === 0) {
            let str = res.data;
            if (str.data && str.data.data && str.data.data.length > 0) {
                // 存储图例原始数据
                const { saveChartData } = this.props.chartStore;
                let oldData = {
                    id: this.state.chartId,
                    data: str.data.data
                }
                saveChartData(oldData);

                // 数据处理
                this.handleData(str.data.data);
            } else {
                this.setState({
                    dataNull: true
                })
            }
        } else {
            this.setState({
                dataNull: true
            })
        }
    }
    // 数据处理
    handleData = (storeAllData) => {
        let storeData = storeAllData[0];
        this.setState({
            dataNull: true
        })

        // 非预览界面
        if (this.state.pageFlag != 'preview') {
            // 存储可操作组件数据
            if (storeData.affect && storeData.affect.length > 0) {
                let obj = {
                    id: this.state.chartId,
                    label: storeData.title,
                    dimensionData: []
                }
                const { saveAffectData } = this.props.chartStore;

                let dimensionData = [];
                storeData.affect.slice().map((item,index) => {
                    dimensionData.push(item);
                })
                obj.dimensionData = dimensionData;
                saveAffectData(obj);
            }
            // 存储联动组件数据
            if (storeData.requires && storeData.requires.length > 0) {
                let obj = {
                    id: this.state.chartId,
                    label: storeData.title,
                    dimensionData: []
                }
                const { saveRequireData } = this.props.chartStore;

                let dimensionData = [];
                storeData.requires.slice().map((item,index) => {
                    dimensionData.push(item);
                })
                obj.dimensionData = dimensionData;
                saveRequireData(obj);
            }
        }

        // 页面初始化
        if (this.state.pageInitFlag) {
            if (storeData.affect &&  storeData.affect.length > 0) {
                this.setState({
                    affectName: storeData.affect[0]
                });
            }
            // 获取选择项
            if (storeData.conditionValues) {
                let selectData = storeData.conditionValues[storeData.affect[0]] ? storeData.conditionValues[storeData.affect[0]] : [];
                this.setState({
                    mainData: selectData
                });
            }
        }


        // 组装当前图例的维度和度量
        let xyArr = [];
        Object.keys(storeData.columnMaps).forEach(function(key){
            xyArr.push(key)
        });

        if (storeData.data.slice().length > 0) {
            this.setState({
                dataNull: false
            })
        }

        this.setState({
            title: storeData.title,
            chartData: storeData.data.slice(),
            axisArr: xyArr,
            field: storeData.field,
            dimension: storeData.dimension,
            color: storeData.colors.split(","),
            haveData: true,
            legend: storeData.legend ? storeData.legend : 'right-center',
            padding: storeData.padding && storeData.padding != '' ? storeData.padding.split(',') : [0,100,0,0], // 默认legend在top位置,
        })
    }

    // 改变全局变量
    onGlobalVariable = (val) => {
        const { saveAffectId } = this.props.chartStore;
        saveAffectId(this.state.chartId);
        this.props.onGlobalVariable(val);
    }
    


    render() {
        const _this = this;
        let Layer = this.props.layer;

        let chartData = this.state.chartData;
        let axisArr = this.state.axisArr;
        let haveData = this.state.haveData;
        let x = this.state.field;
        let y = this.state.dimension;

        if (haveData) {
            document.getElementById('chart_'+this.state.id+'_'+this.state.chartId).innerHTML = '';
        }
        if (haveData && chartData.length > 0) {
            document.getElementById('chart_'+this.state.id+'_'+this.state.chartId).innerHTML = '';
            _this.setState({
                haveData: false
            })

            let data = chartData;
            console.log('data======',data);

            let sum = 0;
            data.map(item=>{
                sum+=item.car_count
            })
            data.map((item,index)=>{
                data[index].percent=item.car_count/sum
            })
            let ds = new DataSet();
            let dv = ds.createView().source(data);
            console.log('dv======',dv);
            dv.transform({
                type: 'map',
                callback: function callback(row) {
                    row.value = parseInt(sum * row.percent);
                    return row;
                    }
            });
            console.log('dv======',dv);
            let chart = new G2.Chart({
                container: 'chart_'+this.state.id+'_'+this.state.chartId,
                forceFit: true,
                height: _this.state.height - 50,
                padding: _this.state.padding,
            });
            chart.source(dv);
            chart.tooltip(false);
            chart.legend({
                position: this.state.legend,
                // offsetX: -50,
                // offsetY: -60,
                marker: 'square',
                // itemWidth: 150,
                // itemGap: 20, // 表示图例每项之间的间距，如果是水平排布则为左右间距，如果是竖直排布则为上下间距。
                textStyle: {
                    fill: '#fff', // 文本的颜色
                    fontSize: '12', // 文本大小
                },
            });
            chart.coord('theta', {
                radius: 0.85,
                innerRadius: 0.7,
            });
            chart.scale('percent', {
                formatter: (val) => {
                  val = val * 100 + '%';
                  return val;
                },
              });
            chart.tooltip({
                triggerOn: 'click',
                title: 'time_range',
                useHtml:true,
                htmlContent:function(title,items){
                    let itemContnt = '';
                    items.map((v,i) => {
                        itemContnt += '<li style="display: block;" style="padding-left: 5px;">'
                                        + '<span style="background-color:'+  v.color +';width: 6px;height: 6px;display: inline-block;border-radius: 3px;margin-right: 3px;" class="g2-tooltip-marker"></span>'
                                        + '<span style="color: #BDE9FF;">'+  v.name +'： </span>'
                                        + '<span style="color: '+  v.color +';">'+  v.value +'</span>'
                                + '</li>';
                    })
                    return '<div class="g2-tooltip" style="padding: 5px 15px 15px 15px;position: absolute;left: '+ items[0].x +';top:'+  items[0].y +';">'
                            + '<div class="g2-tooltip-title" style="margin-bottom: 4px;color: #6AEBFC;margin-top: 7px;">'+ title +'</div>'
                            + '<ul class="g2-tooltip-list">'
                                + itemContnt
                            + '</ul>'
                        + '</div>';
                }
            });

            // chart.intervalStack().position(y1).color(x,this.state.color).opacity(1).label(y1, {
            //     offset: -18,
            //     textStyle: {
            //         fill: 'white',
            //         fontSize: 12,
            //         shadowBlur: 2,
            //         shadowColor: 'rgba(0, 0, 0, .45)'
            //     },
            //     rotate: 0,
            //     autoRotate: false,
            //     formatter: function formatter(text, item) {
            //     //return String(parseInt(item.point.percent * 100)) + '%';
            //     }
            // });
            // chart.render();
            let interval = chart.intervalStack().position(y).color(x,this.state.color)
            .label(y,(xValue,item) => {
                return {
                  content:`${xValue}辆(${Math.round(100*xValue/sum)}%)`,
                //   + Math.round(100*xValue/sum) + '%',
                };
              },
              {
                offset: 15,
                style: {
                  fontSize: 12,
                  fontWeight: 'bold',
                  fill: '#fff',
                },
              }
              )
            .tooltip(x+'*'+y, function(item, percent) {
                return {
                    name: item,
                    value: percent
                };
            });



            // tooltip自动轮播
            // setTimeout(() => {
            //     let num = 0;
            //     //去掉定时器的方法 
            //     var intervalDodgeTimer = intervalDodgeTimer + this.state.id+'_'+this.state.chartId;   
            //     if (intervalDodgeTimer) window.clearInterval(intervalDodgeTimer);

            //     intervalDodgeTimer = window.setInterval(function() {
            //         if (num == data.length) {
            //             num = 0;
            //         }
                
            //         let point = chart.getXY(data[num]);
            //         console.log("data[num].company_name",chart,point,data[num],data[num].rate)
            //         chart.showTooltip(point);
            //         num++;
            //     },2000)
            // }, 10);

            // 辅助文本
            chart.guide().html({
                position: ['50%', '50%'],
                html: '<div id="g2-guide-html'+_this.state.chartId+'" style="width: 10em;height: 3em;text-align: center;line-height: 0.2;">'+
                        '<p id="pie-center-title'+_this.state.chartId+'" style="line-height: 25px;font-size: 12px;color: #26D3FA;font-weight: 300;margin: 0;">'+ data[0][x]+'</p>'+
                        '<p id="pie-center-value'+_this.state.chartId+'" style="font-size: 24px;color: #97FAFF;font-weight: bold;;margin: 0;font-family: myNumberFamily;">'+ data[0][y]+'</p>'+
                        '</div>', 
            });
            
            chart.on('interval:click', function(ev) {
                let data = ev.data._origin;
                document.getElementById('g2-guide-html'+_this.state.chartId).style.opacity = 1;
                document.getElementById('pie-center-title'+_this.state.chartId).innerText = data[x];
                document.getElementById('pie-center-value'+_this.state.chartId).innerText = data[y];
            });
            chart.render();
            interval.setSelected(dv.origin[0]);

        }

        return (
            <div id={this.state.id+'_'+this.state.chartId} style={{zIndex: 99,width: this.state.width,height: this.state.height,padding: '50px 2% 10px 2%'}}>
                <Layer width={this.state.width} title={this.state.title} chartId={this.state.chartId} choiceData={this.state.mainData} onGlobalVariable={this.onGlobalVariable}></Layer>
                <div id={'chart_'+this.state.id+'_'+this.state.chartId}></div>
                { this.state.dataNull && <p style={{height: this.state.height,lineHeight: this.state.height+'px',color: '#fff',textAlign: 'center'}}> 暂无数据 </p>}
            </div>
        )
    }


}
export default PieHollowChart;


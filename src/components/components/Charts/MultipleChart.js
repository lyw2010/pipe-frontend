/**
  * @description: g2--多图例组件
  * @author: hj
  * @update: hj(2020-01-14)
  */

import React, {Component} from 'react';
import G2 from '@antv/g2';
import DataSet from '@antv/data-set';
import {getMediaById} from '@/service/Api/Chart';
import { observer,inject,Provider } from 'mobx-react';

let lineBottomTimer;

 
@inject('chartStore') 
@observer


class MultipleChart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            params: [],
            data: [],
            forceFit: true, //forceFit自动试用宽度，默认false
            width: this.props.data.width ? this.props.data.width : '100%',   // 固定宽度    当自适应宽度开启，此功能失效
            height: this.props.data.height ? this.props.data.height: '100%',
            condition: this.props.data.condition ? this.props.data.condition : '',
            pageFlag: this.props.data.pageFlag ? this.props.data.pageFlag : '',
            nofirst: this.props.data.nofirst ? this.props.data.nofirst : '',
            color: [],
            fields: [],
            xAxis: "",
            title: [],
            chartId: this.props.data.chartId,
            id: this.props.data.componentId,
            chartData: [],
            axisArr: {},
            haveData: false,
            legend: 'top',

            dataNull: [], // 暂无数据

            // 全局变量
            mainData: [], // 操作组件可操作数据
            mainName: '', // 操作组件可操作当前维度
            mainFlag: false, // 是否是操作组件

            pageInitFlag: true, // 页面初始化
            requireName: '', // 请求参数名
            requireVal: '', // 当前选中参数值
            requireCondition: '', // 请求参数值
        };
    };


    componentWillMount() { //初始化
        const _this = this;
        
        // 预览界面
        if (_this.state.pageFlag == 'preview') {
            // 判断当前组件是否是可操作组件
            const { globalData } = _this.props.chartStore;
            if (globalData.slice().length > 0) {
                globalData.slice().map((item,index) => {
                    if (item.affect == _this.state.chartId) { // 可操作组件
                        let affectName = item.affectDimension;
                        _this.setState({
                            mainFlag: true,
                            mainName: affectName
                        })
                    }
                })
            }

            _this.interfaceRequest(_this.state.chartId,_this.state.condition);
        } else { // 配置面板
            if (_this.state.nofirst == 'nofirst') {
                _this.saveHandleData();
            } else {
                _this.interfaceRequest(_this.state.chartId,_this.state.condition);
            }
        }
    }

    // 钩子函数--组件挂载完成后
    componentDidMount() { //初始化
        this.setState({
            haveData: true
        })
    }
    // 接收父组件的数据改变
    componentWillReceiveProps = (nextProps) => {
        const _this = this;
        _this.setState({
            pageInitFlag: false
        })
        const { globalData, affectId } = _this.props.chartStore;

        // 预览界面---判断是否是变化组件
        if (nextProps.data.pageFlag == 'preview') {
            if (nextProps.data.condition != _this.state.requireCondition && globalData.slice().length > 0) {
                globalData.slice().map((item,index) => {
                    if (item.affect == affectId && item.require == nextProps.data.chartId) { // 联动组件
                        let condition = {};
                        Object.keys(_this.state.requireCondition).forEach(function(keys){
                            condition[keys] = keys == item.requireDimension ? nextProps.data.condition[keys] : _this.state.requireCondition[keys];
                        })
                        _this.interfaceRequest(nextProps.data.chartId,condition);
                    }
                })
            }
        }
    }

    // 数据接口请求
    interfaceRequest = (chartId,condition) => {
        this.setState({
            requireCondition: condition,
        });

        let data = {
            charId: chartId,
            condition: condition
        };
        this.getMediaById(data);
    }
    
    // 数据存储刷新
    saveHandleData = () => {
        const { chartArrData } = this.props.chartStore;
        if (chartArrData.slice().length > 0) {
            chartArrData.slice().map((item) => {
                if (item.id == this.state.chartId) {
                    this.setState({
                        haveData: false
                    },function() {
                        // 数据处理
                        this.handleData(item.data);
                    })
                }
            })
        }
    }

    async getMediaById(data) {
        const res = await getMediaById(data);
        if (res.data && res.data.code === 0) {
            let str = res.data;
            if (str.data && str.data.data && str.data.data.length > 0) {
                // 存储图例原始数据
                const { saveChartData } = this.props.chartStore;
                let oldData = {
                    id: this.state.chartId,
                    data: str.data.data
                }
                saveChartData(oldData);

                // 页面初始化
                if (this.state.pageInitFlag) {
                    if (str.data.requires &&  str.data.requires.length > 0) {
                        this.setState({
                            requireName: str.data.requires[0]
                        });
                    }
                    // 获取选择项
                    if (str.data.conditionValues) {
                        let selectData = [];
                        Object.keys(str.data.conditionValues).forEach(function(key){
                            selectData = str.data.conditionValues[key];
                        })
                        this.setState({
                            mainData: selectData
                        });
                    }
                }
                // 数据处理
                this.handleData(str.data.data);
            }
        }
    }
    // 数据处理
    handleData = (storeAllData) => {
        let newChartData = [];
        let newTitleData = [];
        let newDataNull = [];

        if (storeAllData.length > 0) {
            storeAllData.map((item,index) => {
                let storeData = item;
                
                newChartData.push(item);
                newTitleData.push(item.title);
                newDataNull.push(false)
            })

            this.setState({
                chartData: newChartData,
                title: newTitleData,
                dataNull: newDataNull,
                haveData: true,
            })
        }
    }

    // 渲染多个仪表盘
    renderGauge = (data,index,color) => {
        // 极坐标下的柱状图
        // 构造数据
        let data1 = [];
        for (let i = 0; i < 100; i++) {
            if (i % 2 == 0) {
                let item = {};
                item.type = i + '';
                item.value = 10;
                if (i === 50) {
                    item.value = 15;
                }
                data1.push(item);
            }
        }
        // console.log('data1',data1);
        

        let data2 = [];
        for (let _i = 0; _i < 100; _i++) {
            if (_i % 2 == 0) {
                let _item = {};
                _item.type = _i + '';
                _item.value = 10;
                if (_i === 50) {
                    _item.value = 15;
                }
                if (_i > data.data) {
                    _item.value = 0;
                }
                data2.push(_item);
            }
        }
        // console.log('data2',data2);

        let chart = new G2.Chart({
            container: 'gauge_'+index,
            forceFit: true,
            height: document.getElementById('gauge_'+index).offsetHeight,
            padding: 0
        });
        chart.scale({
            type: {
                range: [0, 1]
            },
            value: {
                sync: true
            }
        });
        chart.legend(false);
        chart.tooltip(false);
        let view1 = chart.view();
        view1.source(data1);
        view1.axis(false);
        view1.coord('polar', {
            startAngle: -9 / 8 * Math.PI,
            endAngle: 1 / 8 * Math.PI,
            innerRadius: 0.68,
            radius: 0.73
        });
        view1.interval().position('type*value').color('#fff').size(2);

        let view2 = chart.view();
        view2.source(data1, {
            type: {
                tickCount: 3
            }
        });
        view2.axis('value', false);
        view2.axis('type', {
            grid: null,
            line: null,
            tickLine: null,
            label: {
                offset: -7,
                offsetY: 7,
                textStyle: {
                    textAlign: 'center',
                    fill: '#fff',
                    fontSize: 12
                },
                formatter: function formatter(val) {
                    if (val === '99' || val === '98') {
                    return 100;
                    }

                    return val;
                }
            }
        });
        view2.coord('polar', {
            startAngle: -9 / 8 * Math.PI,
            endAngle: 1 / 8 * Math.PI,
            innerRadius: 0.85,
            radius: 0.48
        });
        view2.interval().position('type*value').color('#fff').size(2);

        let view3 = chart.view();
        view3.source(data2);
        view3.axis(false);
        view3.coord('polar', {
            startAngle: -9 / 8 * Math.PI,
            endAngle: 1 / 8 * Math.PI,
            innerRadius: 0.68,
            radius: 0.73
        });
        view3.interval().position('type*value').color('value', color).opacity(1).size(2);
        view3.guide().text({
            position: ['50%', '65%'],
            content: data.data+'%',
            style: {
            fill: '#8AE5EB',
            fontSize: 20,
            textAlign: 'center',
            textBaseline: 'middle'
            }
        });

        chart.render();
    }
    // 渲染单折线图例
    renderLine = (data,axisArr,color,legend,padding,index) => {
        const _this = this;

        let x = axisArr[0].key;

        let newData = data.map((item) => {
            let obj = {};
            axisArr.map((v) => {
                if (v.key != x) {
                    obj[v.label] = item[v.key];
                } else {
                    obj[x] = item[x];
                }
            })
            return obj;
        });

        axisArr.splice(0,1);
        valueArr = axisArr.map((item) => {
            return item.label;
        });



        let dv = new DataSet.View().source(newData);
        dv.transform({
            type: 'fold',
            fields: valueArr,
            key: 'type',
            value: 'value'
        });
        let chart = new G2.Chart({
            container: 'chart_' + this.state.id + '_' + this.state.chartId+'_Bottom_'+index,
            forceFit: true,
            height: this.state.height * 0.5 - 50,
            padding: padding &&  padding != '' ? padding.split(',') : [40, 40, 40, 50], // 默认legend在top位置,
            // animate: false
        });
        chart.source(dv, {
            month: {
                range: [0, 1]
            }
        });
        chart.axis(x, {
            position: 'bottom', // 设置坐标轴的显示位置，可取值 top bottom left right
            line: {
                lineWidth: 1, // 设置线的宽度
                stroke: '#536f8e' //  设置线的颜色
            }, // 设置坐标轴线的样式，如果值为 null，则不显示坐标轴线 图形属性
            label: {
                // offset: 10, // 数值，设置坐标轴文本 label 距离坐标轴线的距离
                // offsetX: 0, // 在 offset 的基础上 x 方向的偏移量
                // offsetY: 0, // 在 offset 的基础上 y 方向的偏移量
                // // 设置文本的显示样式，还可以是个回调函数，回调函数的参数为该坐标轴对应字段的数值
                // rotate: -30, // 注意，旋转角度的配置不再在 textStyle 里配置
                textStyle: {
                    textAlign: 'center', // 文本对齐方向，可取值为： start center end
                    fill: '#24C6F5', // 文本的颜色
                    fontSize: '12', // 文本大小
                    // textBaseline: 'top' // 文本基准线，可取 top middle bottom，默认为middle
                }
            },
            title: null, // 坐标轴标题设置，如果值为 null，则不显示标题
            tickLine: {
                lineWidth: 2, // 刻度线宽
                stroke: '#536f8e', // 刻度线的颜色
                length: -5, // 刻度线的长度
            }, // 坐标轴刻度线配置
        });
        let prevNum = 0;
        chart.axis('value', {
            position: 'left', // 设置坐标轴的显示位置，可取值 top bottom left right
            line: {
                lineWidth: 1, // 设置线的宽度
                stroke: '#536f8e' //  设置线的颜色
            }, // 设置坐标轴线的样式，如果值为 null，则不显示坐标轴线 图形属性
            label: {
                offset: 0, // 数值，设置坐标轴文本 label 距离坐标轴线的距离
                offsetX: -10, // 在 offset 的基础上 x 方向的偏移量
                offsetY: 0, // 在 offset 的基础上 y 方向的偏移量
                // 设置文本的显示样式，还可以是个回调函数，回调函数的参数为该坐标轴对应字段的数值
                textStyle: {
                    textAlign: 'end', // 文本对齐方向，可取值为： start center end
                    fill: '#24C6F5', // 文本的颜色
                    fontSize: '12', // 文本大小
                },
                // 使用 formatter 回调函数
                formatter: val => {
                    prevNum++;
                    if (prevNum % 2 != 0) {
                        return val;
                    }
                }
            },
            title: null, // 坐标轴标题设置，如果值为 null，则不显示标题
            tickLine: null,
            grid: {
                align: 'center', // 声明网格顶点从两个刻度中间开始，默认从刻度点开始
                type: 'line', // 声明网格的类型，line 表示线，polygon 表示矩形框
                // 当网格类型 type 为 line 时，使用 lineStyle 设置样式
                lineStyle: {
                    stroke: '#21355f', // 网格线的颜色
                    lineWidth: 1, // 网格线的粗细
                    lineDash: [4, 4 ] // 网格线的虚线配置，第一个参数描述虚线的实部占多少像素，第二个参数描述虚线的虚部占多少像素
                },
                hideFirstLine: true, // 是否隐藏第一条网格线，默认为 false
                hideLastLine: true, // 是否隐藏最后一条网格线，默认为 false
            }, // 坐标轴栅格线的配置信息，默认只有左边的坐标轴带有栅格线，null 为不显示。
        });
        chart.legend({
            useHtml: true,
            clickable: false,
            itemGap: 15,
            position: legend ? legend : 'top',
            containerTpl: '<div class="g2-legend">' +
                            '<ul class="g2-legend-list" style="list-style-type:none;margin:0;padding:0;"></ul>' +
                        '</div>',
            itemTpl: (value, color1, checked, index) => {
                let content = '';
                let color = color[index] ? color[index] : color[0];
                checked = checked ? 'checked' : 'unChecked';
                content += '<li class="g2-legend-list-item item-' + index + ' ' + checked + '" '+
                            'data-value="' + value + '" '+
                            'data-color=' + color + '" '+
                            'style="cursor: pointer;font-size: 12px;float: right;">' +
                            '<span width=150 style="border: none;padding:0;">'+
                                '<span class="g2-legend-marker" style="background: ' + color + ';margin-top: -4px;"></span>' +
                                '<i class="g2-legend-marker2" style="width:10px;height:10px;display:inline-block;margin-right:5px;border-radius: 5px;border: 2px solid ' + color + ';"></i>'+
                            '</span>' +
                            '<span class="g2-legend-text" style="color: #24C6F5;">'+ value +'</span>'+
                        '</li>';
                return content;
            },
            offsetX: 15,
            offsetY: -5,
            'g2-legend': {
                marginLeft: '100px',
                marginTop: '0px'
            },
            'g2-legend-list': {
                border: 'none',
                float: 'right',
                marginRight: '20px',
            },
            'g2-legend-marker': {
                width: '22px',
                height: '2px',
                borderRadius: 0,
                marginRight: 0
            }
        });
        chart.tooltip({
            crosshairs: {
                type: 'line',
                style: {
                    // 图形样式
                    fill: '', // 填充的颜色
                    stroke: '#666', // 边框的颜色
                    strokeOpacity: 1, // 边框颜色的透明度，数值为 0 - 1 范围
                    fillOpacity: 1, // 填充的颜色透明度，数值为 0 - 1 范围
                    lineWidth: 1, // 边框的粗细
                    lineDash: 'solid' // 线的虚线样式
                }
            },
            triggerOn: 'click',
            useHtml:true,
            htmlContent:function(title,items){
                let itemContnt = '';
                items.map((v,i) => {
                    itemContnt += '<li style="display: block;" style="padding-left: 5px;">'
                                + '<span style="background-color:'+  v.color +';width: 6px;height: 6px;display: inline-block;border-radius: 3px;margin-right: 3px;" class="g2-tooltip-marker"></span>'
                                + '<span style="color: #BDE9FF;">'+  v.name +'： </span>'
                                + '<span style="color: '+  v.color +';">'+  v.value +'</span>'
                        + '</li>';
                })
                // console.log('itemContnt------',itemContnt);
                return '<div class="g2-tooltip" style="padding: 5px 15px 15px 15px;position: absolute;left: '+ items[0].x +';top:'+  items[0].y +';">'
                        + '<div class="g2-tooltip-title" style="margin-bottom: 4px;color: #6AEBFC;margin-top: 7px;">'+ title +'</div>'
                        + '<ul class="g2-tooltip-list">'
                            + itemContnt
                        + '</ul>'
                    + '</div>';
            }
        });

        chart.point().position(x+'*value').size(4).color('type',color).shape('circle');
        chart.line().position(x+'*value').color('type',color).size(2);

        chart.render();

        // tooltip自动轮播
        setTimeout(() => {
            let num = 0;
            //去掉定时器的方法  
            lineBottomTimer = lineBottomTimer + this.state.id+'_'+this.state.chartId+index;  
            if (lineBottomTimer) window.clearInterval(lineBottomTimer);

            lineBottomTimer = window.setInterval(function() {
                if (num == data.length) {
                    num = 0;
                }

                let point = chart.getXY({time: data[num][x], value: data[num][axisArr[0].label]});
                // debugger;
                chart.showTooltip(point);
                num++;
            },2000)
        }, 20);
    }
    // 渲染多折线图例
    renderDoubleLine = (data,axisArr,color,legend,padding,index) => {
        const _this = this;

        let x = axisArr[0].key;
        let y1 = axisArr[1].key;
        let y2 = axisArr.length >= 3 ? axisArr[2].key : '';


        let chart = new G2.Chart({
            container: 'chart_' + this.state.id + '_' + this.state.chartId+'_Bottom_'+index,
            forceFit: true,
            height: this.state.height * 0.5 - 50,
            padding: padding &&  padding != '' ? padding.split(',') : [40, 50, 40, 50], // 默认legend在top位置,
            // animate: false
        });
        chart.source(data, {
            month: {
                range: [0, 1]
            }
        });
        chart.axis('time', {
            position: 'bottom', // 设置坐标轴的显示位置，可取值 top bottom left right
            line: {
                lineWidth: 1, // 设置线的宽度
                stroke: '#536f8e' //  设置线的颜色
            }, // 设置坐标轴线的样式，如果值为 null，则不显示坐标轴线 图形属性
            label: {
                // offset: 10, // 数值，设置坐标轴文本 label 距离坐标轴线的距离
                // offsetX: 0, // 在 offset 的基础上 x 方向的偏移量
                // offsetY: 0, // 在 offset 的基础上 y 方向的偏移量
                // // 设置文本的显示样式，还可以是个回调函数，回调函数的参数为该坐标轴对应字段的数值
                // rotate: -30, // 注意，旋转角度的配置不再在 textStyle 里配置
                textStyle: {
                    textAlign: 'center', // 文本对齐方向，可取值为： start center end
                    fill: '#24C6F5', // 文本的颜色
                    fontSize: '12', // 文本大小
                    // textBaseline: 'top' // 文本基准线，可取 top middle bottom，默认为middle
                }
            },
            title: null, // 坐标轴标题设置，如果值为 null，则不显示标题
            tickLine: {
                lineWidth: 2, // 刻度线宽
                stroke: '#536f8e', // 刻度线的颜色
                length: -5, // 刻度线的长度
            }, // 坐标轴刻度线配置
        });
        let prevNum = 0;
        chart.axis(y1, {
            position: 'left', // 设置坐标轴的显示位置，可取值 top bottom left right
            line: {
                lineWidth: 1, // 设置线的宽度
                stroke: '#536f8e' //  设置线的颜色
            }, // 设置坐标轴线的样式，如果值为 null，则不显示坐标轴线 图形属性
            label: {
                offset: 0, // 数值，设置坐标轴文本 label 距离坐标轴线的距离
                offsetX: -10, // 在 offset 的基础上 x 方向的偏移量
                offsetY: 0, // 在 offset 的基础上 y 方向的偏移量
                // 设置文本的显示样式，还可以是个回调函数，回调函数的参数为该坐标轴对应字段的数值
                textStyle: {
                    textAlign: 'end', // 文本对齐方向，可取值为： start center end
                    fill: '#24C6F5', // 文本的颜色
                    fontSize: '12', // 文本大小
                },
                // 使用 formatter 回调函数
                formatter: val => {
                    prevNum++;
                    if (prevNum % 2 != 0) {
                        return val;
                    }
                }
            },

            title: null, // 坐标轴标题设置，如果值为 null，则不显示标题
            tickLine: null,
            grid: {
                align: 'center', // 声明网格顶点从两个刻度中间开始，默认从刻度点开始
                type: 'line', // 声明网格的类型，line 表示线，polygon 表示矩形框
                // 当网格类型 type 为 line 时，使用 lineStyle 设置样式
                lineStyle: {
                    stroke: '#21355f', // 网格线的颜色
                    lineWidth: 1, // 网格线的粗细
                    lineDash: [4, 4 ] // 网格线的虚线配置，第一个参数描述虚线的实部占多少像素，第二个参数描述虚线的虚部占多少像素
                },
                hideFirstLine: true, // 是否隐藏第一条网格线，默认为 false
                hideLastLine: true, // 是否隐藏最后一条网格线，默认为 false
            }, // 坐标轴栅格线的配置信息，默认只有左边的坐标轴带有栅格线，null 为不显示。
        });
        let y2PrevNum = 0;
        if (y2) {
            chart.axis(y2, {
                position: 'right', // 设置坐标轴的显示位置，可取值 top bottom left right
                label: {
                    offset: 0, // 数值，设置坐标轴文本 label 距离坐标轴线的距离
                    offsetX: 10, // 在 offset 的基础上 x 方向的偏移量
                    offsetY: 0, // 在 offset 的基础上 y 方向的偏移量
                    // 设置文本的显示样式，还可以是个回调函数，回调函数的参数为该坐标轴对应字段的数值
                    textStyle: {
                        textAlign: 'start', // 文本对齐方向，可取值为： start center end
                        fill: '#24C6F5', // 文本的颜色
                        fontSize: '12', // 文本大小
                    },
                    // formatter: function formatter(val) {
                    //     return val + '%';
                    // },
                    // 使用 formatter 回调函数
                    formatter: val => {
                        y2PrevNum++;
                        if (y2PrevNum % 2 != 0) {
                            return val + '%';
                        }
                    }
                },
                title: null, // 坐标轴标题设置，如果值为 null，则不显示标题
                tickLine: null,
                grid: null
            });
            chart.scale(y2,{
                alias: axisArr[2].label //设置y1别名
            });
        }
        chart.scale(y1,{
            alias: axisArr[1].label //设置y1别名
        });
        chart.legend({
            useHtml: true,
            clickable: false,
            itemGap: 15,
            position: legend ? legend : 'top',
            containerTpl: '<div class="g2-legend">' +
                            '<ul class="g2-legend-list" style="list-style-type:none;margin:0;padding:0;"></ul>' +
                        '</div>',
            itemTpl: (value, color, checked, index) => {
                let content = '';
                checked = checked ? 'checked' : 'unChecked';
                if (index % 2 == 0) {
                    content += '<li class="g2-legend-list-item item-' + index + ' ' + checked + '" '+
                                'data-value="' + value + '" '+
                                'data-color=' + color + '" '+
                                'style="cursor: pointer;font-size: 12px;float: right;">' +
                                '<span width=150 style="border: none;padding:0;">'+
                                    '<span class="g2-legend-marker" style="background: ' + color + ';margin-top: -4px;"></span>' +
                                    '<i class="g2-legend-marker2" style="width:10px;height:10px;display:inline-block;margin-right:5px;border-radius: 5px;border: 2px solid ' + color + ';"></i>'+
                                '</span>' +
                                '<span class="g2-legend-text" style="color: #24C6F5;">'+ value +'</span>'+
                            '</li>';
                } else {
                    content += '<li></li>'
                }
                return content;
            },
            offsetX: 15,
            offsetY: -5,
            'g2-legend': {
                marginLeft: '100px',
                marginTop: '0px'
            },
            'g2-legend-list': {
                border: 'none',
                float: 'right',
                marginRight: '30px',
            },
            'g2-legend-marker': {
                width: '22px',
                height: '2px',
                borderRadius: 0,
                marginRight: 0
            }
        });
        chart.tooltip({
            crosshairs: {
                type: 'line',
                style: {
                    // 图形样式
                    fill: '', // 填充的颜色
                    stroke: '#666', // 边框的颜色
                    strokeOpacity: 1, // 边框颜色的透明度，数值为 0 - 1 范围
                    fillOpacity: 1, // 填充的颜色透明度，数值为 0 - 1 范围
                    lineWidth: 1, // 边框的粗细
                    lineDash: 'solid' // 线的虚线样式
                }
            },
            triggerOn: 'click',
            useHtml:true,
            htmlContent:function(title,items){
                let itemContnt = '';
                items.map((v,i) => {
                    let val = i==1 ? v.value+'%' : v.value;
                    itemContnt += '<li style="display: block;" style="padding-left: 5px;">'
                                + '<span style="background-color:'+  v.color +';width: 6px;height: 6px;display: inline-block;border-radius: 3px;margin-right: 3px;" class="g2-tooltip-marker"></span>'
                                + '<span style="color: #BDE9FF;">'+  v.name +'： </span>'
                                + '<span style="color: '+  v.color +';">'+  val +'</span>'
                        + '</li>';
                })
                // console.log('itemContnt------',itemContnt);
                return '<div class="g2-tooltip" style="padding: 5px 15px 15px 15px;position: absolute;left: '+ items[0].x +';top:'+  items[0].y +';">'
                        + '<div class="g2-tooltip-title" style="margin-bottom: 4px;color: #6AEBFC;margin-top: 7px;">'+ title +'</div>'
                        + '<ul class="g2-tooltip-list">'
                            + itemContnt
                        + '</ul>'
                    + '</div>';
            }
        });

        chart.line().position('time*'+y1).color(color[0]).size(2);
        chart.point().position('time*'+y1).size(4).color(color[0]).shape('circle');
        if (y2) {
            chart.line().position('time*'+y2).color(color[1]).size(2);
            chart.point().position('time*'+y2).size(4).color(color[1]).shape('circle');
        }
        

        chart.render();

        // tooltip自动轮播
        setTimeout(() => {
            let num = 0;
            //去掉定时器的方法 
            lineBottomTimer = lineBottomTimer + this.state.id+'_'+this.state.chartId+index;  
            if (lineBottomTimer) window.clearInterval(lineBottomTimer);

            lineBottomTimer = window.setInterval(function() {
                if (num == data.length) {
                    num = 0;
                }

                let point = chart.getXY({time: data[num]['time'], y1Key: data[num]['y1Key']});
                // debugger;
                chart.showTooltip(point);
                num++;
            },2000)
        }, 10);
    }

    // 改变全局变量
    onGlobalVariable = (val) => {
        const _this = this;

        _this.setState({
            pageInitFlag: false
        })

        let obj = {};
        Object.keys(_this.state.requireCondition).forEach(function(key){
            obj[key] = key == _this.state.requireName ? val : '';
        })
        
        _this.interfaceRequest(_this.state.chartId,obj);
        // this.props.onGlobalVariable(val);
    }
    
    

    render() {
        const _this = this;
        let Layer = this.props.layer;
        let selectVal = this.state.requireCondition[this.state.requireName] ? this.state.requireCondition[this.state.requireName] : _this.state.mainData[0];

        let chartData = this.state.chartData;
        let haveData = this.state.haveData;
        if (haveData && chartData.length > 0) {
            _this.setState({
                haveData: false
            });

            let oldDataNull = this.state.dataNull;
            chartData.map((item,index) => {
                if (item.data.length > 0) {
                    oldDataNull.splice(index,1,false);
                    let colorData = item.colors.split(',');
                    if (index == 0) {
                        item.data.map((v,i) => {
                            let obj = v;
                            setTimeout(()=> {
                                document.getElementById('gauge_'+i).innerHTML = '';
                                let color = colorData[i] ? colorData[i] : 'red';
                                _this.renderGauge(obj,i,color);
                            },100);
                        });
                    } else {
                        // 组装当前图例的维度和度量
                        let xyArr = [];
                        let chartGeoms = item.chartGeoms;
                        let axisName = [];
                        if (chartGeoms){
                            axisName.push(chartGeoms[0].position.split("*")[0]);
                            for (let i = 0; i < chartGeoms.length; i++){
                                axisName.push(chartGeoms[i].position.split("*")[1])
                            }
                        }

                        axisName.map((v,i) => {
                            Object.keys(item.columnMaps).forEach(function(key){
                                if (key == v) {
                                    let keyName = i == 0 ? 'time' : i == 1 ? 'y1Key' : v;
                                    xyArr.push({
                                        key: keyName,
                                        label: item.columnMaps[key]
                                    })
                                }
                            });
                        })

                        let newData = [];
                        if (item.data.length > 0) {
                            item.data.map((v,i) => {
                                let obj = {};
                                Object.keys(v).forEach(function(key){
                                    let keyName = key == axisName[0] ? 'time' : key == axisName[1] ? 'y1Key' : key;
                                    obj[keyName] = v[key];
                                });
                                newData.push(obj);
                            })
                        }

                        let legend = item.legend;
                        let padding = item.padding;
                        document.getElementById('chart_' + this.state.id + '_' + this.state.chartId+'_Bottom_'+index).innerHTML = '';
                        if (item.chartType == 'LineDoubleYAxisChart') {
                            _this.renderDoubleLine(newData,xyArr,colorData,legend,padding,index);
                        } else if (item.chartType == 'LineChart') {
                            _this.renderDoubleLine(newData,xyArr,colorData,legend,padding,index);
                        }
                    }
                } else {
                    oldDataNull.splice(index,1,true);
                }
            })

            this.setState({
                dataNull: oldDataNull,
            })

            

        }

        let w = chartData.length > 0 && (100 / chartData[0].data.length).toFixed(2) + '%';

        return (
            <div id={this.state.id + '_' + this.state.chartId}  style={{zIndex: 99,width: this.state.width, height: this.state.height - 50,padding: '0 2%'}}>
                <Layer width={this.state.width} 
                        title={this.state.title} 
                        chartId={this.state.chartId} 
                        choiceData={this.state.mainData} 
                        selectVal={selectVal}
                        onGlobalVariable={this.onGlobalVariable}
                ></Layer>
                <div id={'chart_' + this.state.id + '_' + this.state.chartId+'_top'} style={{height: this.state.height * 0.5 - 34,marginTop: '50px'}}>
                    {chartData.length > 0 && chartData[0].data && chartData[0].data.length > 0 && chartData[0].data.map((item,index) => {
                        return (
                            <div style={{width: w,height: '100%',display: 'inline-block'}}>
                                <div id={'gauge_'+index} style={{width: '100%',height: '70%'}}></div>
                                <p style={{fontSize: '14px',color: '#8AE5EB',margin: 0,textAlign: 'center'}}>{item.title ? item.title : ''}</p>
                                <p style={{fontSize: '12px',color: '#24C6F5',margin: 0,textAlign: 'center'}}>{item.botmText ? item.botmText : ''}</p>
                            </div>
                        )
                    })}
                    { this.state.dataNull[0] && <p style={{color: '#fff',textAlign: 'center'}}> 暂无数据 </p>}
                </div>
                <div id={'chart_' + this.state.id + '_' + this.state.chartId+'_Bottom'} style={{height: this.state.height * 0.5 - 50,marginTop: '15px'}}>
                    <div id={'chart_' + this.state.id + '_' + this.state.chartId+'_Bottom_1'} style={{width: '33.3%',display: 'inline-block'}}>
                        { this.state.dataNull[1] && <p style={{color: '#fff',textAlign: 'center'}}> 暂无数据 </p>}
                    </div>
                    <div id={'chart_' + this.state.id + '_' + this.state.chartId+'_Bottom_2'} style={{width: '33.3%',display: 'inline-block'}}>
                        { this.state.dataNull[2] && <p style={{color: '#fff',textAlign: 'center'}}> 暂无数据 </p>}
                    </div>
                    <div id={'chart_' + this.state.id + '_' + this.state.chartId+'_Bottom_3'} style={{width: '33.3%',display: 'inline-block'}}>
                        { this.state.dataNull[3] && <p style={{color: '#fff',textAlign: 'center'}}> 暂无数据 </p>}
                    </div>
                </div>
            </div>
        )
    }


}

export default MultipleChart;


/**
  * @description: g2--双图例组件（上/下）
  * @author: hj
  * @update: hj(2020-01-14)
  */

import React, {Component} from 'react';
import G2 from '@antv/g2';
import DataSet from '@antv/data-set';
import styles from '../Tables/TableZebra/index.module.scss';
import {getMediaById} from '@/service/Api/Chart';
import { observer,inject,Provider } from 'mobx-react';
import '../Maps/AMap/js/map.js'; // 标注地图

let doubleTimer;

 
@inject('chartStore') 
@observer


class DoubleChart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            params: [],
            data: [],
            forceFit: true, //forceFit自动试用宽度，默认false
            width: this.props.data.width ? this.props.data.width : '100%',   // 固定宽度    当自适应宽度开启，此功能失效
            height: this.props.data.height ? this.props.data.height: '100%',
            condition: this.props.data.condition ? this.props.data.condition : '',
            pageFlag: this.props.data.pageFlag ? this.props.data.pageFlag : '',
            nofirst: this.props.data.nofirst ? this.props.data.nofirst : '',
            color: [],
            fields: [],
            xAxis: "",
            title: [],
            chartId: this.props.data.chartId,
            id: this.props.data.componentId,
            chartData: [],
            axisArr: {},
            haveData: false,
            legend: 'top',

            dataNull: [], // 暂无数据

            // 全局变量
            mainData: [], // 操作组件可操作数据
            mainName: '', // 操作组件可操作当前维度
            mainFlag: false, // 是否是操作组件

            // 表格数据
            colnums: [], // 表头
            colnumsData: [], // 表数据

            bottomTableFlag: false,

            pageInitFlag: true,
            affectName: '',
            requireCondition: '',
        };
    };


    componentWillMount() { //初始化
        const _this = this;
        
        // 预览界面
        if (_this.state.pageFlag == 'preview') {
            // 判断当前组件是否是可操作组件
            const { globalData } = _this.props.chartStore;
            if (globalData.slice().length > 0) {
                globalData.slice().map((item,index) => {
                    if (item.affect == _this.state.chartId) { // 可操作组件
                        let affectName = item.affectDimension;
                        _this.setState({
                            mainFlag: true,
                            mainName: affectName
                        })
                    }
                })
            }

            _this.interfaceRequest(_this.state.chartId,_this.state.condition);
        } else { // 配置面板
            if (_this.state.nofirst == 'nofirst') {
                _this.saveHandleData();
            } else {
                _this.interfaceRequest(_this.state.chartId,_this.state.condition);
            }
        }
    }

    // 钩子函数--组件挂载完成后
    componentDidMount() { //初始化
        this.setState({
            haveData: true
        })
    }
    // 接收父组件的数据改变
    componentWillReceiveProps = (nextProps) => {
        const _this = this;
        const { globalData, affectId } = _this.props.chartStore;

        // 预览界面---判断是否是变化组件
        if (nextProps.data.pageFlag == 'preview') {
            if (nextProps.data.condition != _this.state.requireCondition && globalData.slice().length > 0) {
                globalData.slice().map((item,index) => {
                    if (item.affect == affectId && item.require == nextProps.data.chartId) { // 联动组件
                        _this.setState({
                            pageInitFlag: false
                        })
                        let condition = {};
                        Object.keys(_this.state.requireCondition).forEach(function(keys){
                            condition[keys] = keys == item.requireDimension ? nextProps.data.condition[keys] : _this.state.requireCondition[keys];
                        })
                        _this.interfaceRequest(nextProps.data.chartId,condition);
                    }
                })
            }
        }
    }

    // 数据接口请求
    interfaceRequest = (chartId,condition) => {
        this.setState({
            requireCondition: condition,
        });

        let data = {
            charId: chartId,
            condition: condition
        };
        this.getMediaById(data);
    }

    // 数据存储刷新
    saveHandleData = () => {
        const { chartArrData } = this.props.chartStore;
        if (chartArrData.slice().length > 0) {
            chartArrData.slice().map((item) => {
                if (item.id == this.state.chartId) {
                    this.setState({
                        haveData: false
                    },function() {
                        // 数据处理
                        this.handleData(item);
                    })
                }
            })
        }
    }


    async getMediaById(data) {
        const res = await getMediaById(data);
        if (res.data && res.data.code === 0) {
            let str = res.data;
            if (str.data && str.data.data && str.data.data.length > 0) {
                // 存储图例原始数据
                const { saveChartData } = this.props.chartStore;
                let oldData = {
                    id: this.state.chartId,
                    data: str.data.data
                }
                saveChartData(oldData);

                // 数据处理
                this.handleData(str.data);
            } else {
                this.setState({
                    dataNull: []
                })
            }
        } else {
            this.setState({
                dataNull: []
            })
        }
    }
    // 数据处理
    handleData = (allData) => {
        let storeAllData = allData.data.slice();
        this.setState({
            dataNull: []
        })

        // 非预览界面
        if (this.state.pageFlag != 'preview') {
            // 存储可操作组件数据
            if (allData.affect && allData.affect.length > 0) {
                let obj = {
                    id: this.state.chartId,
                    label: allData.doubleChartTitle ? allData.doubleChartTitle : '',
                    dimensionData: []
                }
                const { saveAffectData } = this.props.chartStore;

                let dimensionData = [];
                allData.affect.slice().map((item,index) => {
                    dimensionData.push(item);
                })
                obj.dimensionData = dimensionData;
                saveAffectData(obj);
            }
            // 存储联动组件数据
            if (allData.requires && allData.requires.length > 0) {
                let obj = {
                    id: this.state.chartId,
                    label: allData.doubleChartTitle ? allData.doubleChartTitle : '',
                    dimensionData: []
                }
                const { saveRequireData } = this.props.chartStore;

                let dimensionData = [];
                allData.requires.slice().map((item,index) => {
                    dimensionData.push(item);
                })
                obj.dimensionData = dimensionData;
                saveRequireData(obj);
            }
        }

        // 页面初始化
        if (this.state.pageInitFlag) {
            if (allData.affect &&  allData.affect.length > 0) {
                this.setState({
                    affectName: allData.affect[0]
                });
            }
            // 获取选择项
            if (allData.conditionValues) {
                let selectData = allData.conditionValues[allData.affect[0]] ? allData.conditionValues[allData.affect[0]] : [];
                this.setState({
                    mainData: selectData
                });
            }
        }


        let newChartData = [];
        let newTitleData = [];
        let newDataNull = [];

        if (storeAllData.length > 0) {
            storeAllData.map((item,index) => {
                let storeData = item;
                
                newChartData.push(item);
                newTitleData.push(item.title);
                newDataNull.push(false)
            })

            this.setState({
                chartData: newChartData,
                title: newTitleData,
                dataNull: newDataNull,
                haveData: true,
            })
        }
    }



    // 渲染实心饼图图例
    renderPie = (chartData,index) => {
        const _this = this;
        let dom = 'top';
        if (index == 1) {
            dom = 'bottom';
        }

        // let data = chartData.data;
        let x = chartData.field;
        let y = chartData.dimension;
        let color = chartData.colors.split(',');

        let data = chartData.data.slice().map((item,index) => {
            let obj = item;
            item.percent = item[y] / 100;
            return obj
        })
          
        let chart = new G2.Chart({
            container: 'chart_' + this.state.id + '_' + this.state.chartId+'_'+dom,
            forceFit: true,
            height: _this.state.height * 0.45 - 85,
            padding:  chartData.padding && chartData.padding != '' ? chartData.padding.split(',') : [0, 0, 80, 0], // 默认legend在bottom位置,
            // padding: 'auto'
        });
        
        // console.log('data---',data);
        // debugger;
        chart.source(data);
        chart.tooltip(false);
        chart.legend({
            position: chartData.legend ? chartData.legend : 'bottom',
            offsetX: 0,
            // offsetY: 20,
            marker: 'square',
            // itemWidth: 70,
            // itemGap: 20, // 表示图例每项之间的间距，如果是水平排布则为左右间距，如果是竖直排布则为上下间距。
            textStyle: {
                fill: '#fff', // 文本的颜色
                fontSize: '12', // 文本大小
            },
        });
        chart.coord('theta', {
            radius: 0.75
        });
        chart.tooltip({
            triggerOn: 'click',
            title: 'time_range',
            useHtml:true,
            htmlContent:function(title,items){
                let itemContnt = '';
                items.map((v,i) => {
                    itemContnt += '<li style="display: block;" style="padding-left: 5px;">'
                                    + '<span style="background-color:'+  v.color +';width: 6px;height: 6px;display: inline-block;border-radius: 3px;margin-right: 3px;" class="g2-tooltip-marker"></span>' 
                                    + '<span style="color: #BDE9FF;">'+  v.name +'： </span>'
                                    + '<span style="color: '+  v.color +';">'+  v.value +'</span>'
                            + '</li>';
                })
                return '<div class="g2-tooltip" style="padding: 5px 15px 15px 15px;position: absolute;left: '+ items[0].x +';top:'+  items[0].y +';">' 
                        + '<div class="g2-tooltip-title" style="margin-bottom: 4px;color: #6AEBFC;margin-top: 7px;">'+ title +'</div>'
                        + '<ul class="g2-tooltip-list">'
                            + itemContnt
                        + '</ul>'
                    + '</div>';
            }
        });

        chart.intervalStack().position(y).color(x,color).opacity(1).label(y, {
            offset: -18,
            textStyle: {
                fill: 'white',
                fontSize: 12,
                shadowBlur: 2,
                shadowColor: 'rgba(0, 0, 0, .45)'
            },
            rotate: 0,
            autoRotate: false,
            formatter: function formatter(text, item) {
            //return String(parseInt(item.point.percent * 100)) + '%';
            }
        });
        chart.render();
    }
    // 渲染空心饼图图例
    renderPieHollow = (chartData,index) => {
        const _this = this;
        let dom = 'top';
        if (index == 1) {
            dom = 'bottom';
        }

        let data = chartData.data.slice();
        let x = chartData.field;
        let y = chartData.dimension;
        let color = chartData.colors.split(',');

        let sum = 500;
        let ds = new DataSet();
        let dv = ds.createView().source(data);
        dv.transform({
            type: 'map',
            callback: function callback(row) {
                row.value = parseInt(sum * row.percent);
                return row;
                }
        });
        let chart = new G2.Chart({
            container: 'chart_' + this.state.id + '_' + this.state.chartId+'_'+dom,
            forceFit: true,
            height: _this.state.height * 0.55 - 75,
            padding:  chartData.padding && chartData.padding != '' ? chartData.padding.split(',') : [0, 0, 80, 0], // 默认legend在bottom位置,
            // padding: 'auto'
        });
        chart.source(dv);
        chart.tooltip(false);
        chart.legend({
            position: chartData.legend ? chartData.legend : 'bottom',
            // offsetY: 20,
            offsetX: 0,
            marker: 'square',
            // itemWidth: 80,
            // itemGap: 20, // 表示图例每项之间的间距，如果是水平排布则为左右间距，如果是竖直排布则为上下间距。
            textStyle: {
                fill: '#fff', // 文本的颜色
                fontSize: '12', // 文本大小
            },
        });
        chart.coord('theta', {
            radius: 0.85,
            innerRadius: 0.75,
        });
        chart.tooltip({
            triggerOn: 'click',
            title: 'time_range',
            useHtml:true,
            htmlContent:function(title,items){
                let itemContnt = '';
                items.map((v,i) => {
                    itemContnt += '<li style="display: block;" style="padding-left: 5px;">'
                                    + '<span style="background-color:'+  v.color +';width: 6px;height: 6px;display: inline-block;border-radius: 3px;margin-right: 3px;" class="g2-tooltip-marker"></span>'
                                    + '<span style="color: #BDE9FF;">'+  v.name +'： </span>'
                                    + '<span style="color: '+  v.color +';">'+  v.value +'</span>'
                            + '</li>';
                })
                return '<div class="g2-tooltip" style="padding: 5px 15px 15px 15px;position: absolute;left: '+ items[0].x +';top:'+  items[0].y +';">'
                        + '<div class="g2-tooltip-title" style="margin-bottom: 4px;color: #6AEBFC;margin-top: 7px;">'+ title +'</div>'
                        + '<ul class="g2-tooltip-list">'
                            + itemContnt
                        + '</ul>'
                    + '</div>';
            }
        });

        // chart.intervalStack().position(y).color(x,color).opacity(1).label(y, {
        //     offset: -18,
        //     textStyle: {
        //         fill: 'white',
        //         fontSize: 12,
        //         shadowBlur: 2,
        //         shadowColor: 'rgba(0, 0, 0, .45)'
        //     },
        //     rotate: 0,
        //     autoRotate: false,
        //     formatter: function formatter(text, item) {
        //     //return String(parseInt(item.point.percent * 100)) + '%';
        //     }
        // });
        // chart.render();
        let interval = chart.intervalStack().position(y).color(x,color).tooltip(x+'*'+y, function(item, percent) {
            return {
                name: item,
                value: percent
            };
        });
            
        // 辅助文本
        chart.guide().html({
            position: ['50%', '50%'],
            html: '<div id="g2-guide-html'+_this.state.chartId+'" style="width: 10em;height: 3em;text-align: center;line-height: 0.2;">'+
                    '<p id="pie-center-title'+_this.state.chartId+'" style="line-height: 30px;font-size: 18px;color: #26D3FA;font-weight: 300;margin: 0;">'+ data[0][x]+'</p>'+
                    '<p id="pie-center-value'+_this.state.chartId+'" style="font-size: 32px;color: #97FAFF;font-weight: bold;;margin: 0;font-family: myNumberFamily;">'+ data[0][y]+'</p>'+
                    '</div>', 
        });
        
        chart.on('interval:click', function(ev) {
            let data = ev.data._origin;
            document.getElementById('g2-guide-html'+_this.state.chartId).style.opacity = 1;
            document.getElementById('pie-center-title'+_this.state.chartId).innerText = data[x];
            document.getElementById('pie-center-value'+_this.state.chartId).innerText = data[y];
        });
        chart.render();
        interval.setSelected(dv.origin[0]);
    }
    // 渲染柱状图图例
    renderBar = (chartData,index) => {
        const _this = this;
        let dom = 'top';
        if (index == 1) {
            dom = 'bottom';
        }

        // 组装当前图例的维度和度量
        let xyArr = chartData.columns ? chartData.columns.split(',') : [];

        let newData = [];
        let y1Name = xyArr[1];
        if (chartData.data.slice().length > 0) {
            chartData.data.slice().map((item,index) => {
                let obj = {};
                Object.keys(item).forEach(function(key){
                    let keyName = key == y1Name ? 'value' : 'name';
                    obj[keyName] = item[key];
                });
                newData.push(obj);
            })
            
            this.setState({
                dataNull: false
            })
        }
        // let axisName = ['name','value'];

        let axisName = [];
        xyArr.map((item,index) => {
            Object.keys(chartData.columnMaps).forEach(function(key){
                if (key == item) {
                    let keyName = index == 0 ? 'name' : index == 1 ? 'value' : item;
                    axisName.push({
                        key: keyName,
                        label: chartData.columnMaps[key]
                    })
                }
            });
        })

        let data = newData;
        let color = chartData.colors.split(',');
        let x = axisName[0].key;
        let y = axisName[1].key;

        let chart = new G2.Chart({
            container: 'chart_' + this.state.id + '_' + this.state.chartId+'_'+dom,
            forceFit: true,
            height: _this.state.height * 0.55 - 75,
            padding:  chartData.padding && chartData.padding != '' ? chartData.padding.split(',') : [20, 20, 50, 30], // 默认legend在bottom位置,
        });
        chart.source(data);
        chart.scale(y, {
          alias: axisName[1].label
        });
        chart.axis(x, {
            position: 'bottom', // 设置坐标轴的显示位置，可取值 top bottom left right
            line: {
                lineWidth: 1, // 设置线的宽度
                stroke: '#536f8e' //  设置线的颜色
            }, // 设置坐标轴线的样式，如果值为 null，则不显示坐标轴线 图形属性
            label: {
                // offset: 0, // 数值，设置坐标轴文本 label 距离坐标轴线的距离
                // offsetX: 0, // 在 offset 的基础上 x 方向的偏移量
                // offsetY: 0, // 在 offset 的基础上 y 方向的偏移量
                // // 设置文本的显示样式，还可以是个回调函数，回调函数的参数为该坐标轴对应字段的数值
                // rotate: 0, // 注意，旋转角度的配置不再在 textStyle 里配置
                textStyle: {
                    textAlign: 'center', // 文本对齐方向，可取值为： start center end
                    fill: '#24C6F5', // 文本的颜色
                    fontSize: '12', // 文本大小
                    textBaseline: 'top' // 文本基准线，可取 top middle bottom，默认为middle
                }
            },
            title: null, // 坐标轴标题设置，如果值为 null，则不显示标题
            tickLine: {
                lineWidth: 1, // 刻度线宽
                stroke: '#536f8e', // 刻度线的颜色
                length: -5, // 刻度线的长度
            }, // 坐标轴刻度线配置
        });
        chart.axis(y, {
            tickInterval: 20,
            position: 'left', // 设置坐标轴的显示位置，可取值 top bottom left right
            line: {
                lineWidth: 1, // 设置线的宽度
                stroke: '#536f8e' //  设置线的颜色
            }, // 设置坐标轴线的样式，如果值为 null，则不显示坐标轴线 图形属性
            label: {
                offset: 0, // 数值，设置坐标轴文本 label 距离坐标轴线的距离
                offsetX: -10, // 在 offset 的基础上 x 方向的偏移量
                offsetY: 0, // 在 offset 的基础上 y 方向的偏移量
                // 设置文本的显示样式，还可以是个回调函数，回调函数的参数为该坐标轴对应字段的数值
                textStyle: {
                    textAlign: 'end', // 文本对齐方向，可取值为： start center end
                    fill: '#24C6F5', // 文本的颜色
                    fontSize: '12', // 文本大小
                }
            },
            title: null, // 坐标轴标题设置，如果值为 null，则不显示标题
            tickLine: null,
            grid: {
                align: 'center', // 声明网格顶点从两个刻度中间开始，默认从刻度点开始
                type: 'line', // 声明网格的类型，line 表示线，polygon 表示矩形框
                // 当网格类型 type 为 line 时，使用 lineStyle 设置样式
                lineStyle: {
                    stroke: '#21355f', // 网格线的颜色
                    lineWidth: 1, // 网格线的粗细
                    lineDash: [4, 4 ] // 网格线的虚线配置，第一个参数描述虚线的实部占多少像素，第二个参数描述虚线的虚部占多少像素
                },
                hideFirstLine: true, // 是否隐藏第一条网格线，默认为 false
                hideLastLine: true, // 是否隐藏最后一条网格线，默认为 false
            }, // 坐标轴栅格线的配置信息，默认只有左边的坐标轴带有栅格线，null 为不显示。
        });
        chart.legend({
            offsetY: -5, // 图例 Y 方向的偏移值，数值类型，数值单位为 'px'，默认值为 0。
            itemGap: 20, // 表示图例每项之间的间距，如果是水平排布则为左右间距，如果是竖直排布则为上下间距。
            position: chartData.legend ? chartData.legend : 'bottom', // 图例的显示位置，有 'top','left','right','bottom'四种位置，默认是'right'。
            textStyle: {
                fill: '#24C6F5', // 文本的颜色
                fontSize: '12', // 文本大小
            }
        });
        chart.tooltip({
            triggerOn: 'click',
            useHtml:true,
            htmlContent:function(title,items){
                let itemContnt = '';
                items.map((v,i) => {
                    itemContnt += '<li style="display: block;" style="padding-left: 5px;">'
                                + '<span style="background-color:'+  v.color +';width: 6px;height: 6px;display: inline-block;border-radius: 3px;margin-right: 3px;" class="g2-tooltip-marker"></span>' 
                                + '<span style="color: #BDE9FF;">'+  v.name +'： </span>'
                                + '<span style="color: '+  v.color +';">'+  v.value +'</span>'
                        + '</li>';
                })
                // console.log('itemContnt------',itemContnt);
                return '<div class="g2-tooltip" style="padding: 5px 15px 15px 15px;position: absolute;left: '+ items[0].x +';top:'+  items[0].y +';">'
                        + '<div class="g2-tooltip-title" style="margin-bottom: 4px;color: #6AEBFC;margin-top: 7px;">'+ title +'</div>'
                        + '<ul class="g2-tooltip-list">'
                            + itemContnt
                        + '</ul>'
                    + '</div>';
            }
        });

        chart.interval().position(x+'*'+y).color(color);
        chart.render();
        
        // tooltip自动轮播
        setTimeout(() => {
            let num = 0;
            //去掉定时器的方法  
            doubleTimer = doubleTimer + this.state.id+'_'+this.state.chartId;
            if (doubleTimer) window.clearInterval(doubleTimer);

            doubleTimer = window.setInterval(function() {
                if (num == data.length) {
                    num = 0;
                }
                
                let point = chart.getXY({name: data[num].name, value: data[num].value});
                chart.showTooltip(point);
                num++;
            },2000)
        }, 10);
    }
    // 渲染多维度柱状图图例
    renderBarDodge = (chartData,index) => {
        const _this = this;
        let dom = 'top';
        if (index == 1) {
            dom = 'bottom';
        }


        // 组装当前图例的维度和度量
        let xArr = [];
        xArr.push(chartData.xAxis);
        // let axisName = xArr.concat(storeData.fields.slice());
        let axisName = chartData.columns ? chartData.columns.split(',') : [];

        let xyArr = [];
        axisName.map((item) => {
            Object.keys(chartData.columnMaps).forEach(function(key){
                if (key == item) {
                    xyArr.push({
                        key: item,
                        label: chartData.columnMaps[key]
                    })
                }
            });
        })

        if (chartData.data.length > 0) {
            this.setState({
                dataNull: false
            })
        }

        // let data = newData;
        let color = chartData.colors.split(',');
        let x = xyArr[0].key;
        let y = xyArr[1].key;
        
        let data = [];
        let newData = chartData.data.slice().map((item) => {
            let obj = {};
            xyArr.map((v) => {
                if (v.key != x) {
                    obj[v.label] = item[v.key];
                } else {
                    obj[x] = item[x];
                }
            })
            return obj;
        });
        newData.map((item,index) => {
            let time = item[x];
            Object.keys(item).forEach(function(key){
                if (key != x) {
                    data.push({
                        type: key,
                        time: time,
                        value: item[key]
                    })
                    
                }
            });
        })

        let chart = new G2.Chart({
            container: 'chart_' + this.state.id + '_' + this.state.chartId+'_'+dom,
            forceFit: true,
            height: _this.state.height * 0.55 - 75,
            padding:  chartData.padding && chartData.padding != '' ? chartData.padding.split(',') : [30, 20, 50, 30], // 默认legend在bottom位置,
        });
        
        chart.source(data);
        chart.axis('time', {
            position: 'bottom', // 设置坐标轴的显示位置，可取值 top bottom left right
            line: {
                lineWidth: 1, // 设置线的宽度
                stroke: '#536f8e' //  设置线的颜色
            }, // 设置坐标轴线的样式，如果值为 null，则不显示坐标轴线 图形属性
            label: {
                // offset: 0, // 数值，设置坐标轴文本 label 距离坐标轴线的距离
                // offsetX: 0, // 在 offset 的基础上 x 方向的偏移量
                // offsetY: 0, // 在 offset 的基础上 y 方向的偏移量
                // // 设置文本的显示样式，还可以是个回调函数，回调函数的参数为该坐标轴对应字段的数值
                // rotate: 0, // 注意，旋转角度的配置不再在 textStyle 里配置
                textStyle: {
                    textAlign: 'center', // 文本对齐方向，可取值为： start center end
                    fill: '#24C6F5', // 文本的颜色
                    fontSize: '12', // 文本大小
                    // textBaseline: 'top' // 文本基准线，可取 top middle bottom，默认为middle
                }
            },
            title: null, // 坐标轴标题设置，如果值为 null，则不显示标题
        });
        chart.axis('value', {
            position: 'left', // 设置坐标轴的显示位置，可取值 top bottom left right
            line: {
                lineWidth: 1, // 设置线的宽度
                stroke: '#536f8e' //  设置线的颜色
            }, // 设置坐标轴线的样式，如果值为 null，则不显示坐标轴线 图形属性
            label: {
                offset: 0, // 数值，设置坐标轴文本 label 距离坐标轴线的距离
                offsetX: -10, // 在 offset 的基础上 x 方向的偏移量
                offsetY: 0, // 在 offset 的基础上 y 方向的偏移量
                // 设置文本的显示样式，还可以是个回调函数，回调函数的参数为该坐标轴对应字段的数值
                textStyle: {
                    textAlign: 'end', // 文本对齐方向，可取值为： start center end
                    fill: '#24C6F5', // 文本的颜色
                    fontSize: '12', // 文本大小
                }
            },
            title: null, // 坐标轴标题设置，如果值为 null，则不显示标题
            tickLine: null,
            grid: {
                align: 'center', // 声明网格顶点从两个刻度中间开始，默认从刻度点开始
                type: 'line', // 声明网格的类型，line 表示线，polygon 表示矩形框
                // 当网格类型 type 为 line 时，使用 lineStyle 设置样式
                lineStyle: {
                  stroke: '#21355f', // 网格线的颜色
                  lineWidth: 1, // 网格线的粗细
                  lineDash: [4, 4 ] // 网格线的虚线配置，第一个参数描述虚线的实部占多少像素，第二个参数描述虚线的虚部占多少像素
                },
                hideFirstLine: true, // 是否隐藏第一条网格线，默认为 false
                hideLastLine: true, // 是否隐藏最后一条网格线，默认为 false
            }, // 坐标轴栅格线的配置信息，默认只有左边的坐标轴带有栅格线，null 为不显示。
        });
        chart.legend({
            // offsetY: -5, // 图例 Y 方向的偏移值，数值类型，数值单位为 'px'，默认值为 0。
            itemGap: 20, // 表示图例每项之间的间距，如果是水平排布则为左右间距，如果是竖直排布则为上下间距。
            position: chartData.legend ? chartData.legend : 'bottom', // 图例的显示位置，有 'top','left','right','bottom'四种位置，默认是'right'。
            textStyle: {
                fill: '#24C6F5', // 文本的颜色
                fontSize: '12', // 文本大小
            }
        });
        chart.tooltip({
            triggerOn: 'click',
            useHtml:true, 
            htmlContent:function(title,items){
                let itemContnt = '';
                items.map((v,i) => {
                    itemContnt += '<li style="display: block;" style="padding-left: 5px;">'
                                + '<span style="background-color:'+  v.color +';width: 6px;height: 6px;display: inline-block;border-radius: 3px;margin-right: 3px;" class="g2-tooltip-marker"></span>' 
                                + '<span style="color: #BDE9FF;">'+  v.name +'： </span>'
                                + '<span style="color: '+  v.color +';">'+  v.value +'</span>'
                        + '</li>';
                })
                // console.log('itemContnt------',itemContnt);
                return '<div class="g2-tooltip" style="padding: 5px 15px 15px 15px;position: absolute;left: '+ items[0].x +';top:'+  items[0].y +';">' 
                        + '<div class="g2-tooltip-title" style="margin-bottom: 4px;color: #6AEBFC;margin-top: 7px;">'+ title +'</div>'
                        + '<ul class="g2-tooltip-list">'
                            + itemContnt
                        + '</ul>'
                    + '</div>';
            }
        });
        
        chart.interval().position('time*value').color('type',color).adjust([{
            type: 'dodge',
            marginRatio: 1 / 32
        }]);

        chart.render();
        
        // tooltip自动轮播
        setTimeout(() => {
            let num = 0;
            //去掉定时器的方法  
            doubleTimer = doubleTimer + this.state.id+'_'+this.state.chartId;
            if (doubleTimer) window.clearInterval(doubleTimer);

            doubleTimer = window.setInterval(function() {
                if (num == data.length) {
                    num = 0;
                }
                
                let point = chart.getXY({time: data[num].time, value: data[num].value});
                chart.showTooltip(point);
                num++;
            },2000)
        }, 10);
    }
    // 渲染条形图图例
    renderInterval = (chartData,index) => {
        const _this = this;
        let dom = 'top';
        if (index == 1) {
            dom = 'bottom';
        }

         // 组装当前图例的维度和度量
         let axisArr = [];
         let axisName = chartData.columns ? chartData.columns.split(',') : [];
        //  Object.keys(storeData.columnMaps).forEach(function(key){
        //      axisName.push(key)
        //  });
         
         axisName.map((item,index) => {
             Object.keys(chartData.columnMaps).forEach(function(key){
                 if (key == item) {
                    axisArr.push({
                         key: item,
                         label: chartData.columnMaps[key]
                     })
                 }
             });
         })
 
         let x = chartData.xAxis ? chartData.xAxis : axisName[0];
         let colorData = chartData.colors.split(',');


        let data = [];
        let newData = chartData.data.slice().map((item) => {
            let obj = {};
            axisArr.map((v) => {
                if (v.key != x) {
                    obj[v.label] = item[v.key];
                } else {
                    obj[x] = item[x];
                }
            })
            return obj;
        });

        newData.map((item,index) => {
            let time = item[x];
            Object.keys(item).forEach(function(key){
                // console.log(key, x);
                if (key != x) {
                    data.push({
                        type: key,
                        name: time,
                        value: item[key]
                    })

                }
            });
        })
        // console.log('data---',data);

        let chart = new G2.Chart({
            container: 'chart_' + this.state.id + '_' + this.state.chartId+'_'+dom,
            forceFit: true,
            height: _this.state.height * 0.55 - 75,
            padding:  chartData.padding && chartData.padding != '' ? chartData.padding.split(',') : [60, 10, 30, 80], // 默认legend在top位置,
        });
        chart.source(data, {
            value: {
                nice: false,
                // alias: '销售额（万）'
            }
        });

        chart.axis('value', false);
        chart.legend({
            position: chartData.legend ? chartData.legend : 'top',
        });
        chart.coord().transpose();
        chart.facet('mirror', {
            fields: ['type'],
            autoSetAxis: false,
            transpose: true,
            showTitle: true,
            colTitle: {
                offsetY: -40, // 列标题垂直方向的偏移
                style: {
                    fontSize: 12,
                    textAlign: 'center',
                    fill: '#94F5FA'
                } // 标题文本样式
            },
            padding: [0, 10, 0, 0],
            eachView: function eachView(view, facet) {
                let facetIndex = facet.colIndex;
                if (facetIndex === 0) {
                    view.axis('name', {
                        position: 'top',
                        label: {
                            textStyle: {
                                fill: '#fff',
                                fontSize: 12
                            },
                        },
                        tickLine: {
                            alignWithLabel: false,
                            length: 0
                        },
                        line: {
                            lineWidth: 0
                        }
                    });
                } else {
                    view.axis('name', false);
                }
                let color = facetIndex === 0 ? colorData[1].split(']')[0] : colorData[3].split(']')[0];
                view.interval().position('name*value').color(color).size(36).opacity(1).label('value', function(val) {
                    let offset = -4;
                    let shadowBlur = 2;
                    let textAlign = facetIndex === 1 ? 'end' : 'start';
                    // let textAlign = 'end';
                    let fill = '#fff';
                    if (val < 15) {
                        offset = 4;
                        textAlign = facetIndex === 1 ? 'start' : 'end';
                        // textAlign = 'end';
                        fill = '#fff';
                        shadowBlur = 0;
                    }
                    return {
                    //position: 'middle',
                        offset: offset,
                        textStyle: {
                            fill: fill,
                            shadowBlur: shadowBlur,
                            shadowColor: 'rgba(0, 0, 0, .45)',
                            textAlign: textAlign
                        }
                    };
                });
            }
        });
        chart.tooltip({
            triggerOn: 'click',
            useHtml:true,
            htmlContent:function(title,items){
                let itemContnt = '';
                items.map((v,i) => {
                    itemContnt += '<li style="display: block;" style="padding-left: 5px;">'
                                + '<span style="background-color:'+  v.color +';width: 6px;height: 6px;display: inline-block;border-radius: 3px;margin-right: 3px;" class="g2-tooltip-marker"></span>' 
                                + '<span style="color: #BDE9FF;">'+  v.point.point.type +'： </span>'
                                + '<span style="color: '+  v.color +';">'+  v.value +'</span>'
                        + '</li>';
                })
                // console.log('itemContnt------',itemContnt);
                return '<div class="g2-tooltip" style="padding: 5px 15px 15px 15px;position: absolute;left: '+ items[0].x +';top:'+  items[0].y +';">'
                        + '<div class="g2-tooltip-title" style="margin-bottom: 4px;color: #6AEBFC;margin-top: 7px;">'+ title +'</div>'
                        + '<ul class="g2-tooltip-list">'
                            + itemContnt
                        + '</ul>'
                    + '</div>';
            }
        });
        // console.log('data---',data);

        chart.render();
    }
    // 渲染地图图例
    renderAMap = (chartData,index) => {
        const _this = this;
        let dom = 'top';
        if (index == 1) {
            dom = 'bottom';
        }

        let mapId = 'chart_' + this.state.id + '_' + this.state.chartId+'_'+dom;
        createMap(mapId);
    }
    // 渲染表格
    renderTable = (chartData,index) => {
        const _this = this;

        let axisArr = chartData.columns ? chartData.columns.split(',') : [];
        let columnMaps = chartData.columnMaps;
        // 表头
        let colnums = [];
        if (axisArr.length > 0) {
            axisArr.map((v,i) => {
                Object.keys(columnMaps).forEach(function(key){
                // for (let key in columnMaps) {
                    if (key == v) {
                        colnums.push({
                            "key": v,
                            "value": columnMaps[v]
                        })
                    }
                })
            })
        }
        
        // 表数据
        let colnumsData = [];
        if (chartData.data.slice().length > 0) {
            chartData.data.slice().map((item,index) => {
                let objData = [];
                axisArr.map((v,i) => {
                    objData.push({
                        "key": v,
                        "value": item[v]
                    })
                })
                colnumsData.push(objData);
            })
        }
        
        _this.setState({
            colnums: colnums,
            colnumsData: colnumsData
        })
    }
    // table无缝隙循环滚动
    Marquee = (table,table1,table2) => {
        if (table2.offsetTop - table.scrollTop <= 0) {
            table.scrollTop -= table1.offsetHeight;
        } else {
            table.scrollTop++;
        }
    }

    // 改变全局变量
    onGlobalVariable = (val) => {
        const { saveAffectId } = this.props.chartStore;
        saveAffectId(this.state.chartId);
        this.props.onGlobalVariable(val);
    }
    
    

    render() {
        const _this = this;
        let Layer = this.props.layer;

        let chartData = this.state.chartData;
        let haveData = this.state.haveData;
        if (haveData && chartData.length > 0) {
            _this.setState({
                haveData: false
            });

            let oldDataNull = this.state.dataNull;
            chartData.map((item,index) => {
                if (item.data.length > 0) {
                    oldDataNull.splice(index,1,false);

                    if (index == 0) {
                        document.getElementById('chart_' + this.state.id + '_' + this.state.chartId+'_top').innerHTML = '';
                        if (item.chartType == 'PieChart') {
                            _this.renderPie(item,index);
                        } else if (item.chartType == 'PieHollowChart') {
                            _this.renderPieHollow(item,index);
                        } else if (item.chartType == 'BarChart') {
                            _this.renderBar(item,index);
                        } else if (item.chartType == 'BarDodgeChart') {
                            _this.renderBarDodge(item,index);
                        } else if (item.chartType == 'IntervalChart') {
                            _this.renderInterval(item,index);
                        } else if (item.chartType == 'AMap') {
                            _this.renderAMap(item,index);
                        }
                    } else {
                        if (item.chartType != 'Table') {
                            _this.setState({
                                bottomTableFlag: false
                            })
                            if (item.chartType == 'PieChart') {
                                _this.renderPie(item,index);
                            } else if (item.chartType == 'PieHollowChart') {
                                _this.renderPieHollow(item,index);
                            } else if (item.chartType == 'BarChart') {
                                _this.renderBar(item,index);
                            } else if (item.chartType == 'BarDodgeChart') {
                                _this.renderBarDodge(item,index);
                            } else if (item.chartType == 'IntervalChart') {
                                _this.renderInterval(item,index);
                            } else if (item.chartType == 'AMap') {
                                _this.renderAMap(item,index);
                            }
                        } else {
                            _this.setState({
                                bottomTableFlag: true
                            })
                            // 渲染表格
                            _this.renderTable(item,index);
                        }
                    }
                } else {
                    oldDataNull.splice(index,1,true);
                }
            })

            this.setState({
                dataNull: oldDataNull,
            })

            

        }

        // 表格
        let maxH = 0;
        let colnums = _this.state.colnums;
        let colnumsData = _this.state.colnumsData;
        let tableH = this.state.height * 0.45 - 80;
        if ((colnumsData.length + 1) * 30 < tableH) {
            maxH = (colnumsData.length + 1) * 30
        } else {
            maxH = Math.ceil((tableH) / 30) * 30;
        }
        let lenNum = Math.ceil(maxH / 30) - 1;
        let tdWidth = Math.floor(100 / colnums.length) + '%';

        if (this.state.pageFlag == 'preview' && colnumsData.length > 0 && lenNum < colnumsData.length) {
            setTimeout(() => {
                doubleTimer = doubleTimer + this.state.id+'_'+this.state.chartId;
                if (doubleTimer) window.clearInterval(doubleTimer);

                let speed = 200;
                let table = document.getElementById(this.state.id+'_'+this.state.chartId+'_table_bottom');
                let table1 = document.getElementById(this.state.id+'_'+this.state.chartId+'_table1_bottom');
                let table2 = document.getElementById(this.state.id+'_'+this.state.chartId+'_table2_bottom');
                table2.innerHTML = table1.innerHTML;

                doubleTimer = window.setInterval(() => {
                    this.Marquee(table,table1,table2);
                },speed)
            },2000)
        }


        return (
            <div id={this.state.id + '_' + this.state.chartId}  style={{zIndex: 99,width: this.state.width, height: this.state.height-75,padding: '0 2%'}}>
                <Layer width={this.state.width} title={this.state.title} chartId={this.state.chartId} choiceData={this.state.mainData} onGlobalVariable={this.onGlobalVariable}></Layer>
                <div id={'chart_' + this.state.id + '_' + this.state.chartId+'_top'} style={{height: this.state.height * 0.55 - 75,margin: '0 5%',marginTop: '75px'}}></div>
                { !this.state.bottomTableFlag && <div id={'chart_' + this.state.id + '_' + this.state.chartId+'_bottom'} style={{height: this.state.height * 0.45 - 40}}></div>}
                
                <div id={'chart_' + this.state.id + '_' + this.state.chartId+'_bottom_table'} style={{display: 'block',width: '100%',height: maxH,overflow: 'hidden',marginTop: '40px'}}>
                    <div style={{height: '30px'}}>
                        <table className={styles.tableBox} cellpadding="0" cellspacing="0">
                            <tr>
                                {colnums.map((item,index) => {
                                    return (
                                        <th style={{width: tdWidth}}> {item.value} </th>
                                    )
                                })}
                            </tr>
                        </table>
                    </div>
                    <div id={this.state.id+'_'+this.state.chartId+"_table_bottom"} style={{overflow: 'hidden',height: maxH-30+ 'px'}}>
                        <div id={this.state.id+'_'+this.state.chartId+"_table1_bottom"}>
                            <table className={styles.tableBox} cellpadding="0" cellspacing="0">
                                {colnumsData.length > 0 && colnumsData.map((item,index) => {
                                    return (
                                        <tr>
                                            {item.map((v,i) => {
                                                return (
                                                    <td style={{padding: 0,margin: 0,width: tdWidth + 'px !important'}}> {v.value} </td>
                                                )
                                                
                                            })}
                                        </tr>
                                    )
                                })}
                            </table>
                        </div>
                        <div id={this.state.id+'_'+this.state.chartId+"_table2_bottom"}></div>
                    </div>
                </div>
            </div>
        )
    }


}

export default DoubleChart;


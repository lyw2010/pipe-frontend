/**
  * @description: g2--双层饼图
  * @author: hj
  * @update: hj(2020-01-14)
  */

import React, {Component} from 'react';
import G2 from '@antv/g2';
import DataSet from '@antv/data-set';
import {getMediaById} from '@/service/Api/Chart';
import { observer,inject,Provider } from 'mobx-react';

 
@inject('chartStore') 
@observer


class BasicPieChart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            params: [],
            data: [],
            forceFit: true, //forceFit自动试用宽度，默认false
            width: this.props.data.width ? this.props.data.width : '100%',   // 固定宽度    当自适应宽度开启，此功能失效
            height: this.props.data.height ? this.props.data.height - 50 : '100%',
            condition: this.props.data.condition ? this.props.data.condition : '',
            pageFlag: this.props.data.pageFlag ? this.props.data.pageFlag : '',
            nofirst: this.props.data.nofirst ? this.props.data.nofirst : '',
            color: [],
            title: '',
            chartId: this.props.data.chartId,
            id: this.props.data.componentId,
            chartData: [],
            axisArr: {},
            haveData: false,
            legend: 'top',
            padding: '',

            dataNull: false, // 暂无数据

            // 全局变量
            mainData: [], // 操作组件可操作数据
            mainName: '', // 操作组件可操作当前维度
            mainFlag: false, // 是否是操作组件

            pageInitFlag: true,
            affectName: '',
            requireCondition: '',
        };
    };


    componentWillMount() { //初始化
        const _this = this;
        
        // 预览界面
        if (_this.state.pageFlag == 'preview') {
            // 判断当前组件是否是可操作组件
            const { globalData } = _this.props.chartStore;
            if (globalData.slice().length > 0) {
                globalData.slice().map((item,index) => {
                    if (item.affect == _this.state.chartId) { // 可操作组件
                        let affectName = item.affectDimension;
                        _this.setState({
                            mainFlag: true,
                            mainName: affectName
                        })
                    }
                })
            }

            _this.interfaceRequest(_this.state.chartId,_this.state.condition);
        } else { // 配置面板
            if (_this.state.nofirst == 'nofirst') {
                _this.saveHandleData();
            } else {
                _this.interfaceRequest(_this.state.chartId,_this.state.condition);
            }
        }
    }

    // 钩子函数--组件挂载完成后
    componentDidMount() { //初始化
        this.setState({
            haveData: true
        })
    }
    // 接收父组件的数据改变
    componentWillReceiveProps = (nextProps) => {
        const _this = this;
        const { globalData, affectId } = _this.props.chartStore;

        // 预览界面---判断是否是变化组件
        if (nextProps.data.pageFlag == 'preview') {
            if (nextProps.data.condition != _this.state.requireCondition && globalData.slice().length > 0) {
                globalData.slice().map((item,index) => {
                    if (item.affect == affectId && item.require == nextProps.data.chartId) { // 联动组件
                        _this.setState({
                            pageInitFlag: false
                        })
                        let condition = {};
                        Object.keys(_this.state.requireCondition).forEach(function(keys){
                            condition[keys] = keys == item.requireDimension ? nextProps.data.condition[keys] : _this.state.requireCondition[keys];
                        })
                        _this.interfaceRequest(nextProps.data.chartId,condition);
                    }
                })
            }
        }
    }

    // 数据接口请求
    interfaceRequest = (chartId,condition) => {
        this.setState({
            requireCondition: condition,
        });

        let data = {
            charId: chartId,
            condition: condition
        };
        this.getMediaById(data);
    }

    // 数据存储刷新
    saveHandleData = () => {
        const { chartArrData } = this.props.chartStore;
        if (chartArrData.slice().length > 0) {
            chartArrData.slice().map((item) => {
                if (item.id == this.state.chartId) {
                    this.setState({
                        haveData: false
                    },function() {
                        // 数据处理
                        this.handleData(item.data);
                    })
                }
            })
        }
    }


    async getMediaById(data) {
        const res = await getMediaById(data);
        if (res.data && res.data.code === 0) {
            let str = res.data;
            if (str.data && str.data.data && str.data.data.length > 0) {
                // 存储图例原始数据
                const { saveChartData } = this.props.chartStore;
                let oldData = {
                    id: this.state.chartId,
                    data: str.data.data
                }
                saveChartData(oldData);

                // 数据处理
                this.handleData(str.data.data);
            } else {
                this.setState({
                    dataNull: true
                })
            }
        } else {
            this.setState({
                dataNull: true
            })
        }
    }
    // 数据处理
    handleData = (storeAllData) => {
        let storeData = storeAllData[0];
        this.setState({
            dataNull: true
        })

        // 非预览界面
        if (this.state.pageFlag != 'preview') {
            // 存储可操作组件数据
            if (storeData.affect && storeData.affect.length > 0) {
                let obj = {
                    id: this.state.chartId,
                    label: storeData.title,
                    dimensionData: []
                }
                const { saveAffectData } = this.props.chartStore;

                let dimensionData = [];
                storeData.affect.slice().map((item,index) => {
                    dimensionData.push(item);
                })
                obj.dimensionData = dimensionData;
                saveAffectData(obj);
            }
            // 存储联动组件数据
            if (storeData.requires && storeData.requires.length > 0) {
                let obj = {
                    id: this.state.chartId,
                    label: storeData.title,
                    dimensionData: []
                }
                const { saveRequireData } = this.props.chartStore;

                let dimensionData = [];
                storeData.requires.slice().map((item,index) => {
                    dimensionData.push(item);
                })
                obj.dimensionData = dimensionData;
                saveRequireData(obj);
            }
        }

        // 页面初始化
        if (this.state.pageInitFlag) {
            if (storeData.affect &&  storeData.affect.length > 0) {
                this.setState({
                    affectName: storeData.affect[0]
                });
            }
            // 获取选择项
            if (storeData.conditionValues) {
                let selectData = storeData.conditionValues[storeData.affect[0]] ? storeData.conditionValues[storeData.affect[0]] : [];
                this.setState({
                    mainData: selectData
                });
            }
        }


        // 组装当前图例的维度和度量
        let xyArr = [];
        Object.keys(storeData.columnMaps).forEach(function (key) {
            xyArr.push(key)
        });
        
        this.setState({
            title: storeData.title,
            chartData: storeData.data,
            axisArr: xyArr,
            innerField: storeData.innerField,
            innerDimension: storeData.innerDimension,
            outerField: storeData.outerField,
            outerDimension: storeData.outerDimension,
            field: storeData.field,
            dimension: storeData.dimension,
            color: storeData.colors,
            haveData: true,
            legend: storeData.legend ? storeData.legend : 'right-top',
            padding: storeData.padding && storeData.padding != '' ? storeData.padding.split(',') : [0,90,0,0], // 默认legend在top位置,
        })
    }

    // 改变全局变量
    onGlobalVariable = (val) => {
        const { saveAffectId } = this.props.chartStore;
        saveAffectId(this.state.chartId);
        this.props.onGlobalVariable(val);
    }
    


    render() {
        const _this = this;
        let Layer = this.props.layer;

        let chartData = this.state.chartData;

        let haveData = this.state.haveData;
        let innerField = this.state.innerField;
        let innerDimension = this.state.innerDimension;
        let outerField = this.state.outerField;
        let outerDimension = this.state.outerDimension;
        let colorArr = this.state.color;

        if (haveData && chartData.length > 0) {
            document.getElementById('chart_'+this.state.id+'_'+this.state.chartId).innerHTML = '';
            _this.setState({
                haveData: false
            });

            // 组装当前图例的维度和度量
            let xyArr = _this.state.axisArr;
            let colorArrInner = colorArr.split(']')[0].split('[')[1].split(',');
            let colorArrOut = colorArr.split(']')[1].split('[')[1].split(',');
            let data = chartData.slice();
            let innerX = innerDimension;
            let outX = outerDimension;
            let y = outerField;
            
            if (data.length > 0) {
                this.setState({
                    dataNullLeft: false
                })

                // 通过 DataSet 计算百分比
                // let dv = new DataView();
                let ds = new DataSet();
                let dv = ds.createView();
                dv.source(data).transform({
                    type: 'percent',
                    field: y,
                    dimension: innerX,
                    as: 'percent'
                });
                let chart = new G2.Chart({
                    container: 'chart_' + this.state.id + '_' + this.state.chartId,
                    forceFit: true,
                    height: _this.state.height - 50,
                    padding: _this.state.padding
                });
                chart.source(dv, {
                    // percent: {
                    // formatter: function formatter(val) {
                    //     val = (val * 100).toFixed(2) + '%';
                    //     return val;
                    // }
                    // }
                });
                chart.coord('theta', {
                    radius: 0.43
                });
                chart.tooltip({
                    // triggerOn: 'click',
                    title: 'time_range',
                    useHtml:true,
                    htmlContent:function(title,items){
                        let itemContnt = '';
                        items.map((v,i) => {
                            itemContnt += '<li style="display: block;" style="padding-left: 5px;">'
                                            + '<span style="background-color:'+  v.color +';width: 6px;height: 6px;display: inline-block;border-radius: 3px;margin-right: 3px;" class="g2-tooltip-marker"></span>'
                                            + '<span style="color: #BDE9FF;">'+  v.name +'： </span>'
                                            + '<span style="color: '+  v.color +';">'+  v.value +'</span>'
                                    + '</li>';
                        })
                        return '<div class="g2-tooltip" style="padding: 5px 15px 15px 10px;background: url('+require('./images/pieDouble-tooltip-bg.png')+') no-repeat;background-size: 100% 100%;position: absolute;left: '+ items[0].x +';top:'+  items[0].y +';">'
                                + '<div class="g2-tooltip-title" style="margin-bottom: 4px;color: #6AEBFC;margin-top: 7px;">'+ title +'</div>'
                                + '<ul class="g2-tooltip-list">'
                                    + itemContnt
                                + '</ul>'
                            + '</div>';
                    }
                });
                chart.legend({
                    position: _this.state.legend,
                    offsetX: -15,
                    marker: 'square',
                    textStyle: {
                        fill: '#24C6F5', // 文本的颜色
                        fontSize: '12', // 文本大小
                    },
                });
                chart.intervalStack().position('percent').color(innerX,colorArrInner).label(innerX, {
                    offset: -10
                }).tooltip(innerX+'*percent', function(item, percent) {
                    percent = (percent * 100).toFixed(2) + '%';
                    return {
                        name: item,
                        value: percent
                    };
                });


                let outterView = chart.view();
                let dv1 = ds.createView();;
                dv1.source(data).transform({
                    type: 'percent',
                    field: y,
                    dimension: outX,
                    as: 'percent'
                });
                outterView.source(dv1, {
                    percent: {
                    formatter: function formatter(val) {
                        val = (val * 100).toFixed(2) + '%';
                        return val;
                    }
                    }
                });
                outterView.coord('theta', {
                    innerRadius: 0.49 / 0.67,
                    radius: 0.67
                });
                outterView.intervalStack().position('percent').color(outX, colorArrOut)
                .tooltip(outX+'*percent', function(item, percent) {
                    percent = (percent * 100).toFixed(2) + '%';
                    return {
                        name: item,
                        value: percent
                    };
                });
                

                chart.render();
            }

        }

        return (
            <div id={this.state.id + '_' + this.state.chartId} style={{zIndex: 99,width: this.state.width, height: this.state.height,padding: '40px 2% 10px 2%'}}>
                 <Layer width={this.state.width} title={this.state.title} chartId={this.state.chartId} choiceData={this.state.mainData} onGlobalVariable={this.onGlobalVariable}></Layer>
                <div id={'chart_' + this.state.id + '_' + this.state.chartId}></div>
                {/* { this.state.dataNull && <p style={{height: this.state.height,lineHeight: this.state.height+'px',color: '#fff',textAlign: 'center'}}> 暂无数据 </p>} */}
            </div>
        )
    }


}

export default BasicPieChart;


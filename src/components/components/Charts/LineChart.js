/**
  * @description: g2--折线图
  * @author: hj
  * @update: hj(2020-01-14)
  */

import React, { Component } from 'react';
import G2 from '@antv/g2';
import DataSet from '@antv/data-set';
import { getMediaById } from '@/service/Api/Chart';
import { observer,inject,Provider } from 'mobx-react';

let lineTimer;

 
@inject('chartStore') 
@observer
class LineChart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            params: [],
            data: [],
            forceFit: true, //forceFit自动试用宽度，默认false
            width: this.props.data.width ? this.props.data.width : '100%',   // 固定宽度    当自适应宽度开启，此功能失效
            height: this.props.data.height ? this.props.data.height - 50 : '100%',
            condition: this.props.data.condition ? this.props.data.condition : '',
            pageFlag: this.props.data.pageFlag ? this.props.data.pageFlag : '',
            nofirst: this.props.data.nofirst ? this.props.data.nofirst : '',
            color: [],
            title: '',
            chartId: this.props.data.chartId,
            id: this.props.data.componentId,
            chartData: [],
            axisArr: {},
            haveData: false,
            legend: 'top',
            padding: '',
            unit: [],

            dataNull: false, // 暂无数据

            // 全局变量
            mainData: [], // 操作组件可操作数据
            mainName: '', // 操作组件可操作当前维度
            mainFlag: false, // 是否是操作组件

            pageInitFlag: true,
            affectName: '',
            requireCondition: '',
        };
    };


    componentWillMount() { //初始化
        const _this = this;
        
        // 预览界面
        if (_this.state.pageFlag == 'preview') {
            // 判断当前组件是否是可操作组件
            const { globalData } = _this.props.chartStore;
            if (globalData.slice().length > 0) {
                globalData.slice().map((item,index) => {
                    if (item.affect == _this.state.chartId) { // 可操作组件
                        let affectName = item.affectDimension;
                        _this.setState({
                            mainFlag: true,
                            mainName: affectName
                        })
                    }
                })
            }

            _this.interfaceRequest(_this.state.chartId,_this.state.condition);
        } else { // 配置面板
            if (_this.state.nofirst == 'nofirst') {
                _this.saveHandleData();
            } else {
                _this.interfaceRequest(_this.state.chartId,_this.state.condition);
            }
        }
    }

    // 钩子函数--组件挂载完成后
    componentDidMount() { //初始化
        this.setState({
            haveData: true
        })
    }
    // 接收父组件的数据改变
    componentWillReceiveProps = (nextProps) => {
        const _this = this;
        const { globalData, affectId } = _this.props.chartStore;

        // 预览界面---判断是否是变化组件
        if (nextProps.data.pageFlag == 'preview') {
            if (nextProps.data.condition != _this.state.requireCondition && globalData.slice().length > 0) {
                globalData.slice().map((item,index) => {
                    if (item.affect == affectId && item.require == nextProps.data.chartId) { // 联动组件
                        _this.setState({
                            pageInitFlag: false
                        })
                        let condition = {};
                        Object.keys(_this.state.requireCondition).forEach(function(keys){
                            condition[keys] = keys == item.requireDimension ? nextProps.data.condition[keys] : _this.state.requireCondition[keys];
                        })
                        _this.interfaceRequest(nextProps.data.chartId,condition);
                    }
                })
            }
        }
    }

    // 数据接口请求
    interfaceRequest = (chartId,condition) => {
        this.setState({
            requireCondition: condition,
        });

        let data = {
            charId: chartId,
            condition: condition
        };
        this.getMediaById(data);
    }

    // 数据存储刷新
    saveHandleData = () => {
        const { chartArrData } = this.props.chartStore;
        if (chartArrData.slice().length > 0) {
            chartArrData.slice().map((item) => {
                if (item.id == this.state.chartId) {
                    this.setState({
                        haveData: false
                    },function() {
                        // 数据处理
                        this.handleData(item.data);
                    })
                }
            })
        }
    }


    async getMediaById(data) {
        const res = await getMediaById(data);
        if (res.data && res.data.code === 0) {
            let str = res.data;
            if (str.data && str.data.data && str.data.data.length > 0) {
                // 存储图例原始数据
                const { saveChartData } = this.props.chartStore;
                let oldData = {
                    id: this.state.chartId,
                    data: str.data.data
                }
                saveChartData(oldData);

                // 数据处理
                this.handleData(str.data.data);
            } else {
                this.setState({
                    dataNull: true
                })
            }
        } else {
            this.setState({
                dataNull: true
            })
        }
    }
    // 数据处理
    handleData = (storeAllData) => {
        let storeData = storeAllData[0];
        this.setState({
            dataNull: true
        })

        // 非预览界面
        if (this.state.pageFlag != 'preview') {
            // 存储可操作组件数据
            if (storeData.affect && storeData.affect.length > 0) {
                let obj = {
                    id: this.state.chartId,
                    label: storeData.title,
                    dimensionData: []
                }
                const { saveAffectData } = this.props.chartStore;

                let dimensionData = [];
                storeData.affect.slice().map((item,index) => {
                    dimensionData.push(item);
                })
                obj.dimensionData = dimensionData;
                saveAffectData(obj);
            }
            // 存储联动组件数据
            if (storeData.requires && storeData.requires.length > 0) {
                let obj = {
                    id: this.state.chartId,
                    label: storeData.title,
                    dimensionData: []
                }
                const { saveRequireData } = this.props.chartStore;

                let dimensionData = [];
                storeData.requires.slice().map((item,index) => {
                    dimensionData.push(item);
                })
                obj.dimensionData = dimensionData;
                saveRequireData(obj);
            }
        }

        // 页面初始化
        if (this.state.pageInitFlag) {
            if (storeData.affect &&  storeData.affect.length > 0) {
                this.setState({
                    affectName: storeData.affect[0]
                });
            }
            // 获取选择项
            if (storeData.conditionValues) {
                let selectData = storeData.conditionValues[storeData.affect[0]] ? storeData.conditionValues[storeData.affect[0]] : [];
                this.setState({
                    mainData: selectData
                });
            }
        }


        // 组装当前图例的维度和度量
        let xyArr = [];
        let chartGeoms = storeData.chartGeoms;
        let axisName = [];
        if (chartGeoms){
            axisName.push(chartGeoms[0].position.split("*")[0]);
            for (let i = 0; i < chartGeoms.length; i++){
                axisName.push(chartGeoms[i].position.split("*")[1])
            }
        }

        axisName.map((item,index) => {
            Object.keys(storeData.columnMaps).forEach(function(key){
                if (key == item) {
                    let keyName = index == 0 ? 'time' : index == 1 ? 'y1Key' : item;
                    xyArr.push({
                        key: keyName,
                        label: storeData.columnMaps[key]
                    })
                }
            });
        })

        let newData = [];
        if (storeData.data.length > 0) {
            storeData.data.map((item,index) => {
                let obj = {};
                Object.keys(item).forEach(function(key){
                    let keyName = key == axisName[0] ? 'time' : key == axisName[1] ? 'y1Key' : key;
                    obj[keyName] = item[key];
                });
                newData.push(obj);
            })
            this.setState({
                dataNull: false
            })
        }

        this.setState({
            title: storeData.title,
            chartData: newData,
            axisArr: xyArr,
            color: storeData.colors.split(","),
            legend: storeData.legend ? storeData.legend : 'top',
            padding: storeData.padding && storeData.padding != '' ? storeData.padding.split(',') : [40, 40, 40, 50], // 默认legend在top位置,
            unit: storeData.units ? storeData.units : '',
            haveData: true,
        })
    }

    // 改变全局变量
    onGlobalVariable = (val) => {
        const { saveAffectId } = this.props.chartStore;
        saveAffectId(this.state.chartId);
        this.props.onGlobalVariable(val);
    }
    


    render() {
        const _this = this;
        let Layer = this.props.layer;
        let selectVal = this.state.requireCondition[this.state.affectName] ? this.state.requireCondition[this.state.affectName] : '';


        let chartData = this.state.chartData;
        let axisArr = this.state.axisArr;
        let haveData = this.state.haveData;
        let valueArr = [];


        if (haveData && chartData.length > 0 && axisArr.length > 0) {
            document.getElementById('chart_'+this.state.id+'_'+this.state.chartId).innerHTML = '';
            _this.setState({
                haveData: false
            })

            let x = axisArr[0].key;

            let data = chartData.map((item) => {
                let obj = {};
                axisArr.map((v) => {
                    if (v.key != x) {
                        obj[v.label] = item[v.key];
                    } else {
                        obj[x] = item[x];
                    }
                })
                return obj;
            });

            axisArr.splice(0,1);
            valueArr = axisArr.map((item) => {
                return item.label;
            });
            valueArr.splice(1,1);



            let dv = new DataSet.View().source(data);
            dv.transform({
                type: 'fold',
                fields: valueArr,
                key: 'type',
                value: 'value'
            });
            let chart = new G2.Chart({
                container: 'chart_'+this.state.id+'_'+this.state.chartId,
                forceFit: true,
                height: _this.state.height,
                padding: _this.state.padding,
                // animate: false
            });
            chart.source(dv, {
                month: {
                  range: [0, 1]
                }
            });
            chart.axis(x, {
                position: 'bottom', // 设置坐标轴的显示位置，可取值 top bottom left right
                line: {
                    lineWidth: 1, // 设置线的宽度
                    stroke: '#536f8e' //  设置线的颜色
                }, // 设置坐标轴线的样式，如果值为 null，则不显示坐标轴线 图形属性
                label: {
                    // offset: 10, // 数值，设置坐标轴文本 label 距离坐标轴线的距离
                    // offsetX: 0, // 在 offset 的基础上 x 方向的偏移量
                    // offsetY: 0, // 在 offset 的基础上 y 方向的偏移量
                    // // 设置文本的显示样式，还可以是个回调函数，回调函数的参数为该坐标轴对应字段的数值
                    // rotate: -30, // 注意，旋转角度的配置不再在 textStyle 里配置
                    textStyle: {
                        textAlign: 'center', // 文本对齐方向，可取值为： start center end
                        fill: '#24C6F5', // 文本的颜色
                        fontSize: '12', // 文本大小
                        // textBaseline: 'top' // 文本基准线，可取 top middle bottom，默认为middle
                    }
                },
                title: null, // 坐标轴标题设置，如果值为 null，则不显示标题
                tickLine: {
                    lineWidth: 2, // 刻度线宽
                    stroke: '#536f8e', // 刻度线的颜色
                    length: -5, // 刻度线的长度
                }, // 坐标轴刻度线配置
            });
            let prevNum = 0;
            chart.axis('value', {
                position: 'left', // 设置坐标轴的显示位置，可取值 top bottom left right
                line: {
                    lineWidth: 1, // 设置线的宽度
                    stroke: '#536f8e' //  设置线的颜色
                }, // 设置坐标轴线的样式，如果值为 null，则不显示坐标轴线 图形属性
                label: {
                    offset: 0, // 数值，设置坐标轴文本 label 距离坐标轴线的距离
                    offsetX: -10, // 在 offset 的基础上 x 方向的偏移量
                    offsetY: 0, // 在 offset 的基础上 y 方向的偏移量
                    // 设置文本的显示样式，还可以是个回调函数，回调函数的参数为该坐标轴对应字段的数值
                    textStyle: {
                        textAlign: 'end', // 文本对齐方向，可取值为： start center end
                        fill: '#24C6F5', // 文本的颜色
                        fontSize: '12', // 文本大小
                    },
                    // 使用 formatter 回调函数
                    formatter: val => {
                        prevNum++;
                        if (prevNum % 2 != 0) {
                            return val + ' '+  _this.state.unit;
                        }
                    }
                },
                title: null, // 坐标轴标题设置，如果值为 null，则不显示标题
                tickLine: null,
                grid: {
                    align: 'center', // 声明网格顶点从两个刻度中间开始，默认从刻度点开始
                    type: 'line', // 声明网格的类型，line 表示线，polygon 表示矩形框
                    // 当网格类型 type 为 line 时，使用 lineStyle 设置样式
                    lineStyle: {
                      stroke: '#21355f', // 网格线的颜色
                      lineWidth: 1, // 网格线的粗细
                      lineDash: [4, 4 ] // 网格线的虚线配置，第一个参数描述虚线的实部占多少像素，第二个参数描述虚线的虚部占多少像素
                    },
                    hideFirstLine: true, // 是否隐藏第一条网格线，默认为 false
                    hideLastLine: true, // 是否隐藏最后一条网格线，默认为 false
                }, // 坐标轴栅格线的配置信息，默认只有左边的坐标轴带有栅格线，null 为不显示。
            });
            chart.legend({
                useHtml: true,
                clickable: false,
                itemGap: 15,
                position: this.state.legend,
                containerTpl: '<div class="g2-legend">' +
                                '<ul class="g2-legend-list" style="list-style-type:none;margin:0;padding:0;"></ul>' +
                            '</div>',
                itemTpl: (value, color1, checked, index) => {
                    let content = '';
                    let color = this.state.color[index] ? this.state.color[index] : this.state.color[0];
                    checked = checked ? 'checked' : 'unChecked';
                    content += '<li class="g2-legend-list-item item-' + index + ' ' + checked + '" '+
                                'data-value="' + value + '" '+
                                'data-color=' + color + '" '+
                                'style="cursor: pointer;font-size: 12px;float: right;">' +
                                '<span width=150 style="border: none;padding:0;">'+
                                    '<span class="g2-legend-marker" style="background: ' + color + ';margin-top: -4px;"></span>' +
                                    '<i class="g2-legend-marker2" style="width:10px;height:10px;display:inline-block;margin-right:5px;border-radius: 5px;border: 2px solid ' + color + ';"></i>'+
                                '</span>' +
                                '<span class="g2-legend-text" style="color: #24C6F5;">'+ value +'</span>'+
                            '</li>';
                    return content;
                },
                offsetX: 15,
                offsetY: -5,
                'g2-legend': {
                    marginLeft: '100px',
                    marginTop: '0px'
                },
                'g2-legend-list': {
                    border: 'none',
                    float: 'right',
                    marginRight: '20px',
                },
                'g2-legend-marker': {
                    width: '22px',
                    height: '2px',
                    borderRadius: 0,
                    marginRight: 0
                }
            });
            chart.tooltip({
                crosshairs: {
                    type: 'line',
                    style: {
                        // 图形样式
                        fill: '', // 填充的颜色
                        stroke: '#666', // 边框的颜色
                        strokeOpacity: 1, // 边框颜色的透明度，数值为 0 - 1 范围
                        fillOpacity: 1, // 填充的颜色透明度，数值为 0 - 1 范围
                        lineWidth: 1, // 边框的粗细
                        lineDash: 'solid' // 线的虚线样式
                    }
                },
                triggerOn: 'click',
                useHtml:true,
                htmlContent:function(title,items){
                    let itemContnt = '';
                    items.map((v,i) => {
                        itemContnt += '<li style="display: block;" style="padding-left: 5px;">'
                                    + '<span style="background-color:'+  v.color +';width: 6px;height: 6px;display: inline-block;border-radius: 3px;margin-right: 3px;" class="g2-tooltip-marker"></span>'
                                    + '<span style="color: #BDE9FF;">'+  v.name +'： </span>'
                                    + '<span style="color: '+  v.color +';">'+  v.value +'</span>'
                            + '</li>';
                    })
                    return '<div class="g2-tooltip" style="padding: 5px 15px 15px 15px;position: absolute;left: '+ items[0].x +';top:'+  items[0].y +';">'
                            + '<div class="g2-tooltip-title" style="margin-bottom: 4px;color: #6AEBFC;margin-top: 7px;">'+ title +'</div>'
                            + '<ul class="g2-tooltip-list">'
                                + itemContnt
                            + '</ul>'
                        + '</div>';
                }
            });

            chart.point().position(x+'*value').size(4).color('type',this.state.color).shape('circle');
            chart.line().position(x+'*value').color('type',this.state.color).size(2);

            chart.render();

            // tooltip自动轮播
            setTimeout(() => {
                let num = 0;
                //去掉定时器的方法  
                lineTimer = lineTimer + this.state.id+'_'+this.state.chartId; 
                if (lineTimer) window.clearInterval(lineTimer);

                lineTimer = window.setInterval(function() {
                    if (num == data.length) {
                        num = 0;
                    }

                    let point = chart.getXY({time: data[num][x], value: data[num][axisArr[0].label]});
                    chart.showTooltip(point);
                    num++;
                },2000)
            }, 20);
        }

        return (
            <div id={this.state.id+'_'+this.state.chartId} style={{zIndex: 99,width: this.state.width,height: this.state.height,padding: '40px 2% 10px 2%'}}>
                <Layer width={this.state.width} 
                        title={this.state.title} 
                        chartId={this.state.chartId} 
                        choiceData={this.state.mainData} 
                        selectVal={selectVal}
                        onGlobalVariable={this.onGlobalVariable}
                ></Layer>
                <div id={'chart_'+this.state.id+'_'+this.state.chartId}></div>
                { this.state.dataNull && <p style={{height: this.state.height,lineHeight: this.state.height+'px',color: '#fff',textAlign: 'center'}}> 暂无数据 </p>}
            </div>
        )
    }


}
export default LineChart;


/**
  * @description: 基础表格
  * @author: hj
  * @update: hj(2020-01-14)
  */

import React, {Component} from 'react';
// import {Table} from '@alifd/next';
import styles from './index.module.scss';
import {getMediaById} from '@/service/Api/Chart';
import { observer,inject,Provider } from 'mobx-react';

 
@inject('chartStore') 
@observer


class Table extends Component {
    constructor(props) {
        super(props);
        this.state = {
            params: [],
            data: [],
            forceFit: true, //forceFit自动试用宽度，默认false
            width: this.props.data.width ? this.props.data.width : '100%',   // 固定宽度    当自适应宽度开启，此功能失效
            height: this.props.data.height ? this.props.data.height - 50 : '100%',
            condition: this.props.data.condition ? this.props.data.condition : '',
            pageFlag: this.props.data.pageFlag ? this.props.data.pageFlag : '',
            nofirst: this.props.data.nofirst ? this.props.data.nofirst : '',
            color: this.props.data.color,
            title: '',
            chartId: this.props.data.chartId,
            id: this.props.data.componentId,
            chartData: [],
            axisArr: {},
            columnMaps:[],
            haveData: false
        };
    };


    componentWillMount() { //初始化
        const { affectData, relationIdData } = this.props.chartStore;
        // console.log('affectData-----Table------',affectData.slice());
        // console.log('relationIdData-----Table------',relationIdData.slice());

        // 预览界面
        if (this.state.pageFlag == 'preview') {
            // console.log('预览界面');
            if (affectData.slice().length > 0 && relationIdData.slice().length > 0) { // 有数据联动
                // console.log('有数据联动');
                if (relationIdData.slice().indexOf(this.state.chartId) > -1) {
                    // console.log('有数据联动----0000-----');
                    this.interfaceRequest();
                } else {
                    // console.log('有数据联动----1111-----');
                    this.saveHandleData();
                }
            } else {
                // console.log('无数据联动');
                this.interfaceRequest();
            }
        } else { // 配置面板
            // console.log('配置面板');
            if (this.state.nofirst == 'nofirst') {
                // console.log('nofirst');
                this.saveHandleData();
            } else {
                // console.log('first');
                this.interfaceRequest();
            }
        }
    }

    // 钩子函数--组件挂载完成后
    componentDidMount() { //初始化
        this.setState({
            haveData: true
        })
    }
    // 数据存储刷新
    saveHandleData = () => {
        const { chartArrData } = this.props.chartStore;
        if (chartArrData.slice().length > 0) {
            chartArrData.slice().map((item) => {
                if (item.id == this.state.chartId) {
                    this.setState({
                        haveData: false
                    },function() {
                        // 数据处理
                        this.handleData(item.data);
                    })
                }
            })
        }
    }

    // 数据接口请求
    interfaceRequest = () => {
        let data = {
            charId: this.state.chartId,
            condition: this.state.condition
        };
        this.getMediaById(data);
    }

    async getMediaById(data) {
        const res = await getMediaById(data);
        if (res.data && res.data.code === 0) {
            let str = res.data;
            if (str.data && str.data.data && str.data.data.length > 0) {
                // 存储图例原始数据
                const { saveChartData } = this.props.chartStore;
                let oldData = {
                    id: this.state.chartId,
                    data: str.data.data
                }
                saveChartData(oldData);

                // 数据处理
                this.handleData(str.data.data);
            }
        }
    }
    // 数据处理
    handleData = (storeAllData) => {
        let storeData = storeAllData[0];
        // 判断是否是联动操作组件并存储
        if (this.state.pageFlag == 'preview' && storeData.affect && !storeData.affect.flag) {
            const { saveAffectData } = this.props.chartStore;

            let affectData = {
                id: this.state.chartId,
                field: storeData.affect.relevanceFields ? storeData.affect.relevanceFields : []
            }
            saveAffectData(affectData);
        }


        // 组装当前图例的维度和度量
        let xyArr = storeData.columns ? storeData.columns.split(',') : [];
        // Object.keys(storeData.data[0]).forEach(function (key) {
        //     xyArr.push(key)
        // });
        // if (xyArr.indexOf('index') > -1) {
        //     let index = xyArr.indexOf('index');
        //     xyArr.splice(index,1);
        //     xyArr.unshift('index');
        // }
        

        // 判断是否是联动组件
        if (this.state.pageFlag == 'preview' && storeData.affect && storeData.affect.flag) {
            let relationFlag = false;
            let filedYArr = xyArr.map((item,index) => {
                return item;
            });
            // filedYArr.splice(0,1);
            
            const { affectData } = this.props.chartStore;
            affectData.slice().map((item,index) => {
                item.field.map((v,i) => {
                    if (filedYArr.indexOf(v) > -1) {
                        relationFlag = true;
                    }
                })
            })

            // 存储联动图例id
            if (relationFlag) {
                const { saveRelationIdData } = this.props.chartStore;

                let id = this.state.chartId;
                saveRelationIdData(id);
            }
        }
        // let xyArr = [];
        // Object.keys(storeData.data[0]).forEach(function (key) {
        //     xyArr.push(key)
        // });
        // let index = xyArr.indexOf('index');
        // xyArr.splice(index,1);
        // xyArr.unshift('index');

        this.setState({
            title: storeData.title,
            chartData: storeData.data,
            axisArr: xyArr,
            columnMaps:storeData.columnMaps,
            haveData: true,
            affect: storeData.affect.flag
        })
    }
    

    render() {
        let Layer = this.props.layer;
        
        const {columnMaps} = this.state;
        const {chartData} = this.state;
        const {axisArr} = this.state;

        // 表头
        let colnums = [];
        if (axisArr.length > 0) {
            axisArr.map((v,i) => {
                Object.keys(columnMaps).forEach(function(key){
                // for (let key in columnMaps) {
                    if (key == v) {
                        colnums.push({
                            "key": v,
                            "value": columnMaps[v]
                        })
                    }
                })
            })
        }
        
        // 表数据
        let colnumsData = [];
        chartData.map((item,index) => {
            let objData = [];
            axisArr.map((v,i) => {
                objData.push({
                    "key": v,
                    "value": item[v]
                })
            })
            colnumsData.push(objData);
        })

        return (
            
            <div id={this.state.id+'_'+this.state.chartId} style={{zIndex: 99,width: this.state.width,height: this.state.height,padding: '6% 2%'}}>
                <Layer width={this.state.width} title={this.state.title}></Layer>
                <table className={styles.tableBox}>
                    <tr>
                        {colnums.map((item,index) => {
                            return (
                                <th> {item.value} </th>
                            )
                        })}
                    </tr>
                    {colnumsData.length > 0 ? colnumsData.map((item,index) => {
                        return (
                            <tr>
                                {item.map((v,i) => {
                                    return (
                                        <td> {v.value} </td>
                                    )
                                    
                                })}
                            </tr>
                        )
                    }) : <p style={{color: '#fff'}}> 暂无数据 </p>}
                </table>
                {/* <Table dataSource={chartData} 
                    useVirtual 
                    fixedHeader={true}
                    maxBodyHeight={this.state.height-40}
                    hasBorder={false}
                    stickyHeader={true}
                    rowProps={setRowProps} 
                    cellProps={setCellProps} 
                    className={styles.tableBox}
                    style={{width: "98%",height: '100%'}}>
                    {
                        colnums.map(
                            o => {
                                return (
                                    <Table.Column align="center" title={o.value} dataIndex={o.key}/>
                                )
                            }
                        )
                    }

                </Table> */}
                
            </div>
        );
    }


}

export default Table;


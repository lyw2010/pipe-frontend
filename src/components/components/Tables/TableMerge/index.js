/**
  * @description: 合并表格
  * @author: hj
  * @update: hj(2020-01-29)
  */

import React, {Component} from 'react';
import styles from './index.module.scss';
import {getMediaById} from '@/service/Api/Chart';
import { observer,inject,Provider } from 'mobx-react';

let tableTimer;

 
@inject('chartStore') 
@observer


class TableMerge extends Component {
    constructor(props) {
        super(props);
        this.state = {
            params: [],
            data: [],
            forceFit: true, //forceFit自动试用宽度，默认false
            width: this.props.data.width ? this.props.data.width : '100%',   // 固定宽度    当自适应宽度开启，此功能失效
            height: this.props.data.height ? this.props.data.height: '100%',
            condition: this.props.data.condition ? this.props.data.condition : '',
            pageFlag: this.props.data.pageFlag ? this.props.data.pageFlag : '',
            nofirst: this.props.data.nofirst ? this.props.data.nofirst : '',
            color: this.props.data.color,
            title: '',
            chartId: this.props.data.chartId,
            id: this.props.data.componentId,
            chartData: [],
            axisArr: {},
            columnMaps:[],
            haveData: false,

            dataNull: false, // 暂无数据

            // 全局变量
            mainData: [], // 操作组件可操作数据
            mainName: '', // 操作组件可操作当前维度
            mainFlag: false, // 是否是操作组件

            pageInitFlag: true,
            affectName: '',
            requireCondition: '',
        };
    };


    componentWillMount() { //初始化
        const _this = this;
        
        // 预览界面
        if (_this.state.pageFlag == 'preview') {
            // 判断当前组件是否是可操作组件
            const { globalData } = _this.props.chartStore;
            if (globalData.slice().length > 0) {
                globalData.slice().map((item,index) => {
                    if (item.affect == _this.state.chartId) { // 可操作组件
                        let affectName = item.affectDimension;
                        _this.setState({
                            mainFlag: true,
                            mainName: affectName
                        })
                    }
                })
            }

            _this.interfaceRequest(_this.state.chartId,_this.state.condition);
        } else { // 配置面板
            if (_this.state.nofirst == 'nofirst') {
                _this.saveHandleData();
            } else {
                _this.interfaceRequest(_this.state.chartId,_this.state.condition);
            }
        }
    }

    // 钩子函数--组件挂载完成后
    componentDidMount() { //初始化
        this.setState({
            haveData: true
        })
    }
    // 接收父组件的数据改变
    componentWillReceiveProps = (nextProps) => {
        const _this = this;
        const { globalData, affectId } = _this.props.chartStore;

        // 预览界面---判断是否是变化组件
        if (nextProps.data.pageFlag == 'preview') {
            if (nextProps.data.condition != _this.state.requireCondition && globalData.slice().length > 0) {
                globalData.slice().map((item,index) => {
                    if (item.affect == affectId && item.require == nextProps.data.chartId) { // 联动组件
                        _this.setState({
                            pageInitFlag: false,
                            // dataNull: true,
                            haveData: false,
                            columnMaps: {},
                            chartData: [],
                            axisArr: []
                        })
                        
                        let condition = {};
                        Object.keys(_this.state.requireCondition).forEach(function(keys){
                            condition[keys] = keys == item.requireDimension ? nextProps.data.condition[keys] : _this.state.requireCondition[keys];
                        })
                        _this.interfaceRequest(nextProps.data.chartId,condition);
                    }
                })
            }
        }
    }

    // 数据接口请求
    interfaceRequest = (chartId,condition) => {
        this.setState({
            requireCondition: condition,
        });

        let data = {
            charId: chartId,
            condition: condition
        };
        this.getMediaById(data);
    }
    
    // 数据存储刷新
    saveHandleData = () => {
        const { chartArrData } = this.props.chartStore;
        if (chartArrData.slice().length > 0) {
            chartArrData.slice().map((item) => {
                if (item.id == this.state.chartId) {
                    this.setState({
                        haveData: true
                    },function() {
                        // 数据处理
                        this.handleData(item.data);
                    })
                }
            })
        }
    }

    async getMediaById(data) {
        const res = await getMediaById(data);
        if (res.data && res.data.code === 0) {
            let str = res.data;
            if (str.data && str.data.data && str.data.data.length > 0) {
                // 存储图例原始数据
                const { saveChartData } = this.props.chartStore;
                let oldData = {
                    id: this.state.chartId,
                    data: str.data.data
                }
                saveChartData(oldData);

                // 数据处理
                this.handleData(str.data.data);
            } else {
                this.setState({
                    dataNull: true
                })
            }
        } else {
            this.setState({
                dataNull: true
            })
        }
    }
    // 数据处理
    handleData = (storeAllData) => {
        let storeData = storeAllData[0];
        this.setState({
            dataNull: true
        });

        // 非预览界面
        if (this.state.pageFlag != 'preview') {
            // 存储可操作组件数据
            if (storeData.affect && storeData.affect.length > 0) {
                let obj = {
                    id: this.state.chartId,
                    label: storeData.title,
                    dimensionData: []
                }
                const { saveAffectData } = this.props.chartStore;

                let dimensionData = [];
                storeData.affect.slice().map((item,index) => {
                    dimensionData.push(item);
                })
                obj.dimensionData = dimensionData;
                saveAffectData(obj);
            }
            // 存储联动组件数据
            if (storeData.requires && storeData.requires.length > 0) {
                let obj = {
                    id: this.state.chartId,
                    label: storeData.title,
                    dimensionData: []
                }
                const { saveRequireData } = this.props.chartStore;

                let dimensionData = [];
                storeData.requires.slice().map((item,index) => {
                    dimensionData.push(item);
                })
                obj.dimensionData = dimensionData;
                saveRequireData(obj);
            }
        }

        // 页面初始化
        if (this.state.pageInitFlag) {
            if (storeData.affect &&  storeData.affect.length > 0) {
                this.setState({
                    affectName: storeData.affect[0]
                });
            }
            // 获取选择项
            if (storeData.conditionValues) {
                let selectData = storeData.conditionValues[storeData.affect[0]] ? storeData.conditionValues[storeData.affect[0]] : [];
                this.setState({
                    mainData: selectData
                });
            }
        }


        // 组装当前图例的维度和度量
        let xyArr = storeData.columns ? storeData.columns.split(',') : [];
        if (storeData.data.length > 0) {
            // Object.keys(storeData.data[0]).forEach(function (key) {
            //     xyArr.push(key)
            // });
            // if (xyArr.indexOf('index') > -1) {
            //     let index = xyArr.indexOf('index');
            //     xyArr.splice(index,1);
            //     xyArr.unshift('index');
            // }
            this.setState({
                dataNull: false
            })
        }

        this.setState({
            title: storeData.title,
            chartData: storeData.data,
            axisArr: storeData.columnMerge,
            columnMaps: storeData.columnMaps,
            color: storeData.colors.split(','),
            haveData: true
        })
    }

    // 改变全局变量
    onGlobalVariable = (val) => {
        const { saveAffectId } = this.props.chartStore;
        saveAffectId(this.state.chartId);
        this.props.onGlobalVariable(val);
    } 
    
    



    render() {
        const _this = this;
        let Layer = this.props.layer;
        let selectVal = this.state.requireCondition[this.state.affectName] ? this.state.requireCondition[this.state.affectName] : '';
        
        const {haveData} = this.state;
        const {columnMaps} = this.state;
        const {chartData} = this.state;
        const {axisArr} = this.state;
        
        let colnums = [];
        let colnumsData = [];
        if (haveData && chartData.length > 0) {
            // 表数据
            let newData = [];
            chartData.map((item,index) => {
                Object.keys(item).forEach(function(itemKey){
                    Object.keys(columnMaps).forEach(function(key){
                        if (key == itemKey) {
                            newData.push({
                                key: key,
                                name: columnMaps[key],
                                number: item[key]
                            })
                        }
                    })
                })
            })



            newData.map((item,index) => {
                if (item.name == '累计发放奖金总额') {
                    colnums.push(item);
                }
            })
            axisArr.map((itemData) => {
                Object.keys(itemData).forEach(function(key){
                    newData.map((item,index) => {
                        if (item.key == key) {
                            let obj = {
                                name: item.name,        
                                number: item.number,
                                data: []                    
                            }
                            let data = [];
                            itemData[key].map((k,j) => {
                                newData.map((v,i) => {
                                    if (v.key == k) {
                                        data.push({
                                            name: v.name,        
                                            number: v.number,
                                        })
                                    }
                                })
                            })
                            obj.data = data;
                            colnumsData.push(obj);
                        }
                    })
                })
            })
            
            console.log('colnumsData=========',colnumsData);


            
            
        }
            
        let maxH = 0;
        if ((colnumsData.length * 2 + 1) * 40 < this.state.height - 90) {
            maxH = (colnumsData.length * 2 + 1) * 40
        } else {
            maxH = Math.ceil((this.state.height - 90) / 40) * 40;
        }
        let lenNum = Math.ceil(maxH / 40) - 1;
        let tdWidth = Math.floor(100 / colnums.length * 2) + '%';
        


        return (
            
            <div id={this.state.id+'_'+this.state.chartId} style={{zIndex: 99,width: this.state.width,height: this.state.height,padding: '50px 3% 30px 3%'}}>
                <Layer width={this.state.width} 
                        title={this.state.title} 
                        chartId={this.state.chartId} 
                        choiceData={this.state.mainData} 
                        selectVal={selectVal}
                        onGlobalVariable={this.onGlobalVariable}
                ></Layer>
                <div style={{display: 'block',width: '100%',overflow: 'hidden'}}>
                    <table className={styles.tableBox} cellpadding="0" cellspacing="0">
                        {colnums.map((item,index) => {
                            return (
                                <tr>
                                    <th style={{width: tdWidth + '%',backgroundColor: 'transparent',color: this.state.color[1]}}> {item.name} </th>
                                    <th style={{width: tdWidth + '%',backgroundColor: 'rgba(7,105,164,0.4)',color: this.state.color[1],fontSize: '20px',fontFamily: 'myNumberFamily'}}> {item.number} </th>
                                </tr>
                            )
                        })}
                        {colnumsData.length > 0 && colnumsData.map((item,index) => {
                            return (
                                <tr>
                                    <td style={{width: tdWidth + '%',backgroundColor: 'transparent',color: this.state.color[1]}}> 
                                        <p style={{lineHeight: '70px'}}> {item.name} </p>
                                        <p style={{color: '#fff',fontSize: '20px',fontFamily: 'myNumberFamily',lineHeight: '20px'}}> {item.number} </p>
                                    </td>
                                    <td style={{width: tdWidth / 2 + '%',color: this.state.color[1]}}>
                                    {item.data.length > 0 && item.data.map((v,i) => {
                                        return (
                                            <p style={{backgroundColor: i == 0 ? 'rgba(7,105,164,0.4)' : 'rgba(36,206,255,0.15)',borderBottom: i == 0 ? '1px solid #72f1f8' : ''}}>
                                                <span style={{borderRight: '1px solid #72f1f8'}}> {v.name} </span>
                                                <span style={{color: '#fff',fontSize: '18px',fontFamily: 'myNumberFamily'}}> {v.number} </span>
                                            </p>
                                        )
                                    })}
                                    </td>
                                    
                                </tr>
                            )
                        })}
                    </table>
                </div>
                { this.state.dataNull && <p style={{height: '100px',lineHeight: '100px',color: '#fff',textAlign: 'center'}}> 暂无数据 </p>}
            </div>
        );
    }


}

export default TableMerge;


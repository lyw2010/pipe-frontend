/**
  * @description: 云朵动画组件
  * @author: hj
  * @update: hj(2020-01-14)
  */

import React, { Component } from 'react';
import styles from './index.module.scss';
import { getMediaById } from '@/service/Api/Chart';
import { observer,inject,Provider } from 'mobx-react';

 
@inject('chartStore') 
@observer


class Cloud extends Component {
    constructor(props) {
        super(props);
        this.state = {
            params: [],
            data: [],
            forceFit: true, //forceFit自动试用宽度，默认false
            width: this.props.data.width ? this.props.data.width : '100%',   // 固定宽度    当自适应宽度开启，此功能失效
            height: this.props.data.height ? this.props.data.height - 5 : '100%',
            condition: this.props.data.condition ? this.props.data.condition : '',
            pageFlag: this.props.data.pageFlag ? this.props.data.pageFlag : '',
            nofirst: this.props.data.nofirst ? this.props.data.nofirst : '',
            color: this.props.data.color,
            title: '',
            chartId: this.props.data.chartId,
            id: this.props.data.componentId,
            chartData: [],
            axisArr: {},
            haveData: false,

            dataNull: false, // 暂无数据

            // 全局变量
            mainData: [], // 操作组件可操作数据
            mainName: '', // 操作组件可操作当前维度
            mainFlag: false, // 是否是操作组件
        };
    };


    componentWillMount() { //初始化
        const _this = this;
        
        // 预览界面
        if (_this.state.pageFlag == 'preview') {
            // 判断当前组件是否是可操作组件
            const { globalData } = _this.props.chartStore;
            if (globalData.slice().length > 0) {
                globalData.slice().map((item,index) => {
                    if (item.affect == _this.state.chartId) { // 可操作组件
                        let affectName = item.affectDimension;
                        _this.setState({
                            mainFlag: true,
                            mainName: affectName
                        })
                    }
                })
            }

            _this.interfaceRequest(_this.state.chartId,_this.state.condition);
        } else { // 配置面板
            if (_this.state.nofirst == 'nofirst') {
                _this.saveHandleData();
            } else {
                _this.interfaceRequest(_this.state.chartId,_this.state.condition);
            }
        }
    }

    // 钩子函数--组件挂载完成后
    componentDidMount() { //初始化
        this.setState({
            haveData: true
        })
    }
    // 接收父组件的数据改变
    componentWillReceiveProps = (nextProps) => {
        const _this = this;
        const { globalData } = _this.props.chartStore;

        // 预览界面---判断是否是变化组件
        if (nextProps.data.pageFlag == 'preview') {
            if (nextProps.data.condition != this.props.data.condition && globalData.slice().length > 0) {
                globalData.slice().map((item,index) => {
                    if (item.affectFlag && item.require == nextProps.data.chartId) { // 联动组件
                        let key = item.requireDimension;
                        let condition = {};
                        Object.keys(nextProps.data.condition).forEach(function(keys){
                            condition[key] = nextProps.data.condition[keys];
                        })
                        _this.interfaceRequest(nextProps.data.chartId,condition);
                    }
                })
            }
        }
    }

    // 数据接口请求
    interfaceRequest = (chartId,condition) => {
        let data = {
            charId: chartId,
            condition: condition
        };
        this.getMediaById(data);
    }
    
    // 数据存储刷新
    saveHandleData = () => {
        const { chartArrData } = this.props.chartStore;
        if (chartArrData.slice().length > 0) {
            chartArrData.slice().map((item) => {
                if (item.id == this.state.chartId) {
                    this.setState({
                        haveData: false
                    },function() {
                        // 数据处理
                        this.handleData(item.data);
                    })
                }
            })
        }
    }

    async getMediaById(data) {
        const res = await getMediaById(data);
        if (res.data && res.data.code === 0) {
            let str = res.data;
            if (str.data && str.data.data && str.data.data.length > 0) {
                // 存储图例原始数据
                const { saveChartData } = this.props.chartStore;
                let oldData = {
                    id: this.state.chartId,
                    data: str.data.data
                }
                saveChartData(oldData);

                // 数据处理
                this.handleData(str.data.data);
            } else {
                this.setState({
                    dataNull: true
                })
            }
        } else {
            this.setState({
                dataNull: true
            })
        }
    }
    // 数据处理
    handleData = (storeAllData) => {
        let storeData = storeAllData[0];

        // 存储可操作组件数据
        if (storeData.affect && storeData.affect.length > 0) {
            let obj = {
                id: this.state.chartId,
                label: storeData.title,
                dimensionData: []
            }
            const { saveAffectData } = this.props.chartStore;

            let dimensionData = [];
            storeData.affect.slice().map((item,index) => {
                dimensionData.push(item);
            })
            obj.dimensionData = dimensionData;
            saveAffectData(obj);

            let newNameData = [];
            let mainName = this.state.mainName;
            storeData.data.map((item,index) => {
                Object.keys(item).forEach(function(key){
                    if (key == mainName) {
                        newNameData.push(item[key]);
                    }
                })
            })
            console.log('Cloud----',newNameData);
            this.setState({
                mainData: newNameData
            })
        }
        // 存储联动组件数据
        if (storeData.requires && storeData.requires.length > 0) {
            let obj = {
                id: this.state.chartId,
                label: storeData.title,
                dimensionData: []
            }
            const { saveRequireData } = this.props.chartStore;

            let dimensionData = [];
            storeData.requires.slice().map((item,index) => {
                dimensionData.push(item);
            })
            obj.dimensionData = dimensionData;
            saveRequireData(obj);
        }


        // 组装当前图例的维度和度量
        let xyArr = [];
        let axisName = [];
        Object.keys(storeData.columnMaps).forEach(function (key) {
            axisName.push({key});
            xyArr.push({
                key: key,
                label: storeData.columnMaps[key]
            });
        });
        
        let {data} = storeData;
        this.setState({
            title: storeData.title,
            chartData: data,
            axisArr: xyArr,
            haveData: true,
            dataNull: false
        })
    }

    // 改变全局变量
    onGlobalVariable = (val) => {
        this.props.onGlobalVariable(val);
    }
    



    render() {
        const _this = this;
        let Layer = this.props.layer;

        let chartData = _this.state.chartData;
        let axisArr = _this.state.axisArr;

        let infoData = [];
        let today_count = {
            title: '',
            value: ''
        }
        if (chartData.length > 0) {
            axisArr.map((item,index) => {
                Object.keys(chartData[0]).forEach(function (key) {
                    if (key == item.key) {
                        if (key == 'message_count') {
                            today_count = {
                                title: item.label,
                                value: chartData[0][key],
                                explain: ''
                            }
                        } else {
                            infoData.push({
                                title: item.label,
                                value: chartData[0][key]
                            })
                        }
                    }
                });
            })
        }

        return (
            <div id={this.state.id+'_'+this.state.chartId} style={{zIndex: 9999,width: this.state.width,height: this.state.height,padding: '6% 2% 32% 2%'}}>
                <Layer width={this.state.width} title={this.state.title} chartId={this.state.chartId} choiceData={this.state.mainData} onGlobalVariable={this.onGlobalVariable}></Layer>
                <div className={styles.cloudBox}  id={'chart_'+this.state.id+'_'+this.state.chartId} style={{height: this.state.height * 0.62}}>
                    <div className={styles.cloud}>
                        <p className={styles.title}> {today_count.title} </p>
                        <p className={styles.number}> {today_count.value} </p>
                        { this.state.dataNull && <p style={{color: '#fff',textAlign: 'center'}}> 暂无数据 </p>}
                    </div>
                    {
                        infoData.length > 0 && infoData.map((item,index) => {
                            return (
                                <div className={styles.cloudCard} 
                                    style={{
                                        left: index == 0 ? '5%' : index == 1 ? '25%' : index == 2 ? '55%' : '75%',
                                        // top: (index == 0 || index == 3) ? '40%' : '20%'
                                    }}>
                                    <p className={styles.title}> {item.title} </p>
                                    <p className={styles.number}> {item.value} </p>
                                    {/* <p className={styles.icon}>  </p> */}
                                </div>
                            )
                        })
                    }
                    
                </div>
            </div>

        )
    }


}
export default Cloud;


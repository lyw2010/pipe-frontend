/**
 * @description: 默认视频组件
 * @author: hj
 * @update: hj(2020-06-11)
 */

import React, { Component } from 'react';
import styles from './index.module.scss';
import { getMediaById } from '@/service/Api/Chart';
import { observer,inject,Provider } from 'mobx-react';


@inject('chartStore')
@observer


class DefaultVideo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            params: [],
            data: [],
            forceFit: true, //forceFit自动试用宽度，默认false
            width: this.props.data.width ? this.props.data.width : '100%',   // 固定宽度    当自适应宽度开启，此功能失效
            height: this.props.data.height ? this.props.data.height - 60 : '100%',
            condition: this.props.data.condition ? this.props.data.condition : '',
            pageFlag: this.props.data.pageFlag ? this.props.data.pageFlag : '',
            nofirst: this.props.data.nofirst ? this.props.data.nofirst : '',
            color: this.props.data.color,
            title: '',
            chartId: this.props.data.chartId,
            id: this.props.data.componentId,
            chartData: [],
            axisArr: {},
            haveData: false,

            dataNull: false, // 暂无数据

            // 全局变量
            mainData: [], // 操作组件可操作数据
            mainName: '', // 操作组件可操作当前维度
            mainFlag: false, // 是否是操作组件

            // 视频当前播放顺序
            vIndex: 0,
            activeFirstFlag: true, // 视频是否是首次播放
        };
    };


    componentWillMount() { //初始化
        const _this = this;

        // 预览界面
        if (_this.state.pageFlag == 'preview') {
            // 判断当前组件是否是可操作组件
            const { globalData } = _this.props.chartStore;
            if (globalData.slice().length > 0) {
                globalData.slice().map((item,index) => {
                    if (item.affect == _this.state.chartId) { // 可操作组件
                        let affectName = item.affectDimension;
                        _this.setState({
                            mainFlag: true,
                            mainName: affectName
                        })
                    }
                })
            }

            _this.interfaceRequest(_this.state.chartId,_this.state.condition);
        } else { // 配置面板
            if (_this.state.nofirst == 'nofirst') {
                _this.saveHandleData();
            } else {
                _this.interfaceRequest(_this.state.chartId,_this.state.condition);
            }
        }
    }

    // 钩子函数--组件挂载完成后
    componentDidMount() { //初始化
        this.setState({
            haveData: true
        })
    }
    // 接收父组件的数据改变
    componentWillReceiveProps = (nextProps) => {
        const _this = this;
        const { globalData, affectId } = _this.props.chartStore;

        // 预览界面---判断是否是变化组件
        if (nextProps.data.pageFlag == 'preview') {
            if (nextProps.data.condition != _this.state.requireCondition && globalData.slice().length > 0) {
                globalData.slice().map((item,index) => {
                    if (item.affect == affectId && item.require == nextProps.data.chartId) { // 联动组件
                        _this.setState({
                            pageInitFlag: false
                        })
                        let condition = {};
                        Object.keys(_this.state.requireCondition).forEach(function(keys){
                            condition[keys] = keys == item.requireDimension ? nextProps.data.condition[keys] : _this.state.requireCondition[keys];
                        })
                        _this.interfaceRequest(nextProps.data.chartId,condition);
                    }
                })
            }
        }
    }

    // 数据接口请求
    interfaceRequest = (chartId,condition) => {
        this.setState({
            requireCondition: condition,
        });

        let data = {
            charId: chartId,
            condition: condition
        };
        this.getMediaById(data);
    }

    // 数据存储刷新
    saveHandleData = () => {
        const { chartArrData } = this.props.chartStore;
        if (chartArrData.slice().length > 0) {
            chartArrData.slice().map((item) => {
                if (item.id == this.state.chartId) {
                    this.setState({
                        haveData: false
                    },function() {
                        // 数据处理
                        this.handleData(item.data);
                    })
                }
            })
        }
    }


    async getMediaById(data) {
        const res = await getMediaById(data);
        if (res.data && res.data.code === 0) {
            let str = res.data;
            if (str.data && str.data.data && str.data.data.length > 0) {
                // 存储图例原始数据
                const { saveChartData } = this.props.chartStore;
                let oldData = {
                    id: this.state.chartId,
                    data: str.data.data
                }
                saveChartData(oldData);

                // 数据处理
                this.handleData(str.data.data);
            } else {
                this.setState({
                    dataNull: true
                })
            }
        } else {
            this.setState({
                dataNull: true
            })
        }
    }
    // 数据处理
    handleData = (storeAllData) => {
        let storeData = storeAllData[0];
        this.setState({
            dataNull: true
        })

        // 非预览界面
        if (this.state.pageFlag != 'preview') {
            // 存储可操作组件数据
            if (storeData.affect && storeData.affect.length > 0) {
                let obj = {
                    id: this.state.chartId,
                    label: storeData.title,
                    dimensionData: []
                }
                const { saveAffectData } = this.props.chartStore;

                let dimensionData = [];
                storeData.affect.slice().map((item,index) => {
                    dimensionData.push(item);
                })
                obj.dimensionData = dimensionData;
                saveAffectData(obj);
            }
            // 存储联动组件数据
            if (storeData.requires && storeData.requires.length > 0) {
                let obj = {
                    id: this.state.chartId,
                    label: storeData.title,
                    dimensionData: []
                }
                const { saveRequireData } = this.props.chartStore;

                let dimensionData = [];
                storeData.requires.slice().map((item,index) => {
                    dimensionData.push(item);
                })
                obj.dimensionData = dimensionData;
                saveRequireData(obj);
            }
        }

        // 页面初始化
        if (this.state.pageInitFlag) {
            if (storeData.affect &&  storeData.affect.length > 0) {
                this.setState({
                    affectName: storeData.affect[0]
                });
            }
            // 获取选择项
            if (storeData.conditionValues) {
                let selectData = storeData.conditionValues[storeData.affect[0]] ? storeData.conditionValues[storeData.affect[0]] : [];
                this.setState({
                    mainData: selectData
                });
            }
        }


        let newData = [];
        if (storeData.data.length > 0) {
            newData = storeData.data.map((item,index) => {
                return item.path;
            })
            this.setState({
                dataNull: false
            })
        }

        this.setState({
            title: storeData.title,
            chartData: newData,
            haveData: true,
        })
    }

    // 改变全局变量
    onGlobalVariable = (val) => {
        const { saveAffectId } = this.props.chartStore;
        saveAffectId(this.state.chartId);
        this.props.onGlobalVariable(val);
    }


    // 视频点击
    videoClick = (index) => {
        const _this = this;
        console.log('videoClick=========',index);

        _this.setState({
            vIndex: index
        },function() {
            _this.playVideo();
        })
    }
    // 视频循环播放
    playVideo = () => {
        const _this = this;
        console.log('playVideo=========',_this.state.vIndex);

        
        let videoData = _this.state.chartData;
        let index = _this.state.vIndex;
        let videoDom = document.getElementById('videoID');
        // console.log('document.querySelector("video").play();======',document.querySelector("video").play());
        videoDom.src = videoData[index];
        videoDom.play();
        videoDom.addEventListener('ended', function(event) {
            if (index == videoData.length - 1) {
                index = 0;
                videoDom.src = videoData[index];
                videoDom.play();
            } else {
                index += 1;
                videoDom.src = videoData[index];
                videoDom.play();
            }
            _this.setState({
                vIndex: index
            })
        },false)
    }




    render() {
        const _this = this;
        let Layer = this.props.layer;

        let videoData = _this.state.chartData;
        // let videoData = [
        //     'https://green-power-dev.oss-cn-beijing.aliyuncs.com/884a884a-884a-884a-884a-884a884a884a',
        //     'http://green-power-dev.oss-cn-beijing.aliyuncs.com/4d7a4d7a-4d7a-4d7a-4d7a-4d7a4d7a4d7a',
        //     'https://green-power-dev.oss-cn-beijing.aliyuncs.com/90ab90ab-90ab-90ab-90ab-90ab90ab90ab'
        // ];
        // console.log('videoData=====',videoData);
        let videoDom = document.getElementById('videoID');
        if (videoDom && videoData.length > 0 && _this.state.vIndex == 0 && _this.state.activeFirstFlag) {
            _this.playVideo();
            _this.setState({
                activeFirstFlag: false
            })
        }



        return (
            <div id={this.state.id+'_'+this.state.chartId} style={{zIndex: 99,width: this.state.width,height: this.state.height,padding: '50px 15px 10px 15px'}}>
                <Layer width={this.state.width} title={this.state.title} chartId={this.state.chartId} choiceData={this.state.mainData} onGlobalVariable={this.onGlobalVariable}></Layer>
                <div className={styles.cloudBox}  id={'chart_'+this.state.id+'_'+this.state.chartId} >
                    <p className={styles.time}>2020/06/04 16:30:31</p>
                    <video id="videoID"
                        width={this.state.width - 30}
                        height={this.state.height}
                        src={videoData.length > 0 ? videoData[0] : ''}
                        controls="controls" 
                        autoplay='autoplay' >
                    </video>
                </div>
            </div>

        )
    }


}
export default DefaultVideo;


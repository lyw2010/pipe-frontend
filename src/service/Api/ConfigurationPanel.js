/**
 * 类名 : 组件Api
 * 描述:
 * 创建人:
 * 创建日期:  2019/9/16
 *
 * 修改历史
 * 修改日期: <修改日期，>
 * 修改人:
 * 修改原因/修改内容: <修改原因描述> <问题单号： ${问题单号}>
 */

import event from '@/request'
import service from '@/service/Services'


const urlHttp = event.responsive.url;

/**
 *  获取推荐布局
 * @method getRecommendList
 * @return {[type]}         [description]
 */
export function getRecommendList(data) {
    return service.get(
        urlHttp + '/v1/recommendDashboard/listRecommendList',
        data
    );
}


/**
 *  根据id获取推荐布局数据
 * @method getRecommendData
 * @return {[type]}         [description]
 */
export function getRecommendData(data) {
    return service.get(
        urlHttp + '/v1/recommendDashboard/getRecommend/'+data.id,
        data
    );
}


/**
 *  获取终端列表
 * @method getMediaList
 * @return {[type]}         [description]
 */
export function getMediaList(data) {
    return service.get(
        urlHttp + '/v1/tMedia/pageList/10000/0',
        data
    );
}


/**
 *  获取图例列表
 * @method getMediaList
 * @return {[type]}         [description]
 */
export function getChartList(data) {
    return service.get(
        urlHttp + '/v1/tChart/getChartList',
        data
    );
}


/**
 *  获取图例组件列表
 * @method getChartComList
 * @return {[type]}         [description]
 */
export function getChartComList(data) {
    return service.get(
        urlHttp + '/v1/tChart/listBusinessTypeChart',
        data
    );
}



/**
 *  新建面板
 * @method createDashboard
 * @return {[type]}         [description]
 */
export function createDashboard(data) {
    return service.post(
        urlHttp + '/v1/tDashboard/createDashboard',
        data
    );
}
/**
 *  修改面板信息
 * @method updateDashboard
 * @return {[type]}         [description]
 */
export function updateDashboard(data) {
    return service.post(
        urlHttp + '/v1/tDashboard/updateDashboard',
        data
    );
}
/**
 *  复制面板
 * @method copyDashboard
 * @return {[type]}         [description]
 */
export function copyDashboard(data) {
    return service.post(
        urlHttp + '/v1/tDashboard/copy',
        data
    );
}
/**
 *  删除面板
 * @method deleteDashboard
 * @return {[type]}         [description]
 */
export function deleteDashboard(data) {
    return service.post(
        urlHttp + '/v1/tDashboard/deleteDashboard',
        data
    );
}
/**
 *  添加组件
 * @method addChart
 * @return {[type]}         [description]
 */
export function addChart(data) {
    return service.post(
        urlHttp + '/v1/tDashboard/addChart/'+data.dashboradId,
        data
    );
}
/**
 *  像素转换坐标
 * @method pixelToQuadrant
 * @return {[type]}         [description]
 */
export function pixelToQuadrant(data) {
    return service.post(
        urlHttp + '/v1/tDashboard/pixelToQuadrant/',
        data
    );
}
/**
 *  保存组件
 * @method saveChart
 * @return {[type]}         [description]
 */
export function saveChart(data) {
    return service.post(
        urlHttp + '/v1/tDashboard/moveChart/'+data.dashboradId,
        data
    );
}
/**
 *  保存组件信息
 * @method saveBoardChart
 * @return {[type]}         [description]
 */
export function saveBoardChart(data) {
    return service.post(
        urlHttp + '/v1/tDashboard/saveBoardChart',
        data
    );
}
/**
 *  删除组件
 * @method deleteChart
 * @return {[type]}         [description]
 */
export function deleteChart(data) {
    return service.post(
        urlHttp + '/v1/tDashboard/deleteChart/'+data.dashboardId+'/dashboardId/'+data.chartId+'/chartId',
        data
    );
}





/**
 *  预览/查看面板
 * @method preview
 * @return {[type]}         [description]
 */
export function preview(data) {
    return service.get(
        urlHttp + '/v1/tDashboard/preview/'+data.dashboardId,
        data
    );
}


/**
 *  获取已发布大屏菜单
 * @method getViewMenus
 * @return {[type]}         [description]
 */
export function getViewMenus(data) {
    let id = (data == '' || data == undefined || data == null) ? '' : data;
    return service.get(
        // urlHttp + '/v1/tDashboard/view/'+id
        urlHttp + '/v1/tDashboardApp/view/' + id
    );
}


/**
 *  生成推荐布局
 * @method generateRecommendLayout
 * @return {[type]}         [description]
 */

export function generateRecommendLayout(data) {
    return service.post(
        urlHttp + '/v1/recommendDashboard/copyRecommendDashboard',
        data
    );
}
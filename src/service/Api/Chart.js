/**
 * 类名 : 组件Api
 * 描述:
 * 创建人:
 * 创建日期:  2019/9/16
 *
 * 修改历史
 * 修改日期: <修改日期，>
 * 修改人:
 * 修改原因/修改内容: <修改原因描述> <问题单号： ${问题单号}>
 */

import event from '@/request'
import service from '@/service/Services'


const urlHttp = event.responsive.url;

/**
 *  根据图表id查询图表数据
 * @method createDashboard
 * @return {[type]}         [description]
 */
export function getMediaById(data) {
    console.log('getMediaById--condition--',data.condition);
    const results = service.post(
        urlHttp + '/v1/assembly/getMediaById/' + data.charId,
        data.condition
    );
    results.then((result) => {
        const dataList = result.data.data ? result.data.data.data : [];
        if (result.data.data){
            result.data.data.data =   dataList.map(
                d => {
                    const {columnTypeMaps} = d;
                    d.data = d.data.map(
                        o => {
                            for (let clo in columnTypeMaps) {
                                const type = columnTypeMaps[clo].toUpperCase();
                                if (type === "LONG" || type === "DOUBLE" || type === "FLOAT" || type === "INTEGER" || type === "INT") {
                                    o[clo] = +o[clo];
                                }
                            }
                            return o;
                        }
                    );
                    return d;
                }
            );
        }
    });
    return results;
}


/**
 *  根据orderId 获取直播数据
 * @method createDashboard
 * @return {[type]}         [description]
 */
export function getVideoParams(video_url,orderId) {
    const results = service.get(`${video_url}/${orderId}`);
    results.then((result) => {
        if (result.data){
            return result.data.data
        }
    });
    return results;
}

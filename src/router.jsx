import {HashRouter as Router, Switch, Route, Redirect} from 'react-router-dom';
import React from 'react';
import path from 'path';
import routes from '@/router/routerConfig';
import {Provider} from 'mobx-react';
import {manageMenuConfig} from "./menuConfig";
import {hasAuth} from "./server";

const RouteItem = (props) => {
    const {redirect, path: routePath, component, key} = props;
    if (redirect) {
        const pathList = [];
        const module = redirect.split("/")[1];
        for (let i = 0; i < manageMenuConfig.length; i++) {
            if (manageMenuConfig[i].title == module) {
                const arr = manageMenuConfig[i].asideData;
                for (let j = 0; j < arr.length; j++) {
                    const has = hasAuth(arr[j]);
                    if (has) {
                        pathList.push(arr[j].path)
                    }
                }
            }
        }
        let rct = pathList.length ? pathList[0] : redirect;
        if (pathList.length) {
            console.warn("根据权限管理规定，需要对重定向的菜单做处理"
                + "\n现在跳转的地址是：" + pathList[0]
                + "\n如果首页跳转的地址不是列表的地址，请把列表的地址放在第一位");
        }
        return (
            <Redirect
                exact
                key={key}
                from={routePath}
                to={rct}
            />
        );
    }
    return (
        <Route
            key={key}
            component={component}
            path={routePath}
        />
    );
};

const router = () => {
    return (
        <Provider>
            <Router>
                <Switch>
                    {routes.map((route, id) => {
                        const {component: RouteComponent, children, ...others} = route;
                        return (
                            <Route
                                key={id}
                                {...others}
                                component={(props) => {
                                    return (
                                        children ? (
                                            <RouteComponent key={id} {...props}>
                                                <Switch>
                                                    {children.map((routeChild, idx) => {
                                                        const {redirect, path: childPath, component} = routeChild;
                                                        return RouteItem({
                                                            key: `${id}-${idx}`,
                                                            redirect,
                                                            path: childPath && path.join(route.path, childPath),
                                                            component,
                                                        });
                                                    })}
                                                </Switch>
                                            </RouteComponent>
                                        ) : (
                                            <div>
                                                {RouteItem({
                                                    key: id,
                                                    ...props,
                                                })}
                                            </div>
                                        )
                                    );
                                }}
                            />
                        );
                    })}
                </Switch>
            </Router>
        </Provider>
    );
};

export default router;

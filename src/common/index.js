import {JSEncrypt} from 'jsencrypt'


/**
 * 特殊字段加密
 * @param {String} fieldPsw 需要加密的字段
 * @return {String} fieldPsw  加密之后的字段
 */
export const craPassword = fieldPsw => {
    var encrypt = new JSEncrypt();
    encrypt.setPublicKey('MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCQ335gDR/KNsRnbWo7W4f0azS8xGZt6KAEZWpSgZYUrr5SKTAAowwAMJ2uJc777+GNHL5vRyPg7VUuLXgAxz6Y0WiwFlIGaxG5ZLB4Vk2OBB0QFML/jbfwmvu39tyTecM23kcvaLa9e36mxXHPL66CUwgQF/f/47NqHIIdPP9eQQIDAQAB');
    return encrypt.encrypt(fieldPsw)
}

/**
 * 把数值转化成大写
 * @param textIndex
 * @returns {string}
 */
export const numberChinese = (textIndex) => {
    let newString = '';
    let newTextIndex = (textIndex + 1) + '';

    function sum(value, index) {
        var newValue = '';
        if ((textIndex === 9)) {
            return !index ? '十' : '';
        }
        let isSeat = (~~textIndex > 9 && ~~textIndex < 19);
        switch (~~value) {
            case 1:
                newValue = !index ? (isSeat ? '' : '一') : '十一';
                break;
            case 2:
                newValue = !index ? (isSeat ? '' : '二') : '十二';
                break;
            case 3:
                newValue = !index ? (isSeat ? '' : '三') : '十三';
                break;
            case 4:
                newValue = !index ? (isSeat ? '' : '四') : '十四';
                break;
            case 5:
                newValue = !index ? (isSeat ? '' : '五') : '十五';
                break;
            case 6:
                newValue = !index ? (isSeat ? '' : '六') : '十六';
                break;
            case 7:
                newValue = !index ? (isSeat ? '' : '七') : '十七';
                break;
            case 8:
                newValue = !index ? (isSeat ? '' : '八') : '十八';
                break;
            case 9:
                newValue = !index ? (isSeat ? '' : '九') : '十九';
                break;
            case 0:
                newValue = '十';
                break;
            default:
                break;
        }
        return newValue;
    }

    for (let i = 0; i < newTextIndex.length; i++) {
        newString += sum(newTextIndex.substring(i, i + 1), i);
    }
    return newString;
}


/**
 * 下拉树移除父节点
 * @param {Array} list 下拉树的所有节点
 * @param {Array} nodeList 下拉树的选中节点
 */
export const recursionChildren = (list, nodeList) => {
    // 树形控件移除传回来的父子节点
    list.map(item => {
        if (item.children && item.children.length > 0) {
            recursionChildren(item.children, nodeList)
            if (!item.children.every(i => nodeList.includes(i.id))) {
                // 如果只有部分子元素被选中， 那么父元素不被选中
                if (nodeList.indexOf(item.id) != -1) {
                    nodeList.splice(nodeList.indexOf(item.id), 1)
                }
            }
        }
    })
    return nodeList
}


/**
 * 下拉树增加父节点
 * @param {Array} list 下拉树的所有节点
 * @param {Array} nodeList 下拉树的选中节点
 */
export const recursionReset = (list, nodeList) => {
    // 树形控件 直接提交找回子节点的父节点
    list.map(item => {
        if (item.children && item.children.length > 0) {
            if (item.children.some(i => nodeList.includes(i.id))) {
                // 如果最后选中的是某个子元素 那么他的父元素也要被选中
                // 并且 如果选中的父元素不存在列表里 才push
                if (!nodeList.includes(item.id)) nodeList.push(item.id)
            }

            item.children.forEach(obj => {
                if (obj.children.some(i => nodeList.includes(i.id))) {
                    if (!nodeList.includes(obj.id)) nodeList.push(obj.id)
                    if (!nodeList.includes(item.id)) nodeList.push(item.id)
                }

                if (obj.children && obj.children.length > 0) {
                    obj.children.forEach(ele => {
                        if (ele.children.some(i => nodeList.includes(i.id))) {
                            if (!nodeList.includes(ele.id)) nodeList.push(ele.id)
                            if (!nodeList.includes(obj.id)) nodeList.push(obj.id)
                            if (!nodeList.includes(item.id)) nodeList.push(item.id)
                        }
                    })
                }
            })
            recursionReset(item.children, nodeList)
        }
    })
}
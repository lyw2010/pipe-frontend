/**
  * @description: 数据中台-码值管理-添加/修改码值
  * @author: hj
  * @update: hj(2020-01-14)
  */
import React from 'react';
import IceContainer from '@icedesign/container';
import {Button, Input, Message, Radio, Select} from '@alifd/next';
import {
    FormBinder as IceFormBinder,
    FormBinderWrapper as IceFormBinderWrapper,
    FormError as IceFormError,
} from '@icedesign/form-binder';
import styles from './index.module.scss';
// import axios from 'axios';
import responsive from '@/request'
import $http from '@/service/Services';

const axios = $http;

const {Option} = Select;
const {Group: RadioGroup} = Radio;
let form;


/**
 * list
 */
class BaseCodeListEdit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: {},
            id: "",
            codeTypeId:"",
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        };
        this.formChange = this.formChange.bind(this);
        this.validateAllFormField = this.validateAllFormField.bind(this);
    }

    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        const params = new URLSearchParams(this.props.location.search);
        const id = params.get("id");
        const codeTypeId = params.get("codeTypeId");
        this.setState({
            id: id,
            codeTypeId:codeTypeId
        }, () => {
            if (id) {
                const that = this;
                axios.get(responsive.responsive.url + '/v1/codeList/getCodeListbyId/' + id)
                    .then(function (response) {
                        const {data} = response;
                        if (data.code === 1) {
                            Message.warning(data.message ? data.message : data.data);
                        } else {
                            that.setState({
                                value: data.data
                            })
                        }
                    })
                    .catch(function (error) {
                        Message.error(error.message);
                    })
            }
        });

    };

    /* 表改变
    * @param formValue
    */
    formChange = (formValue) => {
        this.setState({
            value: formValue
        })
    };

    /**
     * 表验证
     */
    validateAllFormField = () => {
        form.validateAll((errors, values) => {

            if (errors) {
                Message.warning('请填写必要的数据.');
                return;
            }
            const that = this;
            values["typeId"] = this.state.codeTypeId;
            axios.post(responsive.responsive.url + '/v1/codeList/saveOrUpdateCodeList', values)
                .then(function (response) {
                    const {data} = response;
                    if (data.code === 1) {
                        Message.warning(data.message ? data.message : data.data);
                    } else {
                        Message.success("操作成功.");
                        that.props.history.go(-1);
                    }
                })
                .catch(function (error) {
                    Message.error(error.message);
                })
        });
    };

    /**
     * 表
     * @returns {*}
     */
    render() {
        return (
            <div>
                <div className='container-header'>
                    <p style={{display: 'inline-block'}}>{this.state.id ? "修改码值" : "添加码值"}</p>
                    <Button type="normal" size="small" className='next-btn-normal table-btn-return' style={{float: 'right'}}>
                        <a onClick={() => {
                            this.props.history.go(-1)
                        }} > 返回</a>
                    </Button>
                </div>

                
                <IceContainer className='container-main'>
                    <IceFormBinderWrapper
                        value={this.state.value}
                        onChange={this.formChange}
                        ref={formRef => form = formRef}
                    >
                        <div className={styles.formContent}>
                            <div className={styles.formItem}>
                                <div className={styles.formLabel}><span className={styles.red}>*</span>码值编码：</div>
                                <IceFormBinder
                                    required
                                    triggerType="onBlur"
                                    message="码值编码不能为空"
                                    name="code"
                                >
                                    <Input readOnly={this.state.id} disabled={this.state.id}
                                        maxLength={20}
                                        placeholder="请填写码值编码"
                                        className={styles.inputNme}
                                    />
                                </IceFormBinder>
                                <div className={styles.formError}>
                                    <IceFormError name="code"/>
                                </div>
                            </div>
                            <div className={styles.formItem}>
                                <div className={styles.formLabel}><span className={styles.red}>*</span>码值名称：</div>
                                <IceFormBinder required name="name" message="码值名称不能为空">
                                    <Input
                                        maxLength={20}
                                        placeholder="请填写码值名称"
                                        className={styles.inputNme}
                                    />
                                </IceFormBinder>
                                <div className={styles.formError}>
                                    <IceFormError name="name"/>
                                </div>
                            </div>
                            <Button
                                type="primary"
                                className='pipe-btn-submit'
                                onClick={this.validateAllFormField}
                            >
                                提 交
                            </Button>
                            <Button
                                type="primary"
                                className='pipe-btn-cancel'
                                onClick={() => {
                                    this.props.history.go(-1)
                                }}
                            >
                                取消
                            </Button>
                        </div>
                    </IceFormBinderWrapper>
                </IceContainer>
            </div>
        );
    }

}

export default BaseCodeListEdit;

/**
  * @description: 数据中台-码值管理
  * @author: hj
  * @update: hj(2020-01-14)
  */
import React from 'react';
import Container from '@icedesign/container';
import styles from './index.module.scss';
import responsive from '@/request'
import {Button, Input, Message, Pagination, Select, Table} from "@alifd/next";
import ZgDialog from '@/components/ZgDialog';
import $http from '@/service/Services';
import AuthButton from "../../../components/AuthComponent/AuthBotton";

const axios = $http;


/**
 * list
 */
class BaseCodeListList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            current: 0,
            codeTypeId: "",
            codeTypeList: [],
            mockData: [],
            condition: "",
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        };
        this.pageNum = 1; // 当前页数
        this.pageSize = 15; // 分页每页显示数据条数
        this.totalNum = 0; // 数据总条数
        this.cancelCall = this.cancelCall.bind(this);
        this.onPageChange = this.onPageChange.bind(this);
        this.getCodeList = this.getCodeList.bind(this);
        this.deleteCodeType = this.deleteCodeType.bind(this);
        this.onChangePage = this.onChangePage.bind(this);
    }

    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        this.getCodeTypeList();
    };


    /**
     * 条件
     * @param e
     * @param value
     */
    searchCondition = (value, e) => {
        this.setState({
            condition: value
        })
    };

    changeCodeType = (value, e)=>{
        this.setState(
            {
                codeTypeId: value
            },()=>{
                this.getCodeList();
            }
        )
    };



    /**
     * 改变页面
     * @param currentPage
     */
    onPageChange = (currentPage) => {
        this.state = {
            current: currentPage,
            mockData: this.props.mockData
        };
        this.getCodeTypeList();
    };

    /**
     * 获取大类的信息
     */
    getCodeTypeList = () => {
        const that = this;
        const url = responsive.responsive.url + "/v1/codeType/listCodeType";
        axios.get(url)
            .then(function (response) {
                const {data} = response;
                const mockData = data.data;
                that.setState({
                    codeTypeList: mockData,
                    codeTypeId: mockData[0].id
                }, () => {
                    that.getCodeList();
                })

            })
            .catch(function (error) {
                Message.error(error.message);
            })
    };

    /**
     * 获取数据
     */
    getCodeList = (pageNum) => {
        const that = this;
        that.pageNum = typeof (pageNum) == "number" ? pageNum : that.pageNum;
        const url = responsive.responsive.url + '/v1/codeList/getCodeListPage/' + that.state.codeTypeId + "/" + that.pageSize + '/' + that.pageNum + "?name=" + this.state.condition;
        axios.get(url)
            .then(function (response) {
                const {data} = response;
                const mockData = data.data.voList;
                that.totalNum = data.data.total;
                mockData.forEach((o, i) => {
                    o.number = i + 1;
                });
                that.setState({
                    mockData: mockData,
                    current: data.data.current
                });
            })
            .catch(function (error) {
                Message.error(error.message);
            })
    };
    /**
     * 改变分页的大小
     * @param pageSize
     */
    onPageSizeChange=(pageSize)=>{
        this.pageSize = pageSize;
        this.pageNum = 1;
        this.getCodeList();
    };
    /**
     * 修改分页数据
     * @param currentPage
     */
    onChangePage = (currentPage) => {
        this.setState({
            loading: true,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        });
        setTimeout(() => {
            this.getCodeList(currentPage);
            this.setState({
                loading: false,
                config: {
                    data: null,
                    visible: false,
                    title: "",
                    content: ""
                }
            });
        }, 0);
    };


    /**
     * 删除
     */
    deleteCodeType = (row) => {
        this.setState({
                config: {
                    data: row,
                    visible: true,
                    title: "提示消息",
                    content: "确认要删除？删除后不能恢复！"
                }
            }
        )
    };

    /**
     * 取消按钮的操作
     */
    cancelCall = () => {
        this.setState({
            config: {
                visible: false
            }
        });
        console.log("点击取消按钮 .");
    };


    /**
     * 删除
     */
    okCall = (data) => {
        const _this = this;

        _this.setState({
            config: {
                visible: false
            }
        });

        axios.post(responsive.responsive.url + '/v1/codeList/deleteCodeType?codeListId=' + data.id)
            .then(function (response) {
                const {data} = response;
                if (data.code === 0) {
                    Message.success("删除面板成功！");
                    _this.getCodeList();
                } else {
                    Message.warning(data.message ? data.message : data.data);
                }
            })
            .catch(function (error) {
                Message.error(error.message);
            })
    };


    /**
     *
     * @param args
     */
    onChange = (...args) => {
        this.setState({
            selectedList: args
        });
    };


    render() {
        const {mockData} = this.state;
        return (
            <div>
                <div className='container-header'>
                    <p>码值管理</p>
                </div>
                <Container className='container-main'>
                    <div className='container-btnBox' style={{marginTop: '0'}}>
                        <AuthButton auth={"RESPONSIVE$_DASHBOARD$_ADD"}
                                    link={{to: "codeListEdit?id=" + "&codeTypeId=" + this.state.codeTypeId, text: "新建码值"}}/>

                        <span className={styles.caseNumber} style={{float: 'right'}}>
                            <Input onChange={this.searchCondition.bind(this)}
                                   className={`${styles.input} ${styles.shortInput}`}/>
                            <Button
                                type="primary"
                                className='pipe-btn-search'
                                onClick={this.getCodeList}
                            >
                                查询
                            </Button>
                        </span>
                        <span className={styles.caseNumber} style={{float: 'right'}}>
                            <Select onChange={this.changeCodeType.bind(this)}
                                placeholder="请选择" value={this.state.codeTypeId}
                                className={styles.select}
                            >
                                {
                                    this.state.codeTypeList.map(
                                        o => {
                                            return <Select.Option value={o.id}>{o.name}</Select.Option>
                                        }
                                    )
                                }
                                    </Select>
                        </span>
                    </div>
                    <div className='container-table'>
                        <Table dataSource={mockData} size="medium" primaryKey="number" className={styles.table}>
                            <Table.Column align="center" title="序号" dataIndex="number"/>
                            <Table.Column align="center" title="类型" dataIndex="code"/>
                            <Table.Column align="center" title="数值" dataIndex="name"/>
                            <Table.Column align="center" title="操作" cell={
                                (value, index, record) => {
                                    return (
                                        <div>
                                            <AuthButton text auth={"RESPONSIVE$_DASHBOARD$_EDIT"} type="normal"
                                                        size="small"
                                                        link={{to: "codeListEdit?id=" + record.id + "&codeTypeId=" + this.state.codeTypeId, text: "修改"}}/>
                                            <AuthButton text auth={"RESPONSIVE$_DASHBOARD$_DELETE"} type="normal"
                                                        onClick={() => this.deleteCodeType(record)} size="small"
                                                        title={"删除"}/>

                                        </div>
                                    )
                                }
                            }/>
                        </Table>
                        <ZgDialog config={this.state.config} cancelCall={this.cancelCall} okCall={this.okCall}
                                  className="container-dailog"/>
                        {/**
                         * 分页
                         * defaultCurrent 初始页码
                         * total 总记录数
                         * pageSize 一页中的记录数
                         */}
                        <Pagination defaultCurrent={1}
                                    pageSize={this.pageSize}
                                    total={this.totalNum}
                                    onChange={this.onChangePage}
                                    onPageSizeChange={this.onPageSizeChange}
                                    pageSizeList={[5,10,20,50,100,200]}
                                    pageSizeSelector="dropdown"
                                    className="page-demo"
                                    size="small"
                        />
                    </div>
                </Container>
            </div>
        )
            ;
    }

}

export default BaseCodeListList;

/**
 * @description: 数据中台-权限管理-租户管理
 * @author: hj
 * @update: hj(2020-01-14)
 */
import React from 'react';
import Container from '@icedesign/container';
import styles from './index.module.scss';
import event from '@/request'
import {Message, Table} from "@alifd/next";
import $http from '@/service/Services';


/**
 * list
 */
class UserTenantList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            subjectId: "",
            current: 0,
            mockData: [],
            selectedList: [],
            condition: "",
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        };
        // 当前页数
        this.pageNum = 1;
        // 分页每页显示数据条数
        this.pageSize = 10;
        // 数据总条数
        this.totalNum = 0;
        this.$http = $http;
        this.onPageChange = this.onPageChange.bind(this);
        this.getAdminTenantList = this.getAdminTenantList.bind(this);
    }

    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        const params = new URLSearchParams(this.props.location.search);
        const id = params.get("id");
        this.setState({
            subjectId: id
        }, () => {
            this.getAdminTenantList();
        });
    };


    /**
     * 条件
     * @param e
     * @param value
     */
    searchCondition = (value, e) => {
        this.setState({
            condition: value,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        });
        this.pageNum = 1;
    };
    /**
     * 改变页面
     * @param currentPage
     */
    onPageChange = (currentPage) => {
        this.state = {
            current: currentPage,
            mockData: this.props.mockData,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        };
        this.getAdminTenantList();
    };
    /**
     * 修改分页数据
     * @param currentPage
     */
    onChangePage = (currentPage) => {
        this.setState({
            loading: true,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        });
        setTimeout(() => {
            this.getAdminTenantList(currentPage);
            this.setState({
                loading: false,
                config: {
                    data: null,
                    visible: false,
                    title: "",
                    content: ""
                }
            });
        }, 0);
    };

    /**
     * 获取数据
     */
    getAdminTenantList = () => {
        const _this = this;
        let url = event.event.url + '/v1/org/getTenantList?name=' + this.state.condition;
        this.$http.get(url)
            .then(function (response) {
                const {data} = response;
                data.data.forEach((o, i) => {
                    o.number = i + 1;
                });
                _this.setState({
                    mockData: data.data,
                    config: {
                        data: null,
                        visible: false,
                        title: "",
                        content: ""
                    }
                });
            })
            .catch(function (error) {
                Message.error(error.message);
            })

    };


    render() {
        const {mockData} = this.state;
        return (
            <div>
                <div className={styles.nav}>
                    <h2 className={styles.breadcrumb}>租户管理</h2>
                </div>
                <Container className={styles.container} style={{minHeight: 'calc(100% - 112px)'}}>
                    <div className={styles.container}>
                        <Table dataSource={mockData} primaryKey="id" className={styles.table}>
                            <Table.Column align="center" title="租户代码" dataIndex="code"/>
                            <Table.Column align="center" title="租户名称" dataIndex="name"/>
                            <Table.Column align="center" title="管理员用户" dataIndex="managerUser"/>
                            <Table.Column align="center" title="服务开通日期" dataIndex="startTime"/>
                            <Table.Column align="center" title="服务结束日期" dataIndex="endTime"/>
                            <Table.Column align="center" title="状态" dataIndex="status" cell={
                                (value) => {
                                    return value === 0 ? "正常" : "已冻结"
                                }
                            }/>
                        </Table>
                    </div>
                </Container>
            </div>
        )
            ;
    }
}

export default UserTenantList;

import React from 'react';
import UserEditComponents from "../../../components/UserEditComponents";

/**
 * list
 */
class UserUserEdit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: {},
            roleList: [],
            id: "",
            tenantId: "",
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        };
    }


    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        const params = new URLSearchParams(this.props.location.search);
        const id = params.get("id");
        const tenantId = "-1";
        this.setState({
            id: id,
            tenantId: tenantId
        }, () => {

        });

    };



    /**
     * 表
     * @returns {*}
     */
    render() {
        return (
             <UserEditComponents user={"admin"}  history={this.props.history} id={this.state.id} tenantId={this.state.tenantId}/>
        );
    }

}

export default UserUserEdit;

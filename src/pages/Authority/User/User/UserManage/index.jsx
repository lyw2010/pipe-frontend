/**
  * @description: 数据中台-权限管理-租户管理-用户管理
  * @author: hj
  * @update: hj(2020-01-14)
  */
import React from 'react';
import styles from './index.module.scss';
import $http from '@/service/Services';
import UserListComponent from "../../../components/UserList";


/**
 * list
 */
class UserUserManageList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tenantId: "",
            current: 0,
            mockData: [],
            selectedList: [],
            condition: "",
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        };
        // 当前页数
        this.pageNum = 1;
        // 分页每页显示数据条数
        this.pageSize = 10;
        // 数据总条数
        this.totalNum = 0;
        this.$http = $http;
    }

    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        const id = "-1";
        this.setState({
            tenantId: id
        }, () => {

        });
    };


    render() {

        return (
            <div>
                <div className={styles.nav}>
                    <h2 className={styles.breadcrumb}>用户管理</h2>
                </div>
                <UserListComponent   user={"user"}  history={this.props.history} tenantId={this.state.tenantId}/>
            </div>
        )
            ;
    }

}

export default UserUserManageList;

/**
 * 数据中台-权限管理-默认权限管理
 */
import React from 'react';
import Container from '@icedesign/container';
import styles from './index.module.scss';
import event from '@/request'
import {Button, Input, Message, Pagination, Table} from "@alifd/next";
import ZgDialog from '@/components/ZgDialog';
import {Link} from "react-router-dom";
import $http from '@/service/Services';


/**
 * list
 */
class DefaultAuth extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            subjectId: "",
            current: 0,
            mockData: [],
            selectedList: [],
            condition: "",
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        };
        // 当前页数
        this.pageNum = 1;
        // 分页每页显示数据条数
        this.pageSize = 10;
        // 数据总条数
        this.totalNum = 0;
        this.$http = $http;
        this.handleClick = this.handleClick.bind(this);
        this.onPageChange = this.onPageChange.bind(this);
        this.getDefaultAuth = this.getDefaultAuth.bind(this);
    }

    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        this.getDefaultAuth();
    };


    /**
     * 条件
     * @param e
     * @param value
     */
    searchCondition = (value, e) => {
        this.setState({
            condition: value,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        });
        this.pageNum = 1;
    };
    /**
     * 改变页面
     * @param currentPage
     */
    onPageChange = (currentPage) => {
        this.state = {
            current: currentPage,
            mockData: this.props.mockData,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        };
        this.getDefaultAuth();
    };
    /**
     * 修改分页数据
     * @param currentPage
     */
    onChangePage = (currentPage) => {
        this.setState({
            loading: true,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        });
        setTimeout(() => {
            this.getDefaultAuth(currentPage);
            this.setState({
                loading: false,
                config: {
                    data: null,
                    visible: false,
                    title: "",
                    content: ""
                }
            });
        }, 0);
    };

    /**
     * 获取数据
     */
    getDefaultAuth = () => {
        const _this = this;
        let url = event.event.url + '/v1/auth/getAuthPage';
        this.$http.post(url,
            {
                pageSize: this.pageSize,
                pageNum: this.pageNum,
                condition: {
                    name: this.state.condition
                }
            })
            .then(function (response) {
                const {data} = response;
                const mockData = data.data.voList;
                _this.totalNum = data.data.total;
                mockData.forEach((o, i) => {
                    o.number = i + 1;
                });

                _this.setState({
                    mockData: mockData,
                    config: {
                        data: null,
                        visible: false,
                        title: "",
                        content: ""
                    }
                });
            })
            .catch(function (error) {
                Message.error(error.message);
            })

    };



    /**
     * 批量操作
     * @param text
     */
    handleClick = () => {
        const {selectedList} = this.state;
        if (selectedList.length == 0) {
            Message.error("请选择需要删除的数据.");
            return null;
        }
        this.setState({
            visible: true
        });
    };

    /**
     *
     * @param args
     */
    onChange = (...args) => {
        this.setState({
            selectedList: args
        });
    };


    render() {
        const {mockData} = this.state;
        const userVo = JSON.parse(sessionStorage.getItem("userInfoVO"));
        return (
            <div>
                <div className='container-header'>
                    <p>权限管理</p>
                </div>
                <Container className='container-main'>
                    <div className='container-btnBox' style={{marginTop: '0'}}>
                        <div className={styles.buttons}>
                            <Link to={"orgAdd?id=&parentId=" + userVo.rolePermissionVO.orgId}><Button
                                className='pipe-btn-add'>新建默认权限</Button></Link>
                            <span className={styles.caseNumber}>
                                <Input onChange={this.searchCondition.bind(this)}
                                       hasClear
                                       placeholder="请输入权限代码/权限名称"
                                       className={`${styles.input} ${styles.shortInput}`}/>
                                <Button
                                    type="primary"
                                    className='pipe-btn-search'
                                    onClick={this.getDefaultAuth}
                                >
                                查询
                                </Button>
                            </span>
                        </div>
                    </div>
                    <div className='container-table'>
                        <Table dataSource={mockData} primaryKey="id" className={styles.table}>
                            <Table.Column align="center" title="序号" dataIndex="number"/>
                            <Table.Column align="center" title="父级权限" dataIndex="parentName"/>
                            <Table.Column align="center" title="权限类型" dataIndex="type"/>
                            <Table.Column align="center" title="权限名称" dataIndex="name"/>
                            <Table.Column align="center" title="权限编码" dataIndex="code"/>
                            <Table.Column align="center" title="权限描述" dataIndex="description"/>
                        </Table>
                        <ZgDialog config={this.state.config} cancelCall={this.cancelCall} okCall={this.okCall}/>
                        <Pagination defaultCurrent={1}
                                    pageSize={this.pageSize}
                                    total={this.totalNum}
                                    onChange={this.onChangePage}
                                    //hideOnlyOnePage
                                    className="page-demo"
                                    size="small"
                        />
                    </div>
                </Container>
            </div>
        )
            ;
    }
}

export default DefaultAuth;

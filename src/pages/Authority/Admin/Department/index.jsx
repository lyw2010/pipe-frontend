/**
 * 数据中台-权限管理-默认权限管理
 */
import React from 'react';
import 'antd/dist/antd.css';
import {Card, Col, Input, Row, Tree} from 'antd';
import Container from "@icedesign/container";
import styles from './index.module.scss';
import {Button, Drawer, Form, Icon, Input as NextInput, Message, Pagination, Select, Table} from "@alifd/next";
import ZgDialog from "../../../../components/ZgDialog";
import event from '@/request'
import $http from '@/service/Services';
import {numberChinese, recursionReset} from "../../../../common";
import PipeTree from "../../../../components/PipeTree";

const {TreeNode} = Tree;
const {Search} = Input;

const FormItem = Form.Item;


const formItemLayout = {
    labelCol: {
        span: 6
    },
    wrapperCol: {
        span: 14
    }
};
const layout = {
    labelCol: {
        span: 4
    },
    wrapperCol: {
        span: 20
    },
}

/**
 * list
 */
class Department extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            },
            departmentConfig: {
                data: null,
                visible: false,
                title: "",
                content: ""
            },
            postConfig: {
                data: null,
                visible: false,
                title: "",
                content: ""
            },
            postAuthManegeDrawerShow: false,
            postAuthManegeDrawerValue: {
                name: "",
                id: "",
                roleId: [],
                roleIdList: []
            },
            roleList: [],
            departmentDrawerValue: {},
            departmentDrawerShow: false,
            userDrawerValue: {},
            userDrawerShow: false,
            currentDepartmentName: "",
            currentDepartmentLevel: "",
            instanceDrawerValue: {},
            instanceDrawerShow: false,
            level: "",
            currentDepartmentLastRange: "",
            currentDepartmentDes: "",
            currentDepartmentParentId: "",
            currentDepartmentId: "",
            data: [],
            expandedKeys: [], //展开的key值
            autoExpandParent: true,
            checkedKeys: [], //选中的key,
            searchValue: '',
            searchTreeKey: [] //搜索得key
        };
        // 当前页数
        this.pageNum = 1;
        // 分页每页显示数据条数
        this.pageSize = 10;
        // 数据总条数
        this.totalNum = 0;
        this.$http = $http;
        this.handleClick = this.handleClick.bind(this);
        this.onPageChange = this.onPageChange.bind(this);
    }
    /**
     * 改变分页的大小
     * @param pageSize
     */
    onPageSizeChange=(pageSize)=>{
        this.pageSize = pageSize;
        this.pageNum = 1;
        this.getDefaultRoles();
    };
    /**
     * 改变页面
     * @param currentPage
     */
    onPageChange = (currentPage) => {
        this.state = {
            current: currentPage,
            mockData: this.props.mockData,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        };
        this.getDefaultRoles();
    };
    /**
     * 批量操作
     * @param text
     */
    handleClick = () => {
        const {selectedList} = this.state;
        if (selectedList.length == 0) {
            Message.error("请选择需要删除的数据.");
            return null;
        }
        this.setState({
            visible: true
        });
    };
    /**
     * 给子级添加父级Key
     * @param tree
     * @returns {any}
     */
    addParentKeyWrapper = (tree) => {
        //深度克隆
        const data = JSON.parse(JSON.stringify(tree));

        function addParentKey(data, parentKey) {
            data.forEach(ele => {
                const {children, key} = ele;
                ele.parent_key = parentKey;
                if (children) {//如果唯一标识不是code可以自行改变
                    addParentKey(children, key)
                }
            })
        }

        addParentKey(data, null); //一开始为null,根节点没有父级
        return data;
    };

    componentDidMount() {
        this.expandedKeysFn();
        this.setState({
            data: this.addParentKeyWrapper(this.state.data) //调用添加父元素key
        })
    }

    /**
     * search变化
     * @param e
     */
    onChange = (e) => {
        const {value} = e.target;
        console.log(value)
        const dataList = [];
        if (value) {
            const generateList = data => { //tree树片扁平化
                for (let i = 0; i < data.length; i++) {
                    const node = data[i];
                    const {key, title, parent_key} = node;
                    dataList.push({key, title: title, parent_key: parent_key}); //根据自己的数据更换parent_key代表父级key
                    if (node.children) {
                        generateList(node.children);
                    }
                }
            };
            generateList(this.state.data);

            const getParentKey = (key, tree) => { //获取父元素可以
                let parentKey;
                for (let i = 0; i < tree.length; i++) {
                    const node = tree[i];
                    if (node.children) {
                        if (node.children.some(item => item.key === key)) {
                            parentKey = node.key;
                        } else if (getParentKey(key, node.children)) {
                            parentKey = getParentKey(key, node.children);
                        }
                    }
                }
                return parentKey;
            };

            const expandedKeys = dataList
                .map(item => {
                    if (item.title.indexOf(value) > -1) {
                        return getParentKey(item.key, this.state.data);
                    }
                    return null;
                })
                .filter((item, i, self) => item && self.indexOf(item) === i);

            this.setState({
                expandedKeys: expandedKeys,
                searchValue: value,
                autoExpandParent: true
            })
        } else {
            this.expandedKeysFn();//重置展开key
            this.setState({
                searchValue: value,
                autoExpandParent: true
            })
        }
    };


    /**
     * 生成树结构函数
     * @param data
     * @returns {*}
     */
    renderTreeNode = (data) => {
        if (data.length === 0) {
            return
        }
        let {searchValue} = this.state;
        return data.map((item) => {
            const index = item.title.indexOf(searchValue);
            const beforeStr = item.title.substr(0, index);
            const afterStr = item.title.substr(index + searchValue.length);
            //https://fusion.design/pc/component/icon?themeid=2
            const title =
                index > -1 ? (
                    <div style={{}}>
                        <div style={{display: 'inline-block', width: '70%', padding: '8px 8px;'}}>
                            {beforeStr}
                            <span style={{color: "red"}}>{searchValue}</span>
                            {afterStr}
                        </div>
                        <div style={{display: 'inline-block', width: '30%', textAlign: 'right'}}>

                            {
                                [0].map(o => {
                                    if (!item.children || item.children.length === 0) {
                                        return (
                                            <Icon type="minus" title={"删除"} onClick={() => {
                                                this.deleteDepartment(item)
                                            }} size={'xs'} style={{paddingRight: '10px'}}/>
                                        )
                                    }
                                })
                            }
                            <Icon type="add" title={"添加"} onClick={() => {
                                this.addDepartment(item)
                            }} size={'xs'} style={{paddingRight: '10px'}}/>

                            <Icon type="edit" title={"编辑"} onClick={() => {
                                this.editDepartment(item, false)
                            }} size={'xs'} style={{paddingRight: '10px'}}/>
                        </div>
                    </div>

                ) : (
                    <div>
                        <span>{item.title}</span>
                    </div>
                );
            if (item.children && item.children.length > 0) {
                //className={searchTreeKey.indexOf(item.key) > -1 ? styles.yes : styles.no}
                return <TreeNode title={title} key={item.key}>
                    {
                        this.renderTreeNode(item.children)
                    }
                </TreeNode>
            }
            return <TreeNode key={item.key} title={title}></TreeNode>
        })
    }
    expandedKeysFn = () => {
        let {data} = this.state;
        let arr = [];
        let loop = (data) => {
            data.map((item, index) => {
                arr.push(item.key);
                if (item.children && item.children.length > 0) {
                    loop(item.children)
                }
            })
        }
        loop(data);
        this.setState({
            expandedKeys: arr
        })
    }
    /**
     * 展开
     * @param expandedKeys
     */
    onExpand = expandedKeys => {
        console.log('onExpand', expandedKeys);
        this.setState({
            expandedKeys,
            autoExpandParent: false
        });
    };
    /**
     * 点击
     * @param checkedKeys
     */
    onCheck = (checkedKeys) => {
        this.setState({checkedKeys})
    };

    /**
     * 选中的事件
     * @param ks
     */
    onSelected = (ks) => {
        if (ks && ks.length > 0) {
            this.getDepartmentDetail(ks[0])
        }
    };
    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        this.getDepartmentTree();
    };

    /**
     * 根据部门的id 获取部门的详情
     * @param departmentId
     */
    getDepartmentDetail = (departmentId) => {
        const that = this;
        let url = event.event.url + '/v1/department/getDepartmentDetail/' + departmentId;
        this.$http.get(url)
            .then(function (response) {
                const {data} = response;
                const resp = data.data;
                let level = numberChinese(resp.level) + '级部门';
                that.setState({
                    currentDepartmentName: resp.name,
                    currentDepartmentLevel: level,
                    level: resp.level,
                    currentDepartmentLastRange: resp.parentName,
                    currentDepartmentDes: resp.description,
                    currentDepartmentParentId: resp.parentId,
                    currentDepartmentId: resp.id,
                    currentDepartmentCode: resp.code
                }, () => that.getInstancePage())
            })
            .catch(function (error) {
                Message.error(error.message);
            })
    };

    /**
     * 展开数据
     * @param arr
     */
    expandedArr = (arr) => {
        function expanded(arrays) {
            const a = [], len = arrays.length;
            if (len === 0) {
                return [];
            }
            for (let i = 0; i < len; i++) {
                var item = arrays[i];
                a.push({
                    title: item.title,
                    key: item.key,
                    children: expanded(item.children)
                })
            }
            return a;
        }

        return expanded(arr);
    };
    /**
     * 获取数据
     */
    getDepartmentTree = () => {
        const that = this;
        let url = event.event.url + '/v1/department/getDepartmentTree';
        this.$http.get(url)
            .then(function (response) {
                const {data} = response;
                const arr = data.data, dataArr = that.expandedArr(arr.children, that);
                if (arr.children && arr.children.length > 0) {
                    that.getDepartmentDetail(arr.children[0].key);
                }
                that.setState({
                    data: dataArr
                })
            })
            .catch(function (error) {
                Message.error(error.message);
            })

    };
    /**
     * 条件
     * @param e
     * @param value
     */
    searchCondition = (value, e) => {
        this.setState({
            condition: value,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        });
        this.pageNum = 1;
    };


    /**
     * 删除当前组织机构
     */
    deleteDepartment = (item) => {
        this.setState({
                departmentConfig: {
                    data: item,
                    visible: true,
                    title: "删除部门",
                    content: "删除后将不能恢复，您确认要删除当前部门吗？"
                }
            }
        )
    };

    /**
     * 添加组织机构
     */
    addDepartment = (item) => {
        this.editDepartment(item, true);
    };

    /**
     * 编辑
     * @param item
     */
    editDepartment = (item, add) => {
        const that = this;
        let url = event.event.url + '/v1/department/getDepartmentDetail/' + item.key;
        this.$http.get(url)
            .then(function (response) {
                const {data} = response;
                const resp = data.data;
                that.setState({
                    departmentDrawerShow: true,
                    departmentAdd: add,
                    departmentDrawerValue: {
                        parentId: add ? resp.id : resp.parentId,
                        level: resp.level + 1,
                        id: add ? "" : resp.id,
                        name: add ? "" : resp.name,
                        remarks: add ? "" : resp.description
                    },
                })
            })
            .catch(function (error) {
                Message.error(error.message);
            })
    };
    /*********************************************** 组织机构 ****************************************************/
    /*********************************************** 部門 ****************************************************/
    /**
     * 编辑部门
     */
    onDepartEdit = () => {
        this.setState({
            departmentDrawerShow: true,
            departmentAdd: false,
            departmentDrawerValue: {
                level: this.state.level,
                parentId: this.state.currentDepartmentParentId,
                id: this.state.currentDepartmentId,
                name: this.state.currentDepartmentName,
                remarks: this.state.currentDepartmentDes
            }
        })
    };

    /**
     * 抽屉关闭调用
     */
    onCloseDepart = () => {
        this.setState({
            departmentDrawerShow: false,
            departmentAdd: false,
            departmentDrawerValue: {
                level: "",
                parentId: "",
                id: "",
                name: "",
                remarks: ""
            }
        });
    };
    /**
     * 提交部门的相关数据
     * @param value 表单的值
     * @param e 2021年6月9日00:45:35
     */
    submitDepartment = (value, e) => {
        if (e == null) {
            const that = this;
            const item = {
                ...value,
                description: value.remarks,
            };
            const {departmentAdd} = this.state;
            let url = departmentAdd ? event.event.url + '/v1/department/addDepartment' : event.event.url + '/v1/department/updateDepartment';
            this.$http.post(url, item)
                .then(function (response) {
                    that.onCloseDepart();
                    that.getDepartmentTree();
                })
                .catch(function (error) {
                    Message.error(error.message);
                })
        }
        console.log(e);
    };


    /**
     * 取消按钮的操作
     */
    departmentCancelCall = () => {
        this.setState({
            departmentConfig: {
                visible: false
            }
        });
        console.log("点击取消按钮 .");
    };


    /**
     * 删除
     */
    departmentOkCall = (data) => {
        this.setState({
            departmentConfig: {
                visible: false
            }
        });
        const _this = this;
        this.$http.post(event.event.url + '/v1/org/changeStatus', {id: data.id, status: data.status === 1 ? 0 : 1})
            .then(function (response) {
                const {data} = response;
                if (data.code === 1) {
                    Message.warning(data.message ? data.message : data.data);
                } else {
                    Message.success("操作成功.");
                    _this.getAdminTenantList();
                }
            })
            .catch(function (error) {
                Message.error(error.message);
            })
    };
    /*********************************************** 部門 ****************************************************/
    /*********************************************** 崗位 ****************************************************/
    /**
     * 开启调整的弹框
     * @param record
     */
    toPostAuthUpdate = record => {
        this.setState({
            postAuthManegeDrawerShow: true,
            postAuthManegeDrawerValue: {
                name: record.name,
                id: record.id,
                roleId: [],
                roleIdList: []
            }
        }, () => {

        });
    };

    /**
     * 获取树
     * @param roleInstanceId
     */
    getPostAuthTree = () => {
        const roleInstanceId = this.state.postAuthManegeDrawerValue.id;
        return this.$http.get(event.event.url + '/v1/instance/getInstanceAuthTree/' + roleInstanceId);
    };

    /**
     * 保存当前的值
     * @param data
     * @param e
     */
    onFinishManageAuth = (data, e) => {
        const {id, checkedKeys, authTree} = this.state;
        recursionReset(authTree, this.state.checkedKeys);
        const v = {
            ...data,
            authIdList: checkedKeys,
            instanceId: data.id
        };
        const _this = this;
        this.$http.post(event.event.url + '/v1/role/updateRoleInstanceAuth', {
            ...v
        })
            .then(function (response) {
                const {data} = response;
                if (data.code === 1) {
                    Message.warning(data.message ? data.message : data.data);
                } else {
                    Message.success("操作成功.");
                    _this.setState({
                        postAuthManegeDrawerShow: false,
                        postAuthManegeDrawerValue: {}
                    },()=>{
                        _this.getInstancePage();
                    });
                }
            })
            .catch(function (error) {
                Message.error(error.message);
            })

    };


    /**
     * 回调
     * @param checkedKeys
     * @param authTree
     */
    callbackPostAuth = (checkedKeys, authTree) => {
        this.setState({
            checkedKeys,
            authTree
        })
    };
    /**
     * 岗位编辑
     * @param record
     */
    toPostUpdate = record => {
        this.setState({
            instanceDrawerShow: true,
            instanceAdd: true,
            instanceDrawerValue: {
                departmentId: record.departmentId,
                description: record.description,
                level: record.level,
                name: record.name,
                roleId: record.roleId,
                id: record.id
            }
        }, () => this.getRoleList());
    };

    /**
     * 删除记录的弹框
     * @param record
     */
    toPostDelete = record => {
        this.setState({
            postConfig: {
                data: record,
                visible: true,
                title: "是否真的要删除当前岗位？",
                content: ""
            },
        });
    };
    /**
     * 获取角色的列表
     */
    getRoleList = () => {
        const _this = this;
        let url = event.event.url + '/v1/role/getRoleList';
        this.$http.get(url)
            .then(function (response) {
                const {data} = response;
                let aa = data.data.map(o => {
                    return {
                        value: o.id,
                        label: o.name
                    }
                });
                _this.setState({
                    roleList: aa
                })
            })
            .catch(function (error) {
                Message.error(error.message);
            })
    };


    /**
     * 获取岗位的列表
     */
    getInstancePage = () => {
        const _this = this;
        let url = event.event.url + '/v1/instance/getInstancePage';
        this.$http.post(url,
            {
                pageSize: this.pageSize,
                pageNum: this.pageNum,
                condition: {
                    "departmentId": this.state.currentDepartmentId,
                    "name": this.state.condition,
                    "roleName": ""
                }
            })
            .then(function (response) {
                const {data} = response;
                const mockData = data.data.voList;
                _this.totalNum = data.data.total;
                mockData.forEach((o, i) => {
                    o.number = i + 1;
                });

                _this.setState({
                    mockData: mockData,
                    config: {
                        data: null,
                        visible: false,
                        title: "",
                        content: ""
                    }
                });
            })
            .catch(function (error) {
                Message.error(error.message);
            })
    };
    /**
     * 抽屉关闭调用
     */
    addInstance = () => {
        this.setState({
            instanceDrawerShow: true,
            instanceAdd: true,
            instanceDrawerValue: {
                departmentId: this.state.currentDepartmentId,
                description: "",
                level: this.state.level,
                name: "",
                roleId: ""
            }
        }, () => this.getRoleList());
    };
    /**
     * 抽屉关闭调用
     */
    onCloseInstance = () => {
        this.setState({
            instanceDrawerShow: false,
            instanceAdd: false,
            instanceDrawerValue: {
                departmentId: this.state.currentDepartmentId,
                description: "",
                level: this.state.level,
                name: "",
                roleId: ""
            }
        });
    };

    /**
     * 提交部门的相关数据
     * @param value 表单的值
     * @param e 2021年6月9日00:45:35
     */
    submitInstance = (value, e) => {
        if (e == null) {
            const that = this;
            const item = {
                ...value
            };
            let url = item.id ? event.event.url + '/v1/instance/updateInstance' : event.event.url + '/v1/instance/addInstance';
            this.$http.post(url, item)
                .then(function (response) {
                    that.setState({
                        instanceDrawerShow: false,
                        instanceAdd: false,
                        instanceDrawerValue: {
                            departmentId: that.state.currentDepartmentId,
                            description: "",
                            level: that.state.level,
                            name: "",
                            roleId: ""
                        }
                    }, () => {
                        that.getInstancePage();
                    });
                })
                .catch(function (error) {
                    Message.error(error.message);
                })
        }
        console.log(e);
    };

    /**
     * 删除
     */
    postOkCall = () => {
        const data = this.state.postConfig.data;
        const _this = this;
        this.$http.post(event.event.url + '/v1/instance/deleteInstance/' + data.id)
            .then(function (response) {
                const {data} = response;
                if (data.code === 1) {
                    Message.warning(data.message ? data.message : data.data);
                } else {
                    Message.success("操作成功.");
                    _this.setState(
                        {
                            postConfig: {
                                data: null,
                                visible: false,
                                title: "",
                                content: ""
                            },
                        }, () => {
                            _this.getInstancePage();
                        });
                }
            })
            .catch(function (error) {
                Message.error(error.message);
            })
    };


    /**
     * 删除
     */
    postCancel = (data) => {
        this.setState({
            postConfig: {
                data: null,
                visible: false,
                title: "",
                content: ""
            },
        });

    };
    /*********************************************** 崗位 ****************************************************/
    /*********************************************** 用户 ****************************************************/
    /**
     * 初始化用户
     */
    initPostUser = () => {
        const {postDrawerEditMsg} = this.state;
        const _this = this;
        this.$http.get(event.event.url + '/v1/instance/getInstanceUser/' + postDrawerEditMsg.id)
            .then(function (resp) {
                const userList = [];
                resp.data.data.all.forEach(item => {
                    userList.push(<Select.Option value={item.id} key={item.id}>{item.name}</Select.Option>)
                });
                _this.setState({
                    userDrawerValue: {
                        userList: userList,
                        userIdList: resp.data.data.selected,
                        name: postDrawerEditMsg.name,
                    }
                })
            })

            .catch(function (error) {
                Message.error(error.message);
            })

    }

    /**
     * 完成添加人员操作
     * @param value
     */
    onFinishAddUser = (value, e) => {
        if (e == null) {
            const {postDrawerEditMsg} = this.state;
            const values = {
                ...value,
                id: postDrawerEditMsg.id
            };
            const _this = this;
            this.$http.post(event.event.url + '/v1/instance/addUserToInstance', {
                ...values
            })
                .then(function (resp) {
                    Message.success('新增成功');
                    _this.onClose()
                })

                .catch(function (error) {
                    Message.error(error.message);
                });
        }
    };


    /**
     *
     */
    onClose = () => {
        this.setState({
            visible: false,
            isVisible: false,
            visibleUser: false
        });
    };


    /**
     * 添加按钮
     * @param record
     */
    toAddUser = record => {
        this.setState({
            visibleUser: true,
            postDrawerEditMsg: record
        }, () => {
            this.initPostUser();
        })
    };

    /*********************************************** 用户 ****************************************************/



    render() {
        const userVo = JSON.parse(sessionStorage.getItem("userInfoVO"));
        const {
            data,
            expandedKeys,
            autoExpandParent,
            checkedKeys,
            currentDepartmentName,
            currentDepartmentLevel,
            currentDepartmentLastRange,
            currentDepartmentDes,
            mockData,
            currentDepartmentParentId,
        } = this.state;
        return (
            <div>
                <Drawer
                    title="修改部门"
                    closeMode={['close', 'esc', 'mask']}
                    visible={this.state.departmentDrawerShow}
                    width={550}
                    placement={this.state.placement}
                    onClose={this.onCloseDepart}
                >
                    <Form {...formItemLayout}>

                        <FormItem
                            hidden={true}
                        >
                            <NextInput
                                id="parentId"
                                name="parentId"
                                defaultValue={this.state.departmentDrawerValue.parentId}
                                aria-required="true"/>
                        </FormItem>
                        <FormItem
                            hidden={true}
                        >
                            <NextInput
                                id="level"
                                name="level"
                                defaultValue={this.state.departmentDrawerValue.level}
                                aria-required="true"/>
                        </FormItem>
                        <FormItem
                            hidden={true}
                        >
                            <NextInput
                                id="id"
                                name="id"
                                defaultValue={this.state.departmentDrawerValue.id}
                                aria-required="true"/>
                        </FormItem>


                        <FormItem

                            required
                            requiredMessage={"名称不能为空"}
                            label="部门名称："
                        >
                            <NextInput
                                id="name"
                                name="name"
                                defaultValue={this.state.departmentDrawerValue.name}
                                aria-required="true"/>
                        </FormItem>
                        <FormItem
                            label="描述："
                        >
                            <NextInput.TextArea id="remarks"
                                                name="remarks"
                                                defaultValue={this.state.departmentDrawerValue.remarks}
                                                placeholder="描述"/>
                        </FormItem>
                        <FormItem wrapperCol={{offset: 6}}>
                            <Form.Submit
                                validate
                                type="primary"
                                onClick={(v, e) => this.submitDepartment(v, e)}
                                style={{marginRight: 10}}
                            >
                                提交
                            </Form.Submit>
                            <Form.Reset>重置</Form.Reset>
                        </FormItem>
                    </Form>
                </Drawer>
                <Drawer
                    title="岗位管理"
                    closeMode={['close', 'esc', 'mask']}
                    visible={this.state.instanceDrawerShow}
                    width={550}
                    placement={this.state.placement}
                    onClose={this.onCloseInstance}
                >
                    <Form {...formItemLayout}>

                        <FormItem
                            hidden={true}
                        >
                            <NextInput
                                id="departmentId"
                                name="departmentId"
                                defaultValue={this.state.instanceDrawerValue.departmentId}
                                aria-required="true"/>
                        </FormItem>
                        <FormItem
                            hidden={true}
                        >
                            <NextInput
                                id="id"
                                name="id"
                                defaultValue={this.state.instanceDrawerValue.id}
                                aria-required="true"/>
                        </FormItem>
                        <FormItem
                            hidden={true}
                        >
                            <NextInput
                                id="level"
                                name="level"
                                defaultValue={this.state.instanceDrawerValue.level}
                                aria-required="true"/>
                        </FormItem>
                        <FormItem
                            hidden={true}
                        >
                            <NextInput
                                id="roleId"
                                name="roleId"
                                defaultValue={this.state.instanceDrawerValue.roleId}
                                aria-required="true"/>
                        </FormItem>

                        <FormItem
                            required
                            requiredMessage={"名称不能为空"}
                            label="岗位名称："
                        >
                            <NextInput
                                id="name"
                                name="name"
                                defaultValue={this.state.instanceDrawerValue.name}
                                aria-required="true"/>
                        </FormItem>

                        <FormItem
                            label="角色："
                            required
                            requiredMessage={"请选择角色"}
                        >
                            <Select style={{width: 200}} defaultValue={this.state.instanceDrawerValue.roleId}
                                    dataSource={this.state.roleList} placeholder="请选择角色" id="roleId" name="roleId">
                            </Select>
                        </FormItem>


                        <FormItem
                            label="岗位描述："
                        >
                            <NextInput.TextArea id="description"
                                                name="description"
                                                defaultValue={this.state.instanceDrawerValue.description}
                                                placeholder="岗位描述"/>
                        </FormItem>
                        <FormItem wrapperCol={{offset: 6}}>
                            <Form.Submit
                                validate
                                type="primary"
                                onClick={(v, e) => this.submitInstance(v, e)}
                                style={{marginRight: 10}}
                            >
                                提交
                            </Form.Submit>
                            <Form.Reset>重置</Form.Reset>
                        </FormItem>
                    </Form>
                </Drawer>


                <Drawer
                    title="添加人员"
                    width={630}
                    onClose={this.onClose}
                    visible={this.state.visibleUser}
                    bodyStyle={{paddingBottom: 80}}
                >
                    <Form {...formItemLayout} name="userDrawer">
                        <Form.Item
                            name="name"
                            label="岗位名称"
                            aria-disabled={"true"}
                            rules={[
                                {
                                    required: true,
                                },
                            ]}
                        >
                            <Input id="name" defaultValue={this.state.userDrawerValue.name}
                                   name="name" disabled={true}/>
                        </Form.Item>
                        <Form.Item
                            label="岗位人员"
                            name="userIdList"
                        >
                            <Select
                                id={"userIdList"}
                                name={"userIdList"}
                                showSearch
                                allowClear
                                defaultValue={this.state.userDrawerValue.userIdList}
                                placeholder="请选择岗位人员"
                                optionFilterProp="children"
                                mode="multiple"
                            >
                                {this.state.userDrawerValue.userList}
                            </Select>
                        </Form.Item>
                        <FormItem wrapperCol={{offset: 6}}>
                            <Form.Submit
                                validate
                                type="primary"
                                onClick={(v, e) => this.onFinishAddUser(v, e)}
                                style={{marginRight: 10}}
                            >
                                提交
                            </Form.Submit>
                            <Form.Reset>重置</Form.Reset>
                        </FormItem>
                    </Form>
                </Drawer>


                <Drawer
                    title="岗位权限管理"
                    width={630}
                    onClose={this.onClose}
                    visible={this.state.postAuthManegeDrawerShow}
                    bodyStyle={{paddingBottom: 80}}
                >
                    <Form {...formItemLayout} name="userDrawer">
                        <FormItem
                            hidden={true}
                        >
                            <NextInput
                                id="id"
                                name="id"
                                defaultValue={this.state.postAuthManegeDrawerValue.id}
                                aria-required="true"/>
                        </FormItem>
                        <Form.Item
                            name="name"
                            label="岗位名称"
                            aria-disabled={"true"}
                            disabled="true"
                            rules={[
                                {
                                    required: true,
                                },
                            ]}
                        >
                            <Input id="name" defaultValue={this.state.postAuthManegeDrawerValue.name}
                                   name="name" disabled={true}/>
                        </Form.Item>

                        <PipeTree
                            id={this.state.postAuthManegeDrawerValue.id}
                            callback={this.callbackPostAuth}
                            functions={this.getPostAuthTree}
                        />

                        <FormItem wrapperCol={{offset: 6}}>
                            <Form.Submit
                                validate
                                type="primary"
                                onClick={(v, e) => this.onFinishManageAuth(v, e)}
                                style={{marginRight: 10}}
                            >
                                提交
                            </Form.Submit>
                            <Form.Reset>重置</Form.Reset>
                        </FormItem>
                    </Form>
                </Drawer>


                <div className='container-header'>
                    <p>权限管理</p>
                </div>
                <Container className='container-main'>
                    <Row>
                        <Col span={18} push={6}>
                            <Card
                                title={
                                    <div>
                                        <span>{currentDepartmentName}</span>
                                        <Button size="small"
                                                style={{margin: '0 20px'}}>{currentDepartmentLevel}</Button>
                                        {
                                            currentDepartmentParentId
                                                ?
                                                <Button type="primary"
                                                        icon={<Icon type="set" title={"编辑"} size={'medium'}
                                                                    style={{paddingRight: '10px'}}/>}
                                                        onClick={() => this.onDepartEdit()}
                                                        style={{marginRight: 20}}>编辑</Button>
                                                :
                                                ''
                                        }
                                        <p className={styles.depart_detail_description}>上级部门：{currentDepartmentLastRange ? currentDepartmentLastRange : '无'}</p>
                                        <p className={styles.depart_detail_description}>部门描述：{currentDepartmentDes ? currentDepartmentDes : '无'}</p>
                                    </div>
                                }
                                bordered={false}
                                style={{minHeight: 360}}
                                className="depart_detail"
                            >
                                <div className={styles.codeBox}>
                                    <Container className='container-main'>
                                        <div className='container-btnBox' style={{marginTop: '0'}}>
                                            <div className={styles.buttons}>
                                                <Button onClick={() => this.addInstance()}
                                                        className='pipe-btn-add'>新建岗位 {this.state.postConfig.visible}</Button>
                                                <span className={styles.caseNumber}>
                                <Input onChange={this.searchCondition.bind(this)}
                                       hasClear
                                       placeholder="请输入岗位代码/岗位名称"
                                       className={`${styles.input} ${styles.shortInput}`}/>
                                <Button
                                    type="primary"
                                    className='pipe-btn-search'
                                    onClick={this.getInstancePage}
                                >
                                查询
                                </Button>
                            </span>
                                            </div>
                                        </div>
                                        <div className='container-table'>
                                            <Table dataSource={mockData} primaryKey="id" className={styles.table}>
                                                <Table.Column align="center" title="序号" dataIndex="number"/>
                                                <Table.Column align="center" title="岗位名称" dataIndex="name"/>
                                                <Table.Column align="center" title="岗位描述" dataIndex="description"/>
                                                <Table.Column align="center" title="操作" width={350} cell={
                                                    (value, index, record) => {
                                                        return (
                                                            <div>
                                                                <Button size="small" type="primary" text
                                                                        className='pipe-btn-delete'
                                                                        onClick={() => this.toPostDelete(record)}>删除岗位</Button>
                                                                <Button size="small" type="primary" text
                                                                        className='pipe-btn-edit'
                                                                        onClick={() => {
                                                                            this.toPostUpdate(record)
                                                                        }}>编辑岗位</Button>
                                                                <Button size="small" type="primary" text
                                                                        className='pipe-btn-edit'
                                                                        onClick={() => {
                                                                            this.toPostAuthUpdate(record)
                                                                        }}>岗位权限调整</Button>
                                                                <Button size="small" type="primary" text
                                                                        className='pipe-btn-edit'
                                                                        onClick={() => {
                                                                            this.toAddUser(record)
                                                                        }}>添加人员</Button>
                                                            </div>
                                                        )

                                                    }
                                                }/>
                                            </Table>
                                            <ZgDialog config={this.state.config} cancelCall={this.cancelCall}
                                                      okCall={this.okCall}/>
                                            <Pagination defaultCurrent={1}
                                                        pageSize={this.pageSize}
                                                        total={this.totalNum}
                                                        onChange={this.onPageChange}
                                                        onPageSizeChange={this.onPageSizeChange}
                                                        pageSizeList={[5,10,20,50,100,200]}
                                                        pageSizeSelector="dropdown"
                                                        className="page-demo"
                                                        size="small"
                                            />
                                        </div>
                                    </Container>
                                </div>
                            </Card>

                        </Col>
                        <Col span={6} pull={18}>
                            <div className={styles.treeDev}>
                                <div className={styles.codeBox}>
                                    <Search style={{marginBottom: 8}} placeholder="Search" onChange={this.onChange}/>
                                    <Tree
                                        blockNode
                                        expandedKeys={expandedKeys} //默认展开的key
                                        onExpand={this.onExpand} //展开事件
                                        autoExpandParent={autoExpandParent} //是否自动展开父节点
                                        checkedKeys={checkedKeys} //选中的key
                                        defaultExpandAll={true} //默认展开所有
                                        onCheck={this.onCheck} //选中事件
                                        onSelect={this.onSelected} //选中的状态
                                    >
                                        {this.renderTreeNode(data)}
                                    </Tree>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Container>

                <ZgDialog config={this.state.departmentConfig} cancelCall={this.departmentCancelCall}
                          okCall={this.departmentOkCall}/>
                <ZgDialog config={this.state.postConfig} cancelCall={this.postCancel}
                          okCall={this.postOkCall}/>
            </div>
        )
            ;
    }
}

export default Department;

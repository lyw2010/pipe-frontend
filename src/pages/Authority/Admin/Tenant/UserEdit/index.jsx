import React from 'react';
import {Select} from '@alifd/next';
import $http from '@/service/Services';
import UserEditComponents from "../../../components/UserEditComponents";

const axios = $http;

const {Option} = Select;
let form;


/**
 * list
 */
class AdminUserEdit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: "",
            tenantId: ""
        };

    }

    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        const params = new URLSearchParams(this.props.location.search);
        const id = params.get("id");
        const tenantId = params.get("tenantId");
        this.setState({
            id: id,
            tenantId: tenantId
        }, () => {

        });


    };



    /**
     * 表
     * @returns {*}
     */
    render() {
        return (
            <UserEditComponents user={"admin"} history={this.props.history} id={this.state.id} tenantId={this.state.tenantId}/>
        );
    }

}

export default AdminUserEdit;

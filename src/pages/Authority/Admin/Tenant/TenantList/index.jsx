/**
 * @description: 数据中台-权限管理-租户管理-新建租户
 * @author: hj
 * @update: hj(2020-01-14)
 */
import React from 'react';
import Container from '@icedesign/container';
import styles from './index.module.scss';
import event from '@/request'
import {Button, Input, MenuButton, Message, Pagination, Table} from "@alifd/next";
import ZgDialog from '@/components/ZgDialog';
import {Link} from "react-router-dom";
import $http from '@/service/Services';

const {Item} = MenuButton;


/**
 * list
 */
class AdminTenantList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            subjectId: "",
            current: 0,
            mockData: [],
            selectedList: [],
            condition: "",
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        };
        // 当前页数
        this.pageNum = 1;
        // 分页每页显示数据条数
        this.pageSize = 10;
        // 数据总条数
        this.totalNum = 0;
        this.$http = $http;
        this.cancelCall = this.cancelCall.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.onPageChange = this.onPageChange.bind(this);
        this.getAdminTenantList = this.getAdminTenantList.bind(this);
        this.changeStatusAdminTenant = this.changeStatusAdminTenant.bind(this);
    }

    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        const params = new URLSearchParams(this.props.location.search);
        const id = params.get("id");
        this.setState({
            subjectId: id
        }, () => {
            this.getAdminTenantList();
        });
    };


    /**
     * 条件
     * @param e
     * @param value
     */
    searchCondition = (value, e) => {
        this.setState({
            condition: value,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        });
        this.pageNum = 1;
    };
    /**
     * 改变页面
     * @param currentPage
     */
    onPageChange = (currentPage) => {
        this.state = {
            current: currentPage,
            mockData: this.props.mockData,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        };
        this.getAdminTenantList();
    };
    /**
     * 改变分页的大小
     * @param pageSize
     */
    onPageSizeChange=(pageSize)=>{
        this.pageSize = pageSize;
        this.pageNum = 1;
        this.getAdminTenantList();
    };
    /**
     * 修改分页数据
     * @param currentPage
     */
    onChangePage = (currentPage) => {
        this.setState({
            loading: true,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        });
        setTimeout(() => {
            this.getAdminTenantList(currentPage);
            this.setState({
                loading: false,
                config: {
                    data: null,
                    visible: false,
                    title: "",
                    content: ""
                }
            });
        }, 0);
    };

    /**
     * 获取数据
     */
    getAdminTenantList = () => {
        const _this = this;
        //that.pageSize + '/' + that.pageNum
        let url = event.event.url + '/v1/org/getOrgPage';
        this.$http.post(url,
            {
                pageSize: this.pageSize,
                pageNum: this.pageNum,
                condition: {
                    name: this.state.condition
                }
            })
            .then(function (response) {
                const {data} = response;
                const mockData = data.data.voList;
                _this.totalNum = data.data.total;
                mockData.forEach((o, i) => {
                    o.number = i + 1;
                });

                _this.setState({
                    mockData: mockData,
                    config: {
                        data: null,
                        visible: false,
                        title: "",
                        content: ""
                    }
                });
            })
            .catch(function (error) {
                Message.error(error.message);
            })

    };


    /**
     * 删除
     */
    changeStatusAdminTenant = (row) => {
        if (row.status === 1) {
            this.setState({
                    config: {
                        data: row,
                        visible: false,
                        title: "",
                        content: ""
                    }
                }
                , () => {
                    this.okCall(row);
                });
        } else {
            this.setState({
                    config: {
                        data: row,
                        visible: true,
                        title: "冻结租户",
                        content: "租户冻结后无法正常登录后台，您确认要冻结该租户吗？"
                    }
                }
            )
        }

    };

    /**
     * 取消按钮的操作
     */
    cancelCall = () => {
        this.setState({
            config: {
                visible: false
            }
        });
        console.log("点击取消按钮 .");
    };


    /**
     * 删除
     */
    okCall = (data) => {
        this.setState({
            config: {
                visible: false
            }
        });
        const _this = this;
        this.$http.post(event.event.url + '/v1/org/changeStatus', {id: data.id, status: data.status === 1 ? 0 : 1})
            .then(function (response) {
                const {data} = response;
                if (data.code === 1) {
                    Message.warning(data.message ? data.message : data.data);
                } else {
                    Message.success("操作成功.");
                    _this.getAdminTenantList();
                }
            })
            .catch(function (error) {
                Message.error(error.message);
            })
    };


    /**
     * 批量操作
     * @param text
     */
    handleClick = () => {
        const {selectedList} = this.state;
        if (selectedList.length == 0) {
            Message.error("请选择需要删除的数据.");
            return null;
        }
        this.setState({
            visible: true
        });
    };

    /**
     *
     * @param args
     */
    onChange = (...args) => {
        this.setState({
            selectedList: args
        });
    };


    render() {
        const {mockData} = this.state;
        const userVo = JSON.parse(sessionStorage.getItem("userInfoVO"));
        return (
            <div>
                <div className='container-header'>
                    <p>租户管理</p>
                </div>
                <Container className='container-main'>
                    <div className='container-btnBox' style={{marginTop: '0'}}>
                        <div className={styles.buttons}>
                            <Link to={"orgAdd?id=&parentId=" + userVo.rolePermissionVO.orgId}><Button
                                className='pipe-btn-add'>新建租户</Button></Link>
                            <span className={styles.caseNumber}>
                                <Input onChange={this.searchCondition.bind(this)}
                                       hasClear
                                       placeholder="请输入租户代码/租户名称"
                                       className={`${styles.input} ${styles.shortInput}`}/>
                                <Button
                                    type="primary"
                                    className='pipe-btn-search'
                                    onClick={this.getAdminTenantList}
                                >
                                查询
                                </Button>
                            </span>
                        </div>
                    </div>
                    <div className='container-table'>
                        <Table dataSource={mockData} primaryKey="id" className={styles.table}>
                            <Table.Column align="center" title="序号" dataIndex="number"/>
                            <Table.Column align="center" title="租户代码" dataIndex="code"/>
                            <Table.Column align="center" title="租户名称" dataIndex="name"/>
                            <Table.Column align="center" title="管理员用户" dataIndex="managerUser"/>
                            <Table.Column align="center" title="服务开通日期" dataIndex="startTime"/>
                            <Table.Column align="center" title="服务结束日期" dataIndex="endTime"/>
                            <Table.Column align="center" title="状态" dataIndex="status" cell={
                                (value) => {
                                    return value === 0 ? "正常" : "已冻结"
                                }

                            }/>
                            <Table.Column align="center" title="操作" cell={
                                (value, index, record) => {
                                    if (record.parentId && record.parentId != 1) {
                                        return (
                                            <div>
                                                <Button type="normal" size="small" text className='pipe-btn-edit'>
                                                    <Link to={"orgAdd?id=" + record.id}>修改</Link>
                                                </Button>
                                                <Button type="normal" size="small" text className='pipe-btn-edit'>
                                                    <Link to={"userManageList?orgId=" + record.id}>用户管理</Link>
                                                </Button>
                                                <Button type="normal" size="small" text
                                                        className='pipe-btn-delete'
                                                        onClick={this.changeStatusAdminTenant.bind(this, record)}
                                                        warning>
                                                    <a onClick={() => {
                                                    }}>{record.status === 0 ? "冻结" : "启用"}</a>
                                                </Button>
                                            </div>
                                        )
                                    } else {
                                        return (
                                            <div>
                                                <Button type="normal" size="small" text className='pipe-btn-edit'>
                                                    <Link to={"orgAdd?id=" + record.id}>修改</Link>
                                                </Button>
                                                <Button type="normal" size="small" text className='pipe-btn-edit'>
                                                    <Link to={"department?id=" + record.id}>岗位配置</Link>
                                                </Button>
                                                <Button type="normal" size="small" text className='pipe-btn-edit'>
                                                    <Link to={"roleList?orgId=" + record.id}>角色管理</Link>
                                                </Button>
                                                <Button type="normal" size="small" text className='pipe-btn-edit'>
                                                    <Link to={"userManageList?orgId=" + record.id}>用户管理</Link>
                                                </Button>
                                                <Button type="normal" size="small" text
                                                        className='pipe-btn-delete'
                                                        onClick={this.changeStatusAdminTenant.bind(this, record)}
                                                        warning>
                                                    <a onClick={() => {
                                                    }}>{record.status === 0 ? "冻结" : "启用"}</a>
                                                </Button>
                                            </div>
                                        )
                                    }

                                }
                            }/>
                        </Table>
                        <ZgDialog config={this.state.config} cancelCall={this.cancelCall} okCall={this.okCall}/>
                        <Pagination defaultCurrent={1}
                                    pageSize={this.pageSize}
                                    total={this.totalNum}
                                    onChange={this.onChangePage}
                                    onPageSizeChange={this.onPageSizeChange}
                                    pageSizeList={[5,10,20,50,100,200]}
                                    pageSizeSelector="dropdown"
                                    hideOnlyOnePage
                                    className="page-demo"
                                    size="small"
                        />
                    </div>
                </Container>
            </div>
        )
            ;
    }
}

export default AdminTenantList;

import React from 'react';
import RoleEditComponents from "../../../components/RoleEditComponents";


/**
 * list
 */
class AdminRoleEdit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: {},
            auths: [],
            id: "",
            defaultActiveKey: "",
            tenantId: "",
            selectedList: [],
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        };

    }

    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        const params = new URLSearchParams(this.props.location.search);
        const id = params.get("id");
        const tenantId = params.get("tenantId");
        this.setState({
            id: id,
            tenantId: tenantId
        });

    };

    /**
     * 表
     * @returns {*}
     */
    render() {
        return (
            <RoleEditComponents user={"admin"}  history={this.props.history} id={this.state.id} tenantId={this.state.tenantId}/>
        );
    }

}

export default AdminRoleEdit;

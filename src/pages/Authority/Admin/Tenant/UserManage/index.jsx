/**
  * @description: 数据中台-权限管理-租户管理-用户管理
  * @author: hj
  * @update: hj(2020-01-14)
  */
import React from 'react';
import {Button, MenuButton} from "@alifd/next";
import $http from '@/service/Services';
import UserListComponent from "../../../components/UserList";

const {Item} = MenuButton;


/**
 * list
 */
class UserManageList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            orgId: "",
            current: 0,
            mockData: [],
            selectedList: [],
            condition: "",
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        };
        // 当前页数
        this.pageNum = 1;
        // 分页每页显示数据条数
        this.pageSize = 10;
        // 数据总条数
        this.totalNum = 0;
        this.$http = $http;
    }

    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        const params = new URLSearchParams(this.props.location.search);
        const id = params.get("orgId");
        this.setState({
            orgId: id
        }, () => {

        });
    };


    render() {
        return (
            <div>
                <div className='container-header'>
                    <p style={{display: 'inline-block'}}>用户管理</p>
                    <Button type="normal" size="small" className='next-btn-normal table-btn-return' style={{float: 'right'}}>
                        <a onClick={() => {
                            this.props.history.go(-1)
                        }} > 返回</a>
                    </Button>
                </div>
                <UserListComponent  user={"admin"}   history={this.props.history} orgId={this.state.orgId}/>
            </div>
        )
            ;
    }

}

export default UserManageList;

/**
  * @description: 数据中台-权限管理-租户管理-租户配置
  * @author: hj
  * @update: hj(2020-01-14)
  */
import React from 'react';
import {Button, Radio, Select, Tab} from '@alifd/next';
import styles from './index.module.scss';
import $http from '@/service/Services';
import QuotaTenant from "./component/Quota";
import Container from "@icedesign/container";

const axios = $http;

const {Option} = Select;
const {Group: RadioGroup} = Radio;
let form;


/**
 * list
 */
class AdminTenantConfig extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: {},
            id: "",
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        };
    }

    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        const params = new URLSearchParams(this.props.location.search);
        const id = params.get("id");
        this.setState({
            id: id
        });

    };
    handleChange = (key) => {
        console.log(key);
    };

    handleClick = () => {
        console.log('hello world');
    };

    /**
     * 表
     * @returns {*}
     */
    render() {
        return (
            <div>
                <div className='container-header'>
                    <p style={{display: 'inline-block'}}>租户配置</p>
                    <Button type="normal" size="small" className='next-btn-normal table-btn-return' style={{float: 'right'}}>
                        <a onClick={() => {
                            this.props.history.go(-1)
                        }} > 返回</a>
                    </Button>
                </div>

                
                <Container className='container-main'>
                    <div className={styles.container}>
                        <div className={styles.demoItemTitle}>租户配置</div>
                        <Tab shape="wrapped" tabPosition="left" onChange={this.handleChange}   contentClassName="custom-tab-content">
                            <Tab.Item title="配额配置" key="3"><QuotaTenant history={this.props.history} id={this.state.id}/></Tab.Item>
                        </Tab>
                    </div>
                </Container>

            </div>
        );
    }

}

export default AdminTenantConfig;

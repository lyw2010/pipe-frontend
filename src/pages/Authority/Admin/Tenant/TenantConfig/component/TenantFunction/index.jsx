/**
  * @description: 数据中台-权限管理-租户管理-租户配置-功能配置
  * @author: hj
  * @update: hj(2020-01-14)
  */
import React from 'react';
import IceContainer from '@icedesign/container';
import {Button, Checkbox, Message} from '@alifd/next';
import {
    FormBinder as IceFormBinder,
    FormBinderWrapper as IceFormBinderWrapper,
    FormError as IceFormError,
} from '@icedesign/form-binder';
import styles from './index.module.scss';
import responsive from '@/request'
import $http from '@/service/Services';

const axios = $http;

let form;


/**
 * list
 */
class TenantFunction extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modules: [],
            value: {},
            id: "",
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        };
        this.formChange = this.formChange.bind(this);
        this.validateAllFormField = this.validateAllFormField.bind(this);
    }

    /**
     * 获取所有的权限
     */
    getAllModule = () => {
        const that = this;
        axios.get(responsive.responsive.url + '/v1/auth/getAllModule')
            .then(function (response) {
                const {data} = response;
                if (data.code === 1) {
                    Message.warning(data.message ? data.message : data.data);
                } else {
                    for (let i = 0; i < data.data.length; i++) {
                        data.data[i]["checked"] = false;
                    }
                    that.setState({
                        modules: data.data
                    })
                }
            })
            .catch(function (error) {
                Message.error(error.message);
            })
    };
    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        const id = this.props.id;
        this.setState({
            id: id
        }, () => {
            this.getAllModule();
        });
        if (id) {
            const that = this;
            axios.get(responsive.responsive.url + '/v1/tTenant/getTenantFunction/' + id)
                .then(function (response) {
                    const {data} = response;
                    if (data.code === 1) {
                        Message.warning(data.message ? data.message : data.data);
                    } else {
                        const {modules} = that.state;
                        for (let i = 0; i < modules; i++) {
                            for (let j = 0; j < data.data.length; j++) {
                                modules[i]["checked"] = data.data[i].id === modules[i].id;
                            }
                        }
                        that.setState({
                            modules: modules,
                            value: data.data
                        })
                    }
                })
                .catch(function (error) {
                    Message.error(error.message);
                })
        }
    };

    /* 表改变
    * @param formValue
    */
    formChange = (formValue) => {
        this.setState({
            value: formValue
        })
    };

    /**
     * 表验证
     */
    validateAllFormField = () => {
        form.validateAll((errors, values) => {
            if (errors) {
                Message.warning('请填写必要的数据.');
                return;
            }
            const that = this;
            values["tenantId"] = this.state.id;
            axios.post(responsive.responsive.url + '/v1/tTenant/updateTenantFunction', values)
                .then(function (response) {
                    const {data} = response;
                    if (data.code === 1) {
                        Message.warning(data.message ? data.message : data.data);
                    } else {
                        Message.success("操作成功.");
                        that.props.history.go(-1);
                    }
                })
                .catch(function (error) {
                    Message.error(error.message);
                })
        });
    };

    /**
     * 表
     * @returns {*}
     */
    render() {
        const {modules} = this.state;
        return (
            <div>
                <IceContainer className='container-main'>
                    <IceFormBinderWrapper
                        value={this.state.value}
                        onChange={this.formChange}
                        ref={formRef => form = formRef}
                    >
                        <div className={styles.formContent}>
                            <div className={styles.formItem}>
                                <IceFormBinder
                                    required
                                    triggerType="onBlur"
                                    message="发布大屏配额不能为空"
                                    name="modules"
                                >
                                    <Checkbox.Group itemDirection="ver">
                                        {
                                            modules.map(
                                                o => {
                                                    return (<Checkbox value={o.id}>{o.name}</Checkbox>)
                                                }
                                            )
                                        }

                                    </Checkbox.Group>
                                </IceFormBinder>
                                <div className={styles.formError}>
                                    <IceFormError name="publishDashboardCount"/>
                                </div>
                            </div>

                            <Button
                                type="primary"
                                className='pipe-btn-submit'
                                onClick={this.validateAllFormField}
                            >
                                提 交
                            </Button>
                        </div>
                    </IceFormBinderWrapper>
                </IceContainer>
            </div>
        );
    }

}

export default TenantFunction;

/**
  * @description: 数据中台-权限管理-租户管理-租户配置-配额配置
  * @author: hj
  * @update: hj(2020-01-14)
  */
import React from 'react';
import IceContainer from '@icedesign/container';
import {Button, Input, Message, Radio, Select} from '@alifd/next';
import {
    FormBinder as IceFormBinder,
    FormBinderWrapper as IceFormBinderWrapper,
    FormError as IceFormError,
} from '@icedesign/form-binder';
import styles from './index.module.scss';
// import axios from 'axios';
import responsive from '@/request'
import $http from '@/service/Services';

const axios = $http;

const {Option} = Select;
const {Group: RadioGroup} = Radio;
let form;


/**
 * list
 */
class QuotaTenant extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: {},
            id: "",
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        };
        this.formChange = this.formChange.bind(this);
        this.validateAllFormField = this.validateAllFormField.bind(this);
    }

    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        const id = this.props.id;
        this.setState({
            id: id
        });
        if (id) {
            const that = this;
            axios.get(responsive.responsive.url + '/v1/tTenant/getQuotaByTenantId/' + id)
                .then(function (response) {
                    const {data} = response;
                    if (data.code === 1) {
                        Message.warning(data.message ? data.message : data.data);
                    } else {
                        that.setState({
                            value: data.data
                        })
                    }
                })
                .catch(function (error) {
                    console.error(error);
                    Message.error(error.message);
                })
        }
    };

    /* 表改变
    * @param formValue
    */
    formChange = (formValue) => {
        this.setState({
            value: formValue
        })
    };

    /**
     * 表验证
     */
    validateAllFormField = () => {
        form.validateAll((errors, values) => {
            if (errors) {
                Message.warning('请填写必要的数据.');
                return;
            }
            const that = this;
            values["tenantId"]=this.state.id;
            axios.post(responsive.responsive.url + '/v1/tTenant/updateQuota', values)
                .then(function (response) {
                    const {data} = response;
                    if (data.code === 1) {
                        Message.warning(data.message ? data.message : data.data);
                    } else {
                        Message.success("操作成功.");
                        that.props.history.go(-1);
                    }
                })
                .catch(function (error) {
                    console.error(error);
                    Message.error(error.message);
                })
        });
    };

    /**
     * 表
     * @returns {*}
     */
    render() {
        return (
            <div>
                <IceContainer className='container-main'>
                    <IceFormBinderWrapper
                        value={this.state.value}
                        onChange={this.formChange}
                        ref={formRef => form = formRef}
                    >
                        <div className={styles.formContent}>
                            <div className={styles.formItem}>
                                <div className={styles.formLabel}> 发布大屏配额：</div>
                                <IceFormBinder
                                    required
                                    triggerType="onBlur"
                                    message="发布大屏配额不能为空"
                                    name="publishDashboardCount"
                                >
                                    <Input
                                        maxLength={2}
                                        placeholder="请输入发布大屏配额"
                                        className={styles.inputNme}
                                    />
                                </IceFormBinder>
                                <div className={styles.formError}>
                                    <IceFormError name="publishDashboardCount"/>
                                </div>
                            </div>
                            <div className={styles.formItem}>
                                <div className={styles.formLabel}> 草稿大屏配额：</div>
                                <IceFormBinder
                                    required
                                    triggerType="onBlur"
                                    message="草稿大屏配额不能为空"
                                    name="draftDashboardCount"
                                >
                                    <Input
                                        maxLength={2}
                                        placeholder="请输入草稿大屏配额"
                                        className={styles.inputNme}
                                    />
                                </IceFormBinder>
                                <div className={styles.formError}>
                                    <IceFormError name="draftDashboardCount"/>
                                </div>
                            </div>
                            <div className={styles.formItem}>
                                <div className={styles.formLabel}> 应用配额：</div>
                                <IceFormBinder
                                    required
                                    triggerType="onBlur"
                                    message="应用配额不能为空"
                                    name="dashboardAppCount"
                                >
                                    <Input
                                        maxLength={2}
                                        placeholder="请输入应用配额"
                                        className={styles.inputNme}
                                    />
                                </IceFormBinder>
                                <div className={styles.formError}>
                                    <IceFormError name="dashboardAppCount"/>
                                </div>
                            </div>
                            <Button
                                type="primary"
                                className='pipe-btn-submit'
                                onClick={this.validateAllFormField}
                            >
                                提 交
                            </Button>
                        </div>
                    </IceFormBinderWrapper>
                </IceContainer>
            </div>
        );
    }

}

export default QuotaTenant;

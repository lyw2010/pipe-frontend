import React from 'react';
import Container from '@icedesign/container';
import styles from './index.module.scss';
import event from '@/request'
import {Button, Dialog, Input, Message, Table} from "@alifd/next";
import ZgDialog from '@/components/ZgDialog';
import {Link} from "react-router-dom";
import $http from '@/service/Services';


/**
 * list
 */
class RoleListComponents extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            orgId: "",
            userType: props.user, //user admin
            current: 0,
            mockData: [],
            selectedList: [],
            condition: "",
            tenant:{},
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        };
        // 当前页数
        this.pageNum = 1;
        // 分页每页显示数据条数
        this.pageSize = 10;
        // 数据总条数
        this.totalNum = 0;
        this.$http = $http;
        this.cancelCall = this.cancelCall.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.onPageChange = this.onPageChange.bind(this);
        this.getAdminRoleList = this.getAdminRoleList.bind(this);
        this.deleteOrgRole = this.deleteOrgRole.bind(this);
    }

    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        const id = this.props.orgId;
        this.setState({
            orgId: id
        }, () => {
            this.getAdminRoleList();
        });
    };


    /**
     * 条件
     * @param e
     * @param value
     */
    searchCondition = (value, e) => {
        this.setState({
            condition: value,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        });
        this.pageNum = 1;
    };
    /**
     * 改变页面
     * @param currentPage
     */
    onPageChange = (currentPage) => {
        this.state = {
            current: currentPage,
            mockData: this.props.mockData,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        };
        this.getAdminRoleList();
    };
    /**
     * 修改分页数据
     * @param currentPage
     */
    onChangePage = (currentPage) => {
        this.setState({
            loading: true,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        });
        setTimeout(() => {
            this.getAdminRoleList(currentPage);
            this.setState({
                loading: false,
                config: {
                    data: null,
                    visible: false,
                    title: "",
                    content: ""
                }
            });
        }, 0);
    };

    /**
     * 获取数据
     */
    getAdminRoleList = () => {
        const _this = this;
        let url = event.event.url + '/v1/role/getRoleList?value=' + this.state.condition + '&orgId=' + this.state.orgId;
        this.$http.get(url)
            .then(function (response) {
                const {data} = response;
                data.data.forEach((o, i) => {
                    o.number = i + 1;
                });
                _this.setState({
                    mockData: data.data,
                    config: {
                        data: null,
                        visible: false,
                        title: "",
                        content: ""
                    },
                });
            })
            .catch(function (error) {
                Message.error(error.message);
            })

    };


    /**
     * 删除
     */
    deleteOrgRole = (row) => {
        this.setState({
                config: {
                    data: row,
                    visible: true,
                    title: "提示消息",
                    content: "删除后不能恢复，确认要删除？"
                }
            }
        )
    };

    /**
     * 取消按钮的操作
     */
    cancelCall = () => {
        this.setState({
            config: {
                visible: false
            }
        });
        console.log("点击取消按钮 .");
    };


    /**
     * 删除
     */
    okCall = (data) => {
        this.setState({
            config: {
                visible: false
            }
        });
        const _this = this;
        this.$http.post(event.event.url + '/v1/role/deleteRole?roleId=' + data.id, {})
            .then(function (response) {
                const {data} = response;
                if (data.code === 1) {
                    Message.warning(data.message ? data.message : data.data);
                } else {
                    Message.success("操作成功.");
                    _this.getAdminRoleList();
                }
            })
            .catch(function (error) {
                Message.error(error.message);
            })
    };


    /**
     *  批量操作
     * @param text
     */
    handleClick = () => {
        const {selectedList} = this.state;
        if (selectedList.length == 0) {
            Message.error("请选择需要删除的数据.");
            return null;
        }
        this.setState({
            visible: true
        });
    };

    /**
     *
     * @param args
     */
    onChange = (...args) => {
        this.setState({
            selectedList: args
        });
    };


    /**
     * 取消提示弹框
     */
    onCloseDialog = reason => {
        this.setState({
            visible: false
        });
    };


    render() {
        const {mockData} = this.state;
        const {userType} = this.state;
        
        return (
            <Container className='container-main'>
                <div className='container-btnBox' style={{marginTop: '0'}}>
                    <div className={styles.buttons}>
                        <Link
                            to={"admin" === userType ? "roleEdit?id=&orgId=" + this.state.orgId : "userRoleEdit?id=&orgId=" + this.state.orgId}><Button
                            className='pipe-btn-add'>新建角色</Button></Link>
                        <span className={styles.caseNumber}>
                                <Input onChange={this.searchCondition.bind(this)} placeholder={"请输入角色名称"}
                                       className={`${styles.input} ${styles.shortInput}`}/>
                                <Button
                                    type="primary"
                                    className='pipe-btn-search'
                                    onClick={this.getAdminRoleList}
                                >
                                查询
                                </Button>
                            </span>
                    </div>
                </div>
                <div className='container-table'>
                    <Table dataSource={mockData} primaryKey="id" className={styles.table}>
                        <Table.Column align="center" title="序号" dataIndex="number"/>
                        <Table.Column align="center" title="角色编码" dataIndex="code"/>
                        <Table.Column align="center" title="角色名称" dataIndex="name"/>
                        <Table.Column align="center" title="创建时间" dataIndex="createTime"/>
                        <Table.Column align="center" title="操作" cell={
                            (value, index, record) => {
                                return (
                                    <div>
                                        <Button type="normal" size="small" text className='pipe-btn-edit'>
                                            <Link
                                                to={"admin" === userType ? "roleEdit?id=" + record.id + "&orgId=" + this.state.orgId : "userRoleEdit?id=" + record.id + "&orgId=" + this.state.orgId}>修改</Link>
                                        </Button>
                                        <Button type="normal" size="small" text className='pipe-btn-edit'>
                                            <Link
                                                to={"admin" === userType ? "authorityEdit?id=" + record.id + "&orgId=" + this.state.orgId + "&type=menu" : "userRoleEdit?id=" + record.id + "&orgId=" + this.state.orgId + "&type=menu"}>菜单权限</Link>
                                        </Button>
                                        <Button type="normal" size="small" text className='pipe-btn-delete'
                                                onClick={this.deleteOrgRole.bind(this, record)} warning>
                                            <a onClick={() => {
                                            }}>删除</a>
                                        </Button>
                                    </div>
                                )
                            }
                        }/>
                    </Table>
                    <ZgDialog config={this.state.config} cancelCall={this.cancelCall} okCall={this.okCall}/>
                    <Dialog
                        className='pipe-dialog'
                        title="提示信息"
                        visible={this.state.visible}
                        onOk={this.onOkDialog}
                        onCancel={this.onCloseDialog.bind(this, 'cancelClick')}
                        onClose={this.onCloseDialog}>
                        删除后不能恢复，确认要删除？
                    </Dialog>
                </div>
            </Container>
        )
            ;
    }

}

export default RoleListComponents;

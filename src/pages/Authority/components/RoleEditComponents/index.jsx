/**
  * @description: 数据中台-权限管理-租户管理-角色管理-新建/修改角色
  * @author: hj
  * @update: hj(2020-01-14)
  */
import React from 'react';
import IceContainer from '@icedesign/container';
import {Button, Input, Message} from '@alifd/next';
import {
    FormBinder as IceFormBinder,
    FormBinderWrapper as IceFormBinderWrapper,
    FormError as IceFormError,
} from '@icedesign/form-binder';
import styles from './index.module.scss';
import responsive from '@/request'
import $http from '@/service/Services';

const axios = $http;

let form;


/**
 * list
 */
class RoleEditComponents extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: {},
            id: "",
            tenantId: "",
            selectedList: [],
            data: [],
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        };
        this.formChange = this.formChange.bind(this);
        this.validateAllFormField = this.validateAllFormField.bind(this);
        this.handleCheck = this.handleCheck.bind(this);
        this.handleCheckStrictly = this.handleCheckStrictly.bind(this);
    }



    /**
     *
     * @param keys
     * @param info
     */
    handleCheck(keys, info) {
        this.setState({
            checkedKeys: keys
        });
    }

    /**
     *
     */
    handleCheckStrictly() {
        this.setState({
            checkStrictly: !this.state.checkStrictly,
            checkedKeys: []
        });
    }

    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        const id = this.props.id;
        const tenantId = this.props.tenantId;
        this.setState({
            id: id,
            tenantId: tenantId
        }, () => {
            if (id) {
                const that = this;
                axios.get(responsive.responsive.url + '/v1/role/getRoleById/' + id)
                    .then(function (response) {
                        const {data} = response;
                        if (data.code === 1) {
                            Message.warning(data.message ? data.message : data.data);
                        } else {
                            const {authList} = data.data;
                            that.setState({
                                value: data.data,
                                checkedKeys:authList
                            })
                        }
                    })
                    .catch(function (error) {
                        Message.error(error.message);
                    })
            }
        });

    };

    /* 表改变
    * @param formValue
    */
    formChange = (formValue) => {
        this.setState({
            value: formValue
        })
    };

    /**
     * 表验证
     */
    validateAllFormField = () => {
        form.validateAll((errors, values) => {
            if (errors) {
                Message.warning('请填写必要的数据.');
                return;
            }
            const that = this;
            values["tenantId"] = this.state.tenantId;
            axios.post(responsive.responsive.url + '/v1/tRole/saveOrUpdateRole', values)
                .then(function (response) {
                    const {data} = response;
                    if (data.code === 1) {
                        Message.warning(data.message ? data.message : data.data);
                    } else {
                        Message.success("操作成功.");
                        that.props.history.go(-1);
                    }
                })
                .catch(function (error) {
                    Message.error(error.message);
                })
        });
    };
    /**
     *
     * @param args
     */
    onChange = (...args) => {
        const selected = this.state.selectedList;
        args.forEach(o => {
            selected.push(o)
        });
        this.setState({
            selectedList: selected
        });
    };

    /**
     *
     * @param rule
     * @param values
     * @param callback
     * @param stateValues
     */
    checkAuths = (rule, values, callback, stateValues) => {
        callback();
    };

    /**
     * 表
     * @returns {*}
     */
    render() {
        return (
            <div>
                <div className='container-header'>
                    <p style={{display: 'inline-block'}}>{this.state.id ? "修改角色" : "新建角色"}</p>
                    <Button type="normal" size="small" className='next-btn-normal table-btn-return' style={{float: 'right'}}>
                        <a onClick={() => {
                            this.props.history.go(-1)
                        }} > 返回</a>
                    </Button>
                </div>

                
                <IceContainer className='container-main'>
                    <IceFormBinderWrapper
                        value={this.state.value}
                        onChange={this.formChange}
                        ref={formRef => form = formRef}
                    >
                        <div className={styles.formContent}>
                            <div className={styles.formItem}>
                                <div className={styles.formLabel}><span style={{color: '#FF3000'}}>*</span> 角色编码：</div>
                                <IceFormBinder
                                    required
                                    triggerType="onBlur"
                                    message=" 角色编码不能为空"
                                    name="code"
                                >
                                    <Input
                                        disabled={this.state.id}
                                        maxLength={10}
                                        placeholder="请输入编码名称"
                                        className={styles.inputNme}
                                    />
                                </IceFormBinder>
                                <div className={styles.formError}>
                                    <IceFormError name="name"/>
                                </div>
                            </div>
                            <div className={styles.formItem}>
                                <div className={styles.formLabel}><span style={{color: '#FF3000'}}>*</span> 角色名称：</div>
                                <IceFormBinder
                                    required
                                    triggerType="onBlur"
                                    message=" 角色名称不能为空"
                                    name="name"
                                >
                                    <Input
                                        maxLength={10}
                                        placeholder="请输入角色名称"
                                        className={styles.inputNme}
                                    />
                                </IceFormBinder>
                                <div className={styles.formError}>
                                    <IceFormError name="name"/>
                                </div>
                            </div>
                            <Button
                                type="primary"
                                className='pipe-btn-submit'
                                onClick={this.validateAllFormField}
                            >
                                提 交
                            </Button>
                        </div>
                    </IceFormBinderWrapper>
                </IceContainer>
            </div>
        );
    }

}

export default RoleEditComponents;

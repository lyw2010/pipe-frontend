/**
 * @description: 数据中台-任务-任务管理
 * @author: hj
 * @update: hj(2020-01-14)
 */
import React from 'react';
import {Button, Card, Grid, Message, Typography} from '@alifd/next';
import $http from '@/service/Services';
import Container from '@icedesign/container';
import event from '@/request'

const {Row, Col} = Grid;


/**
 * list
 */
class SystemDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cup: {},
            jvm: {},
            mem: {},
            sys: {},
            sysFiles: []
        };
        this.$http = $http;
        this.setInterval = function() {

        }
    }

    getServerInfoByServiceId =(serverId)=>{
        const that = this;
        this.$http.get(event.event.url + '/v1/monitorSysInfo/getServerInfoByServiceId/' + serverId)
            .then(function (response) {
                const {data} = response;
                if (data.code === 1) {
                    Message.warning(data.message ? data.message : data.data);
                } else {
                    that.setState({
                        cup: data.data.cpu,
                        jvm: data.data.jvm,
                        mem: data.data.mem,
                        sys: data.data.sys,
                        sysFiles: data.data.sysFiles
                    })
                }
            })
            .catch(function (error) {
                Message.error(error.message);
            })
    }
    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        const params = new URLSearchParams(this.props.location.search);
        const serverId = params.get('serverId');
        this.setState({serverId}, () => {
            this.setInterval = setInterval(()=>{
                this.getServerInfoByServiceId(serverId);
            },10000);
        });
    };
    /**
     * 离开的时候
     */
    componentWillUnmount =()=>{
        window.clearInterval(this.setInterval);
    }


    render() {
        const {cup,
            jvm,
            mem,
            sys,
            sysFiles} = this.state;
        return (
            <div>
                <div className='container-header'>
                    <p>服务详情</p>
                    <Button type="normal" size="small" className='next-btn-normal table-btn-return' style={{float: 'right'}}>
                        <a onClick={() => {
                            this.props.history.go(-1)
                        }} > 返回</a>
                    </Button>
                </div>

                <Container className='container-main'>
                    <div style={{margin: '15px 0 '}}>
                        <Row gutter={24} style={{marginBottom: 15}}>
                            <Col span={12}>
                                <Card free className='is-always-shadow' style={{width: '100%', boxShadow: '0 2px 12px 0 rgb(0 0 0/10%)'}}>
                                    <Card.Header title="CPU" extra={() => {
                                        return (
                                            <Typography.H3>CPU</Typography.H3>
                                        )
                                    }}/>
                                    <Card.Divider/>
                                    <Card.Content>
                                        <div className="el-table">
                                            <table cellSpacing="0" style={{
                                                width: '100%',
                                                display: 'table',
                                                borderCollapse: 'separate',
                                                textIndent: 'initial',
                                                fontSize: '14px',
                                                color: '#606266'
                                            }}>
                                                <thead>
                                                <tr>
                                                    <th className="is-leaf">
                                                        <div className="cell">属性</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">值</div>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <div className="cell">核心数</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{cup.cpuNum}</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="cell">用户使用率</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{cup.used}%</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="cell">系统使用率</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{cup.sys}%</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="cell">当前空闲率</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{cup.free}%</div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                    </Card.Content>
                                </Card>
                            </Col>
                            <Col span={12}>
                                <Card free style={{width: '100%', boxShadow: '0 2px 12px 0 rgb(0 0 0/10%)'}}>
                                    <Card.Header title="内存" extra={() => {
                                        return (
                                            <Typography.H3>内存</Typography.H3>
                                        )
                                    }}/>
                                    <Card.Divider/>
                                    <Card.Content>
                                        <div className="el-table">
                                            <table cellSpacing="0" style={{
                                                width: '100%',
                                                display: 'table',
                                                borderCollapse: 'separate',
                                                textIndent: 'initial',
                                                fontSize: '14px',
                                                color: '#606266'
                                            }}>
                                                <thead>
                                                <tr>
                                                    <th className="is-leaf">
                                                        <div className="cell">属性</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">内存</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">JVM</div>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <div className="cell">总内存</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{mem.total}G</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{jvm.total}M</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="cell">已用内存</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{mem.used}G</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{jvm.max}M</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="cell">剩余内存</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{mem.free}G</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{jvm.free}M</div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </Card.Content>
                                </Card>
                            </Col>
                        </Row>
                        <Row gutter={24} style={{marginBottom: 15}}>
                            <Col span={24}>
                                <Card free style={{width: '100%', boxShadow: '0 2px 12px 0 rgb(0 0 0/10%)'}}>
                                    <Card.Header title="服务器信息" extra={() => {
                                        return (
                                            <Typography.H3>服务器信息</Typography.H3>
                                        )
                                    }}/>
                                    <Card.Divider/>
                                    <Card.Content>
                                        <div className="el-table">
                                            <table cellSpacing="0" style={{
                                                width: '100%',
                                                display: 'table',
                                                borderCollapse: 'separate',
                                                textIndent: 'initial',
                                                fontSize: '14px',
                                                color: '#606266'
                                            }}>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <div className="cell">服务器名称</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{sys.computerName}</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">操作系统</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{sys.osName}</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="cell">服务器IP</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{sys.computerIp}</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">系统架构</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{sys.osArch}</div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </Card.Content>
                                </Card>
                            </Col>
                        </Row>
                        <Row gutter={24} style={{marginBottom: 15}}>
                            <Col span={24}>
                                <Card free style={{width: '100%', boxShadow: '0 2px 12px 0 rgb(0 0 0/10%)'}}>
                                    <Card.Header title="Java虚拟机信息" extra={() => {
                                        return (
                                            <Typography.H3>Java虚拟机信息</Typography.H3>
                                        )
                                    }}/>
                                    <Card.Divider/>
                                    <Card.Content>
                                        <div className="el-table">
                                            <table cellSpacing="0" style={{
                                                width: '100%',
                                                display: 'table',
                                                borderCollapse: 'separate',
                                                textIndent: 'initial',
                                                fontSize: '14px',
                                                color: '#606266'
                                            }}>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <div className="cell">Java名称</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{jvm.name}</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">Java版本</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{jvm.version}</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="cell">启动时间</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{jvm.startTime}</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">运行时长</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{jvm.runTime}</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="cell">安装路径</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{jvm.home}</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell"></div>
                                                    </td>
                                                    <td>
                                                        <div className="cell"></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="cell">项目路径</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{sys.userDir}</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell"></div>
                                                    </td>
                                                    <td>
                                                        <div className="cell"></div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </Card.Content>
                                </Card>
                            </Col>
                        </Row>
                        <Row gutter={24} style={{marginBottom: 15}}>
                            <Col span={24}>
                                <Card free style={{width: '100%', boxShadow: '0 2px 12px 0 rgb(0 0 0/10%)'}}>
                                    <Card.Header title="磁盘信息" extra={() => {
                                        return (
                                            <Typography.H3>磁盘信息</Typography.H3>
                                        )
                                    }}/>
                                    <Card.Divider/>
                                    <Card.Content>
                                        <div className="el-table">
                                            <table cellSpacing="0" style={{
                                                width: '100%',
                                                display: 'table',
                                                borderCollapse: 'separate',
                                                textIndent: 'initial',
                                                fontSize: '14px',
                                                color: '#606266'
                                            }}>
                                                <thead>
                                                <tr>
                                                    <th className="is-leaf">
                                                        <div className="cell">盘符</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">文件系统</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">盘符类型</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">总大小</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">可用大小</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">已用</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">已用百分比</div>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {
                                                    Array.isArray(sysFiles)
                                                    && sysFiles.length > 0
                                                    && sysFiles.map((o,i)=>{
                                                        console.log(o);
                                                        return (
                                                            <tr>
                                                                <td>
                                                                    <div className="cell">{o.dirName}</div>
                                                                </td>
                                                                <td>
                                                                    <div className="cell">{o.typeName}</div>
                                                                </td>
                                                                <td>
                                                                    <div className="cell">{o.sysTypeName}</div>
                                                                </td>
                                                                <td>
                                                                    <div className="cell">{o.total}</div>
                                                                </td>
                                                                <td>
                                                                    <div className="cell">{o.free}</div>
                                                                </td>
                                                                <td>
                                                                    <div className="cell">{o.used}</div>
                                                                </td>
                                                                <td>
                                                                    <div className="cell">{o.usage}%</div>
                                                                </td>
                                                            </tr>
                                                        )
                                                    })
                                                }
                                                </tbody>
                                            </table>
                                        </div>
                                    </Card.Content>
                                </Card>
                            </Col>
                        </Row>
                    </div>
                </Container>

            </div>
        )
            ;
    }

}

export default SystemDetail;

/**
 * @description: 数据中台-任务-任务管理
 * @author: hj
 * @update: hj(2020-01-14)
 */
import React from 'react';
import {Button, Card, Grid, Message, Typography} from '@alifd/next';
import $http from '@/service/Services';
import Container from '@icedesign/container';
import event from '@/request'
import {Line} from '@ant-design/charts';

const {Row, Col} = Grid;


/**
 * list
 */
class NodeDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cup: {},
            jvm: {},
            mem: {},
            sys: {},
            sysFiles: [],
            nodeProcess: [],
            nodesIndex: [],
            unit: [] //单位
        };
        this.$http = $http;
        this.getMonitorServerByNodeId = this.getMonitorServerByNodeId.bind(this);
        this.getMonitorNodesIndexByBodeId = this.getMonitorNodesIndexByBodeId.bind(this);
        this.getAgentNodeProcessByNodeId = this.getAgentNodeProcessByNodeId.bind(this);
        this.setInterval = function () {
        };
    }



    /**
     * 根据id获取节点的中的JAVA信息
     * @param id
     */
    getAgentNodeProcessByNodeId = (nodeId) => {
        const that = this;
        this.$http.get(event.event.url + '/v1/monitorNode/getAgentNodeProcessByNodeId/' + nodeId)
            .then(function (response) {
                const {data} = response;
                if (data.code === 1) {
                    Message.warning(data.message ? data.message : data.data);
                } else {
                    that.setState({
                        nodeProcess: data.data
                    })
                }
            })
            .catch(function (error) {
                Message.error(error.message);
            })
    };
    /**
     * 根據節點的id节点的指标
     * @param nodeId
     */
    getMonitorNodesIndexByBodeId = (nodeId) => {
        const that = this;
        this.$http.get(event.event.url + '/v1/monitorNode/getMonitorNodesIndexByBodeId/' + nodeId)
            .then(function (response) {
                const {data} = response;
                if (data.code === 1) {
                    Message.warning(data.message ? data.message : data.data);
                } else {
                    const arr = data.data.map(d=>{
                        return {
                            cpu: +d.cpu,
                            disk: +d.disk,
                            id:  +d.id,
                            memory: +d.memory,
                            time: d.time
                        }
                    });
                    that.setState({
                        nodesIndex: arr
                    })
                }
            })
            .catch(function (error) {
                Message.error(error.message);
            })
    };
    /**
     * 获取详情
     * @param nodeId
     */
    getMonitorServerByNodeId = (nodeId) => {
        const that = this;
        this.$http.get(event.event.url + '/v1/monitorNode/getMonitorServerByNodeId/' + nodeId)
            .then(function (response) {
                const {data} = response;
                if (data.code === 1) {
                    Message.warning(data.message ? data.message : data.data);
                } else {
                    that.setState({
                        cup: data.data.cpu,
                        jvm: data.data.jvm,
                        mem: data.data.mem,
                        sys: data.data.sys,
                        sysFiles: data.data.sysFiles
                    })
                }
            })
            .catch(function (error) {
                Message.error(error.message);
            })
    }
    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        const params = new URLSearchParams(this.props.location.search);
        const nodeId = params.get('nodeId');
        this.setState({nodeId}, () => {
            this.setInterval= setInterval(()=>{
                this.getMonitorServerByNodeId(nodeId);
                this.getMonitorNodesIndexByBodeId(nodeId);
                this.getAgentNodeProcessByNodeId(nodeId);
            },10000)
        });
    };

    /**
     * 离开的时候
     */
    componentWillUnmount =()=>{
        window.clearInterval(this.setInterval);
    }

    render() {
        const {
            cup,
            jvm,
            mem,
            sys,
            sysFiles,
            nodesIndex,
            nodeProcess
        } = this.state;
        if (nodesIndex && nodesIndex.length > 0){
            document.getElementById('mountNode').innerHTML = '';
            const ds = new DataSet();
            const dv = ds.createView().source(nodesIndex);
            // fold 方式完成了行列转换，如果不想使用 DataSet 直接手工转换数据即可
            dv.transform({
                type: 'fold',
                fields: ['cpu', 'disk','memory'], // 展开字段集
                key: 'city', // key字段
                value: 'value' // value字段
            });
            const chart = new G2.Chart({
                container: 'mountNode',
                forceFit: true,
                height: 350
            });
            chart.source(dv);
            chart.tooltip({
                crosshairs: {
                    type: 'line'
                }
            });
            let prevNum = 0;
            chart.axis('value', {
                position: 'left', // 设置坐标轴的显示位置，可取值 top bottom left right
                line: {
                    lineWidth: 1, // 设置线的宽度
                    stroke: '#536f8e' //  设置线的颜色
                }, // 设置坐标轴线的样式，如果值为 null，则不显示坐标轴线 图形属性
                label: {
                    offset: 0, // 数值，设置坐标轴文本 label 距离坐标轴线的距离
                    offsetX: -10, // 在 offset 的基础上 x 方向的偏移量
                    offsetY: 0, // 在 offset 的基础上 y 方向的偏移量
                    // 设置文本的显示样式，还可以是个回调函数，回调函数的参数为该坐标轴对应字段的数值
                    textStyle: {
                        textAlign: 'end', // 文本对齐方向，可取值为： start center end
                        fill: '#24C6F5', // 文本的颜色
                        fontSize: '12', // 文本大小
                    },
                    // 使用 formatter 回调函数
                    formatter: val => {
                        prevNum++;
                        if (prevNum % 2 != 0) {
                            return val + ' '+  this.state.unit;
                        }
                    }
                },
                title: null, // 坐标轴标题设置，如果值为 null，则不显示标题
                tickLine: null,
                // grid: {
                //     align: 'center', // 声明网格顶点从两个刻度中间开始，默认从刻度点开始
                //     type: 'line', // 声明网格的类型，line 表示线，polygon 表示矩形框
                //     // 当网格类型 type 为 line 时，使用 lineStyle 设置样式
                //     lineStyle: {
                //         stroke: '#21355f', // 网格线的颜色
                //         lineWidth: 1, // 网格线的粗细
                //         lineDash: [4, 4 ] // 网格线的虚线配置，第一个参数描述虚线的实部占多少像素，第二个参数描述虚线的虚部占多少像素
                //     },
                //     hideFirstLine: true, // 是否隐藏第一条网格线，默认为 false
                //     hideLastLine: true, // 是否隐藏最后一条网格线，默认为 false
                // }, // 坐标轴栅格线的配置信息，默认只有左边的坐标轴带有栅格线，null 为不显示。
            });
            chart.line().position('time*value').color('city').shape('smooth');
            chart.point().position('time*value').color('city').size(4).shape('circle').style({
                stroke: '#fff',
                lineWidth: 1
            });
            chart.render();
        }
        return (
            <div>
                <div className='container-header'>
                    <p>服务详情</p>
                    <Button type="normal" size="small" className='next-btn-normal table-btn-return'
                            style={{float: 'right'}}>
                        <a onClick={() => {
                            this.props.history.go(-1)
                        }}> 返回</a>
                    </Button>
                </div>

                <Container className='container-main'>
                    <div style={{margin: '15px 0 '}}>
                        <Row gutter={24} style={{marginBottom: 15}}>
                            <Col span={12}>
                                <Card free className='is-always-shadow'
                                      style={{width: '100%', boxShadow: '0 2px 12px 0 rgb(0 0 0/10%)'}}>
                                    <Card.Header title="CPU" extra={() => {
                                        return (
                                            <Typography.H3>CPU</Typography.H3>
                                        )
                                    }}/>
                                    <Card.Divider/>
                                    <Card.Content>
                                        <div className="el-table">
                                            <table cellSpacing="0" style={{
                                                width: '100%',
                                                display: 'table',
                                                borderCollapse: 'separate',
                                                textIndent: 'initial',
                                                fontSize: '12px',
                                                color: '#606266'
                                            }}>
                                                <thead>
                                                <tr>
                                                    <th className="is-leaf">
                                                        <div className="cell">属性</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">值</div>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <div className="cell">核心数</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{cup.cpuNum}</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="cell">用户使用率</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{cup.used}%</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="cell">系统使用率</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{cup.sys}%</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="cell">当前空闲率</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{cup.free}%</div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                    </Card.Content>
                                </Card>
                            </Col>
                            <Col span={12}>
                                <Card free style={{width: '100%', boxShadow: '0 2px 12px 0 rgb(0 0 0/10%)'}}>
                                    <Card.Header title="内存" extra={() => {
                                        return (
                                            <Typography.H3>内存</Typography.H3>
                                        )
                                    }}/>
                                    <Card.Divider/>
                                    <Card.Content>
                                        <div className="el-table">
                                            <table cellSpacing="0" style={{
                                                width: '100%',
                                                display: 'table',
                                                borderCollapse: 'separate',
                                                textIndent: 'initial',
                                                fontSize: '12px',
                                                color: '#606266'
                                            }}>
                                                <thead>
                                                <tr>
                                                    <th className="is-leaf">
                                                        <div className="cell">属性</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">内存</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">JVM</div>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <div className="cell">总内存</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{mem.total}G</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{jvm.total}M</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="cell">已用内存</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{mem.used}G</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{jvm.max}M</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="cell">剩余内存</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{mem.free}G</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{jvm.free}M</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="cell">使用率</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">
                                                            {mem.usage}%
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">
                                                            {jvm.usage}%
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </Card.Content>
                                </Card>
                            </Col>
                        </Row>
                        <Row gutter={24} style={{marginBottom: 15}}>
                            <Col span={24}>
                                <Card free style={{width: '100%', boxShadow: '0 2px 12px 0 rgb(0 0 0/10%)'}}>
                                    <Card.Header title="节点TOP" extra={() => {
                                        return (
                                            <Typography.H3>节点TOP</Typography.H3>
                                        )
                                    }}/>
                                    <Card.Divider/>
                                    <Card.Content>
                                        <div className="el-table">
                                            <div id="mountNode"></div>
                                        </div>
                                    </Card.Content>
                                </Card>
                            </Col>
                        </Row>
                        <Row gutter={24} style={{marginBottom: 15}}>
                            <Col span={24}>
                                <Card free style={{width: '100%', boxShadow: '0 2px 12px 0 rgb(0 0 0/10%)'}}>
                                    <Card.Header title="服务器信息" extra={() => {
                                        return (
                                            <Typography.H3>服务器信息</Typography.H3>
                                        )
                                    }}/>
                                    <Card.Divider/>
                                    <Card.Content>
                                        <div className="el-table">
                                            <table cellSpacing="0" style={{
                                                width: '100%',
                                                display: 'table',
                                                borderCollapse: 'separate',
                                                textIndent: 'initial',
                                                fontSize: '12px',
                                                color: '#606266'
                                            }}>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <div className="cell">服务器名称</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{sys.computerName}</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">操作系统</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{sys.osName}</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="cell">服务器IP</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{sys.computerIp}</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">系统架构</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{sys.osArch}</div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </Card.Content>
                                </Card>
                            </Col>
                        </Row>
                        <Row gutter={24} style={{marginBottom: 15}}>
                            <Col span={24}>
                                <Card free style={{width: '100%', boxShadow: '0 2px 12px 0 rgb(0 0 0/10%)'}}>
                                    <Card.Header title="Java虚拟机信息" extra={() => {
                                        return (
                                            <Typography.H3>Java虚拟机信息</Typography.H3>
                                        )
                                    }}/>
                                    <Card.Divider/>
                                    <Card.Content>
                                        <div className="el-table">
                                            <table cellSpacing="0" style={{
                                                width: '100%',
                                                display: 'table',
                                                borderCollapse: 'separate',
                                                textIndent: 'initial',
                                                fontSize: '12px',
                                                color: '#606266'
                                            }}>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <div className="cell">Java名称</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{jvm.name}</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">Java版本</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{jvm.version}</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="cell">启动时间</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{jvm.startTime}</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">运行时长</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{jvm.runTime}</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="cell">安装路径</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{jvm.home}</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell"></div>
                                                    </td>
                                                    <td>
                                                        <div className="cell"></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="cell">项目路径</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell">{sys.userDir}</div>
                                                    </td>
                                                    <td>
                                                        <div className="cell"></div>
                                                    </td>
                                                    <td>
                                                        <div className="cell"></div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </Card.Content>
                                </Card>
                            </Col>
                        </Row>
                        <Row gutter={24} style={{marginBottom: 15}}>
                            <Col span={24}>
                                <Card free style={{width: '100%', boxShadow: '0 2px 12px 0 rgb(0 0 0/10%)'}}>
                                    <Card.Header title="磁盘信息" extra={() => {
                                        return (
                                            <Typography.H3>磁盘信息</Typography.H3>
                                        )
                                    }}/>
                                    <Card.Divider/>
                                    <Card.Content>
                                        <div className="el-table">
                                            <table cellSpacing="0" style={{
                                                width: '100%',
                                                display: 'table',
                                                borderCollapse: 'separate',
                                                textIndent: 'initial',
                                                fontSize: '12px',
                                                color: '#606266'
                                            }}>
                                                <thead>
                                                <tr>
                                                    <th className="is-leaf">
                                                        <div className="cell">盘符</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">文件系统</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">盘符类型</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">总大小</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">可用大小</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">已用</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">已用百分比</div>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {
                                                    Array.isArray(sysFiles)
                                                    && sysFiles.length > 0
                                                    && sysFiles.map((o, i) => {
                                                        return (
                                                            <tr>
                                                                <td>
                                                                    <div className="cell">{o.dirName}</div>
                                                                </td>
                                                                <td>
                                                                    <div className="cell">{o.typeName}</div>
                                                                </td>
                                                                <td>
                                                                    <div className="cell">{o.sysTypeName}</div>
                                                                </td>
                                                                <td>
                                                                    <div className="cell">{o.total}</div>
                                                                </td>
                                                                <td>
                                                                    <div className="cell">{o.free}</div>
                                                                </td>
                                                                <td>
                                                                    <div className="cell">{o.used}</div>
                                                                </td>
                                                                <td>
                                                                    <div className="cell">{o.usage}%</div>
                                                                </td>
                                                            </tr>
                                                        )
                                                    })
                                                }
                                                </tbody>
                                            </table>
                                        </div>
                                    </Card.Content>
                                </Card>
                            </Col>
                        </Row>
                        <Row gutter={24} style={{marginBottom: 15}}>
                            <Col span={24}>
                                <Card free style={{width: '100%', boxShadow: '0 2px 12px 0 rgb(0 0 0/10%)'}}>
                                    <Card.Header title="进程信息" extra={() => {
                                        return (
                                            <Typography.H3>进程信息</Typography.H3>
                                        )
                                    }}/>
                                    <Card.Divider/>
                                    <Card.Content>
                                        <div className="el-table">
                                            <table cellSpacing="0" style={{
                                                width: '100%',
                                                display: 'table',
                                                borderCollapse: 'separate',
                                                textIndent: 'initial',
                                                fontSize: '12px',
                                                color: '#606266'
                                            }}>
                                                <thead>
                                                <tr>
                                                    <th className="is-leaf">
                                                        <div className="cell">进程 ID</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">进程名称</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">所有者</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">使用物理内存</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">进程状态</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">占用 CPU</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">占用物理内存</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">时间总计</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">优先级</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">nice 值</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">使用虚拟内存</div>
                                                    </th>
                                                    <th className="is-leaf">
                                                        <div className="cell">共享内存</div>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {
                                                    Array.isArray(nodeProcess)
                                                    && nodeProcess.length > 0
                                                    && nodeProcess.map((o, i) => {
                                                        return (
                                                            <tr>
                                                                <td>
                                                                    <div className="cell">{o.pid}</div>
                                                                </td>
                                                                <td>
                                                                    <div className="cell">{o.command}</div>
                                                                </td>
                                                                <td>
                                                                    <div className="cell">{o.user}</div>
                                                                </td>
                                                                <td>
                                                                    <div className="cell">{o.res}</div>
                                                                </td>
                                                                <td>
                                                                    <div className="cell">{o.status}</div>
                                                                </td>
                                                                <td>
                                                                    <div className="cell">{o.cpu}</div>
                                                                </td>
                                                                <td>
                                                                    <div className="cell">{o.mem}</div>
                                                                </td>
                                                                <td>
                                                                    <div className="cell">{o.time}</div>
                                                                </td>
                                                                <td>
                                                                    <div className="cell">{o.pr}</div>
                                                                </td>
                                                                <td>
                                                                    <div className="cell">{o.ni}</div>
                                                                </td>
                                                                <td>
                                                                    <div className="cell">{o.virt}</div>
                                                                </td>
                                                                <td>
                                                                    <div className="cell">{o.shr}</div>
                                                                </td>
                                                            </tr>
                                                        )
                                                    })
                                                }
                                                </tbody>
                                            </table>
                                        </div>
                                    </Card.Content>
                                </Card>
                            </Col>
                        </Row>
                    </div>
                </Container>

            </div>
        )
            ;
    }

}

export default NodeDetail;

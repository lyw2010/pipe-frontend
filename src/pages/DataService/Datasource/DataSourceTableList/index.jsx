/**
 * @description: 数据中台-数据源-数据源管理
 * @author: hj
 * @update: hj(2020-01-14)
 */
import React from 'react';
import Container from '@icedesign/container';
import styles from './index.module.scss';
import event from '@/request'
import {Button, Message, Pagination, Table} from "@alifd/next";
import $http from '@/service/Services';
import AuthButton from "@/components/AuthComponent/AuthBotton";


/**
 * list
 */
class DataSourceTableList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            current: 0,
            mockData: [],
            selectedList: [],
            uploadVisible: false,
            errorMsg: [],
            condition: "",
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            },
            loading: false
        };
        this.pageNum = 1; // 当前页数
        this.pageSize = 10; // 分页每页显示数据条数
        this.totalNum = 0; // 数据总条数
        this.$http = $http;
        this.onPageChange = this.onPageChange.bind(this);
        this.getMetadataItemList = this.getMetadataItemList.bind(this);
    }

    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        const params = new URLSearchParams(this.props.location.search);
        const id = params.get("datasourceId");
        this.setState({
            id: id,
        }, () => {
            this.getMetadataItemList();
        });

    };

    /**
     * 获取表详情
     * @param record
     */
    getDatasourceTable = () => {
        this.setState({
            loading: true
        });
        const _this = this;
        let urls = event.event.url + '/v1/tDatasourceTable/getDatasourceTable?datasourceId=' + this.state.id
        this.$http.post(urls, {
            params: {
                subjectId: this.state.subjectId
            }
        })
            .then(function (response) {
                Message.success("操作成功.");
                this.setState({
                    loading: false
                },()=>{
                    _this.getMetadataItemList();
                })

            })
            .catch(function (error) {
                Message.error(error.message);
                _this.setState({
                    loading: false
                });
            })

    }
    /**
     * 条件
     * @param e
     * @param value
     */
    searchCondition = (value, e) => {
        this.setState({
            condition: value,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        });
        this.pageNum = 1;
    };
    /**
     * 改变页面
     * @param currentPage
     */
    onPageChange = (currentPage) => {
        this.state = {
            current: currentPage,
            mockData: this.props.mockData,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        };
        this.getMetadataItemList();
    };
    /**
     * 分页大小改变执行
     * @param pageSize
     */
    onPageSizeChange =(pageSize)=>{
        this.pageSize = pageSize;
        this.pageNum = 1;
        this.getMetadataItemList();
    };
    /**
     * 修改分页数据
     * @param currentPage
     */
    onChangePage = (currentPage) => {
        this.setState({
            loading: true,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        });
        setTimeout(() => {
            this.getMetadataItemList(currentPage);
            this.setState({
                loading: false,
                config: {
                    data: null,
                    visible: false,
                    title: "",
                    content: ""
                }
            });
        }, 0);
    };

    /**
     * 获取数据
     */
    getMetadataItemList = (pageNum) => {
        const _this = this;
        this.setState({
            loading: true
        });
        _this.pageNum = typeof (pageNum) == "number" ? pageNum : _this.pageNum;
        let urls = event.event.url + '/v1/tDatasourceTable/getPlatformDatasource/' + _this.pageSize + '/' + _this.pageNum + "?datasourceId=" + this.state.id;
        this.$http.get(urls)
            .then(function (response) {
                const {data} = response;
                _this.totalNum = data.data.total;
                data.data.voList.forEach((o, i) => {
                    o.number = i + 1;
                });
                _this.setState({
                    loading: false,
                    mockData: data.data.voList,
                    current: data.data.current,
                    config: {
                        data: null,
                        visible: false,
                        title: "",
                        content: ""
                    }
                });
            })
            .catch(function (error) {
                Message.error(error.message);
                _this.setState({
                    loading: false,
                });
            })

    };

    /**
     *
     * @param args
     */
    onChange = (...args) => {
        this.setState({
            selectedList: args
        });
    };


    /**
     * 导入数据
     */
    uploadVersion = () => {
        this.setState({
            uploadVisible: true
        });
    };
    /**
     *
     * @param file  {Object} 出错的文件
     * @param fileList  {Array} 当前值
     */
    onError = (file, fileList) => {
    };


    render() {
        const {mockData} = this.state;
        return (
            <div>
                <div className='container-header'>
                    <p style={{display: 'inline-block'}}>数据源表详情</p>
                    <Button type="normal" size="small" className='next-btn-normal table-btn-return'
                            style={{float: 'right'}}>
                        <a onClick={() => {
                            this.props.history.go(-1)
                        }}> 返回</a>
                    </Button>
                </div>
                <Container className='container-main'>
                    <div className='container-btnBox' style={{marginTop: '0'}}>
                        <div className={styles.buttons}>
                            <AuthButton auth={"DATA_CENTER$_DATASOURCE$_BATCH_DELETE"}
                                        onClick={() => this.getDatasourceTable()}
                                        title={"更新数据表"}/>
                        </div>
                    </div>
                    <div className='container-table'>
                        <Table dataSource={mockData} size={'small'} loading={this.state.loading} primaryKey="id" className={styles.table}>
                            <Table.Column align="center" title="序号" width={50} cell={
                                (value, index, record) => {
                                    return index + 1 + (this.pageNum - 1) * this.pageSize;
                                }
                            }/>
                            <Table.Column align="center" title="数据源名称" dataIndex="datasourceName"/>
                            <Table.Column align="center" title="表名" dataIndex="name"/>
                            <Table.Column align="center" title="备注" dataIndex="remarks"/>
                            <Table.Column align="center" title="创建时间" width={160} dataIndex="createTime"/>
                            <Table.Column align="center" title="操作" cell={
                                (value, index, record) => {
                                    return (
                                        <div>
                                            <AuthButton text auth={"DATA_CENTER$_DATASOURCE$_DETAIL"} type="normal"
                                                        size="small"
                                                        link={{
                                                            to: "dataSourceDetailColumn?tableId=" + record.id,
                                                            text: "详情"
                                                        }}/>
                                            <AuthButton text auth={"DATA_CENTER$_DATASOURCE$_DETAIL"} type="normal"
                                                        size="small"
                                                        link={{
                                                            to: "dataSourceDetailColumn?tableId=" + record.id,
                                                            text: "查看数据"
                                                        }}/>
                                        </div>
                                    )
                                }
                            }/>
                        </Table>
                    </div>
                    {/**
                     * 分页
                     * defaultCurrent 初始页码
                     * total 总记录数
                     * pageSize 一页中的记录数
                     */}
                    <Pagination defaultCurrent={1}
                                pageSize={this.pageSize}
                                total={this.totalNum}
                                onChange={this.onChangePage}
                                onPageSizeChange={this.onPageSizeChange}
                                pageSizeList={[5,10,20,50,100,200]}
                                pageSizeSelector="dropdown"
                                className="page-demo"
                                size="small"
                    />
                </Container>
            </div>
        )
            ;
    }

}

export default DataSourceTableList;

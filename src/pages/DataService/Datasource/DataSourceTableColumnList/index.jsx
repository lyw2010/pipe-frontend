/**
 * @description: 数据中台-数据源-数据源管理
 * @author: hj
 * @update: hj(2020-01-14)
 */
import React from 'react';
import Container from '@icedesign/container';
import styles from './index.module.scss';
import event from '@/request'
import {Button, Message, Pagination, Table} from "@alifd/next";
import $http from '@/service/Services';
import AuthButton from "@/components/AuthComponent/AuthBotton";


const commonProps = {
    style: {width: 450},
    title: '提示信息',
    subTitle: '上传数据提示'
};

/**
 * list
 */
class DataSourceTableColumnList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            current: 0,
            mockData: [],
            selectedList: [],
            uploadVisible: false,
            errorMsg: [],
            condition: "",
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        };
        this.pageNum = 1; // 当前页数
        this.pageSize = 10; // 分页每页显示数据条数
        this.totalNum = 0; // 数据总条数
        this.$http = $http;
        this.onPageChange = this.onPageChange.bind(this);
        this.getMetadataItemList = this.getMetadataItemList.bind(this);
    }

    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        const params = new URLSearchParams(this.props.location.search);
        const id = params.get("tableId");
        this.setState({
            id: id,
        }, () => {
            this.getMetadataItemList();
        });
    };


    /**
     * 条件
     * @param e
     * @param value
     */
    searchCondition = (value, e) => {
        this.setState({
            condition: value,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        });
        this.pageNum = 1;
    };
    /**
     * 改变页面
     * @param currentPage
     */
    onPageChange = (currentPage) => {
        this.state = {
            current: currentPage,
            mockData: this.props.mockData,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        };
        this.getMetadataItemList();
    };
    /**
     * 分页大小改变执行
     * @param pageSize
     */
    onPageSizeChange =(pageSize)=>{
        this.pageSize = pageSize;
        this.pageNum = 1;
        this.getMetadataItemList();
    };
    /**
     * 修改分页数据
     * @param currentPage
     */
    onChangePage = (currentPage) => {
        this.setState({
            loading: true,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        });
        setTimeout(() => {
            this.getMetadataItemList(currentPage);
            this.setState({
                loading: false,
                config: {
                    data: null,
                    visible: false,
                    title: "",
                    content: ""
                }
            });
        }, 0);
    };

    /**
     * 获取数据
     */
    getMetadataItemList = (pageNum) => {
        const _this = this;

        _this.pageNum = typeof (pageNum) == "number" ? pageNum : _this.pageNum;
        let urls = event.event.url + '/v1/tDatasourceTable/getTableColumns/' + this.state.id + "/" + _this.pageSize + '/' + _this.pageNum;
        this.$http.get(urls, {
            params: {
                subjectId: this.state.subjectId
            }
        })
            .then(function (response) {
                const {data} = response;
                _this.totalNum = data.data.total;
                data.data.voList.forEach((o, i) => {
                    o.number = i + 1;
                });
                _this.setState({
                    mockData: data.data.voList,
                    current: data.data.current,
                    config: {
                        data: null,
                        visible: false,
                        title: "",
                        content: ""
                    }
                });
            })
            .catch(function (error) {
                Message.error(error.message);
            })

    };

    /**
     * 获取表详情
     * @param record
     */
    setIncrementalColumn = (record, bool) => {
        const _this = this;
        let urls = event.event.url + '/v1/tDatasourceTable/setIncrementalColumn/' + record.id + "/" + bool
        this.$http.post(urls, {
            params: {
                subjectId: this.state.subjectId
            }
        })
            .then(function (response) {
                Message.success("操作成功.");
                _this.getMetadataItemList();
            })
            .catch(function (error) {
                Message.error(error.message);
            })

    }

    render() {
        const {mockData} = this.state;
        const actionUrl = event.event.url + '/v1/tDataSource/importDataSource/' + this.state.subjectId + "?save=true";
        return (
            <div>
                <div className='container-header'>
                    <p style={{display: 'inline-block'}}>数据表字段详情</p>
                    <Button type="normal" size="small" className='next-btn-normal table-btn-return'
                            style={{float: 'right'}}>
                        <a onClick={() => {
                            this.props.history.go(-1)
                        }}> 返回</a>
                    </Button>
                </div>
                <Container className='container-main'>
                    <div className='container-table'>
                        <Table dataSource={mockData} size={'small'} primaryKey="id" className={styles.table}
                               rowSelection={{
                                   onChange: this.onChange,
                                   getProps: (record, index) => {
                                       return {
                                           children: ""
                                       };
                                   },
                                   columnProps: () => {
                                       return {
                                           lock: 'left',
                                           width: 90,
                                           align: 'center'
                                       };
                                   },
                                   titleAddons: () => {
                                       return;
                                   },
                                   titleProps: () => {
                                       return {
                                           children: ''
                                       };
                                   }
                               }}>
                            <Table.Column align="center" title="序号" width={50} cell={
                                (value, index, record) => {
                                    return index + 1 + (this.pageNum - 1) * this.pageSize;
                                }
                            }/>
                            <Table.Column align="center" title="数据列名称" dataIndex="name"/>
                            <Table.Column align="center" title="字段名称" dataIndex="remarks"/>
                            <Table.Column align="center" width={100} title="数据库类型" dataIndex="jdbcTypename"/>
                            <Table.Column align="center" width={80} title="是否为空" dataIndex="nullable" cell={
                                (value, index, record) => {
                                    return value == 1 ? (
                                        <span>为空</span>
                                    ) : (
                                        <span>不能为空</span>
                                    )
                                }
                            }/>
                            <Table.Column align="center" width={90} title="是否主键" dataIndex="primaryKey"/>
                            <Table.Column align="center" width={90} title="是否增量字段" dataIndex="incrementalColumn"/>
                            <Table.Column align="center" width={90} title="字段长度" dataIndex="length"/>
                            <Table.Column align="center" width={90} title="小数位" dataIndex="scale"/>
                            <Table.Column align="center" title="Java类型" dataIndex="javaProperty"/>
                            <Table.Column align="center" title="默认值" dataIndex="defaultValue"/>
                            <Table.Column align="center" title="操作" cell={
                                (value, index, record) => {
                                    return (
                                        <div>
                                            <AuthButton text auth={"DATA_CENTER$_DATASOURCE$_DETAIL"} type="normal"
                                                        size="small"
                                                        title={record.incrementalColumn == "是" ? "普通字段" : "增量字段"}
                                                        onClick={() => this.setIncrementalColumn(record, record.incrementalColumn != "是")}/>
                                        </div>
                                    )
                                }
                            }/>
                        </Table>
                    </div>
                    {/**
                     * 分页
                     * defaultCurrent 初始页码
                     * total 总记录数
                     * pageSize 一页中的记录数
                     */}
                    <Pagination defaultCurrent={1}
                                pageSize={this.pageSize}
                                total={this.totalNum}
                                onChange={this.onChangePage}
                                onPageSizeChange={this.onPageSizeChange}
                                pageSizeList={[5,10,20,50,100,200]}
                                pageSizeSelector="dropdown"
                                className="page-demo"
                                size="small"
                    />
                </Container>
            </div>
        )
            ;
    }

}

export default DataSourceTableColumnList;

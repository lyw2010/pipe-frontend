/**
 * @description: 数据中台-任务-任务管理
 * @author: hj
 * @update: hj(2020-01-14)
 */
import React from 'react';
import Container from '@icedesign/container';
import styles from './index.module.scss';
import event from '@/request'
import {Button, Input, Message, Pagination, Table} from '@alifd/next';
import $http from '@/service/Services';
import AuthButton from "../../../../components/AuthComponent/AuthBotton";


/**
 * list
 */
class BusinessIntelligenceChartTypeList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            subjectId: '',
            current: 0,
            mockData: [],
            selectedList: [],
            condition: '',
            config: {
                data: null,
                visible: false,
                title: '',
                content: ''
            }
        };
        this.pageNum = 1; // 当前页数
        this.pageSize = 10; // 分页每页显示数据条数
        this.totalNum = 0; // 数据总条数
        this.$http = $http;
        this.cancelCall = this.cancelCall.bind(this);
        this.onPageChange = this.onPageChange.bind(this);
        this.getBusinessIntelligenceChartTypeList = this.getBusinessIntelligenceChartTypeList.bind(this);
        this.deleteEventTracking = this.deleteEventTracking.bind(this);
    }

    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        this.getBusinessIntelligenceChartTypeList();
    };


    /**
     * 条件
     * @param e
     * @param value
     */
    searchCondition = (value, e) => {
        this.setState({
            condition: {
                name:value
            },
            config: {
                data: null,
                visible: false,
                title: '',
                content: ''
            }
        });
        this.pageNum = 1;
    };
    /**
     * 改变页面
     * @param currentPage
     */
    onPageChange = (currentPage) => {
        this.state = {
            current: currentPage,
            mockData: this.props.mockData,
            config: {
                data: null,
                visible: false,
                title: '',
                content: ''
            }
        };
        this.getBusinessIntelligenceChartTypeList();
    };
    /**
     * 分页大小改变执行
     * @param pageSize
     */
    onPageSizeChange =(pageSize)=>{
        this.pageSize = pageSize;
        this.pageNum = 1;
        this.getBusinessIntelligenceChartTypeList();
    };
    /**
     * 修改分页数据
     * @param currentPage
     */
    onChangePage = (currentPage) => {
        this.setState({
            loading: true,
            config: {
                data: null,
                visible: false,
                title: '',
                content: ''
            }
        });
        setTimeout(() => {
            this.getBusinessIntelligenceChartTypeList(currentPage);
            this.setState({
                loading: false,
                config: {
                    data: null,
                    visible: false,
                    title: '',
                    content: ''
                }
            });
        }, 0);
    };

    /**
     * 获取数据
     */
    getBusinessIntelligenceChartTypeList = (pageNum) => {
        const _this = this;
        _this.pageNum = typeof (pageNum) == 'number' ? pageNum : _this.pageNum;
        let urls = event.event.url + '/v1/intelligenceChartType/getBusinessIntelligenceChartTypePage';
        this.$http.post(urls, {
            pageSize:_this.pageSize,
            pageNum: _this.pageNum,
            condition: {
                code: _this.state.condition.code,
                name: _this.state.condition.name
            }
        })
            .then(function (response) {
                const {data} = response;
                _this.totalNum = data.data.total;
                _this.setState({
                    mockData: data.data.voList.map(o=>{
                        o["_code"] = o["code"];
                        return o;
                    }),
                    current: data.data.current,
                    config: {
                        data: null,
                        visible: false,
                        title: '',
                        content: ''
                    }
                });
            })
            .catch(function (error) {
                Message.error(error.message);
            })

    };


    /**
     * 删除
     */
    deleteEventTracking = (row) => {
        this.setState({
                config: {
                    data: row,
                    visible: true,
                    title: '提示消息',
                    content: '删除后不能恢复，确认要删除？'
                }
            }
        )
    };

    /**
     * 取消按钮的操作
     */
    cancelCall = () => {
        this.setState({
            config: {
                visible: false
            }
        });
        console.log('点击取消按钮 .');
    };



    render() {
        const {mockData} = this.state;
        return (
            <div>
                <div className='container-header'>
                    <p>图列类型</p>
                </div>
                <Container className='container-main'>
                    <div className='container-btnBox' style={{marginTop: '0'}}>
                        <div className={styles.buttons}>
                            <AuthButton auth={'DATA_CENTER$_METADATA$_ADD_METADATA'} type="normal" size="small"
                                        link={{to: 'chartTypeAdd?id=', text: '新建图例类型'}}/>
                            <span className={styles.caseNumber}>
                                <Input onChange={this.searchCondition.bind(this)} placeholder={'图列类型'}
                                       className={`${styles.input} ${styles.shortInput}`}/>

                                <Button
                                    type="primary"
                                    className='pipe-btn-search'
                                    onClick={this.getBusinessIntelligenceChartTypeList}
                                >
                                查询
                                </Button>
                            </span>
                        </div>
                    </div>
                    <div className='container-table'>
                        <Table dataSource={mockData} size={'small'} primaryKey="id" className={styles.table}>
                            <Table.Column align="center" lock={"left"} title="序号" width={50} cell={
                                (value, index, record) => {
                                    return index + 1 + (this.pageNum - 1) * this.pageSize;
                                }
                            }/>
                            <Table.Column align="center" title="" cell={
                                (value, index, record) => {
                                    return (
                                        <div>
                                             <img width={150} height={100} src={"./chart/" + record._code + ".png"} onError={()=>{
                                                 record._code = "Chart";
                                                 this.forceUpdate()
                                             }}/>
                                        </div>
                                    )
                                }
                            }/>
                            <Table.Column align="center" title="图表类型编码" dataIndex="code"/>
                            <Table.Column align="center" title="图表类型名字" dataIndex="name"/>
                            <Table.Column align="center" title="创建时间" dataIndex="createTime"/>
                            <Table.Column align="center"  lock={"right"} title="操作" cell={
                                (value, index, record) => {
                                    return (
                                        <div>
                                            <AuthButton auth={'DATA_CENTER$_METADATA$_ADD_METADATA'} type="normal"
                                                        size="small"
                                                        link={{to: 'chartTypeAdd?id=' + record.id, text: '修改'}}/>
                                        </div>
                                    )
                                }
                            }/>
                        </Table>
                    </div>
                    {/**
                     * 分页
                     * defaultCurrent 初始页码
                     * total 总记录数
                     * pageSize 一页中的记录数
                     */}
                    <Pagination defaultCurrent={1}
                                pageSize={this.pageSize}
                                total={this.totalNum}
                                onPageSizeChange={this.onPageSizeChange}
                                pageSizeList={[5,10,20,50,100,200]}
                                pageSizeSelector="dropdown"
                                onChange={this.onChangePage}
                                className="page-demo"
                                size="small"
                    />
                </Container>
            </div>
        )
            ;
    }


}

export default BusinessIntelligenceChartTypeList;

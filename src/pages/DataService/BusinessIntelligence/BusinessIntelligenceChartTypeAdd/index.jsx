import React from 'react';
import IceContainer from '@icedesign/container';
import {Button, Input, Message, Radio, Select} from '@alifd/next';
import {
    FormBinder as IceFormBinder,
    FormBinderWrapper as IceFormBinderWrapper,
    FormError as IceFormError,
} from '@icedesign/form-binder';
import styles from './index.module.scss';
import event from '@/request'
import $http from '@/service/Services';


const axios = $http;

const {Option} = Select;
const {Group: RadioGroup} = Radio;
let form;

const Shape = {
    NO: 'normal',
    OFF: 'button'
};
const ItemDirection = {
    HORIZON: 'hoz',
    VERTICAL: 'ver'
};

/**
 * list
 */
class BusinessIntelligenceChartTypeAdd extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            typeConfig: {},
            value: {},
            schemaList: [],
            addValue: {},
            mockData: [],
            businessList:[],
            change: false,
            arr: [{
                _id: Math.random().toString(36).substring(2),
                name: "",
                value: ""
            }],
            id: "",
            subjectId: "",
            addVisible: false,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            },
            placeholder: "请输入数据",
            shape: Shape.NO,
            itemDirection: ItemDirection.HORIZON
        };
        this.$http = $http;
        this.formChange = this.formChange.bind(this);
        this.validateAllFormField = this.validateAllFormField.bind(this);
    }

    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        const params = new URLSearchParams(this.props.location.search);
        const id = params.get("id");
        this.setState({
            id: id,
        }, () => {
            this.getBusinessList();
            if (id) {
                const that = this;
                this.$http.get(event.event.url + '/v1/intelligenceChartType/getBusinessIntelligenceChartById/' + id)
                    .then(function (response) {
                        const {data} = response;
                        if (data.code === 1) {
                            Message.warning(data.message ? data.message : data.data);
                        } else {
                            that.setState({
                                value: data.data
                            })
                        }
                    })
                    .catch(function (error) {
                        console.error(error);
                        Message.error(error.message);
                    })
            }
        });
    };

    /**
     * 根据id查询详情
     */
    getBusinessList  = () => {
        const that = this;
        let urls = event.event.url + '/v1/platformBusiness/getPlatformBusinessList';
        this.$http.post(urls)
            .then(function (response) {
                const {data} = response;
                that.setState({
                    businessList:data.data
                })
            })
            .catch(function (error) {
                Message.error(error.message);
            })
    };
    /**
     * 高级属性是否可用
     * @param checked
     */
    onChange = (checked) => {
        this.setState({
            change: checked
        })
    }

    /* 表改变
    * @param formValue
    */
    formChange = (formValue) => {
        this.setState({
            value: formValue
        })
    };


    /**
     * 表验证
     */
    validateAllFormField = () => {
        const that = this;
        form.validateAll((errors, values) => {
            const urls =  event.event.url + '/v1/intelligenceChartType/saveOrUpdateChartType'
            this.$http.post(urls, values)
                .then(function (response) {
                    const {data} = response;
                    if (data.code === 1) {
                        Message.warning(data.message ? data.message : data.data);
                    } else {
                        Message.success("操作成功.");
                        that.props.history.go(-1);
                    }
                })
                .catch(function (error) {
                    Message.error(error.message);
                })
        });
    };
    /**
     * 取消提示弹框
     */
    onCloseDialog = reason => {
        this.setState({
            visible: false
        });
    };



    /**
     * 表
     * @returns {*}
     */
    render() {
        return (
            <div>
                <div className='container-header'>
                    <p style={{display: 'inline-block'}}>{this.state.id ? "修改图例类型" : "新建图例类型"}</p>
                    <Button type="normal" size="small" className='next-btn-normal table-btn-return' style={{float: 'right'}}>
                        <a onClick={() => {
                            this.props.history.go(-1)
                        }}> 返回</a>
                    </Button>
                </div>


                <IceContainer className='container-main'>
                    <IceFormBinderWrapper
                        value={this.state.value}
                        onChange={this.formChange}
                        ref={formRef => form = formRef}
                    >
                        <div className={styles.formContent}>
                            <h4 className={styles.dTitle}>基础信息</h4>
                            <div className={styles.formItem}>
                                <div className={styles.formLabel}><span className={styles.red}>*</span>业务：</div>
                                <IceFormBinder required name="businessId" message="业务不能为空">
                                    <Select
                                        disabled={this.state.id}
                                        placeholder="请选择"
                                        className={styles.inputNme}
                                    >
                                        {
                                            this.state.businessList.map(
                                                o => {
                                                    return (
                                                        <Option value={o.id}>{o.name}</Option>
                                                    )
                                                }
                                            )
                                        }
                                    </Select>
                                </IceFormBinder>
                                <div className={styles.formError}>
                                    <IceFormError name="sourceType"/>
                                </div>
                            </div>
                            <div className={styles.formItem}>
                                <div className={styles.formLabel}><span className={styles.red}>*</span>图表类型编码：</div>
                                <IceFormBinder
                                    required
                                    disabled={this.state.id}
                                    triggerType="onBlur"
                                    message=" 图表类型编码不能为空"
                                    name="code"
                                >
                                    <Input  disabled={this.state.id}
                                        placeholder="请输入图表类型编码"
                                        className={styles.inputNme}
                                    />
                                </IceFormBinder>
                                <div className={styles.formError}>
                                    <IceFormError name="code"/>
                                </div>
                            </div>
                            <div className={styles.formItem}>
                                <div className={styles.formLabel}><span className={styles.red}>*</span>图表类型名字：</div>
                                <IceFormBinder
                                    required
                                    triggerType="onBlur"
                                    message="图表类型名字不能为空"
                                    name="name"
                                >
                                    <Input
                                        placeholder="请输入图表类型名字"
                                        className={styles.inputNme}
                                    />
                                </IceFormBinder>
                                <div className={styles.formError}>
                                    <IceFormError name="name"/>
                                </div>
                            </div>

                            <Button
                                type="primary"
                                className='sync-btn-submit'
                                onClick={this.validateAllFormField}
                            >
                                提 交
                            </Button> &nbsp;&nbsp;
                            <Button
                                type="primary"
                                className='zgph-btn-cancel'
                                onClick={() => {
                                    this.props.history.go(-1)
                                }}
                            >
                                取 消
                            </Button>
                        </div>
                    </IceFormBinderWrapper>
                </IceContainer>
            </div>
        );
    }

}

export default BusinessIntelligenceChartTypeAdd;

import React from 'react';
import IceContainer from "@icedesign/container";
import styles from './index.module.scss';
import {Button, Input, Message} from "@alifd/next";
import {
    FormBinder as IceFormBinder,
    FormBinderWrapper as IceFormBinderWrapper,
    FormError as IceFormError,
} from '@icedesign/form-binder';
import event from '@/request'
import $http from '@/service/Services';

let form;

const Shape = {
    NO: 'normal',
    OFF: 'button'
};
const ItemDirection = {
    HORIZON: 'hoz',
    VERTICAL: 'ver'
};

/**
 * list
 */
class BusinessAdd extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            typeConfig: {},
            value: {},
            schemaList: [],
            addValue: {},
            mockData: [],
            change: false,
            businessList: [],
            arr: [{
                _id: Math.random().toString(36).substring(2),
                name: "",
                value: ""
            }],
            id: "",
            subjectId: "",
            addVisible: false,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            },
            placeholder: "请输入数据",
            shape: Shape.NO,
            itemDirection: ItemDirection.HORIZON
        };
        this.$http = $http;
        this.validateAllFormField = this.validateAllFormField.bind(this);
    }



    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {

        const params = new URLSearchParams(this.props.location.location ? this.props.location.location.search : this.props.location.search);
        const id = params.get("businessId");
        this.setState({
            id: id,
        }, () => {

            if (id) {
                const that = this;
                this.$http.post(event.event.url + '/v1/platformBusiness/getPlatformBusinessById/' + id)
                    .then(function (response) {
                        const {data} = response;
                        if (data.code === 1) {
                            Message.warning(data.message ? data.message : data.data);
                        } else {


                            that.setState({
                                value: data.data
                            })
                        }
                    })
                    .catch(function (error) {
                        console.error(error);
                        Message.error(error.message);
                    })
            }
        });

    };

    /**
     * 表验证
     */
    validateAllFormField = () => {
        const that = this;
        form.validateAll((errors, values) => {
            const urls =  event.event.url + '/v1/platformBusiness/saveOrUpdateBusiness' ;
            this.$http.post(urls, values)
                .then(function (response) {
                    const {data} = response;
                    if (data.code === 1) {
                        Message.warning(data.message ? data.message : data.data);
                    } else {
                        Message.success("操作成功.");
                        that.props.history.go(-1);
                    }
                })
                .catch(function (error) {
                    Message.error(error.message);
                })
        });
    };

    /**
     *
     * @param keys
     * @param info
     */
    handleCheck(keys, info) {
        this.setState({
            checkedKeys: keys
        });
    }

    /**
     *
     */
    handleCheckStrictly() {
        this.setState({
            checkStrictly: !this.state.checkStrictly,
            checkedKeys: []
        });
    }

    /**
     * 表
     * @returns {*}
     */
    render() {
        return (
            <div>
                <div className='container-header'>
                    <p style={{display: 'inline-block'}}>{this.state.id ? "修改业务" : "新建业务"}</p>
                    <Button type="normal" size="small" className='next-btn-normal table-btn-return' style={{float: 'right'}}>
                        <a onClick={() => {
                            this.props.history.go(-1)
                        }} > 返回</a>
                    </Button>
                </div>


                <IceContainer className='container-main'>
                    <IceFormBinderWrapper
                        value={this.state.value}
                        onChange={this.formChange}
                        ref={formRef => form = formRef}
                    >
                        <div className={styles.formContent}>
                            <h4 className={styles.dTitle}>基础信息</h4>
                            <div className={styles.formItem}>
                                <div className={styles.formLabel}><span className={styles.red}>*</span>业务名称：</div>
                                <IceFormBinder
                                    required
                                    triggerType="onBlur"
                                    message=" 业务名称不能为空"
                                    name="name"
                                >
                                    <Input
                                        placeholder="请输入业务名称"
                                        className={styles.inputNme}
                                    />
                                </IceFormBinder>
                                <div className={styles.formError}>
                                    <IceFormError name="sourceName"/>
                                </div>
                            </div>
                            <div className={styles.formItem}>
                                <div className={styles.formLabel}><span className={styles.red}>*</span>业务编码：</div>
                                <IceFormBinder
                                    required
                                    triggerType="onBlur"
                                    message="业务编码不能为空"
                                    name="code"
                                >
                                    <Input
                                        placeholder="请输入业务编码"
                                        className={styles.inputNme}
                                    />
                                </IceFormBinder>
                                <div className={styles.formError}>
                                    <IceFormError name="sourceName"/>
                                </div>
                            </div>
                            <div className={styles.formItem}>
                                <div className={styles.formLabel}>描述:</div>
                                <IceFormBinder
                                    triggerType="onBlur"
                                    name="remark"
                                >
                                    <Input.TextArea
                                        maxLength={50}
                                        rows={8}
                                        hasLimitHint
                                        placeholder="请输入描述"
                                        className={styles.inputNme}
                                    />
                                </IceFormBinder>
                            </div>
                            <Button
                                type="primary"
                                className='sync-btn-submit'
                                onClick={this.validateAllFormField}
                            >
                                提 交
                            </Button> &nbsp;&nbsp;
                            <Button
                                type="primary"
                                className='zgph-btn-cancel'
                                onClick={() => {
                                    this.props.history.go(-1)
                                }}
                            >
                                取 消
                            </Button>
                        </div>
                    </IceFormBinderWrapper>
                </IceContainer>
            </div>
        );
    }

}

export default BusinessAdd;

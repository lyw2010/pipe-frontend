import React from 'react';
import BusinessAdd from "../BusinessAdd";


/**
 * list
 */
class BusinessEdit extends React.Component {
    constructor(props) {
        super(props);

    }



    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        const params = new URLSearchParams(this.props.location.search);
        const id = params.get("businessId");
        this.setState({
            id: id,
        });
    };

    /**
     * 表
     * @returns {*}
     */
    render() {
        return (
            <BusinessAdd location={this.props} />
        );
    }

}

export default BusinessEdit;

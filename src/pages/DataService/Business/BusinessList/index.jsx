/**
 * @description: 数据中台-数据服务
 * @author: hj
 * @update: hj(2020-01-14)
 */
import React from 'react';
import 'antd/dist/antd.css';
import {Input} from 'antd';
import Container from "@icedesign/container";
import styles from './index.module.scss';
import {Button, Dialog, Message, Pagination, Table} from "@alifd/next";
import ZgDialog from "../../../../components/ZgDialog";
import event from '@/request'
import $http from '@/service/Services';
import {Link} from "react-router-dom";
import AuthButton from "../../../../components/AuthComponent/AuthBotton";


/**
 * list
 */
class BusinessList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            current: 0,
            mockData: [],
            selectedList: [],
            uploadVisible: false,
            errorMsg: [],
            condition: "",
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        };
        this.pageNum = 1; // 当前页数
        this.pageSize = 10; // 分页每页显示数据条数
        this.totalNum = 0; // 数据总条数
        this.$http = $http;
        this.cancelCall = this.cancelCall.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.onPageChange = this.onPageChange.bind(this);
        this.getMetadataItemList = this.getMetadataItemList.bind(this);
        this.deleteEventTracking = this.deleteEventTracking.bind(this);
    }

    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        this.getMetadataItemList();
    };


    /**
     * 条件
     * @param e
     * @param value
     */
    searchCondition = (value, e) => {
        this.setState({
            condition: value,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        });
        this.pageNum = 1;
    };
    /**
     * 改变页面
     * @param currentPage
     */
    onPageChange = (currentPage) => {
        this.state = {
            current: currentPage,
            mockData: this.props.mockData,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        };
        this.getMetadataItemList();
    };
    /**
     * 分页大小改变执行
     * @param pageSize
     */
    onPageSizeChange = (pageSize) => {
        this.pageSize = pageSize;
        this.pageNum = 1;
        this.getMetadataItemList();
    };
    /**
     * 修改分页数据
     * @param currentPage
     */
    onChangePage = (currentPage) => {
        this.setState({
            loading: true,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        });
        setTimeout(() => {
            this.getMetadataItemList(currentPage);
            this.setState({
                loading: false,
                config: {
                    data: null,
                    visible: false,
                    title: "",
                    content: ""
                }
            });
        }, 0);
    };
    /**
     * 获取数据
     */
    getMetadataItemList = (pageNum) => {
        const _this = this;

        _this.pageNum = typeof (pageNum) == "number" ? pageNum : _this.pageNum;
        let urls = event.event.url + '/v1/platformBusiness/getPlatformBusinessByPage/' + _this.pageSize + '/' + _this.pageNum;
        this.$http.post(urls, {
            params: {
                subjectId: this.state.subjectId
            }
        })
            .then(function (response) {
                const {data} = response;
                _this.totalNum = data.data.total;
                data.data.voList.forEach((o, i) => {
                    o.number = i + 1;
                });
                _this.setState({
                    mockData: data.data.voList,
                    current: data.data.current,
                    config: {
                        data: null,
                        visible: false,
                        title: "",
                        content: ""
                    }
                });
            })
            .catch(function (error) {
                Message.error(error.message);
            })

    };


    /**
     * 删除
     */
    deleteEventTracking = (row) => {
        this.setState({
                config: {
                    data: row,
                    selectedList: [row],
                    visible: true,
                    title: "提示消息",
                    content: "删除后不能恢复，确认要删除？"
                }
            }
        )
    };

    /**
     * 取消按钮的操作
     */
    cancelCall = () => {
        this.setState({
            config: {
                visible: false
            }
        });
    };


    /**
     * 删除
     */
    okCall = (data) => {
        this.setState({
            config: {
                visible: false
            }
        });
        const _this = this;
        this.$http.post(event.event.url + '/v1/platformBusiness/deleteBusinessById/' + data.id)
            .then(function (response) {
                const {data} = response;
                if (data.code === 1) {
                    Message.warning(data.message ? data.message : data.data);
                } else {
                    Message.success("操作成功.");
                    _this.getMetadataItemList();
                }
            })
            .catch(function (error) {
                Message.error(error.message);
            })
    };

    /**
     * 批量删除
     * @param ids
     */
    handleSelect = (ids) => {
        const _this = this;
        this.$http.post(event.event.url + '/v1/platformBusiness/deleteBusinessById', {ids: ids})
            .then(function (response) {
                const {data} = response;
                if (data.code === 1) {
                    Message.warning(data.message ? data.message : data.data);
                } else {
                    Message.success("操作成功.");
                    _this.getMetadataItemList();
                }
            })
            .catch(function (error) {
                Message.error(error.message);
            })
    };
    /**
     * 批量操作
     * @param text
     */
    handleClick = () => {
        const {selectedList} = this.state;
        if (selectedList.length == 0) {
            Message.error("请选择需要删除的数据.");
            return null;
        }
        this.setState({
            visible: true
        });
    };

    /**
     *
     * @param args
     */
    onChange = (...args) => {
        this.setState({
            selectedList: args
        });
    };


    /**
     * 确定提示弹框
     */
    onOkDialog = () => {
        this.setState({
            visible: false
        });
        const {selectedList} = this.state;
        this.handleSelect(selectedList[1].map(o => {
            return o.id;
        }));
    };
    /**
     * 取消提示弹框
     */
    onCloseDialog = reason => {
        this.setState({
            visible: false
        });
    };

    render() {
        const {mockData} = this.state;
        const userVo = JSON.parse(sessionStorage.getItem("userInfoVO"));
        return (
            <div>
                <div className='container-header'>
                    <p>业务管理</p>
                </div>
                <Container className='container-main'>
                    <div className='container-btnBox' style={{marginTop: '0'}}>
                        <div className={styles.buttons}>
                            <Link to={"add?id=&parentId=" + userVo.rolePermissionVO.orgId}><Button
                                className='pipe-btn-add'>新建业务</Button></Link>
                            <span className={styles.caseNumber}>
                                <Input onChange={this.searchCondition.bind(this)}
                                       hasClear
                                       placeholder="请输入租户代码/租户名称"
                                       className={`${styles.input} ${styles.shortInput}`}/>
                                <Button
                                    type="primary"
                                    className='pipe-btn-search'
                                    onClick={this.getMetadataItemList}
                                >
                                查询
                                </Button>
                            </span>
                        </div>
                    </div>
                    <div className='container-table'>
                        <Table dataSource={mockData} primaryKey="id" className={styles.table}>
                            <Table.Column align="center" title="序号" width={50} cell={
                                (value, index, record) => {
                                    return index + 1 + (this.pageNum - 1) * this.pageSize;
                                }
                            }/>
                            <Table.Column align="center" title="业务名称" dataIndex="name"/>
                            <Table.Column align="center" title="业务编码" dataIndex="code"/>
                            <Table.Column align="center" title="备注" dataIndex="remark"/>
                            <Table.Column align="center" title="是否默认" dataIndex="defaultData" cell={
                                (value, index, record) => {
                                    return value === 1 ? "默认" : "用户添加"
                                }
                            }/>
                            <Table.Column align="center" title="创建时间" dataIndex="createTime"/>
                            <Table.Column align="center" title="操作" cell={
                                (value, index, record) => {
                                    if (record.defaultData === 1) {
                                        return null
                                    } else {
                                        return (
                                            <div>
                                                <AuthButton text type="normal"
                                                            size="small"
                                                            link={{
                                                                to: "edit?businessId=" + record.id,
                                                                text: "修改"
                                                            }}/>
                                                <AuthButton text
                                                            onClick={() => this.deleteEventTracking(record)}
                                                            title={"删除"}/>
                                            </div>
                                        )
                                    }

                                }
                            }/>

                        </Table>
                        <ZgDialog config={this.state.config} cancelCall={this.cancelCall} okCall={this.okCall}/>
                        <Dialog
                            className='pipe-dialog'
                            title="提示信息"
                            visible={this.state.visible}
                            onOk={this.onOkDialog}
                            onCancel={this.onCloseDialog.bind(this, 'cancelClick')}
                            onClose={this.onCloseDialog}>
                            删除后不能恢复，确认要删除？
                        </Dialog>
                        <Pagination defaultCurrent={1}
                                    pageSize={this.pageSize}
                                    total={this.totalNum}
                                    onChange={this.onChangePage}
                                    onPageSizeChange={this.onPageSizeChange}
                                    pageSizeList={[5, 10, 20, 50, 100, 200]}
                                    pageSizeSelector="dropdown"
                                    className="page-demo"
                                    size="small"
                        />
                    </div>
                </Container>
            </div>
        )

    }

}

export default BusinessList;

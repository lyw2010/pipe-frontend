/**
 * @description: 数据中台-任务-任务管理
 * @author: hj
 * @update: hj(2020-01-14)
 */
import React from 'react';
import Container from '@icedesign/container';
import styles from './index.module.scss';
import event from '@/request'
import {Button, Message, Pagination, Table} from '@alifd/next';
import ZgDialog from '@/components/ZgDialog';
import $http from '@/service/Services';
import Link from "react-router-dom/es/Link";


/**
 * list
 */
class BusinessIntelligenceTaskHistoryList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            subjectId: '',
            current: 0,
            mockData: [],
            selectedList: [],
            condition: '',
            config: {
                data: null,
                visible: false,
                title: '',
                content: ''
            }
        };
        this.pageNum = 1; // 当前页数
        this.pageSize = 10; // 分页每页显示数据条数
        this.totalNum = 0; // 数据总条数
        this.$http = $http;
        this.onPageChange = this.onPageChange.bind(this);
        this.getTaskDetailHistoryList = this.getTaskDetailHistoryList.bind(this);
    }

    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        const params = new URLSearchParams(this.props.location.search);
        const id = params.get("taskId");
        this.setState({
            taskDetailId: id
        }, () => {
            this.getTaskDetailHistoryList();
        });
    };


    /**
     * 条件
     * @param e
     * @param value
     */
    searchCondition = (value, e) => {
        this.setState({
            condition: value,
            config: {
                data: null,
                visible: false,
                title: '',
                content: ''
            }
        });
        this.pageNum = 1;
    };
    /**
     * 改变页面
     * @param currentPage
     */
    onPageChange = (currentPage) => {
        this.state = {
            current: currentPage,
            mockData: this.props.mockData,
            config: {
                data: null,
                visible: false,
                title: '',
                content: ''
            }
        };
        this.getTaskDetailHistoryList();
    };

    /**
     * 分页大小改变执行
     * @param pageSize
     */
    onPageSizeChange =(pageSize)=>{
        this.pageSize = pageSize;
        this.pageNum = 1;
        this.getTaskDetailHistoryList();
    };
    /**
     * 修改分页数据
     * @param currentPage
     */
    onChangePage = (currentPage) => {
        this.setState({
            loading: true,
            config: {
                data: null,
                visible: false,
                title: '',
                content: ''
            }
        });
        setTimeout(() => {
            this.getTaskDetailHistoryList(currentPage);
            this.setState({
                loading: false,
                config: {
                    data: null,
                    visible: false,
                    title: '',
                    content: ''
                }
            });
        }, 0);
    };

    /**
     * 获取数据
     */
    getTaskDetailHistoryList = (pageNum) => {
        const _this = this;
        _this.pageNum = typeof (pageNum) == 'number' ? pageNum : _this.pageNum;
        let url = event.event.url + '/v1/taskDetailPlan/getBusinessIntelligenceTaskPlanPage';
        this.$http.post(url,
            {
                pageSize: _this.pageSize,
                pageNum: _this.pageNum,
                condition: {
                    name: _this.state.condition,
                    taskDetailId: _this.state.taskDetailId,
                    createStartDateTime: _this.state.createStartDateTime,
                    createEndDateTime: _this.state.createEndDateTime
                }
            })
            .then(function (response) {
                const {data} = response;
                const mockData = data.data.voList;
                _this.totalNum = data.data.total;
                mockData.forEach((o, i) => {
                    o.number = i + 1;
                });

                _this.setState({
                    mockData: mockData,
                    config: {
                        data: null,
                        visible: false,
                        title: "",
                        content: ""
                    }
                });
            })
            .catch(function (error) {
                Message.error(error.message);
            })

    };


    /**
     * 手动执行任务
     */
    executeBusinessIntelligenceTaskById = (record) => {
        const _this = this;
        _this.pageNum = typeof (pageNum) == 'number' ? pageNum : _this.pageNum;
        let url = event.event.url + '/v1/taskDetailPlan/executeBusinessIntelligenceTaskById/' + record.id;
        this.$http.post(url)
            .then(function (response) {
                Message.success("执行成功.");
                _this.getTaskDetailHistoryList();
            })
            .catch(function (error) {
                Message.error(error.message);
            })
    };

    /**
     * 取消执行
     */
    cancelExecuteTaskById = (record) => {
        const _this = this;
        _this.pageNum = typeof (pageNum) == 'number' ? pageNum : _this.pageNum;
        let url = event.event.url + '/v1/taskDetailPlan/cancelExecuteTaskById/' + record.id;
        this.$http.post(url)
            .then(function (response) {
                Message.success("执行成功.");
                _this.getTaskDetailHistoryList();
            })
            .catch(function (error) {
                Message.error(error.message);
            })
    };



    render() {
        const {mockData} = this.state;
        return (
            <div>
                <div className='container-header'>
                    <p>任务管理</p>
                    <Button type="normal" size="small" className='next-btn-normal table-btn-return'
                            style={{float: 'right'}}>
                        <a onClick={() => {
                            this.props.history.go(-1)
                        }}> 返回</a>
                    </Button>
                </div>
                <Container className='container-main'>

                    <div className='container-table'>
                        <Table dataSource={mockData} primaryKey="id" className={styles.table}
                               rowSelection={{
                                   onChange: this.onChange,
                                   getProps: (record, index) => {
                                       return {
                                           children: ''
                                       };
                                   },
                                   columnProps: () => {
                                       return {
                                           lock: 'left',
                                           width: 90,
                                           align: 'center'
                                       };
                                   },
                                   titleAddons: () => {
                                       return;
                                   },
                                   titleProps: () => {
                                       return {
                                           children: ''
                                       };
                                   }
                               }}>
                            <Table.Column align="center" title="序号" width={50} cell={
                                (value, index, record) => {
                                    return index + 1 + (this.pageNum - 1) * this.pageSize;
                                }
                            }/>
                            <Table.Column align="center" title="任务状态" width={100} dataIndex="statusName"/>
                            <Table.Column align="center" title="创建时间" dataIndex="createTime"/>
                            <Table.Column align="center" title="开始执行时间" dataIndex="startDateTime"/>
                            <Table.Column align="center" title="结束执行时间" dataIndex="endDateTime"/>
                            <Table.Column align="center" width={230} title="操作" cell={
                                (value, index, record) => {
                                    if (record.execId){
                                        return (
                                            <div>
                                                <Button type="normal" size="small"
                                                        onClick={() => this.executeBusinessIntelligenceTaskById(record)}
                                                        text className='pipe-btn-edit'>
                                                    执行
                                                </Button>
                                                <Button type="normal" size="small"
                                                        text className='pipe-btn-edit'>
                                                    <Link to={"log?taskPlanId=" + record.id}>执行日志</Link>
                                                </Button>
                                                <Button type="normal" size="small"
                                                        onClick={() => this.cancelExecuteTaskById(record)}
                                                        text className='pipe-btn-edit'>
                                                    取消执行
                                                </Button>
                                            </div>
                                        )
                                    }else {
                                        return (
                                            <div>
                                                <Button type="normal" size="small"
                                                        onClick={() => this.executeBusinessIntelligenceTaskById(record)}
                                                        text className='pipe-btn-edit'>
                                                    执行
                                                </Button>
                                                <Button type="normal" size="small"
                                                        onClick={() => this.cancelExecuteTaskById(record)}
                                                        text className='pipe-btn-edit'>
                                                    取消执行
                                                </Button>
                                            </div>
                                        )
                                    }

                                }
                            }/>
                        </Table>
                        <ZgDialog config={this.state.config} cancelCall={this.cancelCall} okCall={this.okCall}/>
                    </div>
                    {/**
                     * 分页
                     * defaultCurrent 初始页码
                     * total 总记录数
                     * pageSize 一页中的记录数
                     */}
                    <Pagination defaultCurrent={1}
                                pageSize={this.pageSize}
                                total={this.totalNum}
                                onChange={this.onChangePage}
                                onPageSizeChange={this.onPageSizeChange}
                                pageSizeList={[5,10,20,50,100,200]}
                                pageSizeSelector="dropdown"
                                className="page-demo"
                                size="small"
                    />
                </Container>
            </div>
        )
            ;
    }

}

export default BusinessIntelligenceTaskHistoryList;

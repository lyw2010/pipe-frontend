/**
 * @description: 数据中台-任务-任务管理
 * @author: hj
 * @update: hj(2020-01-14)
 */
import React from 'react';
import Container from '@icedesign/container';
import event from '@/request'
import {Button, Message} from '@alifd/next';
import $http from '@/service/Services';
import styles from './index.module.scss';


/**
 * list
 */
class BusinessIntelligenceTaskHistoryLog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
        };
        this.$http = $http;
        this.fetchExecutionJobLogs = this.fetchExecutionJobLogs.bind(this);
    }

    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        const params = new URLSearchParams(this.props.location.search);
        const id = params.get("taskPlanId");
        this.setState({
            id: id
        }, () => {
            this.fetchExecutionJobLogs();
        });
    };


    /**
     * 获取数据
     */
    fetchExecutionJobLogs = () => {
        const _this = this;
        let url = event.event.url + '/v1/taskDetailPlan/fetchExecutionJobLogs/' + this.state.id;
        this.$http.get(url)
            .then(function (response) {
                const {data} = response;
                _this.setState({
                    message: data.message
                })
            })
            .catch(function (error) {
                Message.error(error.message);
            })

    };


    render() {
        const {message} = this.state;
        return (
            <div>
                <div className='container-header'>
                    <p>任务执行日志</p>
                    <Button type="normal" size="small" className='next-btn-normal table-btn-return'
                            style={{float: 'right'}}>
                        <a onClick={() => {
                            this.props.history.go(-1)
                        }}> 返回</a>
                    </Button>
                </div>
                <Container className='container-main'>
                    <div className='container-table'>
                        <div className={styles.panelBody}>
                            <pre className={styles.log}>
                                {message}
                            </pre>
                        </div>

                    </div>
                </Container>
            </div>
        )
            ;
    }

}

export default BusinessIntelligenceTaskHistoryLog;

/**
 * @description: 数据中台-数据源-数据源管理
 * @author: hj
 * @update: hj(2020-01-14)
 */
import React from 'react';
import Container from '@icedesign/container';
import styles from './index.module.scss';
import event from '@/request'
import {Button, Card, Dialog, Input, Message, Pagination, Table, Upload, Badge, Select} from "@alifd/next";
import ZgDialog from '@/components/ZgDialog';
import axios from 'axios';
import $http from '@/service/Services';
import AuthButton from "../../../../components/AuthComponent/AuthBotton";


/**
 * list
 */
class FileMetadataList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            current: 0,
            mockData: [],
            selectedList: [],
            uploadVisible: false,
            errorMsg: [],
            dataSources: [],
            name: "",
            datasourceId: "",
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            },
            loading: false
        };
        this.pageNum = 1; // 当前页数
        this.pageSize = 10; // 分页每页显示数据条数
        this.totalNum = 0; // 数据总条数
        this.$http = $http;
        this.onPageChange = this.onPageChange.bind(this);
        this.getFileMetadataList = this.getFileMetadataList.bind(this);
    }

    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        setTimeout(()=>{
            this.getAllDatasourceList();
        },0);
        setTimeout(()=>{
            this.getFileMetadataList();
        }, 0)
    };

    /**
     * 获取所有数据源列表
     */
    getAllDatasourceList = () => {
        const that = this;
        this.$http.get(event.event.url + '/v1/tDataSource/getAllDatasourceList').then(function (response) {
            const {data} = response;
            if (data.code === 1) {
                Message.warning(data.message ? data.message : data.data);
            } else {
                that.setState({
                    dataSources: data.data
                })
            }
        })
            .catch(function (error) {
                Message.error(error.message);
            });
    };
    /**
     * 条件
     * @param e
     * @param value
     */
    searchCondition = (value, e) => {
        this.setState({
            name: value,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        });
        this.pageNum = 1;
    };
    /**
     * 条件
     * @param e
     * @param value
     */
    changeBusiness = (value, e) => {
        this.setState({
            datasourceId: value,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        });
        this.pageNum = 1;
    };
    /**
     * 改变页面
     * @param currentPage
     */
    onPageChange = (currentPage) => {
        this.state = {
            current: currentPage,
            mockData: this.props.mockData,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        };
        this.getFileMetadataList();
    };

    /**
     * 分页大小改变执行
     * @param pageSize
     */
    onPageSizeChange = (pageSize) => {
        this.pageSize = pageSize;
        this.pageNum = 1;
        this.getFileMetadataList();
    };
    /**
     * 修改分页数据
     * @param currentPage
     */
    onChangePage = (currentPage) => {
        this.setState({
            loading: true,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        });
        setTimeout(() => {
            this.getFileMetadataList(currentPage);
            this.setState({
                loading: false,
                config: {
                    data: null,
                    visible: false,
                    title: "",
                    content: ""
                }
            });
        }, 0);
    };
    /**
     * 获取数据
     */
    getFileMetadataList = (pageNum) => {
        this.setState({
            loading: true
        });
        const _this = this;
        _this.pageNum = typeof (pageNum) == 'number' ? pageNum : _this.pageNum;
        let urls = event.event.url + '/v1/filePlatformMetadata/getFilePlatformMetadataPage';
        this.$http.post(urls, {
            pageSize:_this.pageSize,
            pageNum: _this.pageNum,
            condition: {
                datasourceId:_this.state.datasourceId,
                name: _this.state.name
            }
        })
            .then(function (response) {
                const {data} = response;
                _this.totalNum = data.data.total;
                _this.setState({
                    mockData: data.data.voList,
                    current: data.data.current,
                    loading: false,
                    config: {
                        data: null,
                        visible: false,
                        title: "",
                        content: ""
                    }
                });
            })
            .catch(function (error) {
                Message.error(error.message);
                _this.setState({
                    loading: false,
                });
            })

    };



    /**
     *
     * @param args
     */
    onChange = (...args) => {
        this.setState({
            selectedList: args
        });
    };


    /**
     * 确定提示弹框
     */
    onOkDialog = () => {
        this.setState({
            visible: false
        });
        const {selectedList} = this.state;
        this.handleSelect(selectedList[1].map(o => {
            return o.id;
        }));
    };
    /**
     * 取消提示弹框
     */
    onCloseDialog = reason => {
        this.setState({
            visible: false
        });
    };


    render() {
        const {mockData} = this.state;
        return (
            <div>
                <div className='container-header'>
                    <p>文件元数据管理</p>
                </div>
                <Container className='container-main'>
                    <div className='container-btnBox' style={{marginTop: '0'}}>
                        <div className={styles.buttons}>
                            <AuthButton text auth={"DATA_CENTER$_DATASOURCE$_DETAIL"} type="normal"
                                        size="small"
                                        link={{
                                            to: "file-metadata-edit?id=",
                                            text: "新建元数据"
                                        }}/>
                            <span className={styles.caseNumber}>
                                <span>
                                     数据源:
                                </span>
                                <Select name={'businessId'}
                                        placeholder="请选择"
                                        className={`${styles.input} ${styles.shortInput} ${styles.select}`}
                                        onChange={this.changeBusiness}
                                >
                                    {
                                        this.state.dataSources.map(
                                            o => {
                                                return (<Select.Option value={o.id}>{o.name}</Select.Option>)
                                            }
                                        )
                                    }
                                </Select>
                                <span>
                                     元数据名称:
                                </span>
                                <Input onChange={this.searchCondition.bind(this)} placeholder={"元数据名称"}
                                       className={`${styles.input} ${styles.shortInput}`}/>
                                <Button
                                    type="primary"
                                    className='pipe-btn-search'
                                    onClick={this.getFileMetadataList}
                                >
                                查询
                                </Button>
                            </span>
                        </div>
                    </div>
                    <div className='container-table'>
                        <Table dataSource={mockData} loading={this.state.loading} lock={"left"} size={'small'}
                               primaryKey="id" className={styles.table}>
                            <Table.Column align="center" title="序号" width={50} cell={
                                (value, index, record) => {
                                    return index + 1 + (this.pageNum - 1) * this.pageSize;
                                }
                            }/>
                            <Table.Column width={240} align="center" title="数据源" dataIndex="datasourceName"/>
                            <Table.Column width={240} align="center" title="中文名称" dataIndex="chineseName"/>
                            <Table.Column width={240} align="center" title="数据表名称" dataIndex="name"/>
                            <Table.Column width={120} align="center" title="创建时间" dataIndex="createTime"/>
                            <Table.Column width={240} align="center" title="备注" dataIndex="remarks"/>
                            <Table.Column align="center" width={80}  lock={"right"} title="操作" cell={
                                (value, index, record) => {
                                    return (
                                        <div>
                                            <AuthButton text auth={"DATA_CENTER$_DATASOURCE$_DETAIL"} type="normal"
                                                        size="small"
                                                        link={{
                                                            to: "file-metadata-edit?id=" + record.id,
                                                            text: "修改"
                                                        }}/>
                                        </div>
                                    )
                                }
                            }/>
                        </Table>

                    </div>
                    {/**
                     * 分页
                     * defaultCurrent 初始页码
                     * total 总记录数
                     * pageSize 一页中的记录数
                     */}
                    <Pagination defaultCurrent={1}
                                pageSize={this.pageSize}
                                total={this.totalNum}
                                onChange={this.onChangePage}
                                className="page-demo"
                                onPageSizeChange={this.onPageSizeChange}
                                pageSizeList={[5, 10, 20, 50, 100, 200]}
                                size="small"
                                pageSizeSelector="dropdown"
                    />
                </Container>
            </div>
        )
            ;
    }

}

export default FileMetadataList;

import React from 'react';
import {
    Badge,
    Input,
    Button,
    Drawer,
    Form,
    Grid,
    Input as NextInput,
    Message,
    Pagination,
    Select,
    Tab,
    Table,
    Icon
} from '@alifd/next';
import $http from '@/service/Services';
import event from '@/request'
import styles from './index.module.scss';
import AuthButton from "../../../../components/AuthComponent/AuthBotton";
import Container from "@icedesign/container";

const FormItem = Form.Item;
const {Row, Col} = Grid;


const formItemLayout = {
    labelCol: {
        span: 6
    },
    wrapperCol: {
        span: 14
    }
};
const layout = {
    labelCol: {
        span: 4
    },
    wrapperCol: {
        span: 20
    },
}

/**
 * list
 */
class MetadataDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: "",
            metadata: {},
            tags: [],
            tableDetailColumnInfo: [],
            tableOwnership: [],
            tagsDrawerValue: {},
            metadataTypeList: [
                {
                    value: "CORP_USER",
                    label: "用户"
                },
                {
                    value: "CORP_GROUP",
                    label: "组织机构"
                },
            ],
            roleList: [
                {
                    value: "DEVELOP",
                    label: "开发人员"
                },
                {
                    value: "DATA_OWNER",
                    label: "所有人"
                },
                {
                    value: "DELEGATE",
                    label: "代表方"
                }, {
                    value: "PRODUCER",
                    label: "生产商"
                }, {
                    value: "CONSUMER",
                    label: "消费者"
                }, {
                    value: "STAKEHOLDER",
                    label: "参与者"
                }

            ],
            contextOver: false,
            contextEdit: false,
            context: "",
            ownerShipDrawerShow: false,
            ownerShipAdd: false,
            ownerShipDrawerValue: {
                id: "",
                metadataId: "",
                name: "",
                metadataRole: "",
                metadataType: ""
            },
            columnTagsDrawerShow: false,
            columnTagsAdd: false,
            columnTagsDrawerValue: {
                metaDataId: "",
                id: "",
                tag: ""
            },
            metadataHistoryId:"",
            metadataVersionList:[],
            metadataVersionIndex:0,
            viewHistory: false,
        };
        this.tableDetailColumnInfoPageNum = 1; // 当前页数
        this.tableDetailColumnInfoPageSize = 10; // 分页每页显示数据条数

        this.tableOwnershipPageNum = 1; // 当前页数
        this.tableOwnershipPageSize = 10; // 分页每页显示数据条数
        this.$http = $http;
    }


    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        const params = new URLSearchParams(this.props.location.location ? this.props.location.location.search : this.props.location.search);
        const id = params.get("id");
        this.setState({
            id
        }, () => {
            this.getPlatformMetaDataById();
            this.getMetadataTags();
            this.getTableOwnershipInfo();
            this.getTableDetailColumnInfo();
            this.getMetadataHistoryByMetadataId();
        });
    };

    /**
     * 元数据的历史管理
     */
    getMetadataHistoryByMetadataId(){
        const _this = this;
        let urls = event.event.url + '/v1/metadataHistory/getMetadataHistoryByMetadataId';
        this.$http.post(urls,{
            metadataId:this.state.id
        })
            .then(function (response) {
                const {data} = response;
                _this.setState({
                    metadataVersionList:data.data
                })
            })
            .catch(function (error) {
                Message.error(error.message);
            })
    }

    /**
     * 获取数据
     */
    getTableDetailColumnHistoryInfo = (tableDetailColumnInfoPageNum) => {
        if (!this.state.viewHistory){
            this.getTableDetailColumnInfo();
            return;
        }
        if (this.state.metadataHistoryId){
            const _this = this;
            _this.tableDetailColumnInfoPageNum = typeof (tableDetailColumnInfoPageNum) == "number" ? tableDetailColumnInfoPageNum : _this.tableDetailColumnInfoPageNum;
            let urls = event.event.url + '/v1/metadataHistory/getMetadataColumns/' + this.state.metadataHistoryId + "/" + _this.tableDetailColumnInfoPageSize + '/' + _this.tableDetailColumnInfoPageNum;
            this.$http.get(urls)
                .then(function (response) {
                    const {data} = response;
                    _this.tableDetailColumnTotalNum = data.data.total;
                    _this.setState({
                        tableDetailColumnInfo: data.data.voList.map(o => {
                            o.addTag = false;
                            return o;
                        }),
                        current: data.data.current,
                        config: {
                            data: null,
                            visible: false,
                            title: "",
                            content: ""
                        }
                    }, () => {
                    });
                })
                .catch(function (error) {
                    Message.error(error.message);
                })
        }
    };
    /**
     * 获取数据
     */
    getPlatformMetaDataById = () => {
        const _this = this;
        let urls = event.event.url + '/v1/metadata/getPlatformMetaDataById/' + this.state.id;
        this.$http.get(urls)
            .then(function (response) {
                const {data} = response;
                _this.setState({
                    metadata: data.data,
                    context: data.data.remarks
                })
            })
            .catch(function (error) {
                Message.error(error.message);
            })

    };
    /********************************** 数据表表的字段信息 ***********************************/

    /**
     * 改变页面
     * @param currentPage
     */
    onPageChange = (currentPage) => {
        this.state = {
            current: currentPage,
            tableDetailColumnInfo: this.props.mockData,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        };
        this.getTableDetailColumnInfo();
    };

    /**
     * 数据大小
     * @param pageSize
     */
    onTableDetailColumnInfoPageChange = (pageSize) => {
        this.tableDetailColumnInfoPageSize = pageSize;
        this.getTableDetailColumnInfo();
    };
    /**
     * 修改分页数据
     * @param currentPage
     */
    onChangePage = (currentPage) => {
        this.setState({
            loading: true,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            }
        });
        setTimeout(() => {
            this.getTableDetailColumnInfo(currentPage);
            this.setState({
                loading: false,
                config: {
                    data: null,
                    visible: false,
                    title: "",
                    content: ""
                }
            });
        }, 0);
    };
    /**
     * 获取数据
     */
    getTableDetailColumnInfo = (tableDetailColumnInfoPageNum) => {
        const _this = this;
        _this.tableDetailColumnInfoPageNum = typeof (tableDetailColumnInfoPageNum) == "number" ? tableDetailColumnInfoPageNum : _this.tableDetailColumnInfoPageNum;
        let urls = event.event.url + '/v1/metadata/getMetadataColumns/' + this.state.id + "/" + _this.tableDetailColumnInfoPageSize + '/' + _this.tableDetailColumnInfoPageNum;
        this.$http.get(urls, {
            params: {
                subjectId: this.state.subjectId
            }
        })
            .then(function (response) {
                const {data} = response;
                _this.tableDetailColumnTotalNum = data.data.total;
                _this.setState({
                    tableDetailColumnInfo: data.data.voList.map(o => {
                        o.addTag = false;
                        return o;
                    }),
                    current: data.data.current,
                    config: {
                        data: null,
                        visible: false,
                        title: "",
                        content: ""
                    }
                }, () => {
                });
            })
            .catch(function (error) {
                Message.error(error.message);
            })

    };

    /**
     * 获取数据
     */
    getMetadataTags = () => {
        const _this = this;
        let urls = event.event.url + '/v1/metadataTags/getMetadataTagsByMetaDataId/' + this.state.id;
        this.$http.get(urls)
            .then(function (response) {
                const {data} = response;
                _this.setState({
                    tags: data.data
                });
            })
            .catch(function (error) {
                Message.error(error.message);
            })

    };
    /********************************** 数据表的字段信息***********************************/
    /********************************** 数据表的所有权信息***********************************/

    /**
     * 页面大小更更改
     * @param pageSize
     */
    onOwnershipPageSizeChange = (pageSize) => {
        this.tableDetailColumnInfoPageSize = pageSize;
        this.getTableOwnershipInfo();
    };
    /**
     * 修改分页数据
     * @param currentPage
     */
    onOwnershipChangePage = (currentPage) => {
        this.setState({
            loading: true
        });
        setTimeout(() => {
            this.getTableOwnershipInfo(currentPage);
            this.setState({
                loading: false
            });
        }, 0);
    };
    /**
     * 获取数据
     */
    getTableOwnershipInfo = (tableDetailColumnInfoPageNum) => {
        const _this = this;
        _this.tableDetailColumnInfoPageNum = typeof (tableDetailColumnInfoPageNum) == "number" ? tableDetailColumnInfoPageNum : _this.tableDetailColumnInfoPageNum;
        let urls = event.event.url + '/v1/metadataOwnerShip/getPlatformMetadataOwnerShip/' + this.state.id + "/" + _this.tableDetailColumnInfoPageSize + '/' + _this.tableDetailColumnInfoPageNum;
        this.$http.get(urls, {
            params: {
                subjectId: this.state.subjectId
            }
        })
            .then(function (response) {
                const {data} = response;
                _this.tableDetailColumnTotalNum = data.data.total;
                _this.setState({
                    tableOwnership: data.data.voList
                });
            })
            .catch(function (error) {
                Message.error(error.message);
            })

    };
    /**
     * 添加或者是修改
     * @param item
     * @param add
     */
    handleAddOwnerShipClick = (item, add) => {
        this.setState({
            ownerShipDrawerShow: true,
            ownerShipAdd: add,
            ownerShipDrawerValue: {
                id: add ? "" : item.id,
                metadataId: add ? this.state.id : item.metadataId,
                name: add ? "" : item.name,
                metadataRole: add ? "" : item.metadataRole,
                metadataType: add ? "" : item.metadataType
            },
        })
    };

    /**
     * 抽屉关闭调用
     */
    onCloseOwnerShip = () => {
        this.setState({
            ownerShipDrawerShow: false,
            ownerShipAdd: false,
            ownerShipDrawerValue: {
                id: "",
                metadataId: "",
                name: "",
                metadataRole: "",
                metadataType: ""
            },
        });
    };

    /**
     * 提交文件夹的相关数据
     * @param value 表单的值
     * @param e 2021年6月9日00:45:35
     */
    submitOwnerShip = (value, e) => {
        if (e == null) {
            const that = this;
            const item = {
                ...value,
            };
            let url = event.event.url + '/v1/metadataOwnerShip/saveOrUpdatePlatformMetadataOwnerShip';
            this.$http.post(url, item)
                .then(function (response) {
                    that.onCloseOwnerShip();
                    that.getTableOwnershipInfo();
                })
                .catch(function (error) {
                    Message.error(error.message);
                })
        }
    };

    /**
     * 删除
     * @param record
     */
    deleteOwnerShip = (record) => {
        const that = this;
        let url = event.event.url + '/v1/metadataOwnerShip/removePlatformMetadataOwnerShipById';
        this.$http.post(url, {
            ids: [record.id],
        })
            .then(function (response) {
                that.onCloseOwnerShip();
                that.getTableOwnershipInfo();
            })
            .catch(function (error) {
                Message.error(error.message);
            })
    };

    /********************************** 数据表的所有权信息***********************************/
    /**********************************  tag **********************************************/

    /**
     * 添加目录
     */
    addTags = (item) => {
        this.editTags(item, true);
    };

    /**
     * 修改目录
     */
    editTags = (item, add) => {
        this.setState({
            tagsDrawerShow: true,
            tagsAdd: add,
            tagsDrawerValue: {
                id: add ? "" : item.id,
                tag: add ? "" : item.tag
            },
        })
    };


    /**
     * 抽屉关闭调用
     */
    onCloseTags = () => {
        this.setState({
            tagsDrawerShow: false,
            tagsAdd: false,
            tagsDrawerValue: {
                id: "",
                tag: "",
            }
        });
    };

    /**
     * 提交文件夹的相关数据
     * @param value 表单的值
     * @param e 2021年6月9日00:45:35
     */
    submitTags = (value, e) => {
        if (e == null) {
            const that = this;
            const item = {
                ...value,
                metaDataId: this.state.id
            };
            let url = event.event.url + '/v1/metadataTags/addMetadataTags';
            this.$http.post(url, item)
                .then(function (response) {
                    that.onCloseTags();
                    that.getMetadataTags();
                })
                .catch(function (error) {
                    Message.error(error.message);
                })
        }
    };
    /**
     * 删除
     * @param tag
     */
    deleteTag = (tag) => {
        const that = this;
        let url = event.event.url + '/v1/metadataTags/removeMetadataTags';
        this.$http.post(url, {
            id: tag.id,
        })
            .then(function (response) {
                that.onCloseTags();
                that.getMetadataTags();
            })
            .catch(function (error) {
                Message.error(error.message);
            })
    };

    onCloseDepart = () => {
        this.onCloseTags();
    };
    /**********************************  tag **********************************************/
    /**********************************  column tag **********************************************/
    /**
     * 抽屉关闭调用
     */
    onColumnCloseTags = () => {
        this.setState({
            columnTagsDrawerShow: false,
            columnTagsAdd: false,
            columnTagsDrawerValue: {
                onColumnCloseDepart: "",
                id: "",
                tag: "",
            }
        });
    };
    /**
     * 抽屉关闭调用
     */
    onColumnCloseDepart = () => {
        this.setState({
            columnTagsDrawerShow: false,
            columnTagsAdd: false,
            columnTagsDrawerValue: {
                onColumnCloseDepart: "",
                id: "",
                tag: "",
            }
        });
    };
    addRecordTag = (record, item, add) => {
        this.setState({
            record: record,
            columnTagsDrawerShow: true,
            columnTagsAdd: add,
            columnTagsDrawerValue: {
                metaDataColumnId: record.id,
                id: add ? "" : item.id,
                tag: add ? "" : item.tag
            },
        })
    };
    /**
     * 提交文件夹的相关数据
     * @param value 表单的值
     * @param e 2021年6月9日00:45:35
     */
    submitColumnTags = (value, e) => {
        if (e == null) {
            const that = this;
            const item = {
                ...value
            };
            let url = event.event.url + '/v1/metadataTags/addMetadataColumnTags';
            this.$http.post(url, item)
                .then(function (response) {
                    that.onColumnCloseTags();
                    that.getTableDetailColumnInfo();
                })
                .catch(function (error) {
                    Message.error(error.message);
                })
        }
    };
    /**
     * 删除
     * @param tag
     */
    deleteColumnTag = (tag) => {
        const that = this;
        let url = event.event.url + '/v1/metadataTags/removeMetadataColumnTagsById';
        this.$http.post(url, {
            id: tag.id,
        })
            .then(function (response) {
                that.onColumnCloseTags();
                that.getTableDetailColumnInfo();
            })
            .catch(function (error) {
                Message.error(error.message);
            })
    };

    /**********************************  column tag **********************************************/

    /**
     * 修改值
     * @param value 值
     * @param event 标签
     */
    changeContext = (value, event) => {
        this.setState({
            context: value
        })
    };
    /**
     *
     */
    editMetadataContext = () => {
        const {context, id} = this.state;
        const that = this;
        const url = event.event.url + "/v1/metadata/saveMetadataRemarks";
        this.$http.post(url, {
            id: id,
            remarks: context
        })
            .then(function (response) {
                that.setState({
                    contextEdit: false
                })
            })
            .catch(function (error) {
                Message.error(error.message);
            })

    };

    /**
     * 表
     * @returns {*}
     */
    render() {
        const {tableDetailColumnInfo, tableOwnership, metadata} = this.state;
        return (
            <div className="container-main">
                <Drawer
                    title="添加标签"
                    closeMode={['close', 'esc', 'mask']}
                    visible={this.state.tagsDrawerShow}
                    width={550}
                    onClose={this.onCloseDepart}
                >
                    <Form {...formItemLayout}>
                        <FormItem
                            hidden={true}
                        >
                            <NextInput
                                id="id"
                                name="id"
                                defaultValue={this.state.tagsDrawerValue.id}
                                aria-required="true"/>
                        </FormItem>


                        <FormItem
                            required
                            requiredMessage={"名称不能为空"}
                            label="名称："
                        >
                            <NextInput
                                id="tag"
                                name="tag"
                                defaultValue={this.state.tagsDrawerValue.tag}
                                aria-required="true"/>
                        </FormItem>
                        <FormItem wrapperCol={{offset: 6}}>
                            <Form.Submit
                                validate
                                type="primary"
                                onClick={(v, e) => this.submitTags(v, e)}
                                style={{marginRight: 10}}
                            >
                                提交
                            </Form.Submit>
                            <Form.Reset>重置</Form.Reset>
                        </FormItem>
                    </Form>
                </Drawer>

                <Drawer
                    title="添加数据列标签"
                    closeMode={['close', 'esc', 'mask']}
                    visible={this.state.columnTagsDrawerShow}
                    width={550}
                    onClose={this.onColumnCloseDepart}
                >
                    <Form {...formItemLayout}>
                        <FormItem
                            hidden={true}
                        >
                            <NextInput
                                id="metaDataColumnId"
                                name="metaDataColumnId"
                                defaultValue={this.state.columnTagsDrawerValue.metaDataColumnId}
                                aria-required="true"/>
                        </FormItem>
                        <FormItem
                            hidden={true}
                        >
                            <NextInput
                                id="id"
                                name="id"
                                defaultValue={this.state.columnTagsDrawerValue.id}
                                aria-required="true"/>
                        </FormItem>

                        <FormItem
                            required
                            requiredMessage={"名称不能为空"}
                            label="名称："
                        >
                            <NextInput
                                id="tag"
                                name="tag"
                                defaultValue={this.state.columnTagsDrawerValue.tag}
                                aria-required="true"/>
                        </FormItem>
                        <FormItem wrapperCol={{offset: 6}}>
                            <Form.Submit
                                validate
                                type="primary"
                                onClick={(v, e) => this.submitColumnTags(v, e)}
                                style={{marginRight: 10}}
                            >
                                提交
                            </Form.Submit>
                            <Form.Reset>重置</Form.Reset>
                        </FormItem>
                    </Form>
                </Drawer>

                <Drawer
                    title={this.state.ownerShipDrawerValue.id ? "添加关系" : "修改关系"}
                    closeMode={['close', 'esc', 'mask']}
                    visible={this.state.ownerShipDrawerShow}
                    width={550}
                    placement={this.state.placement}
                    onClose={this.onCloseOwnerShip}
                >
                    <Form {...formItemLayout}>

                        <FormItem
                            hidden={true}
                        >
                            <NextInput
                                id="metadataId"
                                name="metadataId"
                                defaultValue={this.state.ownerShipDrawerValue.metadataId}
                                aria-required="true"/>
                        </FormItem>
                        <FormItem
                            hidden={true}
                        >
                            <NextInput
                                id="id"
                                name="id"
                                defaultValue={this.state.ownerShipDrawerValue.id}
                                aria-required="true"/>
                        </FormItem>


                        <FormItem

                            required
                            requiredMessage={"名称不能为空"}
                            label="名称："
                        >
                            <NextInput
                                id="name"
                                name="name"
                                defaultValue={this.state.ownerShipDrawerValue.name}
                                aria-required="true"/>
                        </FormItem>


                        <FormItem
                            label="角色："
                            required
                            requiredMessage={"请选择角色"}
                        >
                            <Select style={{width: 200}} defaultValue={this.state.ownerShipDrawerValue.metadataRole}
                                    dataSource={this.state.roleList} placeholder="请选择角色"
                                    id="metadataRole"
                                    name="metadataRole">
                            </Select>
                        </FormItem>


                        <FormItem
                            label="类型："
                            required
                            requiredMessage={"请选择类型"}
                        >
                            <Select style={{width: 200}} defaultValue={this.state.ownerShipDrawerValue.metadataType}
                                    dataSource={this.state.metadataTypeList}
                                    placeholder="请选择类型"
                                    id="metadataType"
                                    name="metadataType">
                            </Select>
                        </FormItem>

                        <FormItem wrapperCol={{offset: 6}}>
                            <Form.Submit
                                validate
                                type="primary"
                                onClick={(v, e) => this.submitOwnerShip(v, e)}
                                style={{marginRight: 10}}
                            >
                                提交
                            </Form.Submit>
                            <Form.Reset>重置</Form.Reset>
                        </FormItem>
                    </Form>
                </Drawer>

                <div className='container-header'>
                    <p>元数据管理</p>
                    <Button type="normal" size="small" className='next-btn-normal table-btn-return'
                            style={{float: 'right'}}>
                        <a onClick={() => {
                            this.props.history.go(-1)
                        }}> 返回</a>
                    </Button>
                </div>
                <main className={styles.antLayoutContent}>
                    <div className={styles.antRow} style={{rowGap: '0px'}}>
                        <Row gutter={24} style={{marginBottom: 15}}>
                            <Col span={16}>
                                <div style={{paddingRight: '24px', 'flex': 'auto', boxSizing: 'border-box'}}>
                                    <div style={{padding: '20px 0px 20px', rowGap: '0px'}}>
                                        <h1>{metadata.orgCode + "."+metadata.schemas + "." + metadata.name}</h1>
                                    </div>
                                    <div style={{gap: '16px', flexDirection: 'column'}}>
                                        <div className={styles.antLayout}>
                                            <div className={styles.hyACbC}>
                                                <div className={styles.illmTA}>
                                                    <div>
                                                        <span style={{color: 'rgba(0,0,0,.45)', frontSize: '11px'}}>
                                                            <strong>
                                                                平台
                                                            </strong>
                                                        </span>
                                                    </div>
                                                    <div className={styles.antTypography}>
                                                        {metadata.schemas}
                                                    </div>
                                                </div>
                                                <div className={styles.illmTA}>
                                                    <div>
                                                        <span style={{color: 'rgba(0,0,0,.45)', frontSize: '11px'}}>
                                                            <strong>
                                                                每月查询次数
                                                            </strong>
                                                        </span>
                                                    </div>
                                                    <div className={styles.antTypography}>
                                                        378
                                                    </div>
                                                </div>
                                                <div className={styles.illmTA}>
                                                    <div>
                                                        <span style={{color: 'rgba(0,0,0,.45)', frontSize: '11px'}}>
                                                            <strong>
                                                                用户
                                                            </strong>
                                                        </span>
                                                    </div>
                                                    <div>
                                                        <div className={styles.antAvatarGroup}>
                                                            <span className={styles.antAvatar}>
                                                                <span className={styles.antAvatarString}>
                                                                    Z
                                                                </span>
                                                            </span>
                                                            <span className={styles.antAvatar}>
                                                                <span className={styles.antAvatarString}>
                                                                    L
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div style={{
                                    display: "block",
                                    overflowWrap: "break-word",
                                    maxWidth: "100%",
                                    height: "auto"
                                }}
                                     onMouseOut={() => {
                                         this.setState({
                                             contextOver: false
                                         })
                                     }}
                                     onMouseOver={() => {
                                         this.setState({
                                             contextOver: true
                                         })
                                     }}>
                                    <p style={{
                                        display: "block",
                                        fontSize: "16px",
                                        lineHeight: "1.5",
                                        marginBlockStart: "1em",
                                        marginBlockEnd: "1em",
                                        marginInlineStart: "0px",
                                        marginInlineEnd: "0px",
                                    }}>
                                        {
                                            [0].map(x => {
                                                if (!this.state.contextEdit) {
                                                    return (
                                                        <div>
                                                            {this.state.context}
                                                            <img width={15}
                                                                 onClick={() => {
                                                                     this.setState({
                                                                         contextEdit: true
                                                                     })
                                                                 }}
                                                                 alt={"编辑"}
                                                                 height={15}
                                                                 style={{
                                                                     marginRight: "10px",
                                                                     visibility: this.state.contextOver ? 'visible' : 'hidden',
                                                                     cursor: this.state.contextOver ? 'pointer' : '',
                                                                 }}
                                                                 src={require("./images/edit.png")}/>
                                                        </div>
                                                    )
                                                } else {
                                                    return (
                                                        <Input id={"context"}
                                                               value={this.state.context}
                                                               defaultValue={this.state.context}
                                                               onChange={this.changeContext}
                                                               placeholder="请输入参数值"
                                                               onBlur={this.editMetadataContext}
                                                               style={{
                                                                   width: 400
                                                               }}
                                                        />
                                                    )
                                                }
                                            })
                                        }
                                    </p>
                                </div>
                            </Col>
                            <Col span={8}>
                                <div className={styles.OJJCp}>
                                    <div className={styles.antCardBody}>
                                        <h4 className={styles.antTypography}>
                                            <span className={styles.anticon}>
                                                <svg viewBox="64 64 896 896" focusable="false" data-icon="tag"
                                                     width="1em" height="1em" fill="currentColor" aria-hidden="true"><path
                                                    d="M938 458.8l-29.6-312.6c-1.5-16.2-14.4-29-30.6-30.6L565.2 86h-.4c-3.2 0-5.7 1-7.6 2.9L88.9 557.2a9.96 9.96 0 000 14.1l363.8 363.8c1.9 1.9 4.4 2.9 7.1 2.9s5.2-1 7.1-2.9l468.3-468.3c2-2.1 3-5 2.8-8zM459.7 834.7L189.3 564.3 589 164.6 836 188l23.4 247-399.7 399.7zM680 256c-48.5 0-88 39.5-88 88s39.5 88 88 88 88-39.5 88-88-39.5-88-88-88zm0 120c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32z"></path></svg>
                                            </span>
                                            Tags
                                        </h4>
                                        <div>
                                            {
                                                Array.isArray(this.state.tags)
                                                && this.state.tags.length > 0
                                                && this.state.tags.map(
                                                    o => {
                                                        return (
                                                            <a className={styles.tagsLink}>
                                                                <span className={styles.tags}>{o.tag}
                                                                    <span role="img" aria-label="close" tabIndex="-1"
                                                                          className={styles.antTagCloseIcon}>
                                                                   <a onClick={() => this.deleteTag(o)}>
                                                                        <svg
                                                                            viewBox="64 64 896 896" focusable="false"
                                                                            data-icon="close"
                                                                            width="1em" height="1em"
                                                                            fill="currentColor"
                                                                            aria-hidden="true">
                                                                        <path
                                                                            d="M563.8 512l262.5-312.9c4.4-5.2.7-13.1-6.1-13.1h-79.8c-4.7 0-9.2 2.1-12.3 5.7L511.6 449.8 295.1 191.7c-3-3.6-7.5-5.7-12.3-5.7H203c-6.8 0-10.5 7.9-6.1 13.1L459.4 512 196.9 824.9A7.95 7.95 0 00203 838h79.8c4.7 0 9.2-2.1 12.3-5.7l216.5-258.1 216.5 258.1c3 3.6 7.5 5.7 12.3 5.7h79.8c6.8 0 10.5-7.9 6.1-13.1L563.8 512z">
                                                                        </path>
                                                                    </svg>
                                                                   </a>
                                                                </span>
                                                                </span>
                                                            </a>
                                                        )
                                                    }
                                                )
                                            }
                                            <span className={styles.antTagSuccess}
                                                  onClick={() => this.addTags({}, true)}>
                                                + 添加标签
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </div>
                    <div className={styles.divider}></div>
                    <div className={styles.antRow} style={{rowGap: '0px', padding: '0px 0px 10px'}}>
                        <Tab disableKeyboard>
                            <Tab.Item title="表结构" style={{fontSize: '16px', padding: '16px 0'}} key="1">
                                <Row gutter={24} style={{marginBottom: 15}} justify={'start'}>
                                    <Col span={18}>
                                        <div style={{marginRight: 20, marginTop: 20,justifyContent: 'flex-end',boxSizing: 'border-box',width:'100%',visibility: this.state.viewHistory ? 'visible' : 'hidden'}}>
                                            <Button type="normal" disabled={this.state.metadataVersionIndex === 0} onClick={
                                                ()=>{
                                                    const {metadataVersionIndex} = this.state;
                                                    this.setState({
                                                        metadataVersionIndex : metadataVersionIndex - 1< 0 ? 0 : metadataVersionIndex - 1,
                                                        metadataHistoryId:this.state.metadataVersionList[metadataVersionIndex - 1< 0 ? 0 : metadataVersionIndex - 1].metadataHistoryId
                                                    },()=>{
                                                        this.getTableDetailColumnHistoryInfo();
                                                    })
                                                }
                                            }>
                                                <Icon type="arrow-left"/>
                                            </Button>
                                            {/*<span style={{lineHeight: '32px',"color":"rgba(0,0,0,.85)",'overflow-wrap':"break-word",boxSizing:"border-box",margin:'0 3px 0 6px'}}>比较从</span>*/}
                                            {/*<span style={{lineHeight: '32px',"color":"#52c41a",'overflow-wrap':"break-word",boxSizing:"border-box",margin:'0 3px 0 3px',cursor: "pointer"}}>version32</span>*/}
                                            {/*<span style={{lineHeight: '32px',"color":"rgba(0,0,0,.85)",'overflow-wrap':"break-word",boxSizing:"border-box",margin:'0 3px 0 3px'}}>到</span>*/}
                                            {/*<span style={{lineHeight: '32px',"color":"#52c41a",'overflow-wrap':"break-word",boxSizing:"border-box",margin:'0 6px 0 3px',cursor: "pointer"}}>version33</span>*/}

                                            <span style={{lineHeight: '32px',"color":"#52c41a",'overflow-wrap':"break-word",boxSizing:"border-box",margin:'0 5px 0 5px',cursor: "pointer"}}>{this.state.metadataVersionList.length > this.state.metadataVersionIndex ? this.state.metadataVersionList[this.state.metadataVersionIndex]["version"]:""}</span>
                                            <Button type="normal" disabled={this.state.metadataVersionIndex === this.state.metadataVersionList.length - 1} onClick={
                                                ()=>{
                                                    const {metadataVersionIndex} = this.state;
                                                    this.setState({
                                                        metadataVersionIndex : metadataVersionIndex + 1 > this.state.metadataVersionList.length - 1 ? this.state.metadataVersionList.length - 1 : metadataVersionIndex + 1,
                                                        metadataHistoryId:this.state.metadataVersionList[metadataVersionIndex + 1 > this.state.metadataVersionList.length - 1 ? this.state.metadataVersionList.length - 1 : metadataVersionIndex + 1].metadataHistoryId
                                                    },()=>{
                                                        this.getTableDetailColumnHistoryInfo();
                                                    })
                                                }
                                            }>
                                                <Icon type="arrow-right"/>
                                            </Button>

                                        </div>
                                    </Col>
                                    <Col span={6}>
                                        <div style={{marginRight: 20, marginTop: 20,justifyContent: 'flex-end',boxSizing: 'border-box',display: 'inline-flex',width:'100%'}}>
                                            <Button  type="normal" onClick={()=>{
                                                const {viewHistory} = this.state ;
                                                this.setState({
                                                    viewHistory :!viewHistory,
                                                    metadataVersionIndex:0,
                                                    metadataHistoryId: this.state.metadataVersionList.length > this.state.metadataVersionIndex ? this.state.metadataVersionList[this.state.metadataVersionIndex]["metadataHistoryId"]:""
                                                },()=>{
                                                    this.getTableDetailColumnHistoryInfo();
                                                });
                                            }} style={{boxSizing: 'border-box'}}>
                                                {this.state.viewHistory?"取消查看":"查看历史版本"}
                                            </Button>
                                        </div>
                                    </Col>
                                </Row>
                                <div style={{marginTop: '20px', marginBottom: '20px'}} className='container-table'>
                                    <Table dataSource={tableDetailColumnInfo} hasBorder={false} isZebra={true}
                                           size={'small'} primaryKey="id" className={styles.table}
                                           onRowMouseEnter={(record, index, e) => {
                                               //悬浮在表格每一行的时候触发的事件
                                               tableDetailColumnInfo[index].addTag = true;
                                               record.addTag = true;
                                               this.forceUpdate();
                                           }}
                                           onRowMouseLeave={(record, index, e) => {
                                               //离开表格每一行的时候触发的事件
                                               record.addTag = false;
                                               tableDetailColumnInfo[index].addTag = false;
                                               this.forceUpdate();
                                           }}
                                    >
                                        <Table.Column width={200} title="数据列名称" dataIndex="name"/>
                                        <Table.Column width={250} title="字段名称" dataIndex="remarks"/>
                                        <Table.Column width={200} title="数据库类型" dataIndex="jdbcTypename"/>
                                        <Table.Column title="标签" dataIndex="标签" cell={
                                            (value, index, record) => {
                                                return (
                                                    <div>
                                                        {
                                                            Array.isArray(record.tags)
                                                            && record.tags.length > 0
                                                            && record.tags.map(
                                                                t => {
                                                                    return (
                                                                        <a className={styles.tagsLink}>
                                                                            <span className={styles.tags}>{t.tag}
                                                                                <span role="img" aria-label="close"
                                                                                      tabIndex="-1"
                                                                                      className={styles.antTagCloseIcon}>
                                                                                       <a onClick={() => this.deleteColumnTag(t)}>
                                                                                            <svg
                                                                                                viewBox="64 64 896 896"
                                                                                                focusable="false"
                                                                                                data-icon="close"
                                                                                                width="1em" height="1em"
                                                                                                fill="currentColor"
                                                                                                aria-hidden="true">
                                                                                            <path
                                                                                                d="M563.8 512l262.5-312.9c4.4-5.2.7-13.1-6.1-13.1h-79.8c-4.7 0-9.2 2.1-12.3 5.7L511.6 449.8 295.1 191.7c-3-3.6-7.5-5.7-12.3-5.7H203c-6.8 0-10.5 7.9-6.1 13.1L459.4 512 196.9 824.9A7.95 7.95 0 00203 838h79.8c4.7 0 9.2-2.1 12.3-5.7l216.5-258.1 216.5 258.1c3 3.6 7.5 5.7 12.3 5.7h79.8c6.8 0 10.5-7.9 6.1-13.1L563.8 512z">
                                                                                            </path>
                                                                                        </svg>
                                                                                       </a>
                                                                                </span>
                                                                             </span>
                                                                        </a>
                                                                    )
                                                                }
                                                            )
                                                        }
                                                        <span style={{visibility: record.addTag ? 'visible' : 'hidden'}}
                                                              className={styles.antColumnsTagSuccess}
                                                              onClick={() => this.addRecordTag(record, {}, true)}>
                                                            +添加标签
                                                        </span>
                                                    </div>
                                                )
                                            }
                                        }/>
                                    </Table>
                                </div>
                                {/**
                                 * 分页
                                 * defaultCurrent 初始页码
                                 * total 总记录数
                                 * tableDetailColumnInfoPageSize 一页中的记录数
                                 */}
                                <Pagination defaultCurrent={1}
                                            pageSize={this.tableDetailColumnInfoPageSize}
                                            total={this.tableDetailColumnTotalNum}
                                            onChange={this.onChangePage}
                                            onPageSizeChange={this.onTableDetailColumnInfoPageChange}
                                            pageSizeList={[5, 10, 20, 50, 100, 200]}
                                            pageSizeSelector="dropdown"
                                            pageSizeSelector="dropdown"
                                            className="page-demo"
                                            size="small"
                                />
                            </Tab.Item>
                            <Tab.Item title="所有权" key="2">
                                <div style={{marginTop: '20px', marginBottom: '20px'}} className='container-table'>
                                    <div className='container-btnBox' style={{marginTop: '0'}}>
                                        <div className={styles.buttons}>
                                            <AuthButton auth={"DATA_CENTER$_DATASOURCE$_BATCH_DELETE"}
                                                        onClick={() => this.handleAddOwnerShipClick({}, true)}
                                                        title={"新建"}/>
                                            <span className={styles.caseNumber}></span>
                                        </div>
                                    </div>
                                    <div style={{marginTop: '20px'}}>
                                        <Table dataSource={tableOwnership} hasBorder={false} isZebra={true}
                                               size={'small'}
                                               primaryKey="id" className={styles.table}>
                                            <Table.Column align="center" title="全名" dataIndex="name"/>
                                            <Table.Column align="center" title="角色" dataIndex="metadataRole"/>
                                            <Table.Column align="center" title="类型" dataIndex="metadataType"/>
                                            <Table.Column align="center" cell={
                                                (value, index, record) => {
                                                    return (
                                                        <div>
                                                            <AuthButton auth={"DATA_CENTER$_DATASOURCE$_BATCH_DELETE"}
                                                                        onClick={() => this.handleAddOwnerShipClick(record, false)}
                                                                        title={"修改"}/>
                                                            <AuthButton text
                                                                        onClick={() => this.deleteOwnerShip(record)}
                                                                        title={"删除"}/>
                                                        </div>
                                                    )
                                                }
                                            }/>
                                        </Table>
                                    </div>
                                </div>
                                {/**
                                 * 分页
                                 * defaultCurrent 初始页码
                                 * total 总记录数
                                 * tableDetailColumnInfoPageSize 一页中的记录数
                                 */}
                                <Pagination defaultCurrent={1}
                                            pageSize={this.tableOwnershipPageSize}
                                            total={this.tableOwnershipPageNum}
                                            onChange={this.onOwnershipChangePage}
                                            onPageSizeChange={this.onOwnershipPageSizeChange}
                                            pageSizeList={[5, 10, 20, 50, 100, 200]}
                                            pageSizeSelector="dropdown"
                                            className="page-demo"
                                            size="small"
                                />
                            </Tab.Item>
                            <Tab.Item title="血缘" key="3">
                                Help Content
                            </Tab.Item>
                        </Tab>
                    </div>
                </main>
            </div>
        );
    }

}

export default MetadataDetail;

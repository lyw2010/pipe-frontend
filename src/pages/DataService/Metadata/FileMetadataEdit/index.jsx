import React from 'react';
import IceContainer from '@icedesign/container';
import {Button, Input, Message, Radio, Select} from '@alifd/next';
import {
    FormBinder as IceFormBinder,
    FormBinderWrapper as IceFormBinderWrapper,
    FormError as IceFormError,
} from '@icedesign/form-binder';
import styles from './index.module.scss';
import event from '@/request'
import $http from '@/service/Services';


const {Option} = Select;
const {Group: RadioGroup} = Radio;
let form;

const Shape = {
    NO: 'normal',
    OFF: 'button'
};
const ItemDirection = {
    HORIZON: 'hoz',
    VERTICAL: 'ver'
};

/**
 * list
 */
class FileMetadataEdit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: {},
            schemaList: [],
            addValue: {},
            mockData: [],
            charTypes: [],
            change: false,
            dataSources: [],
            arr: [{
                _id: Math.random().toString(36).substring(2),
                chineseName: "",
                type: "VARCHAR",
                length: "0",
                scale: "0",
                nullable: "1",
                primaryKey: "0",
                remarks: ""
            }],
            id: "",
            addVisible: false,
            config: {
                data: null,
                visible: false,
                title: "",
                content: ""
            },
            shape: Shape.NO,
            itemDirection: ItemDirection.HORIZON
        };
        this.$http = $http;
        this.formChange = this.formChange.bind(this);
        this.validateAllFormField = this.validateAllFormField.bind(this);
    }

    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {
        const params = new URLSearchParams(this.props.location.search);
        const id = params.get("id");
        this.setState({
            id: id,
        }, () => {
            setTimeout(() => {
                this.getAllDatasourceList();
            }, 0);
            if (id) {
                const that = this;
                this.$http.get(event.event.url + '/v1/filePlatformMetadata/getFilePlatformMetadataById/' + id)
                    .then(function (response) {
                        const {data} = response;
                        if (data.code === 1) {
                            Message.warning(data.message ? data.message : data.data);
                        } else {
                            const arr = data.data.columns.map(
                                o => {
                                    const id = Math.random().toString(36).substring(2);
                                    o["_id"] = id;
                                    return o;
                                }
                            );
                            that.setState({
                                value: data.data,
                                arr: arr
                            })
                        }
                    })
                    .catch(function (error) {
                        console.error(error);
                        Message.error(error.message);
                    })
            }
        });
    };
    /**
     * 获取所有数据源列表
     */
    getAllDatasourceList = () => {
        const that = this;
        this.$http.get(event.event.url + '/v1/tDataSource/getAllDatasourceList').then(function (response) {
            const {data} = response;
            if (data.code === 1) {
                Message.warning(data.message ? data.message : data.data);
            } else {
                that.setState({
                    dataSources: data.data
                })
            }
        })
            .catch(function (error) {
                Message.error(error.message);
            });
    };
    /**
     * 高级属性是否可用
     * @param checked
     */
    onChange = (checked) => {
        this.setState({
            change: checked
        })
    }

    /* 表改变
    * @param formValue
    */
    formChange = (formValue) => {
        this.setState({
            value: formValue
        })
    };


    /**
     * 表验证
     */
    validateAllFormField = () => {
        const that = this;
        const {arr} = this.state;
        form.validateAll((errors, values) => {
            values["columns"] = arr;
            const urls = event.event.url + '/v1/filePlatformMetadata/saveOrUpdateFilePlatformMetadata';
            that.$http.post(urls, values)
                .then(function (response) {
                    const {data} = response;
                    if (data.code === 1) {
                        Message.warning(data.message ? data.message : data.data);
                    } else {
                        Message.success("操作成功.");
                        that.props.history.go(-1);
                    }
                })
                .catch(function (error) {
                    Message.error(error.message);
                })
        });
    };
    /**
     * 取消提示弹框
     */
    onCloseDialog = reason => {
        this.setState({
            visible: false
        });
    };

    /**
     * 新增参数
     */
    handleClickAdd = () => {
        const arr = this.state.arr;
        arr.push({
            _id: Math.random().toString(36).substring(2),
            chineseName: "",
            type: "VARCHAR",
            length: "0",
            scale: "0",
            nullable: "1",
            primaryKey: "0",
            remarks: ""
        });
        this.setState(
            {
                arr: arr
            }
        )
    };
    /**
     * 删除参数
     */
    deleteClick = (ob) => {
        const {arr} = this.state;
        for (let i = 0; i < arr.length; i++) {
            if (arr[i]["_id"] === ob["_id"]) {
                arr.splice(i, 1);
                break;
            }
        }
        this.setState(
            {
                arr: arr
            }
        );

    };


    /**
     *
     * @param value value: {String} 数据
     * @param event e_: {Event} DOM事件对象
     */
    changeColumnName = (value, event) => {
        const id = event.target.id;
        const {arr} = this.state;
        for (let i = 0; i < arr.length; i++) {
            if (arr[i]["_id"] === id) {
                arr[i]["chineseName"] = value;
                arr[i][id + "_chineseName"] = value;
            }
        }
        this.setState(
            {
                arr: arr
            }
        )
    };

    /**
     *
     * @param value value: {String} 数据
     * @param event e_: {Event} DOM事件对象
     */
    changeRemarks = (value, event) => {
        const id = event.target.id;
        const {arr} = this.state;
        for (let i = 0; i < arr.length; i++) {
            if (arr[i]["_id"] === id) {
                arr[i]["remarks"] = value;
                arr[i][id + "_remarks"] = value;
            }
        }
        this.setState(
            {
                arr: arr
            }
        )
    };

    /**
     * 小数位
     * @param value value: {String} 数据
     * @param event e_: {Event} DOM事件对象
     */
    changeScale = (value, event) => {
        const id = event.target.id;
        const {arr} = this.state;
        for (let i = 0; i < arr.length; i++) {
            if (arr[i]["_id"] === id) {
                arr[i]["scale"] = value;
                arr[i][id + "_scale"] = value;
            }
        }
        this.setState(
            {
                arr: arr
            }
        )
    };
    /**
     * 长度
     * @param value value: {String} 数据
     * @param event e_: {Event} DOM事件对象
     */
    changeLength = (value, event) => {
        const id = event.target.id;
        const {arr} = this.state;
        for (let i = 0; i < arr.length; i++) {
            if (arr[i]["_id"] === id) {
                arr[i]["length"] = value;
                arr[i][id + "_length"] = value;
            }
        }
        this.setState(
            {
                arr: arr
            }
        )
    };
    /**
     *
     * @param value value: {String} 数据
     * @param id e_: {Event} DOM事件对象
     */
    changeColumnType = (value, id) => {
        const {arr} = this.state;
        for (let i = 0; i < arr.length; i++) {
            if (arr[i]["_id"] === id) {
                arr[i]["type"] = value;
                arr[i][id + "_type"] = value;
            }
        }
        this.setState(
            {
                arr: arr
            }
        )
    };
    /**
     * 是否为空
     * @param value value: {String} 数据
     * @param id e_: {Event} DOM事件对象
     */
    changeNullable = (value, id) => {
        const {arr} = this.state;
        for (let i = 0; i < arr.length; i++) {
            if (arr[i]["_id"] === id) {
                arr[i]["nullable"] = value;
                arr[i][id + "_nullable"] = value;
            }
        }
        this.setState(
            {
                arr: arr
            }
        )
    };
    /**
     * 否主键
     * @param value value: {String} 数据
     * @param id e_: {Event} DOM事件对象
     */
    changePrimaryKey = (value, id) => {
        const {arr} = this.state;
        for (let i = 0; i < arr.length; i++) {
            if (arr[i]["_id"] === id) {
                arr[i]["primaryKey"] = value;
                arr[i][id + "_primaryKey"] = value;
                if (value ===1){
                    arr[i]["nullable"] = "0";
                    arr[i][id + "_nullable"] = "0";
                }
            }
        }
        this.setState(
            {
                arr: arr
            }
        )
    };

    /**
     *
     * @param value value: {String} 数据
     * @param event e_: {Event} DOM事件对象
     */
    changeDefaultValue = (value, event) => {
        const id = event.target.id;
        const {arr} = this.state;
        for (let i = 0; i < arr.length; i++) {
            if (arr[i]["_id"] === id) {
                arr[i]["defaultValue"] = value;
                arr[i][id + "_defaultValue"] = value;
            }
        }
        this.setState(
            {
                arr: arr
            }
        )
    };

    /**
     * 表
     * @returns {*}
     */
    render() {
        return (
            <div>
                <div className='container-header'>
                    <p style={{display: 'inline-block'}}>{this.state.id ? "修改图例" : "新建图例"}</p>
                    <Button type="normal" size="small" className='next-btn-normal table-btn-return'
                            style={{float: 'right'}}>
                        <a onClick={() => {
                            this.props.history.go(-1)
                        }}> 返回</a>
                    </Button>
                </div>


                <IceContainer className='container-main'>
                    <IceFormBinderWrapper
                        value={this.state.value}
                        onChange={this.formChange}
                        ref={formRef => form = formRef}
                    >
                        <div className={styles.formContent}>
                            <h4 className={styles.dTitle}>基础信息</h4>
                            <div className={styles.formItem}>
                                <div className={styles.formLabel}><span className={styles.red}>*</span>表名:</div>
                                <IceFormBinder
                                    required
                                    triggerType="onBlur"
                                    message="表名不能为空"
                                    name="chineseName"
                                >
                                    <Input
                                        placeholder="请输入表名"
                                        className={styles.inputNme}
                                    />
                                </IceFormBinder>
                                <div className={styles.formError}>
                                    <IceFormError name="chineseName"/>
                                </div>
                            </div>
                            <div className={styles.formItem}>
                                <div className={styles.formLabel}><span className={styles.red}>*</span>数据库:</div>
                                <IceFormBinder required name="datasourceId" message="数据库不能为空">
                                    <Select name={'datasourceId'}
                                            placeholder="请选择"
                                            disabled={this.state.value.id}
                                            className={styles.inputNme}
                                    >
                                        {
                                            this.state.dataSources.map(
                                                o => {
                                                    return (<Option value={o.id}>{o.name}</Option>)
                                                }
                                            )
                                        }
                                    </Select>
                                </IceFormBinder>
                                <div className={styles.formError}>
                                    <IceFormError name="datasourceId"/>
                                </div>
                            </div>


                            <div className={styles.formItem}>
                                <div className={styles.formLabel}>描述:</div>
                                <IceFormBinder
                                    triggerType="onBlur"
                                    name="remarks"
                                >
                                    <Input.TextArea
                                        maxLength={50}
                                        rows={8}
                                        hasLimitHint
                                        placeholder="请输入描述"
                                        className={styles.inputNme}
                                    />
                                </IceFormBinder>
                            </div>


                            <div>
                                <div className={styles.formContent}>
                                    <h4 className={styles.dTitle}>元数据数据列</h4>
                                    <div className={styles.container} style={{marginTop: '0'}}>
                                        <div className={styles.buttons}>
                                            <Button
                                                className={styles.button}
                                                onClick={() => this.handleClickAdd()}
                                            >
                                                添加数据列
                                            </Button>
                                        </div>
                                    </div>
                                </div>
                                {
                                    this.state.arr.map(
                                        (o, index) => {
                                            return (
                                                <div id="addInput" className={styles.formItem}>

                                                    {/*数据列名称*/}
                                                    <div className={styles.formLabel}>列名称:</div>
                                                    <Input id={o._id}
                                                           value={o.chineseName}
                                                           defaultValue={o.chineseName}
                                                           placeholder={"请输入列名称"} onChange={this.changeColumnName}
                                                           className={styles.inputKeyValueNme}
                                                    />
                                                    <div className={styles.formError}>
                                                        <IceFormError name="chineseName"/>
                                                    </div>
                                                    {/*数据列名称*/}


                                                    {/*数据列类型*/}
                                                    <div className={styles.formLabel}  style={{width: '60'}}>类型:</div>
                                                    <Select id={o._id}
                                                            style={{
                                                                width: 100
                                                            }}
                                                            value={o.type}
                                                            onChange={(value => {
                                                                this.changeColumnType(value, o._id);
                                                            })}
                                                            defaultValue={o.type}
                                                            placeholder="类型"
                                                            className={styles.inputNme}
                                                    >
                                                        <Option value="VARCHAR">字符</Option>
                                                        <Option value="BIGINT">长整数</Option>
                                                        <Option value="INTEGER">整数</Option>
                                                        <Option value="NUMERIC">浮点数</Option>
                                                        <Option value="DATE">日期</Option>
                                                        <Option value="TIMESTAMP">日期时间</Option>
                                                        <Option value="TEXT">大文本</Option>
                                                    </Select>
                                                    <div className={styles.formError}>
                                                        <IceFormError name="type"/>
                                                    </div>
                                                    {/*数据列类型*/}

                                                    {/*数据列名称*/}
                                                    <div className={styles.formLabel} style={{width: '60'}}>长度:</div>
                                                    <Input id={o._id}
                                                           value={o.length}
                                                           defaultValue={o.length}
                                                           placeholder={"长度"} onChange={this.changeLength}
                                                           className={styles.inputKeyValueNme}
                                                           style={{width:'80px'}}
                                                    />
                                                    <div className={styles.formError}>
                                                        <IceFormError name="length"/>
                                                    </div>
                                                    {/*数据列名称*/}

                                                    {/*小数位*/}
                                                    <div className={styles.formLabel}  style={{width: '60'}}>小数位:</div>
                                                    <Input id={o._id}
                                                           value={o.scale}
                                                           defaultValue={o.scale}
                                                           placeholder={"小数位"} onChange={this.changeScale}
                                                           className={styles.inputKeyValueNme}
                                                           style={{width:'80px'}}
                                                    />
                                                    <div className={styles.formError}>
                                                        <IceFormError name="scale"/>
                                                    </div>
                                                    {/*小数位*/}


                                                    {/*是否为空*/}
                                                    <div className={styles.formLabel}  style={{width: '60'}}>是否为空:</div>
                                                    <Select id={o._id}
                                                            value={o.nullable}
                                                            onChange={(value => {
                                                                this.changeNullable(value, o._id);
                                                            })}
                                                            defaultValue={o.nullable}
                                                            placeholder="是否为空"
                                                            className={styles.inputNme}
                                                            style={{width:'80px'}}
                                                    >
                                                        <Option value="1">是</Option>
                                                        <Option value="0">否</Option>
                                                    </Select>
                                                    <div className={styles.formError}>
                                                        <IceFormError name="nullable"/>
                                                    </div>
                                                    {/*是否为空*/}


                                                    {/*是否主键*/}
                                                    <div className={styles.formLabel}  style={{width: '60'}}>是否主键:</div>
                                                    <Select id={o._id}
                                                            value={o.primaryKey}
                                                            onChange={(value => {
                                                                this.changePrimaryKey(value, o._id);
                                                            })}
                                                            defaultValue={o.primaryKey}
                                                            placeholder="是否主键"
                                                            className={styles.inputNme}
                                                            style={{width:'80px'}}
                                                    >
                                                        <Option value="0">否</Option>
                                                        <Option value="1">是</Option>

                                                    </Select>
                                                    <div className={styles.formError}>
                                                        <IceFormError name="primaryKey"/>
                                                    </div>
                                                    {/*是否主键*/}


                                                    {/*默认值*/}
                                                    <div className={styles.formLabel}>默认值:</div>
                                                    <Input id={o._id}
                                                           value={o.defaultValue}
                                                           defaultValue={o.defaultValue}
                                                           onChange={this.changeDefaultValue}
                                                           placeholder="表达式:=now()"
                                                           className={styles.inputKeyValueNme}
                                                    />
                                                    <div className={styles.formError}>
                                                        <IceFormError name="defaultValue"/>
                                                    </div>
                                                    {/*默认值*/}


                                                    {/*备注*/}
                                                    <div className={styles.formLabel}> 备注:</div>
                                                    <Input id={o._id}
                                                           value={o.remarks}
                                                           defaultValue={o.remarks}
                                                           onChange={this.changeRemarks}
                                                           placeholder="备注"
                                                           className={styles.inputKeyValueNme}
                                                           style={{width: '300px'}}
                                                    />
                                                    <div className={styles.formError}>
                                                        <IceFormError name="remarks"/>
                                                    </div>
                                                    {/*备注*/}

                                                    <Button type="normal" onClick={() => this.deleteClick(o)}
                                                            warning>删除</Button> &nbsp;&nbsp;
                                                </div>
                                            )
                                        }
                                    )
                                }
                            </div>
                            <Button
                                type="primary"
                                className='sync-btn-submit'
                                onClick={this.validateAllFormField}
                            >
                                提 交
                            </Button> &nbsp;&nbsp;
                            <Button
                                type="primary"
                                className='zgph-btn-cancel'
                                onClick={() => {
                                    this.props.history.go(-1)
                                }}
                            >
                                取 消
                            </Button>
                        </div>
                    </IceFormBinderWrapper>
                </IceContainer>
            </div>
        );
    }

}

export default FileMetadataEdit;

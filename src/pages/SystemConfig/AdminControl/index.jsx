/**
 * 数据中台-系统配置-监控管理
 */
import React from 'react';
import event from '@/request'
import {Message} from "@alifd/next";
import $http from '@/service/Services';


/**
 * list
 */
class AdminControl extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };

        this.$http = $http;
    }

    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {

    };


    /**
     * 获取数据
     */
    getAdminControl = () => {
        const _this = this;
        let url = event.event.url + '/v1/auth/getAuthPage';
        this.$http.post(url)
            .then(function (response) {
                const {data} = response;
                console.log(data);
            })
            .catch(function (error) {
                Message.error(error.message);
            })

    };





    render() {
        return (
            <div>
                <iframe
                style={{width:'100%', height:this.state.iFrameHeight, overflow:'visible'}}
                onLoad={() => {//iframe高度不超过content的高度即可
                    let h = document.documentElement.clientHeight - 20;
                    this.setState({
                        "iFrameHeight": h + 'px'
                    });
                }}
                ref="iframe"
                src={"http://localhost:2632/login"}
                width="100%"
                height={this.state.iFrameHeight}
                scrolling="no"
                frameBorder="0"
            />
            </div>
        )
            ;
    }
}

export default AdminControl;

/**
 * 数据中台-系统配置-接口文档
 */
import React from 'react';
import event from '@/request'
import {Message} from "@alifd/next";
import $http from '@/service/Services';


/**
 * list
 */
class Druid extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };

        this.$http = $http;
    }

    /**
     * 在渲染前调用,在客户端也在服务端
     */
    componentWillMount = () => {

    };
    

    render() {
        return (
            <div>
                <iframe
                style={{width:'100%', height:this.state.iFrameHeight, overflow:'visible'}}
                onLoad={() => {//iframe高度不超过content的高度即可
                    let h = document.documentElement.clientHeight - 20;
                    this.setState({
                        "iFrameHeight": h + 'px'
                    });
                }}
                ref="iframe"
                src={event.event.url + "/druid/login.html"}
                width="100%"
                height={this.state.iFrameHeight}
                scrolling="no"
                frameBorder="0"
            />
            </div>
        )
            ;
    }
}

export default Druid;

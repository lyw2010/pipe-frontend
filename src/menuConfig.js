/**
 * 顶部模块管理
 * @type {*[]}
 */
const headerMenuConfig = [

    {
        name: '权限管理',
        path: '/aAuth',
        auth: "AUTH",
        role: "1"
    },
    // {
    //     name: '权限管理',
    //     path: '/uAuth',
    //     auth: "AUTH",
    //     role: "!1" //对于角色的处理
    // },
    {
        name: '数据服务',
        path: '/dataService',
        auth: ""
    },
    {
        name: '系统管理',
        path: '/systemInfo',
        auth: ""
    },
    {
        name: '系统配置',
        path: '/systemConfig',
        auth: ""
    },
];
/**
 * 菜单管理
 * 管理系统（埋点管理&&布局管理）
 * @type {*[]}
 */
const manageMenuConfig = [
    {
        title: 'systemConfig',
        asideData: [
            {
                name: '首页图片管理',
                path: '/systemConfig/pagesList',
                icon: 'home',
            }
            , {
                name: '码表管理',
                path: '/systemConfig/baseCodeTypeList',
                icon: 'home',
            }
            , {
                name: '码值管理',
                path: '/systemConfig/baseCodeListList',
                icon: 'home',
            }
            , {
                name: '接口文档',
                path: '/systemConfig/apiPage',
                icon: 'home',
            }
            , {
                name: '系统监控',
                path: '/systemConfig/adminControl',
                icon: 'home',
            }
            , {
                name: '数据库监控',
                path: '/systemConfig/druid',
                icon: 'home',
            }
        ]
    },
    {
        title: 'systemInfo',
        asideData: [
            {
                name: '服务管理',
                path: '/systemInfo/systemInfoList',
                icon: 'home',
            },
            {
                name: '主机管理',
                path: '/systemInfo/serverManage',
                icon: 'home',
            },
            {
                name: '节点管理',
                path: '/systemInfo/nodeManage',
                icon: 'chart-pie',
                children: [
                    {
                        name: '节点列表',
                        path: '/systemInfo/nodeManage/node-list',
                        icon: 'home',
                    }
                ]
            }
        ]
    },
    {
        title: 'dataService',
        asideData: [
            {
                name: '任务管理',
                path: '/dataService/taskManage/task-list',
                icon: 'chart-pie',
                children: [
                    {
                        name: '数据同步',
                        path: '/dataService/taskManage/task-list',
                    },
                    {
                        name: '图表指标任务',
                        path: '/dataService/businessIntelligence/list',
                    }
                ]
            },
            {
                name: '图例管理',
                path: '/dataService/biManage',
                icon: 'chart-pie',
                children: [
                    {
                        name: '图例类型管理',
                        path: '/dataService/biManage/chartTypeList',
                    },

                ]
            },
            {
                name: '元数据管理',
                path: '/dataService/metadata',
                icon: 'chart-pie',
                children: [
                    {
                        name: '数据表元数据',
                        path: '/dataService/metadata/metadata-list',
                    },
                    {
                        name: '指标元数据',
                        path: '/dataService/biManage/chartList',
                    },
                    {
                        name: '文件元数据元数据',
                        path: '/dataService/metadata/file-metadata-list',
                    }
                ],
            },
            {
                name: '数据源管理',
                path: '/dataService/datasource',
                icon: 'chart-pie',
                children: [
                    {
                        name: '数据源列表',
                        path: '/dataService/datasource/datasource-list',
                    }
                ],
            },
            {
                name: '业务数据',
                path: '/dataService/business',
                icon: 'chart-pie',
                children: [
                    {
                        name: '业务数据',
                        path: '/dataService/business/list',
                    }
                ],
            },
        ],
    },
    {
        //超级管理员权限
        title: 'aAuth',
        asideData: [
            {
                name: '租户管理',
                path: '/aAuth/adminTenantList',
                icon: 'home',
                auth: "AUTH$_TENANT"
            },
            {
                name: '角色管理',
                path: '/aAuth/adminDefaultList',
                icon: 'home',
                auth: "AUTH$_TENANT"
            },
            {
                name: '权限管理',
                path: '/aAuth/adminDefaultAuthList',
                icon: 'home',
                auth: "AUTH$_TENANT"
            }
        ]
    },
    // {
    //     title: 'uAuth',
    //     asideData: [
    //         {
    //             name: '租户管理',
    //             path: '/uAuth/userTenantList',
    //             icon: 'home',
    //             auth:"AUTH$_TENANT"
    //         },
    //         {
    //             name: '角色管理',
    //             path: '/uAuth/userRoleList',
    //             icon: 'home',
    //             auth:"AUTH$_TENANT$_ADD_ROLE"
    //         },
    //         {
    //             name: '用户管理',
    //             path: '/uAuth/userManageList',
    //             icon: 'home',
    //             auth:"AUTH$_TENANT$_ADD_USER"
    //         },
    //     ]
    // }
];
export {manageMenuConfig, headerMenuConfig};

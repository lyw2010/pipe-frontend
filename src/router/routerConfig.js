/**
 * 使用asyncComponent动态导入组件
 */
import asyncComponent from '../asyncComponent';



/**
 * 导入布局页面
 */
function loadLayouts(component) {
    return import(`@/layouts/${component}`)//$pages在webpack里面配置过是“./src/pages”的别名
}

const UserLayout = asyncComponent(() => loadLayouts('UserLayout')); // 用户登陆注册
const BasicLayout = asyncComponent(() => loadLayouts('BasicLayout')); // 我的工作台
const ManageLayout = asyncComponent(() => loadLayouts('ManageLayout')); // 管理页面
const WebShell = asyncComponent(() => loadLayouts('WebShell')); // 用户登陆注册

/**
 * 导入pages下组件
 */
function loadPages(component) {
    return import(`@/pages/${component}`)//$pages在webpack里面配置过是“./src/pages”的别名
}
// 系统配置
const ApiPage = asyncComponent(() => loadPages('SystemConfig/ApiPage')); // API 文档
const AdminControl = asyncComponent(() => loadPages('SystemConfig/AdminControl')); // 监控
const Druid = asyncComponent(() => loadPages('SystemConfig/Druid')); // 数据库监控

// 用户登录注册
const UserLogin = asyncComponent(() => loadPages('User/UserLogin')); // 用户登录
const UserRegister = asyncComponent(() => loadPages('User/UserRegister')); // 用户注册

// 权限管理
const AdminTenantList = asyncComponent(() => loadPages('Authority/Admin/Tenant/TenantList')); // 
const AdminRoleList = asyncComponent(() => loadPages('Authority/Admin/Tenant/RoleList')); // 
const TenantAdd = asyncComponent(() => loadPages('Authority/Admin/Tenant/TenantAdd')); // 
const AdminRoleEdit = asyncComponent(() => loadPages('Authority/Admin/Tenant/RoleEdit')); // 
const AdminTenantConfig = asyncComponent(() => loadPages('Authority/Admin/Tenant/TenantConfig')); // 
const UserManageList = asyncComponent(() => loadPages('Authority/Admin/Tenant/UserManage')); // 
const AdminUserEdit = asyncComponent(() => loadPages('Authority/Admin/Tenant/UserEdit')); // 用户修改（基本信息）
const AuthorityEdit = asyncComponent(() => loadPages('Authority/components/AuthorityEditComponents')); // 用户权限修改

const UserTenantList = asyncComponent(() => loadPages('Authority/User/Tenant/TenantList')); // 
const UserRoleList = asyncComponent(() => loadPages('Authority/User/Role/RoleList')); // 
const UserUserManageList = asyncComponent(() => loadPages('Authority/User/User/UserManage')); // 
const UserUserEdit = asyncComponent(() => loadPages('Authority/User/User/UserEdit')); // 
const UserRoleEdit = asyncComponent(() => loadPages('Authority/User/Role/RoleEdit')); // 
const PagesList = asyncComponent(() => loadPages('Authority/LoginPageImages/PagesList')); // 

const DefaultRoles = asyncComponent(() => loadPages('Authority/Admin/DefaultRoles')); // 默认角色管理
const DefaultAuth = asyncComponent(() => loadPages('Authority/Admin/DefaultAuth')); // 默认权限管理
const Department = asyncComponent(() => loadPages('Authority/Admin/Department')); // 角色管理

const BaseCodeListEdit = asyncComponent(() => loadPages('Base/BaseCodeListEdit')); //
const BaseCodeTypeEdit = asyncComponent(() => loadPages('Base/BaseCodeTypeEdit')); //
const BaseCodeTypeList = asyncComponent(() => loadPages('Base/BaseCodeTypeList')); //
const BaseCodeListList = asyncComponent(() => loadPages('Base/BaseCodeListList')); //

//数据管理
const BusinessEdit = asyncComponent(() => loadPages('DataService/Business/BusinessEdit'));
const BusinessAdd = asyncComponent(() => loadPages('DataService/Business/BusinessAdd'));
const BusinessList = asyncComponent(() => loadPages('DataService/Business/BusinessList'));
const TaskDetailHistoryList = asyncComponent(() => loadPages('DataService/TaskManage/TaskDetailHistoryList'));
const TaskEdit = asyncComponent(() => loadPages('DataService/TaskManage/TaskDetailAdd'));
const TaskList = asyncComponent(() => loadPages('DataService/TaskManage/TaskDetailList'));
const DataSourceTableColumnList = asyncComponent(() => loadPages('DataService/Datasource/DataSourceTableColumnList'));
const DataSourceTableList = asyncComponent(() => loadPages('DataService/Datasource/DataSourceTableList'));
const DataSourceDetail = asyncComponent(() => loadPages('DataService/Datasource/DataSourceDetail'));
const DataSourceAdd = asyncComponent(() => loadPages('DataService/Datasource/DataSourceAdd'));
const DataSourceList = asyncComponent(() => loadPages('DataService/Datasource/DataSourceList'));
const BusinessIntelligenceTaskList = asyncComponent(() => loadPages('DataService/BusinessIntelligenceTask/BusinessIntelligenceTaskList'));
const BusinessIntelligenceTaskAdd = asyncComponent(() => loadPages('DataService/BusinessIntelligenceTask/BusinessIntelligenceTaskAdd'));
const BusinessIntelligenceTaskHistoryList = asyncComponent(() => loadPages('DataService/BusinessIntelligenceTask/BusinessIntelligenceTaskHistoryList'));
const BusinessIntelligenceTaskHistoryLog = asyncComponent(() => loadPages('DataService/BusinessIntelligenceTask/BusinessIntelligenceTaskHistoryLog'));
const MetadataList = asyncComponent(() => loadPages('DataService/Metadata/MetadataList'));
const MetadataDetail = asyncComponent(() => loadPages('DataService/Metadata/MetadataDetail'));
const FileMetadataList = asyncComponent(() => loadPages('DataService/Metadata/FileMetadataList'));
const FileMetadataEdit = asyncComponent(() => loadPages('DataService/Metadata/FileMetadataEdit'));
const BusinessIntelligenceChartAdd = asyncComponent(() => loadPages('DataService/BusinessIntelligence/BusinessIntelligenceChartAdd'));
const BusinessIntelligenceChartList = asyncComponent(() => loadPages('DataService/BusinessIntelligence/BusinessIntelligenceChartList'));
const BusinessIntelligenceChartTypeAdd = asyncComponent(() => loadPages('DataService/BusinessIntelligence/BusinessIntelligenceChartTypeAdd'));
const BusinessIntelligenceChartTypeList = asyncComponent(() => loadPages('DataService/BusinessIntelligence/BusinessIntelligenceChartTypeList'));



//系统管理
const SystemInfoList = asyncComponent(() => loadPages("SystemInfo/SystemInfoList"));
const SystemDetail = asyncComponent(() => loadPages("SystemInfo/SystemDetail"));
const ServerManage = asyncComponent(() => loadPages("SystemInfo/ServerManage"));
const ServerManageSSH = asyncComponent(() => loadPages("SystemInfo/ServerManageSSH"));
const NodeList = asyncComponent(() => loadPages("SystemInfo/NodeList"));
const NodeEdit = asyncComponent(() => loadPages("SystemInfo/NodeEdit"));
const NodeDetail = asyncComponent(() => loadPages("SystemInfo/NodeDetail"));

// 我的工作台
const Dashboard = asyncComponent(() => loadPages('MyWorkbench/Dashboard')); // 我的工作台
const Dismantling = asyncComponent(() => loadPages('MyWorkbench/Dismantling')); // 拆预收案
const Allocation = asyncComponent(() => loadPages('MyWorkbench/Allocation')); // 案款账号分配
const Selfhelp = asyncComponent(() => loadPages('MyWorkbench/Selfhelp')); // 当事人自助收案
const List = asyncComponent(() => loadPages('MyWorkbench/List')); // 在办案件列表
const Batch = asyncComponent(() => loadPages('MyWorkbench/Batch')); // 批量操作
const NewCase = asyncComponent(() => loadPages('MyWorkbench/NewCase')); // 案件录入

// 404
const NotFound = asyncComponent(() => loadPages('NotFound')); //


const routerConfig = [
    {
        path: '/user',
        component: UserLayout,
        children: [
            {
                path: '/login',
                component: UserLogin,
            },
            {
                path: '/register',
                component: UserRegister,
            },
            {
                path: '/',
                redirect: '/user/login',
            },
            {
                component: NotFound,
            },
        ],
    },
    {
        path: '/webShell',
        component: WebShell,
        children: [
            {
                path: '/ssh',
                component: ServerManageSSH,
            },
            {
                path: '/',
                redirect: '/webShell/ssh',
            },
            {
                component: NotFound,
            },
        ],
    },
    // 管理员权限
    {
        path: '/aAuth',
        component: ManageLayout,
        children: [
            {
                path: '/adminTenantList',
                component: AdminTenantList,
            },
            {
                path: '/adminDefaultList',
                component: DefaultRoles
            },
            {
                path: '/adminDefaultAuthList',
                component: DefaultAuth
            },
            {
                path: '/department',
                component:Department
            },
            {
                path: '/orgAdd',
                component: TenantAdd,
            },
            {
                path: '/roleList',
                component: AdminRoleList,
            },
            {
                path: '/roleEdit',
                component: AdminRoleEdit,
            },
            {
                path: '/adminTenantConfig',
                component: AdminTenantConfig,
            },
            {
                path: '/userManageList',
                component: UserManageList
            },
            {
                path: '/userEdit',
                component: AdminUserEdit
            },
            {
                path: '/authorityEdit',
                component: AuthorityEdit
            },
            {
                path: '/pagesList',
                component: PagesList
            },
            {
                path: '/',
                redirect: '/aAuth/adminTenantList',
            },
            {
                component: NotFound,
            },
        ],
    },
    {
        path: '/dataService',
        component: ManageLayout,
        children: [
            {
                path: '/taskManage/task-list',
                component: TaskList,
            },
            {
                path: '/datasource/datasource-list',
                component: DataSourceList,
            },
            {
                path: '/datasource/dataSourceAdd',
                component: DataSourceAdd,
            },
            {
                path: '/datasource/dataSourceDetail',
                component: DataSourceDetail,
            },
            {
                path: '/datasource/dataSourceTableDetail',
                component: DataSourceTableList,
            },
            {
                path: '/datasource/dataSourceDetailColumn',
                component: DataSourceTableColumnList,
            },
            {
                path: '/taskManage/task-add',
                component: TaskEdit,
            },
            {
                path: '/task/task-history',
                component: TaskDetailHistoryList,
            },
            {
                path: '/business/list',
                component: BusinessList,
            },
            {
                path: '/business/add',
                component: BusinessAdd,
            },
            {
                path: '/business/edit',
                component: BusinessEdit,
            },
            {
                path: '/businessIntelligence/list',
                component:BusinessIntelligenceTaskList
            },
            {
                path: '/businessIntelligence/add',
                component:BusinessIntelligenceTaskAdd
            },
            {
                path: '/businessIntelligence/plan',
                component:BusinessIntelligenceTaskHistoryList
            },
            {
                path: '/businessIntelligence/log',
                component:BusinessIntelligenceTaskHistoryLog
            },
            {
                path: '/metadata/metadata-list',
                component: MetadataList,
            },
            {
                path: '/metadata/metadata-detail',
                component: MetadataDetail,
            },
            {
                path: '/metadata/file-metadata-list',
                component: FileMetadataList,
            },
            {
                path: '/metadata/file-metadata-edit',
                component: FileMetadataEdit,
            },
            {
                path: '/biManage/chartTypeList',
                component: BusinessIntelligenceChartTypeList,
            },
            {
                path: '/biManage/chartTypeAdd',
                component: BusinessIntelligenceChartTypeAdd,
            },
            {
                path: '/biManage/chartList',
                component: BusinessIntelligenceChartList,
            },
            {
                path: '/biManage/chartAdd',
                component: BusinessIntelligenceChartAdd,
            },
            {
                path: '/',
                redirect: '/dataService/taskManage/task-list',
            },
            {
                component: NotFound,
            },
        ]
    },
    //系统配置
    {
        path: '/systemConfig',
        component: ManageLayout,
        children: [
            {
                path: '/authorityEdit',
                component: AuthorityEdit
            },
            {
                path: '/pagesList',
                component: PagesList
            },
            {
                path: '/baseCodeEdit',
                component: BaseCodeTypeEdit
            },
            {
                path: '/codeListEdit',
                component: BaseCodeListEdit
            },
            {
                path: '/baseCodeTypeList',
                component: BaseCodeTypeList
            },
            {
                path: '/baseCodeListList',
                component: BaseCodeListList
            },
            {
                path: '/apiPage',
                component: ApiPage
            },
            {
                path: '/druid',
                component: Druid
            },
            {
                path: '/adminControl',
                component: AdminControl
            },
            {
                path: '/',
                redirect: '/systemConfig/pagesList',
            },
            {
                component: NotFound,
            },
        ],
    },
    {
        path: '/systemInfo',
        component: ManageLayout,
        children: [
            {
                path: '/systemInfoList',
                component: SystemInfoList
            },
            {
                path: '/systemDetail',
                component: SystemDetail
            },
            {
                path: '/serverManage',
                component: ServerManage
            },
            {
                path: '/nodeManage/node-list',
                component: NodeList
            },
            {
                path: '/nodeManage/node-add',
                component: NodeEdit
            },
            {
                path: '/nodeManage/node-detail',
                component: NodeDetail
            },
            {
                path: '/',
                redirect: '/systemInfo/systemInfoList',
            },
            {
                component: NotFound,
            },
        ],
    },
    //用户的权限管理
    {
        path: '/uAuth',
        component: ManageLayout,
        children: [
            {
                path: '/userTenantList',
                component: UserTenantList,
            },
            {
                path: '/userRoleList',
                component: UserRoleList
            },
            {
                path: '/userManageList',
                component: UserUserManageList
            },
            {
                path: "/userRoleEdit",
                component: UserRoleEdit
            },
            {
                path: '/authorityEdit',
                component: AuthorityEdit
            },
            {
                path: '/userEdit',
                component: UserUserEdit
            },
            {
                path: '/',
                redirect: '/uAuth/userTenantList',
            },
            {
                component: NotFound,
            },
        ],
    },
    // 我的工作台
    {
        path: '/',
        component: BasicLayout,
        children: [
            {
                path: '/dashboard',
                component: Dashboard,
            },
            {
                path: '/dismantling',
                component: Dismantling,
            },
            {
                path: '/allocation',
                component: Allocation,
            },
            {
                path: '/selfHelp',
                component: Selfhelp,
            },
            {
                path: '/list',
                component: List,
            },
            {
                path: '/batch',
                component: Batch,
            },
            {
                path: '/new',
                component: NewCase,
            },
            {
                path: '/',
                redirect: '/user',
            },
            {
                component: NotFound,
            },
        ],
    },
];

export default routerConfig;

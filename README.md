# ice-government-management-admin

## 使用

- 启动调试服务: `npm start`
- 构建 dist: `npm run build`

## 目录结构

- react-router @4.x 默认采用 hashHistory 的单页应用
- 入口文件: `src/index.jsx`
- 导航配置: `src/menuConfig.js`
- 路由配置: `src/router/routerConfig.js`
- 路由入口: `src/router.jsx`
- 布局文件: `src/layouts`
- 通用组件: `src/components`
- 页面文件: `src/pages`
- 服务配置: `src/server`
- 状态管理: `src/stores`
- 样式文件: `src/styles`

## 主题包

#### 默认主题包

- @icedesign/theme

#### 可选主题包

- @alifd/theme-1

## 效果图

![screenshot](https://img.alicdn.com/tfs/TB1w4vGw3HqK1RjSZFPXXcwapXa-2872-1580.png)

## react 的物料
```````
https://fusion.design/mc/list?spm=fusion-design.mc-design-fusion.0.0.26a833e6EHdI8D&sort=gmtModified
```````

#登录
http://localhost:4444/#/user/login

## 三个
```````
https://fusion.design/pc/component/form?themeid=2
```````
